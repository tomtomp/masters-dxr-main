/**
* @file ThreadingTest.cpp
* @author Tomas Polasek
* @brief Threading unit testing.
*/

#include "stdafx.h"

#include "CatchUtils.h"

#include "engine/util/Threading.h"

using namespace util::thread;

TEST_CASE("Threading", "[Threading]")
{
    SECTION("SpinLock")
    {
        SpinLock sl;

        REQUIRE(sl.tryLock());
        REQUIRE(!sl.tryLock());

        sl.unlock();
        REQUIRE(sl.tryLock());

        sl.unlock();

        sl.lock();
        REQUIRE(true);
    }

    SECTION("Barrier simple")
    {
        Barrier barrier(1u);

        barrier.wait();
        REQUIRE(true);
    }
    
    SECTION("Barrier multithreaded")
    {
        Barrier barrier(2u);

        std::thread thr1([&] ()
        {
            barrier.wait();
        });

        std::thread thr2([&] ()
        {
            barrier.wait();
        });

        thr1.join();
        thr2.join();
        REQUIRE(true);
    }
}
