/**
* @file OptionParserTest.cpp
* @author Tomas Polasek
* @brief OptionParser unit testing.
*/

#include "stdafx.h"

#include "CatchUtils.h"

#include "engine/util/OptionParser.h"

TEST_CASE("Option parsing", "[OptionParser]")
{
    SECTION("Parsing simple flag argument list")
    {
        util::OptionParser parser;

        bool aFlag{ false };
        bool bFlag{ false };
        bool longAFlag{ false };
        bool longBFlag{ false };

        parser.addOption(L"-a", [&] ()
        {
            aFlag = true;
        });

        parser.addOption(L"--longA", [&] ()
        {
            longAFlag = true;
        });

        parser.addOption(L"-b", [&] ()
        {
            bFlag = true;
        });

        parser.addOption(L"--longB", [&] ()
        {
            longBFlag = true;
        });

        REQUIRE_NOTHROW(parser.parse({ L"app", L"-a", L"--longA" }));

        CHECK(aFlag == true);
        CHECK(longAFlag == true);

        CHECK(bFlag == false);
        CHECK(longBFlag == false);
    }

    SECTION("Parsing argument list with string values")
    {
        util::OptionParser parser;

        std::wstring aStrVal;
        std::wstring bStrVal;
        std::wstring longAStrVal;
        std::wstring longBStrVal;

        parser.addOption<std::wstring>(L"-a", [&](const std::wstring &val)
        {
            aStrVal = val;
        });

        parser.addOption<std::wstring>(L"--longA", [&](const std::wstring &val)
        {
            longAStrVal = val;
        });

        parser.addOption<std::wstring>(L"-b", [&](const std::wstring &val)
        {
            bStrVal = val;
        });

        parser.addOption<std::wstring>(L"--longB", [&](const std::wstring &val)
        {
            longBStrVal = val;
        });

        REQUIRE_NOTHROW(parser.parse({ L"app", L"-a", L"hello", L"--longA", L"world" }));

        CHECK(aStrVal == L"hello");
        CHECK(longAStrVal == L"world");

        CHECK(bStrVal.empty());
        CHECK(longBStrVal.empty());
    }

    SECTION("Parsing argument list with uint values")
    {
        util::OptionParser parser;

        uint32_t aUintVal{ 0u };
        uint32_t bUintVal{ 0u };
        uint32_t longAUintVal{ 0u };
        uint32_t longBUintVal{ 0u };

        parser.addOption<uint32_t>(L"-a", [&] (const uint32_t &val)
        {
            aUintVal = val;
        });

        parser.addOption<uint32_t>(L"--longA", [&] (const uint32_t &val)
        {
            longAUintVal = val;
        });

        parser.addOption<uint32_t>(L"-b", [&] (const uint32_t &val)
        {
            bUintVal = val;
        });

        parser.addOption<uint32_t>(L"--longB", [&] (const uint32_t &val)
        {
            longBUintVal = val;
        });

        REQUIRE_NOTHROW(parser.parse({ L"app", L"-a", L"42", L"--longA", L"1337" }));

        CHECK(aUintVal == 42u);
        CHECK(longAUintVal == 1337u);

        CHECK(bUintVal == 0u);
        CHECK(longBUintVal == 0u);
    }

    SECTION("Parsing argument list with errors")
    {
        util::OptionParser parser;

        uint32_t uintVal{ 0u };

        parser.addOption<uint32_t>(L"-i", [&] (const uint32_t &val)
        {
            uintVal = val;
        }, L"Testing uint option.");

        std::cout << "Parsing 'a' as uint: " << std::endl;
        CHECK_THROWS_MATCHES(parser.parse({ L"app", L"-i", L"a" }), util::wexception, WExceptionPrinterMatcher());

        std::cout << "Missing value: " << std::endl;
        CHECK_THROWS_MATCHES(parser.parse({ L"app", L"-i" }), util::wexception, WExceptionPrinterMatcher());

        std::cout << "Unknown parameter: " << std::endl;
        CHECK_THROWS_MATCHES(parser.parse({ L"app", L"-x" }), util::wexception, WExceptionPrinterMatcher());

        CHECK(uintVal == 0u);
    }
}
