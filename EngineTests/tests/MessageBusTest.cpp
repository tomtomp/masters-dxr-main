/**
* @file MessageBusTest.cpp
* @author Tomas Polasek
* @brief MessageBus unit testing.
*/

#include "stdafx.h"

#include "CatchUtils.h"

#include "engine/application/MessageBus.h"
#include "engine/application/Events.h"
#include "engine/util/Threading.h"

using namespace ::engine::app;

TEST_CASE("Message bus", "[MessageBus]")
{
    SECTION("Initialization, registration, desctruction")
    {
        auto intBus{ MessageBus<int>::create() };
        auto stringBus{ MessageBus<std::wstring>::create() };
        auto resizeEventBus{ MessageBus<events::ResizeEvent>::create() };

        for (uint32_t iii = 0; iii < 10; ++iii)
        {
            auto intReader{ intBus->reader() };
            auto stringReader{ stringBus->reader() };
            auto resizeEventReader{ resizeEventBus->reader() };

            auto intWriter{ intBus->writer() };
            UNUSED(intWriter);
            auto stringWriter{ stringBus->writer() };
            UNUSED(stringWriter);
            auto resizeEventWriter{ resizeEventBus->writer() };
            UNUSED(resizeEventWriter);
        }

        REQUIRE(intBus->numMailboxes() == 0u);
        REQUIRE(stringBus->numMailboxes() == 0u);
        REQUIRE(resizeEventBus->numMailboxes() == 0u);

        REQUIRE(intBus.use_count() == 1u);
        REQUIRE(stringBus.use_count() == 1u);
        REQUIRE(resizeEventBus.use_count() == 1u);

        MessageBus<int>::ReaderT r;
        REQUIRE(!r);
        r.fetchMessages();
        r.getMessages();

        MessageBus<int>::WriterT w;
        REQUIRE(!w);
        w.postMessage(0);
    }

    SECTION("Single threaded access")
    {
        auto intBus{ MessageBus<int>::create() };

        std::array<MessageBusReader<int>, 5> readers;
        for (auto &reader : readers)
        {
            reader = intBus->reader();
        }

        auto intWriter{ intBus->writer() };

        static constexpr uint32_t ITERATIONS{ 10u };

        for (uint32_t iii = 0; iii < ITERATIONS; ++iii)
        {
            intWriter.postMessage(iii);

            for (auto &reader : readers)
            {
                const auto messages{ reader.getMessages() };
                REQUIRE(messages.size() == iii + 1u);
            }
        }

        for (auto &reader : readers)
        {
            auto messages{ reader.getMessages() };

            REQUIRE(messages.size() == ITERATIONS);

            messages = reader.fetchMessages();
            for (uint32_t iii = 0; iii < ITERATIONS; ++iii)
            {
                REQUIRE(messages[iii] == iii);
            }

            messages = reader.fetchMessages();
            REQUIRE(messages.size() == 0u);
        }
    }

    SECTION("Parallel access")
    {
        auto intBus{ MessageBus<int>::create() };

        std::mutex resultMtx;
        std::vector<int> result;

        static constexpr uint32_t THREADS{ 5u };
        static constexpr uint32_t ITERATIONS{ 10u };

        util::thread::Barrier b1(THREADS);
        util::thread::Barrier b2(THREADS);
        util::thread::Barrier b3(THREADS);

        std::vector<std::thread> threads;

        for (uint32_t iii = 0; iii < THREADS; ++iii)
        {
            threads.emplace_back([&] ()
            {
                b1.wait();

                auto intReader{ intBus->reader() };
                auto intWriter{ intBus->writer() };

                b2.wait();

                for (uint32_t jjj = 0; jjj < ITERATIONS; ++jjj)
                {
                    intWriter.postMessage(jjj);
                }

                b3.wait();

                {
                    std::unique_lock<std::mutex> fl(resultMtx);
                    const auto messages{ intReader.getMessages() };
                    result.insert(result.end(), messages.begin(), messages.end());
                }
            });
        }

        for (auto &thread : threads)
        {
            thread.join();
        }

        std::array<int, ITERATIONS> counts{ 0, };
        for (auto &val : result)
        {
            counts[val]++;
        }

        for (auto &count : counts)
        {
            // Every number has been send THREAD times to THREAD mailboxes...
            REQUIRE(count == THREADS * THREADS);
        }
    }
}
