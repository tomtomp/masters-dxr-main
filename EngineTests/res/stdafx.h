/**
* @file stdafx.h
* @author Tomas Polasek
* @brief Pre-compiled header containing common standard 
* headers and some engine headers.
*/

#pragma once

#include "targetver.h"

// Common types from the engine.
#include "util/Types.h"
