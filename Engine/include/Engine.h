/**
* @file Engine.h
* @author Tomas Polasek
* @brief Main engine class, connects resource manager, renderer and application.
*/

#pragma once

//#include "EngineConfig.h"
#include "EngineRuntimeConfig.h"

#include "engine/resources/win32/WindowClass.h"
#include "engine/resources/win32/Window.h"
#include "engine/resources/ThreadPool.h"

#include "engine/application/MessageHandler.h"
#include "engine/application/MessageBus.h"
#include "engine/application/Events.h"
#include "engine/application/WindowRunner.h"

#include "engine/renderer/Renderer.h"

#include "engine/util/prof/Profiler.h"

#include "engine/lib/dxtk.h"

/// Namespace containing the engine code.
namespace engine
{

/// Main engine class, used for running an application.
class Engine
{
public:
    /**
     * Initialize engine and its subsystems.
     * @param cfg Runtime engine configuration.
     * @param hInstance Handle to main module.
     */
    Engine(const EngineRuntimeConfig &cfg, HINSTANCE hInstance);

    /// Destroy all subsystems and quit.
    ~Engine();

    /**
     * Run specified application in this engine.
     * @tparam AppT Application type.
     * @tparam ConfigT Runtime configuration type.
     * @param cfg Application configuration.
     * @return Returns code returned by the 
     * application run function.
     */
    template <typename AppT, typename ConfigT>
    int32_t run(ConfigT &cfg);

    /// Get the main window.
    res::win::Window &window()
    { return mWindow; }

    /// Get the main window.
    const res::win::Window &window() const
    { return mWindow; }

    /// Get renderer sub-system.
    rndr::Renderer &renderer()
    { return mRenderer; }

    /// Get renderer sub-system.
    const rndr::Renderer &renderer() const
    { return mRenderer; }

    /// Get message handling sub-system.
    app::MessageHandler &msgHandler()
    { return mMessageHandler; }

    /// Get message handling sub-system.
    const app::MessageHandler &msgHandler() const 
    { return mMessageHandler; }
private:
    /// Compile-time configuration.
    using Config = EngineConfig;

    /// Handle to the main module.
    HINSTANCE mHInstance;
    /// Main thread pool containing the worker threads.
    res::thread::ThreadPool mThreadPool;
    /// Message handling sub-system.
    app::MessageHandler mMessageHandler;
    /// Main window class registrator.
    res::win::WindowClass mWindowClass;
    /// Main window.
    res::win::Window mWindow;
    /// Message pump runner.
    app::WindowRunner mWindowRunner;
    /// Rendering sub-system.
    rndr::Renderer mRenderer;
protected:
}; // class Engine

} // namespace engine

// Template implementation

namespace engine
{

template <typename AppT, typename ConfigT>
int32_t Engine::run(ConfigT &cfg)
{
    //mWindow.setVisible(true);

    int32_t result{ 0 };

    AppT app(cfg, *this);

    app.initialize();
    {
        mWindowRunner.runMessagePump(mMessageHandler);
        mWindow.sendVisibleMessage(true);
        result = app.run();
    }
    app.destroy();

    return result;
}

}

// Template implementation end
