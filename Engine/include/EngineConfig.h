/**
* @file EngineConfig.h
* @author Tomas Polasek
* @brief Engine configuration static settings can be changed 
* in this file. The runtime config is provided by the user when 
* initializing the engine.
*/

#pragma once

#include <initializer_list>
#include <vector>

#include <d3d12.h>
#include <dxgi1_5.h>
#include <dxgidebug.h>

/// Namespace containing the engine code.
namespace engine
{

/// Compile-time engine configuration, can be changed in this file.
class EngineConfig
{
public:
    /// Name of the main window class.
    static constexpr const wchar_t *WINDOW_CLASS_MAIN_NAME{ L"MainWindowClassName" };

    /// Required Direct3D feature level.
    static constexpr D3D_FEATURE_LEVEL ADAPTER_FEATURE_LEVEL{ D3D_FEATURE_LEVEL_11_0 };

    /// Debug name for the main device.
    static constexpr const wchar_t *DEVICE_DEBUG_NAME{ L"MainDevice" };

    /// Debug name for the main direct command queue.
    static constexpr const wchar_t *DIRECT_QUEUE_DEBUG_NAME{ L"MainDirectQueue" };
    /// Debug name for the main compute command queue.
    static constexpr const wchar_t *COMPUTE_QUEUE_DEBUG_NAME{ L"MainComputeQueue" };
    /// Debug name for the main copy command queue.
    static constexpr const wchar_t *COPY_QUEUE_DEBUG_NAME{ L"MainCopyQueue" };

    /// Number of back buffers in the default swap chain.
    static constexpr uint64_t SWAP_CHAIN_NUM_BUFFERS{ 3u };
    /// Usage specification for buffers in the main swap chain.
    static constexpr DXGI_USAGE SWAP_CHAIN_USAGE{ DXGI_USAGE_RENDER_TARGET_OUTPUT };
    /// Format of buffers in the main swap chain.
    static constexpr DXGI_FORMAT SWAP_CHAIN_FORMAT{ DXGI_FORMAT_R8G8B8A8_UNORM };
    /// Scaling behaviour for buffers in the main swap chain.
    static constexpr DXGI_SCALING SWAP_CHAIN_SCALING{ DXGI_SCALING_STRETCH };
    /// Alpha value behaviour for buffers in the main swap chain.
    static constexpr DXGI_ALPHA_MODE SWAP_CHAIN_ALPHA_MODE{ DXGI_ALPHA_MODE_UNSPECIFIED };

    /// What should be reported by the live object reporter.
    static inline const DXGI_DEBUG_ID LIVE_REPORTER_DEBUG_ID{ ::DXGI_DEBUG_ALL };
    /// How should the live objects be reported.
    static constexpr DXGI_DEBUG_RLO_FLAGS LIVE_REPORTER_FLAGS{ ::DXGI_DEBUG_RLO_ALL };

    // Default values for the runtime configuration: 

    // TODO - MSVC bug, inlint const not working with incomplete types?
    /// Default value for disabled debugging message categories.
    static const std::initializer_list<::D3D12_MESSAGE_CATEGORY>
        RUNTIME_DEFAULT_DEVICE_DISABLE_MSG_CATEGORIES;
    /// Default value for disabled debugging message severities.
    static const std::initializer_list<::D3D12_MESSAGE_SEVERITY>
        RUNTIME_DEFAULT_DEVICE_DISABLE_MSG_SEVERITIES;
    /// Default value for disabled debugging message IDs.
    static const std::initializer_list<::D3D12_MESSAGE_ID>
        RUNTIME_DEFAULT_DEVICE_DISABLE_MSG_IDS;

    /// Default value for runtime option adapter use warp.
    static constexpr bool RUNTIME_DEFAULT_ADAPTER_USE_WARP{ false };
    /// Default value for runtime option window width.
    static constexpr uint32_t RUNTIME_DEFAULT_WINDOW_WIDTH{ 1024u };
    /// Default value for runtime option window height.
    static constexpr uint32_t RUNTIME_DEFAULT_WINDOW_HEIGHT{ 768u };
    /// Default value for runtime option window title.
    static constexpr const wchar_t *RUNTIME_DEFAULT_WINDOW_TITLE{ L"Direct3D App" };

    /// Default value for number of back buffers in the main swap chain.
    static constexpr uint32_t RUNTIME_DEFAULT_SWAP_CHAIN_NUM_BACKBUFFERS{ 3u };

    /// Default value for target frames per second for the application.
    static constexpr uint32_t RUNTIME_DEFAULT_APPLICATION_TARGET_FPS{ 60u };
    /// Default value for target updates per second for the application.
    static constexpr uint32_t RUNTIME_DEFAULT_APPLICATION_TARGET_UPS{ 60u };
    /// Default value for vsync settings.
    static constexpr bool RUNTIME_DEFAULT_APPLICATION_USE_VSYNC{ false };
    /// Default value for using tearing if supported.
    static constexpr bool RUNTIME_DEFAULT_APPLICATION_USE_TEARING{ false };

    /// Default value for displaying the help menu.
    static constexpr bool RUNTIME_DEFAULT_META_DISPLAY_HELP{ false };

#ifdef _DEBUG
    /// Default value for enabling the debug layer.
    static constexpr bool RUNTIME_DEFAULT_DEBUG_ENABLE_LAYER{ true };
    /// Default value for enabling the GPU-based validation.
    static constexpr bool RUNTIME_DEFAULT_DEBUG_GPU_VALIDATION{ false };
    /// Default value for enabling the live object reporting.
    static constexpr bool RUNTIME_DEFAULT_DEBUG_REPORT_LIVE{ true };
#else
    /// Default value for enabling the debug layer.
    static constexpr bool RUNTIME_DEFAULT_DEBUG_ENABLE_LAYER{ false };
    /// Default value for enabling the GPU-based validation.
    static constexpr bool RUNTIME_DEFAULT_DEBUG_GPU_VALIDATION{ false };
    /// Default value for enabling the live object reporting.
    static constexpr bool RUNTIME_DEFAULT_DEBUG_REPORT_LIVE{ false };
#endif
private:
protected:
public:
    EngineConfig() = delete;
    ~EngineConfig() = delete;
    EngineConfig(const EngineConfig &other) = delete;
    EngineConfig(EngineConfig &&other) = delete;
    EngineConfig &operator=(const EngineConfig &other) = delete;
    EngineConfig &operator=(EngineConfig &&other) = delete;
}; // class EngineConfig

} // namespace engine
