/**
 * @file scene/SceneManager.h
 * @author Tomas Polasek
 * @brief Helper class for managing scenes - loading, unloading, activating, etc.
 */

#pragma once

#include "engine/scene/ECS.h"

/// Namespace containing the engine code.
namespace engine
{

/// Virtual scene types.
namespace scene
{

/**
 * Helper class used for managing scenes and 
 * manipulating entities in them.
 */
class SceneManager
{
public:
    /**
     * Initialize the scene manager to default 
     * state.
     */
    SceneManager();
private:
    /// ECS universe used for storing scene entities.
    SceneUniverse &mUniverse;
protected:
}; // class SceneManager

} // namespace scene

} // namespace engine
