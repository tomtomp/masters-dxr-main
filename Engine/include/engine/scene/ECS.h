/**
 * @file scene/ECS.h
 * @author Tomas Polasek
 * @brief Definition of Entity-Component-System universe.
 */

#pragma once

#include "engine/lib/Entropy.h"

/// Namespace containing the engine code.
namespace engine
{

/// Virtual scene types.
namespace scene
{

// Forward declaration for extern template.
class SceneUniverse;
// Template is defined in the cpp source file.
extern template ent::Universe<SceneUniverse>;

/**
 * Entity-Component-System universe used for storing scene entities.
 */
class SceneUniverse : public ent::Universe<SceneUniverse>
{
public:
private:
    // Scene manager need to be able to get the instance.
    friend class SceneManager;

    // Only one instance should ever exist -> singleton.
    SceneUniverse() = default;
    ~SceneUniverse() = default;
    SceneUniverse(const SceneUniverse& other) = delete;
    SceneUniverse &operator=(const SceneUniverse& other) = delete;

    /// Get the singleton instance.
    static SceneUniverse &instance();
protected:
}; // class SceneUniverse

/// Type of an entity in the scene.
using Entity = SceneUniverse::EntityT;
/// Base type for all system working in the ECS.
using System = SceneUniverse::SystemT;

} // namespace scene

} // namespace engine
