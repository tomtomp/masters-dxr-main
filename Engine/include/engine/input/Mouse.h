/**
 * @file input/Mouse.h
 * @author Tomas Polasek
 * @brief Mouse handling class.
 */

#pragma once

#include "engine/input/MouseEvent.h"

/// Namespace containing the engine code.
namespace engine
{

/// Input processing.
namespace input
{

/// Mouse handling class.
class Mouse
{
public:
    /// Application mouse button callback when using mouse callback function.
    using AppButtonCallbackT = void (*)(MouseButtonEvent event);
    /// Application mouse movement callback when using mouse callback function.
    using AppMoveCallbackT = void (*)(MouseMoveEvent event);
    /// Application mouse scroll callback when using mouse callback function.
    using AppScrollCallbackT = void (*)(MouseScrollEvent event);

    Mouse() = default;

    /**
     * Resets the hook, if this mouse has been used with setCallback.
     */
    ~Mouse();

    Mouse(const Mouse &other) = delete;
    Mouse(Mouse &&other) = delete;
    Mouse &operator=(const Mouse &other) = delete;
    Mouse &operator=(Mouse &&other) = delete;

    /**
     * Set mouse callback hook. Only one mouse hook can be used!
     * Does NOT need to be used if messages are passed directly.
     * Parsed events are passed to provided application callback.
     * @param hInstance which application should be hooked.
     * @param btnCallback Parsed button events are passed to this callback.
     * @param mvCallback Parsed move events are passed to this callback.
     * @param scCallback Parsed scroll events are passed to this callback.
     * @throw Throws std::runtime_error if mouse cannot be hooked.
     */
    void setCallbacks(HINSTANCE hInstance, AppButtonCallbackT btnCallback, 
        AppMoveCallbackT mvCallback, AppScrollCallbackT scCallback);

    /**
     * Unhook the current mouse callback.
     */
    static void resetCallback();

    /**
     * Process given event message.
     * @param msg Message to process.
     * @return Returns a mouse button event created from the message. Can be tested 
     * as a bool, result is whether the event is valid.
     */
    MouseButtonEvent processBtnEvent(const MSG& msg)
    { return processBtnInner(msg.message, msg.wParam, msg.lParam); }

    /**
     * Process given event message.
     * @param messageCode Code of the message.
     * @param wParam Parameters of the message.
     * @param lParam Parameters of the message.
     * @return Returns a mouse button event created from the message. Can be tested 
     * as a bool, result is whether the event is valid.
     */
    MouseButtonEvent processBtnEvent(int messageCode, WPARAM wParam, LPARAM lParam)
    { return processBtnInner(messageCode, wParam, lParam); }

    /**
     * Process given event message.
     * @param msg Message to process.
     * @return Returns a mouse move event created from the message. Can be tested 
     * as a bool, result is whether the event is valid.
     */
    MouseMoveEvent processMoveEvent(const MSG& msg)
    { return processMoveInner(msg.message, msg.wParam, msg.lParam); }

    /**
     * Process given event message.
     * @param messageCode Code of the message.
     * @param wParam Parameters of the message.
     * @param lParam Parameters of the message.
     * @return Returns a mouse move event created from the message. Can be tested 
     * as a bool, result is whether the event is valid.
     */
    MouseMoveEvent processMoveEvent(int messageCode, WPARAM wParam, LPARAM lParam)
    { return processMoveInner(messageCode, wParam, lParam); }

    /**
     * Process given event message.
     * @param msg Message to process.
     * @return Returns a mouse scroll event created from the message. Can be tested 
     * as a bool, result is whether the event is valid.
     */
    MouseScrollEvent processScrollEvent(const MSG& msg)
    { return processScrollInner(msg.message, msg.wParam, msg.lParam); }

    /**
     * Process given event message.
     * @param messageCode Code of the message.
     * @param wParam Parameters of the message.
     * @param lParam Parameters of the message.
     * @return Returns a mouse scroll event created from the message. Can be tested 
     * as a bool, result is whether the event is valid.
     */
    MouseScrollEvent processScrollEvent(int messageCode, WPARAM wParam, LPARAM lParam)
    { return processScrollInner(messageCode, wParam, lParam); }
private:
    /// Mouse button callback function type.
    using MouseButtonCallbackT = LRESULT (*)(_In_ int nCode, _In_ WPARAM wParam, _In_ LPARAM lParam);

    /// Get callback function usable in SetWindowsHook.
    static MouseButtonCallbackT callback()
    { return mouseCallbackDispatch; }

    /**
     * Callback method called by Windows.
     * Dispatches the call to the selected Mouse object.
     */
    static LRESULT mouseCallbackDispatch(_In_ int code, _In_ WPARAM wParam, _In_ LPARAM lParam);

    /// Reset the mouse hook.
    void resetHook();

    /**
     * Get mouse button from provided message code.
     * @param messageCode Code of the message.
     * @param wParam parameters of the message, used for X1 and X2 mouse buttons.
     * @return Returns mouse button code.
     */
    static MouseButton btnFromMessage(int messageCode, WPARAM wParam);

    /**
     * Get mouse modifiers from provided message parameters.
     * @param wParam Message parameters.
     * @return Returns mouse modifier.
     */
    static MouseMods modsFromWParam(WPARAM wParam);

    /**
     * Get mouse action from provided message code.
     * @param messageCode Code of the message.
     * @return Returns mouse action.
     */
    static MouseAction actionFromMessage(int messageCode);

    /**
     * Get mouse buttons from provided message parameters.
     * @param wParam Message parameters.
     * @return Returns pressed mouse buttons.
     */
    static MouseButtons buttonsFromWParam(WPARAM wParam);

    /**
     * Get mouse position from provided message parameters.
     * @param lParam Message parameters.
     * @return Returns mouse position.
     */
    static util::Point2<int16_t> posFromLParam(LPARAM lParam);

    /**
     * Get mouse wheel delta from provided message parameters.
     * @param wParam Message parameters.
     * @return Returns mouse wheel delta, multiplied by WHEEL_DELTA;
     */
    static int16_t deltaFromWParam(WPARAM wParam);

    /**
     * Process given event message.
     * @param messageCode Code of the message.
     * @param wParam Parameters of the message.
     * @param lParam Parameters of the message.
     * @return Returns a mouse button event created from the message. Can be tested 
     * as a bool, result is whether the event is valid.
     */
    static MouseButtonEvent processBtnInner(int messageCode, WPARAM wParam, LPARAM lParam);

    /**
     * Process given event message.
     * @param messageCode Code of the message.
     * @param wParam Parameters of the message.
     * @param lParam Parameters of the message.
     * @return Returns a mouse scroll event created from the message. Can be tested 
     * as a bool, result is whether the event is valid.
     */
    static MouseScrollEvent processScrollInner(int messageCode, WPARAM wParam, LPARAM lParam);

    /**
     * Process given event message.
     * @param messageCode Code of the message.
     * @param wParam Parameters of the message.
     * @param lParam Parameters of the message.
     * @return Returns a mouse move event created from the message. Can be tested 
     * as a bool, result is whether the event is valid.
     */
    static MouseMoveEvent processMoveInner(int messageCode, WPARAM wParam, LPARAM lParam);

    /// Currently selected keyboard, which has been set as a callback.
    static Mouse *sHookMouse;

    /// Handle to callback hook.
    HHOOK mCallbackHook{ nullptr };
    /// Callback for passing button events.
    AppButtonCallbackT mBtnCallback{ nullptr };
    /// Callback for passing move events.
    AppMoveCallbackT mMoveCallback{ nullptr };
    /// Callback for passing scroll events.
    AppScrollCallbackT mScrollCallback{ nullptr };
protected:
}; // class Mouse

} // namespace input

} // namespace engine
