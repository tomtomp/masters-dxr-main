/**
* @file input/KeyBinding.h
* @author Tomas Polasek
* @brief Mapping from input events to actions using lambda expressions.
*/

#pragma once

#include "engine/input/KeyboardBinding.h"
#include "engine/input/MouseBinding.h"

/// Namespace containing the engine code.
namespace engine
{

/// Input processing.
namespace input
{

/// Aggregation of binding schemes.
class KeyBinding
{
public:
    /// Create empty key binding scheme.
    KeyBinding() = default;
    ~KeyBinding() = default;

    KeyBinding(const KeyBinding &other) = default;
    KeyBinding(KeyBinding &&other) = default;
    KeyBinding &operator=(const KeyBinding &other) = default;
    KeyBinding &operator=(KeyBinding &&other) = default;

    /// Process keyboard event.
    void processEvent(const KeyboardEvent &event) const
    { mKb.processInput(event); }

    /// Process mouse movement event.
    void processEvent(const MouseMoveEvent &event) const
    { mMouse.processInput(event); }

    /// Process mouse button event.
    void processEvent(const MouseButtonEvent &event) const
    { mMouse.processInput(event); }

    /// Process mouse scroll event.
    void processEvent(const MouseScrollEvent &event) const
    { mMouse.processInput(event); }

    /// Get the inner keyboard bindings scheme.
    KeyboardBinding &kb()
    { return mKb; }

    /// Get the inner keyboard bindings.
    const KeyboardBinding &kb() const
    { return mKb; }

    /// Get the inner mouse binding scheme.
    MouseBinding &mouse()
    { return mMouse; }

    /// Get the inner mouse binding scheme.
    const MouseBinding &mouse() const
    { return mMouse; }
private:
    /// Keyboard binding scheme.
    KeyboardBinding mKb;
    /// Mouse binding scheme.
    MouseBinding mMouse;

    // TODO - Add more input methods - Mouse, Gamepad, ...
protected:
}; // class KeyBinding

} // namespace input

} // namespace engine
