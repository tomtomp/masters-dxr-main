/**
* @file input/MouseEvent.h
* @author Tomas Polasek
* @brief Containers for mouse input data.
*/

#pragma once

#include "engine/util/BitwiseEnum.h"

/// Namespace containing the engine code.
namespace engine
{

/// Input processing.
namespace input
{

// Actions for the mouse.
enum class MouseAction : uint8_t
{
    None = 0x00, 
    Press,
    DoubleClick,
    Release
}; // enum class KeyAction

// Modifiers on the keyboard, can be combined.
enum class MouseMods : uint8_t
{
    None = 0x00,
    Control = MK_CONTROL, 
    Shift = MK_SHIFT
}; // enum class KeyMods

// Mouse button codes.
enum class MouseButton : uint16_t
{
    None = 0x00, 

    LButton = MK_LBUTTON,
    MButton = MK_MBUTTON, 
    RButton = MK_RBUTTON,

    X1Button = MK_XBUTTON1, 
    X2Button = MK_XBUTTON2
};

/// Mouse button flags, can be combined using bitwise operations.
using MouseButtons = MouseButton;

/// Event triggered when mouse button changes state.
struct MouseButtonEvent
{
    /// Name of this event.
    static constexpr const char *Name{ "MouseButtonEvent" };

    MouseButtonEvent() = default;
    MouseButtonEvent(MouseButton b, MouseMods m, MouseAction a) :
        btn{ b }, mods{ m }, action{ a }
    { }
    MouseButtonEvent(MouseButton b, MouseMods m, MouseAction a, MouseButtons p, util::Point2<int16_t> xy) :
        btn{ b }, mods{ m }, action{ a }, pressedButtons{ p }, pos{ xy }
    { }
    MouseButtonEvent(MouseButton b, MouseMods m, MouseAction a, MouseButtons p, int16_t x, int16_t y) :
        btn{ b }, mods{ m }, action{ a }, pressedButtons{ p }, pos{ x, y }
    { }

    MouseButton btn{ MouseButton::None };
    MouseMods mods{ MouseMods::None };
    MouseAction action{ MouseAction:: None };

    MouseButtons pressedButtons{ MouseButtons::None };
    util::Point2<int16_t> pos{ 0, 0};

    /// Is given mouse button pressed?
    bool buttonPressed(MouseButton btn) const
    { return (pressedButtons & btn) == btn; }
    
    /// Is given mouse modifier pressed?
    bool modPressed(MouseMods modifier) const
    { return (mods & modifier) == modifier; }

    /// Test for valid structure.
    explicit operator bool() const
    { return btn != MouseButton::None; }

    /// Comparison operator.
    bool operator<(const MouseButtonEvent &rhs) const
    { return (btn < rhs.btn) || ((btn == rhs.btn) && (mods < rhs.mods)) || ((btn == rhs.btn) && (mods == rhs.mods) && (action < rhs.action)); }
    /// Comparison equal operator.
    bool operator==(const MouseButtonEvent &rhs) const
    { return (btn == rhs.btn) && (mods == rhs.mods) && (action == rhs.action); }
}; // struct MouseButtonEvent

/// Event triggered when mouse moves.
struct MouseMoveEvent
{
    /// Name of this event.
    static constexpr const char *Name{ "MouseMoveEvent" };

    MouseMoveEvent() = default;
    MouseMoveEvent(MouseMods m, MouseButtons p, util::Point2<int16_t> xy) :
        mods{ m }, pressedButtons{ p }, pos{ xy }
    { }
    MouseMoveEvent(MouseMods m, MouseButtons p, int16_t x, int16_t y) :
        mods{ m }, pressedButtons{ p }, pos{ x, y }
    { }

    MouseMods mods{ MouseMods::None };

    MouseButtons pressedButtons{ MouseButtons::None };
    util::Point2<int16_t> pos{ 0, 0};

    /// Is given mouse button pressed?
    bool buttonPressed(MouseButton btn) const
    { return (pressedButtons & btn) == btn; }
    
    /// Is given mouse modifier pressed?
    bool modPressed(MouseMods modifier) const
    { return (mods & modifier) == modifier; }

    /// Test for valid structure.
    explicit operator bool() const
    { return true; }
}; // struct MouseMoveEvent

/// Event triggered when mouse scroll wheel is used.
struct MouseScrollEvent
{
    /// Name of this event.
    static constexpr const char *Name{ "MouseScrollEvent" };

    MouseScrollEvent() = default;
    MouseScrollEvent(int16_t d, MouseMods m, MouseButtons p, util::Point2<int16_t> xy) :
        delta{ d }, mods { m }, pressedButtons{ p }, pos{ xy }
    { }
    MouseScrollEvent(int16_t d, MouseMods m, MouseButtons p, int16_t x, int16_t y) :
        delta{ d }, mods { m }, pressedButtons{ p }, pos{ x, y }
    { }

    int16_t delta{ 0 };
    MouseMods mods{ MouseMods::None };

    MouseButtons pressedButtons{ MouseButtons::None };
    util::Point2<int16_t> pos{ 0, 0};

    /// Is given mouse button pressed?
    bool buttonPressed(MouseButton btn) const
    { return (pressedButtons & btn) == btn; }
    
    /// Is given mouse modifier pressed?
    bool modPressed(MouseMods modifier) const
    { return (mods & modifier) == modifier; }

    /// Get unscaled wheel delta value.
    float deltaUnscaled() const
    { return static_cast<float>(delta) / WHEEL_DELTA; }

    /// Test for valid structure.
    explicit operator bool() const
    { return delta != 0; }
}; // struct MouseScrollEvent

} // namespace input

} // namespace engine
