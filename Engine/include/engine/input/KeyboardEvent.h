/**
* @file input/KeyboardEvent.h
* @author Tomas Polasek
* @brief Container for keyboard input data.
*/

#pragma once

#include "engine/util/BitwiseEnum.h"

/// Namespace containing the engine code.
namespace engine
{

/// Input processing.
namespace input
{

// Actions for the keyboard.
enum class KeyAction : uint8_t
{
    None = 0x00, 
    Press = 0x01,
    Repeat = (KF_REPEAT >> 8u) + 1u,
    Release = ((KF_REPEAT >> 8u) | (KF_UP >> 8u)) + 1u,
}; // enum class KeyAction

// Modifiers on the keyboard, can be combined.
enum class KeyMods : uint8_t
{
    None = 0b00000000,
    Shift = 0b00000001, 
    Control = 0b00000010, 
    Alt = 0b00000100, 
}; // enum class KeyMods

// Type for the virtual keycodes.
enum class Key : uint8_t
{
    None = 0x00, 

    LButton = 0x01,
    RButton = 0x02,
    Cancel = 0x03,
    MButton = 0x04,

#if(_WIN32_WINNT >= 0x0500)
    Xbutton1 = 0x05,
    XButton2 = 0x06,
#endif
    /* 0x07 : reserved */
    Back = 0x08,
    Tab = 0x09,

    /* 0x0A - 0x0B : reserved */
    Clear = 0x0C,
    Return = 0x0D,
    /* 0x0E - 0x0F : unassigned */

    Shift = 0x10,
    Control = 0x11,
    Alt = 0x12,

    Pause = 0x13,
    Capital = 0x14,
    Kana = 0x15,
    Kangeul = 0x15,
    Hangul = 0x15,
    /* 0x16 : unassigned */
    Junja = 0x17,
    Final = 0x18,
    Hanja = 0x19,
    Kanji = 0x19,
    /* 0x1A : unassigned */
    Escape = 0x1B,
    Convert = 0x1C,
    NonConvert = 0x1D,
    Accept = 0x1E,
    ModeChange = 0x1F,

    Space = 0x20,
    Prior = 0x21,
    Next = 0x22,
    End = 0x23,
    Home = 0x24,
    Left = 0x25,
    Up = 0x26,
    Right = 0x27,
    Down = 0x28,
    Select = 0x29,
    Print = 0x2A,
    Execute = 0x2B,
    Snapshot = 0x2C,
    Insert = 0x2D,
    Delete = 0x2E,
    Help = 0x2F,

    N0 = '0', 
    N1 = '1', 
    N2 = '2', 
    N3 = '3', 
    N4 = '4', 
    N5 = '5', 
    N6 = '6', 
    N7 = '7', 
    N8 = '8', 
    N9 = '9', 

    /* 0x3A - 0x40 : unassigned */

    A = 'A',
    B = 'B',
    C = 'C',
    D = 'D',
    E = 'E',
    F = 'F',
    G = 'G',
    H = 'H',
    I = 'I',
    J = 'J',
    K = 'K',
    L = 'L',
    M = 'M',
    N = 'N',
    O = 'O',
    P = 'P',
    Q = 'Q',
    R = 'R',
    S = 'S',
    T = 'T',
    U = 'U',
    V = 'V',
    W = 'W',
    X = 'X',
    Y = 'Y',
    Z = 'Z',

    LWin = 0x5B,
    RWin = 0x5C,
    Apps = 0x5D,

    /* 0x5E : reserved */

    Sleep = 0x5F,

    Numpad0 = 0x60,
    Numpad1 = 0x61,
    Numpad2 = 0x62,
    Numpad3 = 0x63,
    Numpad4 = 0x64,
    Numpad5 = 0x65,
    Numpad6 = 0x66,
    Numpad7 = 0x67,
    Numpad8 = 0x68,
    Numpad9 = 0x69,
    Multiply = 0x6A,
    Add = 0x6B,
    Separator = 0x6C,
    Subtract = 0x6D,
    Decimal = 0x6E,
    Divide = 0x6F,
    F1 = 0x70,
    F2 = 0x71,
    F3 = 0x72,
    F4 = 0x73,
    F5 = 0x74,
    F6 = 0x75,
    F7 = 0x76,
    F8 = 0x77,
    F9 = 0x78,
    F10 = 0x79,
    F11 = 0x7A,
    F12 = 0x7B,
    F13 = 0x7C,
    F14 = 0x7D,
    F15 = 0x7E,
    F16 = 0x7F,
    F17 = 0x80,
    F18 = 0x81,
    F19 = 0x82,
    F20 = 0x83,
    F21 = 0x84,
    F22 = 0x85,
    F23 = 0x86,
    F24 = 0x87,

#if(_WIN32_WINNT >= 0x0604)

    /* 0x88 - 0x8F : UI navigation */

    NavigationView = 0x88,
    NavigationMenu = 0x89,
    NavigationUp = 0x8A,
    NavigationDown = 0x8B,
    NavigationLeft = 0x8C,
    NavigationRight = 0x8D,
    NavigationAccept = 0x8E,
    NavigationCancel = 0x8F,

#endif /* _WIN32_WINNT >= =0x0604 */

    Numlock = 0x90,
    Scroll = 0x91,

    // '=' key on numpad
    Equal =0x92,

    // 'Dictionary' key
    Jisho =0x92,
    // 'Unregister word' key
    Masshou =0x93,
    // 'Register word' key
    Touroku =0x94,
    // 'Left OYAYUBI' key
    Loya =0x95,
    // 'Right OYAYUBI' key
    Roya =0x96,

    /* 0x97 - 0x9F : unassigned */

    /*
     * L* & R* - left and right Alt, Ctrl and Shift virtual keys.
     * Used only as parameters to GetAsyncKeyState() and GetKeyState().
     * No other API or message will distinguish left and right keys in this way.
     */
    LShift = 0xA0,
    RShift = 0xA1,
    LControl = 0xA2,
    RControl = 0xA3,
    LMenu = 0xA4,
    RMenu = 0xA5,

#if(_WIN32_WINNT >= 0x0500)
    BrowserBack = 0xA6,
    BrowserForward = 0xA7,
    BrowserRefresh = 0xA8,
    BrowserStop = 0xA9,
    BrowserSearch = 0xAA,
    BrowserFavorites = 0xAB,
    BrowserHome = 0xAC,

    VolumeMute = 0xAD,
    VolumeDown = 0xAE,
    VolumeUp = 0xAF,
    MediaNextTrack = 0xB0,
    MediaPrevTrack = 0xB1,
    MediaStop = 0xB2,
    MediaPlayPause = 0xB3,
    LaunchMail = 0xB4,
    LaunchMedia = 0xB5,
    LaunchApp1 = 0xB6,
    LaunchApp2 = 0xB7,

#endif /* _WIN32_WINNT >= 0x0500 */

    /* 0xB8 - 0xB9 : reserved */

    // ';:' for US
    Oem1 = 0xBA,
    // '+' any country
    OemPlus = 0xBB,
    // ',' any country
    OemComma = 0xBC,
    // '-' any country
    OemMinus = 0xBD,
    // '.' any country
    OemPeriod = 0xBE,
    // '/?' for US
    Oem2 = 0xBF,
    // '`~' for US
    Oem3 = 0xC0,

    /* 0xC1 - 0xC2 : reserved */

#if(_WIN32_WINNT >= 0x0604)

    /* 0xC3 - 0xDA : Gamepad input */

    GamepadA = 0xC3,
    GamepadX = 0xC4,
    GamepadY = 0xC5,
    GamepadB = 0xC6,
    GamepadRShoulder = 0xC7,
    GamepadLShoulder = 0xC8,
    GamepadLTrigger = 0xC9,
    GamepadRTrigger = 0xCA,
    GamepadDpadUp = 0xCB,
    GamepadDpadDown = 0xCC,
    GamepadDpadLeft = 0xCD,
    GamepadDpadRight = 0xCE,
    GamepadMenu = 0xCF,
    GamepadView = 0xD0,
    GamepadLThumbstickBtn = 0xD1,
    GamepadRThumbstickBtn = 0xD2,
    GamepadLThumbstickUp = 0xD3,
    GamepadLThumbstickDown = 0xD4,
    GamepadLThumbstickRight = 0xD5,
    GamepadLThumbstickLeft = 0xD6,
    GamepadRThumbstickUp = 0xD7,
    GamepadRThumbstickDown = 0xD8,
    GamepadRThumbstickRight = 0xD9,
    GamepadRThumbstickLeft = 0xDA,

#endif /* _WIN32_WINNT >= 0x0604 */


    //  '[{' for US
    Oem4 = 0xDB,
    //  '\|' for US
    Oem5 = 0xDC,
    //  ']}' for US
    Oem6 = 0xDD,
    //  ''"' for US
    Oem7 = 0xDE,
    Oem8 = 0xDF,

    /* 0xE0 : reserved */

    //  'AX' key on Japanese AX kbd
    OemAx = 0xE1,
    //  "<>" or "\|" on RT 102-key kbd.
    Oem102 = 0xE2,
    //  Help key on ICO
    IcoHelp = 0xE3,
    //  00 key on ICO
    Ico00 = 0xE4,

#if(WINVER >= 0x0400)
    Process = 0xE5,
#endif /* WINVER >= 0x0400 */

    IcoClear = 0xE6,


#if(_WIN32_WINNT >= 0x0500)
    Packet = 0xE7,
#endif /* _WIN32_WINNT >= 0x0500 */

    /* 0xE8 : unassigned */

    OemReset = 0xE9,
    OemJump = 0xEA,
    OemPa1 = 0xEB,
    OemPa2 = 0xEC,
    OemPa3 = 0xED,
    OemWsctrl = 0xEE,
    OemCusel = 0xEF,
    OemAttn = 0xF0,
    OemFinish = 0xF1,
    OemCopy = 0xF2,
    OemAuto = 0xF3,
    OemEnlw = 0xF4,
    OemBacktab = 0xF5,

    Attn = 0xF6,
    Crsel = 0xF7,
    Exsel = 0xF8,
    Ereof = 0xF9,
    Play = 0xFA,
    Zoom = 0xFB,
    Noname = 0xFC,
    Pa1 = 0xFD,
    OemClear = 0xFE

    /* 0xFF : reserved */
}; // enum class Key

// Type for the hardware scan code.
using ScanCode = DWORD;

/// Event triggered when keyboard changes state.
struct KeyboardEvent
{
    /// Name of this event.
    static constexpr const char *Name{ "KeyboardEvent" };

    KeyboardEvent() = default;

    KeyboardEvent(Key k, KeyMods m, KeyAction a) :
        key{ k }, mods{ m }, action{ a }
    { }

    KeyboardEvent(Key k, ScanCode s, KeyMods m, KeyAction a) :
        key{ k }, scanCode{ s }, mods{ m }, action{ a }
    { }

    Key key{ Key::None };
    ScanCode scanCode{ 0x00 };
    KeyMods mods{ KeyMods::None };
    KeyAction action{ KeyAction::None };

    /// Test for valid structure.
    explicit operator bool() const
    { return key != Key::None; }

    /// Comparison operator.
    bool operator<(const KeyboardEvent &rhs) const
    { return (key < rhs.key) || ((key == rhs.key) && (mods < rhs.mods)) || ((key == rhs.key) && (mods == rhs.mods) && (action < rhs.action)); }
    /// Comparison equal operator.
    bool operator==(const KeyboardEvent &rhs) const
    { return (key == rhs.key) && (mods == rhs.mods) && (action == rhs.action); }
}; // KeyEvent

} // namespace input

} // namespace engine