/**
* @file input/MouseBinding.h
* @author Tomas Polasek
* @brief Mapping from mouse events to actions using lambda expressions.
*/

#pragma once

#include "engine/input/MouseEvent.h"

/// Namespace containing the engine code.
namespace engine
{

/// Input processing.
namespace input
{

/// Binding from mouse events to actions.
class MouseBinding
{
public:
    /// Type of the action function, called when corresponding key is pressed.
    using ActionFun = std::function<void(const MouseButtonEvent &event)>;
    /// Type of the default action.
    using DefaultActionFun = std::function<void(const MouseButtonEvent &event)>;
    /// Action taken, when scroll occurs.
    using ScrollActionFun = std::function<void(const MouseScrollEvent &event)>;
    /// Action taken, when mouse position is taken.
    using MoveActionFun = std::function<void(const MouseMoveEvent &event)>;

    /**
     * Set action for given key combination.
     * @param btn Mouse button.
     * @param mods Modifiers.
     * @param action Action which occurred.
     * @param fun Function to call.
     */
    void setAction(MouseButton btn, MouseMods mods, MouseAction action, ActionFun fun)
    { mButtonCbs[{btn, mods, action}] = fun; }

    /**
     * Reset action for given key combination.
     * @param btn Mouse button.
     * @param mods Modifiers.
     * @param action Action which occurred.
     */
    void setAction(MouseButton btn, MouseMods mods, MouseAction action)
    { mButtonCbs.erase({btn, mods, action}); }

    /**
     * Set the default action, called when no other mapping is found.
     * @param fun Function to call.
     */
    void setDefaultAction(DefaultActionFun fun)
    { mDefaultAction = fun; }

    /**
     * Reset the default action.
     */
    void resetDefaultAction()
    { mDefaultAction = nullptr; }

    /**
     * Set function called, when scrolling occurs
     * @param fun Function to call.
     */
    void setScrollAction(ScrollActionFun fun)
    { mScrollCb = fun; }

    /**
     * Reset the scroll action.
     */
    void resetScrollAction()
    { mMoveCb = nullptr; }

    /**
     * Set function called, when mouse position is received.
     * @param fun Function to call.
     */
    void setMoveAction(MoveActionFun fun)
    { mMoveCb = fun; }

    /**
     * Reset the mouse position callback.
     */
    void resetMoveAction()
    { mMoveCb = nullptr; }

    /**
     * Process given mouse button event and trigger any bound actions.
     */
    void processInput(const MouseButtonEvent &event) const
    {
        const auto search{mButtonCbs.find(event)};

        if (search != mButtonCbs.end())
        { // We found an action!
            search->second(event);
        }
        else if (mDefaultAction)
        {
            mDefaultAction(event);
        }
    }

    /**
     * Process given mouse scroll event and trigger any bound actions.
     */
    void processInput(const MouseScrollEvent &event) const
    {
        if (mScrollCb)
        {
            mScrollCb(event);
        }
    }

    /**
     * Process given mouse move event and trigger any bound actions.
     */
    void processInput(const MouseMoveEvent &event) const
    {
        if (mMoveCb)
        {
            mMoveCb(event);
        }
    }
private:
    /// Mapping from keys to actions.
    std::map<MouseButtonEvent, ActionFun> mButtonCbs;
    /// Default action, called when no button mapping is found.
    DefaultActionFun mDefaultAction;
    /// Function called, when scrolling occurs.
    ScrollActionFun mScrollCb;
    /// Function called, when mouse position is received.
    MoveActionFun mMoveCb;
protected:
}; // class MouseBinding

} // namespace input

} // namespace engine 
