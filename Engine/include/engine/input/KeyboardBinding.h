/**
* @file input/KeyboardBinding.h
* @author Tomas Polasek
* @brief Mapping from keyboard events to actions using lambda expressions.
*/

#pragma once

#include "engine/input/KeyboardEvent.h"

/// Namespace containing the engine code.
namespace engine
{

/// Input processing.
namespace input
{

/// Binding from keyboard events to actions.
class KeyboardBinding
{
public:
    /// Type of the action function, called when corresponding key is pressed.
    using ActionFun = std::function<void()>;
    /// Type of the default action, arguments.
    using DefaultActionFun = std::function<void(const KeyboardEvent &event)>;

    /// Create empty key binding scheme.
    KeyboardBinding() = default;
    ~KeyboardBinding() = default;

    KeyboardBinding(const KeyboardBinding &other) = default;
    KeyboardBinding(KeyboardBinding &&other) = default;
    KeyboardBinding &operator=(const KeyboardBinding &other) = default;
    KeyboardBinding &operator=(KeyboardBinding &&other) = default;

    /**
     * Set action for given key combination.
     * @param key Key - e.g. Key::Up.
     * @param mods Modifiers - e.g. KeyMods::Shift.
     * @param action Action taken with the key - e.g. KeyAction::Press.
     * @param fun Function to call.
     */
    void setAction(Key key, KeyMods mods, KeyAction action, ActionFun fun)
    { mMapping[{key, mods, action}] = fun; }

    /**
     * Reset action for given key combination.
     * @param key Key - e.g. Key::Up.
     * @param mods Modifiers - e.g. KeyMods::Shift.
     * @param action Action taken with the key - e.g. KeyAction::Press.
     */
    void setAction(Key key, KeyMods mods, KeyAction action)
    { mMapping.erase({key, mods, action}); }

    /**
     * Set the default action, called when no other mapping is found.
     * Function will be passed following arguments :
     *  Key key - Key pressed, e.g. Key::Up.
     *  ScanCode scanCode - Scan-code of the key.
     *  int mods - Mods - e.g. KeyMods::Shift.
     *  KeyAction action - Action of the key - e.g. KeyAction::Press.
     * @param fun Function to call.
     */
    void setDefaultAction(DefaultActionFun fun)
    { mDefaultAction = fun; }

    /**
     * Reset the default action.
     */
    void resetDefaultAction()
    { mDefaultAction = nullptr; }

    /**
     * Process given keyboard event and trigger any bound actions.
     */
    void processInput(const KeyboardEvent &event) const
    {
        const auto search{mMapping.find(event)};
        if (search != mMapping.end())
        { // We found an action!
            search->second();
        }
        else if (mDefaultAction)
        {
            mDefaultAction(event);
        }
    }
private:
    /// Mapping from keys to actions.
    std::map<KeyboardEvent, ActionFun> mMapping;
    /// Default action, called when no mapping is found.
    DefaultActionFun mDefaultAction;
protected:
}; // class KeyboardBinding

} // namespace input

} // namespace engine
