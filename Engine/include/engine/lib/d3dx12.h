/**
* @file lib/d3dx12.h
* @author Tomas Polasek
* @brief Helper header for including the d3dx12 library.
*/

#pragma once

#include "../lib/d3dx12/d3dx12.h"
