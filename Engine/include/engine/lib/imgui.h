/**
* @file lib/imgui.h
* @author Tomas Polasek
* @brief Helper header for including imgui library headers.
*/

#pragma once

// Main library: 
#define IMGUI_USER_CONFIG "engine/lib/imconfig.h"
#include "../lib/imgui/imgui.h"
