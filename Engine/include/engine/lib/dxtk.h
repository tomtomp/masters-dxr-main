/**
* @file lib/dxtk.h
* @author Tomas Polasek
* @brief Helper header for the DirectX ToolKit library.
*/

#pragma once

#include "../lib/dxtk/SimpleMath.h"
