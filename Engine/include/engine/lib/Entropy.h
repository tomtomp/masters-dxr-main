/**
* @file lib/Entropy.h
* @author Tomas Polasek
* @brief Helper header for including Entropy library headers.
*/

#pragma once

#include "../lib/Entropy/Entropy.h"
