/**
* @file util/logging/LoggingStreams.h
* @author Tomas Polasek
* @brief Logging streams used in the logging system.
*/

#pragma once

#include "engine/util/logging/MessageOutput.h"

/// Utilities and helpers.
namespace util
{

/// Logging classes and utilities.
namespace logging
{

/// Simple logging stream which discards everything.
struct VoidLogStream
{
    /// Simple stream which discards everything.
    struct VoidStream
    {
        template <typename T>
        VoidStream &operator<<(const T &v)
        { return *this; }

        VoidStream &operator<<(std::ostream &(*fun)(std::ostream&))
        { return *this; }
    };

    static VoidStream &instance()
    {
        static VoidStream vs;
        return vs;
    }
}; // class VoidLogStream

/// Forward to util::dout.
struct DoutLogStream
{
    static inline decltype(util::dout) &instance()
    { return util::dout; }
}; // class DoutStream

/// Forward to util::wdout.
struct WdoutLogStream
{
    static inline decltype(util::wdout) &instance()
    { return util::wdout; }
}; // class WdoutStream

/**
 * Thread safe logging stream, synchronized between 
 * all instances.
 * @tparam StrT String type.
 */
template <typename StrT>
struct ThreadSafeLogStream
{
    using SafeDebugBufferT = StringBuffer<syncers::GlobalThreadSafe<syncers::DebugStringSyncer<StrT>>>;
    using SafeConsoleBufferT = StringBuffer<syncers::GlobalThreadSafe<syncers::WriteConsoleSyncer<StrT>>>;

    using SafeDebugBufferChooserT = DebugBufferChooser<StrT, SafeDebugBufferT, SafeConsoleBufferT>;

    using OStreamT = std::basic_ostream<typename StrT::value_type, std::char_traits<typename StrT::value_type>>;

    static inline OStreamT &instance()
    {
        static thread_local SafeDebugBufferChooserT buffer;
        static thread_local OStreamT messageStream(buffer.ptr());
        return messageStream;
    }
}; // ThreadSafeLogStream

} // namespace logging

} // namespace util
