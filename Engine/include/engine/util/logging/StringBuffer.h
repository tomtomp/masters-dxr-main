/**
* @file util/logging/StringBuffer.h
* @author Tomas Polasek
* @brief String buffer used for displaying messages in WIN32.
*/

#pragma once

#include <sstream>
#include <iostream>
#include <mutex>

/// Utilities and helpers.
namespace util
{

/**
 * String buffer synchronizing messages 
 * using specified syncer object, with 
 * following attributes: 
 *   StringT : Type of string used.
 *   int sync(const StringT &str) : Synchronization function.
 * @tparam SyncerTT Type of the syncer object.
 * @tparam StringTT Type of string used, deduced 
 * from the SyncerT.
 * @tparam StringValueTT Type of an element in 
 * the string.
 * @tparam ParentClass Stringbuffer base class.
 */
template <typename SyncerTT,
          typename StringTT = typename SyncerTT::StringT,
          typename StringValueTT = typename StringTT::value_type, 
          typename ParentClass = std::basic_stringbuf<StringValueTT, 
                                                      std::char_traits<StringValueTT>, 
                                                      std::allocator<StringValueTT>>>
class StringBuffer : public ParentClass
{
public:
    /// Syncer used by this buffer.
    using SyncerT = SyncerTT;
    /// Type of string used.
    using StringT = StringTT;
    /// Base class used by this class.
    using BaseStringBufferT = ParentClass;
    /// Base stream buffer.
    using BaseStreamBufferT = std::basic_streambuf<
        typename BaseStringBufferT::char_type, typename BaseStringBufferT::traits_type>;
    static_assert(std::is_base_of_v<BaseStreamBufferT, BaseStringBufferT>, 
        "Testing correct stream buffer type.");

    /**
     * Create string buffer, passing parameters to the 
     * syncer object.
     * @tparam ArgTs Types of parameters for the syncer 
     * object.
     * @param args Arguments passed to the syncer object.
     */
    template <typename... ArgTs>
    explicit StringBuffer(ArgTs... args);

    StringBuffer(const StringBuffer &other) = default;
    StringBuffer(StringBuffer &&other) = default;
    StringBuffer &operator=(const StringBuffer &rhs) = default;
    StringBuffer &operator=(StringBuffer &&rhs) = default;

    /// Flush on destroy.
    virtual ~StringBuffer();

    /// Call syncer when syncing.
    virtual int sync() override;
private:
    /// Syncer object.
    SyncerT mSyncer;
protected:
}; // class StringBuffer

/// Contains syncers for util::StringBuffer
namespace syncers
{

/// Synchronizes message using MessageBoxA.
template <typename StrT>
struct MessageBoxSyncer
{
    using StringT = StrT;

    /// Set name displayed on the message box.
    MessageBoxSyncer(const StringT &name) :
        mName{ name }
    { }

    int sync(const StringT &str) const;
private:
    /// Name displayed on the message box.
    const StringT mName;
}; // struct MessageBoxSyncer

/// Synchronizes message using DebugStringA.
template <typename StrT>
struct DebugStringSyncer
{
    using StringT = StrT;
    int sync(const StringT &str) const;
}; // struct DebugStringSyncer

/// Synchronizes message using WriteConsole.
template <typename StrT>
struct WriteConsoleSyncer
{
    using StringT = StrT;
    int sync(const StringT &str) const;
}; // struct WriteConsoleSyncer

/// Thread safe adapter for syncer structures.
template <typename SyncerT>
struct ThreadSafe : public SyncerT
{
    /**
     * Pass potencial constructor 
     * arguments to the inner syncer.
     */
    template <typename... ArgTs>
    ThreadSafe(ArgTs... args) :
        SyncerT(std::forward<ArgTs>(args)...)
    { }

    using StringT = typename SyncerT::StringT;

    /**
     * Sync with mutual exclusion with 
     * other syncers of the same type.
     */
    inline int sync(const StringT &str) const
    {
        std::unique_lock<std::mutex> lock(mSyncerMutex);
        return SyncerT::sync(str);
    }
private:
    /**
     * Mutex used for synchronizing syncing 
     * between multiple syncer objects.
     */
    static std::mutex mSyncerMutex;
}; // struct ThreadSafe

/// Mutex storage for the GlobalThreadSafe adapter.
struct GlobalThreadSafeMutex
{
private:
    // Allow access to the adapter.
    template <typename SyncerT>
    friend struct GlobalThreadSafe;

    /// Global mutex for the adapter.
    static inline std::mutex sMtx;
}; // GlobalThreadSafeMutex.

/// Thread safe adapter common to all syncers.
template <typename SyncerT>
struct GlobalThreadSafe : public SyncerT
{
    /**
     * Pass potential constructor 
     * arguments to the inner syncer.
     */
    template <typename... ArgTs>
    GlobalThreadSafe(ArgTs... args) :
        SyncerT(std::forward<ArgTs>(args)...)
    { }

    using StringT = typename SyncerT::StringT;

    /**
     * Sync with mutual exclusion with 
     * all other syncers even other types.
     */
    inline int sync(const StringT &str) const
    {
        std::unique_lock<std::mutex> lock(GlobalThreadSafeMutex::sMtx);
        return SyncerT::sync(str);
    }
}; // struct GlobalThreadSafe

} // namespace syncers

} // namespace util

// Template implementation.

namespace util
{

template <typename SyncerTT, typename StringTT, typename StringValueTT, typename ParentClassTT>
template <typename ... ArgTs>
::util::StringBuffer<SyncerTT, StringTT, StringValueTT, ParentClassTT>::StringBuffer(ArgTs... args) :
    ParentClassTT(),
    mSyncer(std::forward<ArgTs>(args)...)
{ }

template <typename SyncerTT, typename StringTT, typename StringValueTT, typename ParentClassTT>
StringBuffer<SyncerTT, StringTT, StringValueTT, ParentClassTT>::~StringBuffer()
{ ParentClassTT::sync(); }

template <typename SyncerTT, typename StringTT, typename StringValueTT, typename ParentClassTT>
int StringBuffer<SyncerTT, StringTT, StringValueTT, ParentClassTT>::sync()
{
    int result{ 0u };

    if (!ParentClassTT::str().empty())
    { // If there is a string, sync it.
        result = mSyncer.sync(ParentClassTT::str());
        // TODO - Clear even when result != 0?
        ParentClassTT::str(StringTT());
    }

    return result;
}

namespace syncers
{

template <>
inline int MessageBoxSyncer<std::string>::sync(const StringT &str) const
{
    ::MessageBoxA(nullptr, str.c_str(), mName.c_str(), 
        MB_OK | MB_ICONERROR | MB_SYSTEMMODAL | MB_DEFAULT_DESKTOP_ONLY);
    return 0;
}

template <>
inline int MessageBoxSyncer<std::wstring>::sync(const StringT &str) const
{
    ::MessageBoxW(nullptr, str.c_str(), mName.c_str(), 
        MB_OK | MB_ICONERROR | MB_SYSTEMMODAL | MB_DEFAULT_DESKTOP_ONLY);
    return 0;
}

template <>
inline int DebugStringSyncer<std::string>::sync(const StringT &str) const
{
    ::OutputDebugStringA(str.c_str());
    return 0;
}

template <>
inline int DebugStringSyncer<std::wstring>::sync(const StringT &str) const
{
    ::OutputDebugStringW(str.c_str());
    return 0;
}

template <>
inline int syncers::WriteConsoleSyncer<std::string>::sync(const StringT &str) const
{
    WriteConsoleA(GetStdHandle(STD_OUTPUT_HANDLE), str.c_str(), 
        static_cast<DWORD>(str.size()), nullptr, nullptr);
    return 0;
}

template <>
inline int syncers::WriteConsoleSyncer<std::wstring>::sync(const StringT &str) const
{
    WriteConsoleW(GetStdHandle(STD_OUTPUT_HANDLE), str.c_str(), 
        static_cast<DWORD>(str.size()), nullptr, nullptr);
    return 0;
}

}

}

// Template implementation end.
