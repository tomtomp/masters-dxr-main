/**
* @file util/logging/Logging.h
* @author Tomas Polasek
* @brief Message logging utilities and macros, inspired 
* by http://www.drdobbs.com/cpp/a-highly-configurable-logging-framework/225700666.
*/

#pragma once

// Prepare the logging configuration.
#include "engine/util/logging/LoggingLevel.h"

/// Utilities and helpers.
namespace util
{

/// Logging classes and utilities.
namespace logging
{

// Logging level to use.
SET_LOGGING_LEVEL(All);

// Default logging level, when unspecified.
SET_DEFAULT_LOG_LEVEL(Debug);

// Set any specific logging streams for chosen levels.
//SET_LEVEL_LOG_STREAM(Fatal, MessageLogStream);
//SET_LEVEL_LOG_STREAM(Error, MessageLogStream);

}; // class Logger

} // namespace util

// Finalize the logging configuration and define logging functions.
#include "engine/util/logging/LoggingLogger.h"

// Shortcut for using logging functions.
using namespace ::util::logging;
