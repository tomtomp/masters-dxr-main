/**
* @file util/Math.h
* @author Tomas Polasek
* @brief Mathematical helper functions.
*/

#pragma once

/// Utilities and helpers.
namespace util
{

/// Mathematical helper functions.
namespace math
{

/**
 * Calculate smallest power of 2 number, which
 * is larger than specified value.
 * @param value Non-zero value, <= to the returned number.
 * @return Power of 2 number, >= to the value.
 */
static inline uint32_t pow2RoundUp(uint32_t value)
{
    --value;
    value |= value >> 0b1;
    value |= value >> 0b10;
    value |= value >> 0b100;
    value |= value >> 0b1000;
    value |= value >> 0b10000;

    return value + 1;
}

/**
 * Calculate smallest power of 2 number, which
 * is larger than specified value.
 * @param value Non-zero value, <= to the returned number.
 * @return Power of 2 number, >= to the value.
 */
static inline uint64_t pow2RoundUp(uint64_t value)
{
    --value;
    value |= value >> 0b1;
    value |= value >> 0b10;
    value |= value >> 0b100;
    value |= value >> 0b1000;
    value |= value >> 0b10000;
    value |= value >> 0b100000;

    return value + 1;
}
/**
 * Align provided value to the closes multiple 
 * of alignTo value.
 * @param val Starting value.
 * @param alignTo To what value should the value be 
 * aligned to.
 * @return Returns the aligned value.
 */
static inline std::size_t alignTo(std::size_t val, std::size_t alignTo)
{
    if (!val || !alignTo)
    {
        return val;
    }

    const auto remainder{ val % alignTo };
    return remainder ? val + (alignTo - remainder) : val;
}

} // namespace math

} // namespace util

