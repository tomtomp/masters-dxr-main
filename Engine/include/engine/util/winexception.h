/**
* @file winexception.h
* @author Tomas Polasek
* @brief Exception incorporating windows error message.
*/

#pragma once

#include "engine/util/Util.h"
#include "engine/util/wexception.h"

/// Utilities and helpers.
namespace util
{

/// Wrapper around std::runtime_error, which adds windows error message to the end.
class winexception : public std::runtime_error
{
public:
    // TODO - Find a better way to insert the middle section.
	explicit winexception(const std::string &message) : 
		runtime_error(message + "\n\t: " + util::getLastErrorAsString())
    { }

	explicit winexception(const char *message) : 
		runtime_error(std::string(message) + "\n\t: " + util::getLastErrorAsString())
    { }
private:
protected:
}; // class win_runtime_error

/// Wrapper around util::wruntime_error which adds windows error message to the end.
class wwinexception : public util::wruntime_error
{
public:
    // TODO - Find a better way to insert the middle section.
	explicit wwinexception(const std::wstring &message) : 
		wruntime_error(message + L"\n\t: " + util::getLastErrorAsWString())
    { }

	explicit wwinexception(const wchar_t *message) : 
		wruntime_error(std::wstring(message) + L"\n\t: " + util::getLastErrorAsWString())
    { }

	explicit wwinexception(const std::string &message) : 
        wruntime_error(message + "\n\t: " + util::getLastErrorAsString())
    { }

	explicit wwinexception(const char *message) : 
        wruntime_error(std::string(message) + "\n\t: " + util::getLastErrorAsString())
    { }
private:
protected:
}; // class win_runtime_error

/**
 * Throw exception if the given result fails.
 * Source: https://github.com/Microsoft/DirectX-Graphics-Samples
 * @param hr Result to check.
 * @param msg Message if the exception is thrown.
 * @throws Throws std::exception if result fails.
 */
inline void throwIfFailed(HRESULT hr, const char *msg)
{
    if (FAILED(hr))
    {
        throw winexception(msg);
    }
}

/**
 * Throw exception if the given result fails.
 * Source: https://github.com/Microsoft/DirectX-Graphics-Samples
 * @param hr Result to check.
 * @param msg Message if the exception is thrown.
 * @throws Throws std::exception if result fails.
 */
inline void throwIfFailed(HRESULT hr, const wchar_t *msg)
{
    if (FAILED(hr))
    {
        throw util::wwinexception(msg);
    }
}

} // namespace util
