/**
* @file util/SafeWindows.h
* @author Tomas Polasek
* @brief Safe way to include windows headers, including: 
*   <windows.h>
*   <shellapi.h>
*   <wrl.h>
*/

#pragma once

#ifndef WIN32_LEAN_AND_MEAN
// Exclude rarely-used functions from the windows header.
#   define WIN32_LEAN_AND_MEAN
#   define DEFINED_WIN32_LEAN_AND_MEAN_DEFINED
#endif

// Windows Header Files:
#include <windows.h>
#include <shellapi.h>
#include <wrl.h>

// Disable windows min/max and use c++ algorithm ones.
#ifdef min
    #undef min
#endif

#ifdef max
    #undef max
#endif

#ifdef DEFINED_WIN32_LEAN_AND_MEAN_DEFINED
#   undef WIN32_LEAN_AND_MEAN
#   undef DEFINED_WIN32_LEAN_AND_MEAN_DEFINED
#endif
