/**
* @file wexception.h
* @author Tomas Polasek
* @brief Wide character alternative to the <stdexcept> header.
*/

#pragma once

#include <cwchar>
#include <stdexcept>
#include <locale>
#include <codecvt>
#include <string>

#include "engine/util/Util.h"

/// Utilities and helpers.
namespace util
{

/// Wide character exception.
class wexception : public std::exception
{
private:
    /// Data container for the exception.
    struct ExceptionData
    {
        /// Default message returned from wwhat();
        static constexpr const wchar_t *DEFAULT_WMESSAGE{ L"Failed to get message!" };
        /// Default message returned from what();
        static constexpr const char *DEFAULT_MESSAGE{ "Failed to get message!" };

        /// Initialize default values.
        ExceptionData() noexcept = default;

        /**
         * Create the message.
         * @param wstr Original string.
         */
        ExceptionData(const wchar_t *wstr) noexcept
        {
            try
            {
                mWideMessage = wstr;
                mWideMessagePtr = mWideMessage.c_str();
                mCharMessage = util::toString(mWideMessage);
                mCharMessagePtr = mCharMessage.c_str();
            } catch (...)
            { }
        }

        /**
         * Create the message.
         * @param str Original string.
         */
        ExceptionData(const char *str) noexcept
        {
            try
            {
                mCharMessage = str;
                mCharMessagePtr = mCharMessage.c_str();
                mWideMessage = util::toWString(mCharMessage);
                mWideMessagePtr = mWideMessage.c_str();
            } catch (...)
            { }
        }

        ExceptionData(const ExceptionData &other) noexcept
        {
            try
            {
                mWideMessage = other.mWideMessage;
                mWideMessagePtr = mWideMessage.c_str();
                mCharMessage = other.mCharMessage;
                mCharMessagePtr = mCharMessage.c_str();
            } catch (...)
            { }
        }

        ExceptionData(ExceptionData &&other) noexcept :
            ExceptionData()
        {
            swap(*this, other);
        }

        ExceptionData &operator=(const ExceptionData &other) noexcept
        {
            *this = ExceptionData(other);
            return *this;
        }

        ExceptionData &operator=(ExceptionData &&other) noexcept
        {
            swap(*this, other);
            return *this;
        }

        /// Free resources, if the string is a copy.
        ~ExceptionData() = default;

        /// Get the inner wide string.
        const wchar_t *wwhat() const noexcept
        { return mWideMessagePtr ? mWideMessagePtr : DEFAULT_WMESSAGE; }
        
        /// Get the inner char string.
        const char *what() const noexcept
        { return mCharMessagePtr ? mCharMessagePtr : DEFAULT_MESSAGE; }

        /// Swap data with given instance.
        friend void swap(ExceptionData &first, ExceptionData &second) noexcept
        {
            try
            {
                std::swap(first.mWideMessage, second.mWideMessage);
                std::swap(first.mWideMessagePtr, second.mWideMessagePtr);
                std::swap(first.mCharMessage, second.mCharMessage);
                std::swap(first.mCharMessagePtr, second.mCharMessagePtr);
            } catch (...)
            { }
        }
    private:
        /// Wide character message.
        std::wstring mWideMessage;
        /// Pointer to the wide message.
        const wchar_t *mWideMessagePtr{ nullptr };
        /// Normal character message.
        std::string mCharMessage;
        /// Pointer to the character message.
        const char *mCharMessagePtr{ nullptr };
    }; // struct ExceptionData
public:
    /// Default message returned by wwhat().
    static constexpr const wchar_t *DEFAULT_ERROR_WMESSAGE{ L"Failed to construct exception data!" };
    /// Default message returned by what().
    static constexpr const char *DEFAULT_ERROR_MESSAGE{ "Failed to construct exception data!" };

    /// Empty exception.
    wexception() noexcept = default;

    /**
     * Exception with a message. The message is copied into the exception.
     * @param message Message to use.
     */
    explicit wexception(const wchar_t *const message) noexcept :
        std::exception()
    { 
        try
        {
            mData = std::make_shared<ExceptionData>(message);
        } catch(...)
        {
            /*
             * Failed to create the exception data, this exception 
             * will return error message when what() and wwhat() is 
             * called.
             */
        }
    }

    explicit wexception(const std::exception &exc) noexcept :
        std::exception()
    {
        try
        {
            mData = std::make_shared<ExceptionData>(exc.what());
        } catch(...)
        {
            /*
             * Failed to create the exception data, this exception 
             * will return error message when what() and wwhat() is 
             * called.
             */
        }
    }

    wexception(const wexception &other) noexcept :
        std::exception(),
        mData(other.mData)
    { }

    wexception(wexception &&other) noexcept :
        std::exception(),
        mData(std::move(other.mData))
    { }

    wexception& operator=(const wexception &rhs) noexcept
    {
        *this = wexception(rhs);
        return *this;
    }

    wexception& operator=(wexception &&rhs) noexcept
    {
        if (this == &rhs)
        {
            return *this;
        }

        swap(mData, rhs.mData);

        return *this;
    }

    virtual ~wexception() noexcept = default;

    /// Standard character message, converted from the wide character message.
    virtual const char *what() const override
    { return mData ? mData->what() : DEFAULT_ERROR_MESSAGE; }

    /// Get message contained within this exception.
    virtual const wchar_t *wwhat() const
    { return mData ? mData->wwhat() : DEFAULT_ERROR_WMESSAGE; }

    /// Convert this exception into standard exception.
    explicit operator std::exception() const
    { return std::exception(what()); }
private:
    /// Container for exception message.
    std::shared_ptr<ExceptionData> mData;
protected:
}; // class wexception

/// Wide character runtime_error.
class wruntime_error : public wexception
{
public:
	explicit wruntime_error(const std::wstring &message) : 
		wexception(message.c_str())
    { }

	explicit wruntime_error(const wchar_t *message) : 
		wexception(message)
    { }

	explicit wruntime_error(const std::string &message) : 
		wexception(convertToWide(message.c_str()).c_str())
    { }

	explicit wruntime_error(const char *message) : 
		wexception(convertToWide(message).c_str())
    { }
private:
    /// Convert a character string to wide character string.
    static std::wstring convertToWide(const char *message)
    {
        // Should be fine, using codecvt, until replacement is standardized...
        #define _SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING

        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
        return converter.from_bytes(message);

        #undef _SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING
    }
protected:
 #if _HAS_EXCEPTIONS
 #else /* _HAS_EXCEPTIONS */
	virtual void _Doraise() const
		{	// perform class-specific exception handling
		_RAISE(*this);
		}
 #endif /* _HAS_EXCEPTIONS */

}; // class wruntime_error

} // namespace util
