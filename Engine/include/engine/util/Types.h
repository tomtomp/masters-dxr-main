/**
* @file util/Types.h
* @author Tomas Polasek
* @brief Common types used in the application.
*/

#pragma once

// Should be fine, using codecvt, until replacement is standardized...
#define _SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING

#include <stdexcept>
#include <iostream>
#include <cassert>
#include <chrono>

#include <algorithm>
#include <functional>
#include <optional>

#include <type_traits>

#include <array>
#include <map>
#include <vector>
#include <queue>
#include <deque>
#include <set>

#include <mutex>
#include <shared_mutex>
#include <atomic>
#include <future>

#include "engine/util/Util.h"
#include "engine/util/Math.h"
#include "engine/util/logging/MessageOutput.h"
#include "engine/util/wexception.h"
#include "engine/util/winexception.h"
#include "engine/util/Asserts.h"
#include "engine/util/logging/Logging.h"

#include <wrl.h>
/// Shortcut for Microsoft ComPtr;
using Microsoft::WRL::ComPtr;
