/**
* @file util/MemoryView.h
* @author Tomas Polasek
* @brief Simple wrapper around a piece of memory.
*/

#pragma once

#include <cstddef>
#include <algorithm>

/// Utilities and helpers.
namespace util
{

/**
 * Wrapper around a piece of user memory.
 * User keeps ownership of the memory and has 
 * to make sure it is valid for whole lifetime 
 * of this object.
 * @tparam T Type contained within the memory.
 */
template <typename T>
class MemoryView
{
public:
    using value_type = T;

    using reference = T&;
    using const_reference = const T&;

    using pointer = T*;
    using const_pointer = const T*;

    using iterator = T*;
    using const_iterator = const T*;

    using difference_type = std::ptrdiff_t;
    using size_type = std::size_t;

    /// Empty memory view.
    constexpr MemoryView() = default;

    /**
     * Create a view for given memory.
     * @param begin Pointer to the beginning of the 
     * memory.
     * @param size Size of the memory in elements.
     * @warning If a nullptr is passed as the first 
     * parameter, the size will be forced to 0!
     */
    constexpr MemoryView(pointer begin, size_type size);

    /// Create a copy of given memory view.
    constexpr MemoryView(const MemoryView &other) noexcept;
    /// Copy given memory view.
    constexpr MemoryView &operator=(const MemoryView &other) noexcept;

    /// Get iterator to the beginning of the memory.
    constexpr iterator begin() noexcept;
    /// Get iterator to the beginning of the memory.
    constexpr const_iterator begin() const noexcept;
    /// Get iterator to the beginning of the memory.
    constexpr const_iterator cbegin() const noexcept;

    /// Get iterator to the end of the memory.
    constexpr iterator end() noexcept;
    /// Get iterator to the end of the memory.
    constexpr const_iterator end() const noexcept;
    /// Get iterator to the end of the memory.
    constexpr const_iterator cend() const noexcept;

    /**
     * Compare memory views, returning true if they 
     * view the same memory.
     * @param other Compare with this memory view.
     * @return Returns true if they view the same 
     * memory.
     * @warning Does NOT compare the data!
     */
    constexpr bool operator==(const MemoryView &other) const noexcept;

    /**
     * Compare memory views, returning false if they 
     * view the same memory.
     * @param other Compare with this memory view.
     * @return Returns false if they view the same 
     * memory.
     * @warning Does NOT compare the data!
     */
    constexpr bool operator!=(const MemoryView &other) const noexcept;

    /**
     * Size of the viewed memory in elements.
     * @return Returns the size of the viewed memory in 
     * elements.
     */
    constexpr size_type size() const noexcept;

    /**
     * Is the viewed memory empty?
     * @return Returns true if there are no elements 
     * in the viewed memory.
     */
    constexpr bool empty() const noexcept;

    /**
     * Swap this memory view with the other 
     * memory view.
     * @param other Swap with this memory view.
     */
    constexpr void swap(MemoryView &other) noexcept;

    /**
     * Swap memory views.
     * @param first Swap this memory view with the second one.
     * @param second Swap this memory view with the first one.
     */
    friend constexpr void swap(MemoryView &first, MemoryView &second) noexcept;

    /**
     * Access the memory without bounds checking.
     * @param index Index of the element.
     * @return Returns reference to the element 
     * on given index.
     */
    constexpr reference operator[](size_type index) noexcept;

    /**
     * Access the memory without bounds checking.
     * @param index Index of the element.
     * @return Returns reference to the element 
     * on given index.
     */
    constexpr const_reference operator[](size_type index) const noexcept;

    /**
     * Access the memory WITH bounds checking.
     * @param index Index of the element.
     * @return Returns reference to the element 
     * on given index.
     * @throws Throws std::out_of_range if the 
     * index would access elements outside of 
     * the viewed memory.
     */
    reference at(size_type index);

    /**
     * Access the memory WITH bounds checking.
     * @param index Index of the element.
     * @return Returns reference to the element 
     * on given index.
     * @throws Throws std::out_of_range if the 
     * index would access elements outside of 
     * the viewed memory.
     */
    const_reference at(size_type index) const;

    /// Does this memory view contain a valid memory?
    constexpr explicit operator bool() const noexcept;
private:
    /**
     * Check given index whether it is within bounds 
     * of contained memory.
     * @param index Index to check.
     * @throws Throws std::out_of_range if the idnex 
     * is out of range, or this MemoryView contains 
     * invalid memory.
     */
    void checkBounds(size_type index);

    /// Pointer to the beginning of the memory.
    pointer mBegin{ nullptr };
    /// Size of the memory, in elements.
    size_type mSize{ 0u };
protected:
}; // class MemoryView

} // namespace util

// Template implementation

namespace util
{

template <typename T>
constexpr MemoryView<T>::MemoryView(pointer begin, size_type size) :
    mBegin{ begin }, mSize{ begin ? size : 0u }
{ }

template <typename T>
constexpr MemoryView<T>::MemoryView(const MemoryView &other) noexcept :
    mBegin{ other.mBegin }, mSize{ other.mSize }
{ }

template <typename T>
constexpr MemoryView<T> &MemoryView<T>::operator=(const MemoryView &other) noexcept
{
    mBegin = other.mBegin;
    mSize = other.mSize;
    return *this;
}

template <typename T>
constexpr void MemoryView<T>::swap(MemoryView &other) noexcept
{ swap(*this, other); }

template <typename T>
constexpr void swap(MemoryView<T> &first, MemoryView<T> &second) noexcept
{
    using std::swap;
    swap(mBegin, other.mBegin);
    swap(mSize, other.mSize);
}

template <typename T>
constexpr typename MemoryView<T>::reference MemoryView<T>::operator[](size_type index) noexcept
{ return begin()[index]; }

template <typename T>
constexpr typename MemoryView<T>::const_reference MemoryView<T>::operator[](size_type index) const noexcept
{ return begin()[index]; }

template <typename T>
typename MemoryView<T>::reference MemoryView<T>::at(size_type index)
{
    checkBounds(index);
    return operator[](index);
}

template <typename T>
typename MemoryView<T>::const_reference MemoryView<T>::at(size_type index) const
{
    checkBounds(index);
    return operator[](index);
}

template <typename T>
constexpr typename MemoryView<T>::iterator MemoryView<T>::begin() noexcept
{ return mBegin; }

template <typename T>
constexpr typename MemoryView<T>::const_iterator MemoryView<T>::begin() const noexcept
{ return mBegin; }

template <typename T>
constexpr typename MemoryView<T>::const_iterator MemoryView<T>::cbegin() const noexcept
{ return mBegin; }

template <typename T>
constexpr typename MemoryView<T>::iterator MemoryView<T>::end() noexcept
{ return mBegin + mSize; }

template <typename T>
constexpr typename MemoryView<T>::const_iterator MemoryView<T>::end() const noexcept
{ return mBegin + mSize; }

template <typename T>
constexpr typename MemoryView<T>::const_iterator MemoryView<T>::cend() const noexcept
{ return mBegin + mSize; }

template <typename T>
constexpr bool MemoryView<T>::operator==(const MemoryView &other) const noexcept
{ return mBegin == other.mBegin && mSize == other.mSize; }

template <typename T>
constexpr bool MemoryView<T>::operator!=(const MemoryView &other) const noexcept
{ return !(*this == other); }

template <typename T>
constexpr typename MemoryView<T>::size_type MemoryView<T>::size() const noexcept
{ return mSize; }

template <typename T>
constexpr bool MemoryView<T>::empty() const noexcept
{ return size() == 0u; }

template <typename T>
constexpr MemoryView<T>::operator bool() const noexcept
{ return !empty(); }

template <typename T>
void MemoryView<T>::checkBounds(size_type index)
{
    if (!mBegin || index >= mSize)
    {
        throw std::out_of_range("Index is out of range of the "
            "memory contained within this MemoryView!");
    }
}

}

// End of template implementation.