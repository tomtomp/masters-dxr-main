/**
* @file util/SceneGraph.h
* @author Tomas Polasek
* @brief Universal scene graph container.
*/

#pragma once

#include <memory>
#include <algorithm>

#include "engine/util/MemoryView.h"
#include "engine/util/Template.h"
#include "engine/util/Util.h"

/// Utilities and helpers.
namespace util
{

/**
 * Base class for the down-propagation data 
 * structure used in the scene graph.
 */
struct DownPropagateBase
{
    /// Default type used for PropagateDown pointer.
    using DefaultPropagateDownPtrT = DownPropagateBase*;
    
    /*
    void propagateFrom(const DownPropagateBase &o)
    { }
    */
}; // struct DownPropagateBase

/**
 * Base class for the down-propagation data 
 * structure used in the scene graph.
 */
struct UpPropagateBase
{
    /// Default type used for PropagateUp pointer.
    using DefaultPropagateUpPtrT = UpPropagateBase*;

    /*
    void propagateFrom(const UpPropagateBase &o)
    { }
    */
}; // struct UpPropagateBase

/// Misc implementation details.
namespace impl
{

// Specialization for both down-propagate and up-propagate.
template <typename KeyT, typename BasePtrT, typename DownPropagatePtrT, typename UpPropagatePtrT>
struct SceneGraphInternalData
{
    /// Type of this node.
    using this_type = SceneGraphInternalData<KeyT, BasePtrT, DownPropagatePtrT, UpPropagatePtrT>;
    /// Type of a pointer to this node.
    using pointer = typename std::pointer_traits<BasePtrT>::template rebind<this_type>;

    /// Does this template instantiation have down-propagate member?
    static constexpr bool HasDownPropagate{ true };
    /// Does this template instantiation have up-propagate member?
    static constexpr bool HasUpPropagate{ true };

    /// Pack used for passing propagation parameters.
    struct DownUpPack
    {
        bool operator==(const DownUpPack &other) const
        { return downPropagate == other.downPropagate && upPropagate == other.upPropagate; }
        DownPropagatePtrT downPropagate;
        UpPropagatePtrT upPropagate;
    }; // struct DownUpPack

    /// Structure available to the user.
    struct UserData
    {
        bool operator==(const UserData &other) const
        { return id == other.id && data == other.data; }
        /// Id of this node.
        const KeyT id;
        /// User specified propagate data.
        DownUpPack data;
    }; // struct UserData

    SceneGraphInternalData(const KeyT &id, const DownUpPack &propagate) :
        userData{ id, propagate }
    { }

    bool operator==(const this_type &other) const
    { return userData == other.userData; }

    /// Data specified by the user.
    UserData userData;
    /// Pointer to the parent node.
    pointer parent{ nullptr };
    /// List of pointers to the child nodes.
    util::MemoryView<pointer> children;
    /// Pointer to the next node in implementation specific order.
    pointer next{ nullptr };
    /// Pointer to the previous node in implementation specific order.
    pointer prev{ nullptr };
}; // struct SceneGraphInternalData

// Specialization for only down-propagate.
template <typename KeyT, typename BasePtrT, typename DownPropagatePtrT>
struct SceneGraphInternalData<KeyT, BasePtrT, DownPropagatePtrT, UpPropagateBase::DefaultPropagateUpPtrT>
{
    /// Type of this node.
    using this_type = SceneGraphInternalData<KeyT, BasePtrT, DownPropagatePtrT, UpPropagateBase::DefaultPropagateUpPtrT>;
    /// Type of a pointer to this node.
    using pointer = typename std::pointer_traits<BasePtrT>::template rebind<this_type>;

    /// Does this template instantiation have down-propagate member?
    static constexpr bool HasDownPropagate{ true };
    /// Does this template instantiation have up-propagate member?
    static constexpr bool HasUpPropagate{ false };

    /// Pack used for passing propagation parameters.
    struct DownUpPack
    {
        bool operator==(const DownUpPack &other) const
        { return downPropagate == other.downPropagate; }
        DownPropagatePtrT downPropagate;
    }; // struct DownUpPack

    /// Structure available to the user.
    struct UserData
    {
        bool operator==(const UserData &other) const
        { return id == other.id && data == other.data; }
        /// Id of this node.
        const KeyT id;
        /// User specified propagate data.
        DownUpPack data;
    }; // struct UserData

    SceneGraphInternalData(const KeyT &id, const DownUpPack &propagate) :
        userData{ id, propagate }
    { }

    bool operator==(const this_type &other) const
    { return userData == other.userData; }

    /// Data specified by the user.
    UserData userData;
    /// Pointer to the parent node.
    pointer parent{ nullptr };
    /// List of pointers to the child nodes.
    util::MemoryView<pointer> children;
    /// Pointer to the next node in implementation specific order.
    pointer next{ nullptr };
    /// Pointer to the previous node in implementation specific order.
    pointer prev{ nullptr };
}; // struct SceneGraphInternalData

// Specialization for only up-propagate.
template <typename KeyT, typename BasePtrT, typename UpPropagatePtrT>
struct SceneGraphInternalData<KeyT, BasePtrT, DownPropagateBase::DefaultPropagateDownPtrT, UpPropagatePtrT>
{
    /// Type of this node.
    using this_type = SceneGraphInternalData<KeyT, BasePtrT, DownPropagateBase::DefaultPropagateDownPtrT, UpPropagatePtrT>;
    /// Type of a pointer to this node.
    using pointer = typename std::pointer_traits<BasePtrT>::template rebind<this_type>;

    /// Does this template instantiation have down-propagate member?
    static constexpr bool HasDownPropagate{ false };
    /// Does this template instantiation have up-propagate member?
    static constexpr bool HasUpPropagate{ true };

    /// Pack used for passing propagation parameters.
    struct DownUpPack
    {
        bool operator==(const DownUpPack &other) const
        { return upPropagate == other.upPropagate; }
        UpPropagatePtrT upPropagate;
    }; // struct DownUpPack

    /// Structure available to the user.
    struct UserData
    {
        bool operator==(const UserData &other) const
        { return id == other.id && data == other.data; }
        /// Id of this node.
        const KeyT id;
        /// User specified propagate data.
        DownUpPack data;
    }; // struct UserData

    SceneGraphInternalData(const KeyT &id, const DownUpPack &propagate) :
        userData{ id, propagate }
    { }

    bool operator==(const this_type &other) const
    { return userData == other.userData; }

    /// Data specified by the user.
    UserData userData;
    /// Pointer to the parent node.
    pointer parent{ nullptr };
    /// List of pointers to the child nodes.
    util::MemoryView<pointer> children;
    /// Pointer to the next node in implementation specific order.
    pointer next{ nullptr };
    /// Pointer to the previous node in implementation specific order.
    pointer prev{ nullptr };
}; // struct SceneGraphInternalData

// Specialization for no propagation at all.
template <typename KeyT, typename BasePtrT>
struct SceneGraphInternalData<KeyT, BasePtrT, DownPropagateBase::DefaultPropagateDownPtrT, UpPropagateBase::DefaultPropagateUpPtrT>
{
    /// Type of this node.
    using this_type = SceneGraphInternalData<KeyT, BasePtrT, DownPropagateBase::DefaultPropagateDownPtrT, UpPropagateBase::DefaultPropagateUpPtrT>;
    /// Type of a pointer to this node.
    using pointer = typename std::pointer_traits<BasePtrT>::template rebind<this_type>;

    /// Does this template instantiation have down-propagate member?
    static constexpr bool HasDownPropagate{ false };
    /// Does this template instantiation have up-propagate member?
    static constexpr bool HasUpPropagate{ false };

    /// Pack used for passing propagation parameters.
    struct DownUpPack
    {
    }; // struct DownUpPack

    /// Structure available to the user.
    struct UserData
    {
        bool operator==(const UserData &other) const
        { return id == other.id; }
        /// Id of this node.
        const KeyT id;
        /// User specified propagate data.
        DownUpPack data;
    }; // struct UserData

    SceneGraphInternalData(const KeyT &id, const DownUpPack &propagate) :
        userData{ id, propagate } 
    { }

    bool operator==(const this_type &other) const
    { return userData == other.userData; }

    /// Data specified by the user.
    UserData userData;
    /// Pointer to the parent node.
    pointer parent{ nullptr };
    /// List of pointers to the child nodes.
    util::MemoryView<pointer> children;
    /// Pointer to the next node in implementation specific order.
    pointer next{ nullptr };
    /// Pointer to the previous node in implementation specific order.
    pointer prev{ nullptr };
}; // struct SceneGraphInternalData

/**
 * Internal implementation of the scene graph, using 
 * standard containers.
 */
template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
class NaiveSceneGraphImplementation
{
public:
    /// Exception thrown when parameter is wrongly specified.
    struct InvalidParameter : std::runtime_error
    {
        InvalidParameter(const char *msg) :
            std::runtime_error(msg)
        { }
    }; // struct InvalidParameter

    using KeyT = KeyTT;
    using NodeDataT = NodeDataTT;

    using allocator_type = AllocatorTT;
    using node_allocator_type = typename std::allocator_traits<allocator_type>::template rebind_alloc<NodeDataT>;
    using allocator_traits = std::allocator_traits<node_allocator_type>;

    using value_type = typename allocator_traits::value_type;
    using pointer = typename allocator_traits::pointer;
    using const_pointer = typename allocator_traits::const_pointer;
    using reference = value_type&;
    using const_reference = const value_type&;

    using iterator = pointer*;
    using const_iterator = const pointer*;

    using size_type = std::size_t;

    using child_pointer = pointer;
    using child_allocator_type = typename std::allocator_traits<allocator_type>::template rebind_alloc<child_pointer>;
    using child_allocator_traits = std::allocator_traits<child_allocator_type>;
    using child_array_pointer = child_pointer*;

    /// Value of a node pointer pointing to no node.
    static constexpr pointer NullNodePtr{ nullptr };

    NaiveSceneGraphImplementation(const allocator_type &allocator = allocator_type());
    ~NaiveSceneGraphImplementation();

    NaiveSceneGraphImplementation(const NaiveSceneGraphImplementation &other);
    NaiveSceneGraphImplementation(NaiveSceneGraphImplementation &&other) noexcept;
    NaiveSceneGraphImplementation &operator=(const NaiveSceneGraphImplementation &other);
    NaiveSceneGraphImplementation &operator=(NaiveSceneGraphImplementation &&other) noexcept;

    // Linked nodes iterator.
    pointer begin() noexcept;
    const_pointer begin() const noexcept;
    const_pointer cbegin() const noexcept;

    // End iterator for the linked list, can be paired with *begin().
    pointer end() noexcept;
    const_pointer end() const noexcept;
    const_pointer cend() const noexcept;

    // Top-level nodes iterator.
    iterator tlbegin() noexcept;
    const_iterator tlbegin() const noexcept;
    const_iterator ctlbegin() const noexcept;

    // Top-level nodes iterator.
    iterator tlend() noexcept;
    const_iterator tlend() const noexcept;
    const_iterator ctlend() const noexcept;

    bool operator==(const NaiveSceneGraphImplementation &other) const noexcept;
    bool operator!=(const NaiveSceneGraphImplementation &other) const noexcept;

    void swap(NaiveSceneGraphImplementation &other) noexcept;
    template <typename KeyST, typename NodeDataST, typename AllocatorST>
    friend void swap(NaiveSceneGraphImplementation<KeyST, NodeDataST, AllocatorST> &first, 
        NaiveSceneGraphImplementation<KeyST, NodeDataST, AllocatorST> &second) noexcept;

    size_type size() const noexcept;
    constexpr size_type max_size() const noexcept;
    bool empty() const noexcept;

    allocator_type get_allocator() const;

    explicit operator bool() const noexcept;

    void clear() noexcept;

    /**
     * Create a new node in the graph and place it 
     * without attaching it to any parents.
     * No action is taken if the node already exists.
     * @param key Key of the inserted node.
     * @param nodeArgs Arguments passed to the node 
     * data constructor.
     * @return Returns a pointer to the new node and 
     * true as the second, or a pointer to the already 
     * existing node and false as the second.
     * @throws  Throws any exceptions the allocator 
     * throws when allocating and constructing.
     * @warning Node data is constructed using following 
     * constructor parameters: 
     *   (key, std::forward<Ts>(nodeArgs)...)
     */
    template <typename... Ts>
    std::pair<pointer, bool> createNode(const KeyT &key, Ts... nodeArgs);

    /**
     * Remove node from the graph, destroying it 
     * and any children attached to it will be left 
     * with no parent. 
     * @param node Which node should be removed.
     * @throws Throws any exceptions the allocator throws 
     * when allocating and constructing.
     */
    void destroyNode(pointer node);

    /**
     * Add child node to the parent node.
     * Child must not have any current parent, else 
     * an exception is thrown.
     * @param parent Make this node the parent of 
     * the child node.
     * @param child Make this node the child of 
     * the parent node.
     * @param topLevel When set to true, the top-level 
     * node structure is searched and the child node is 
     * removed if found. If unsure, set to true.
     * @throw Throws InvalidParameter exception if 
     * the child already has a parent. InvalidParameter 
     * exception is also thrown is the child is 
     * indirect parent of the specified parent node. 
     * Throws any exceptions encountered by when 
     * allocating child array.
     */
    void addChild(pointer parent, pointer child, bool topLevel = true);

    /**
     * Remove a child node from the parent node.
     * Parent node must be the parent of the child 
     * node, else an exception is thrown.
     * @param parent Current parent of the child node.
     * @param child Child of the parent node.
     * @throws Throws InvalidParameter exception if 
     * the parent node is not the parent of the child 
     * node.
     * @warning Does NOT add the child the the 
     * top-level note list!
     */
    void removeChild(pointer parent, pointer child);

    /**
     * Get node attached to the provided key.
     * @param key Key to search for.
     * @return Returns iterator to the node. If 
     * no such node exists, the returned value 
     * will be equal to end();
     */
    pointer findNode(const KeyT &key);

    /**
     * Get node attached to the provided key.
     * @param key Key to search for.
     * @return Returns iterator to the node. If 
     * no such node exists, the returned value 
     * will be equal to cend();
     */
    const_pointer findNode(const KeyT &key) const;

    /**
     * Make given parent node the parent of given 
     * child node.
     * Parent node can be end(), in which case the 
     * child will be a top-level node - root of a 
     * new tree in the forest.
     * @param parent Node which should become the 
     * parent of the child node.
     * @param child Node which should become the 
     * child of the provided parent node.
     * @warning The parent and child nodes must not 
     * be the same!
     * @throws Throws InvalidParameter if the parent 
     * and child nodes are the same node. Also throws 
     * InvalidParameter in case the child is already 
     * indirect parent of the target parent. Throws 
     * any exceptions encountered when allocating the 
     * child array.
     */
    void makeParentOf(pointer parent, pointer child);

    /**
     * Is given parent node the parent of given child 
     * node?
     * @param parent Is this node the parent of the 
     * child node?
     * @param child Is this node the child of the parent 
     * node?
     * @return Returns true if the parent node is the 
     * parent of the child node.
     */
    bool isParentOf(const_pointer parent, const_pointer child) const;

    /**
     * Is given parent node indirect parent of given 
     * child node? Indirect parent is the direct 
     * parent of the node, or any parent on the chain 
     * from the child to the top-most node.
     * @param parent Is this node the indirect parent 
     * of the child node?
     * @param child Is this node the indirect child of 
     * the parent node?
     * @return Returns true if the parent node is an 
     * indirect parent of the child node.
     */
    bool isIndirectParentOf(const_pointer parent, const_pointer child) const;

    /**
     * Set given node as dirty, specifying that the UpPropagate 
     * structure has been changed.
     * @param node Node to set as dirty.
     * @throws Throws InvalidParameter exception if the node does 
     * not belong into this scene graph.
     */
    void setUpDirty(pointer node);

    /**
     * Set given node as not dirty, specifying that the UpPropagate 
     * structure has not been changed.
     * @param node Node to set as not dirty.
     * @throws Throws InvalidParameter exception if the node does 
     * not belong into this scene graph.
     */
    void resetUpDirty(pointer node);

    /**
     * Set given node as dirty, specifying that the DownPropagate 
     * structure has been changed.
     * @param node Node to set as dirty.
     * @throws Throws InvalidParameter exception if the node does 
     * not belong into this scene graph.
     */
    void setDownDirty(pointer node);

    /**
     * Set given node as not dirty, specifying that the DownPropagate 
     * structure has not been changed.
     * @param node Node to set as not dirty.
     * @throws Throws InvalidParameter exception if the node does 
     * not belong into this scene graph.
     */
    void resetDownDirty(pointer node);

    /**
     * Set given node as not dirty for both down and up.
     * @param node Node to set as not dirty.
     * @throws Throws InvalidParameter exception if the node does 
     * not belong into this scene graph.
     */
    void resetDirty(pointer node);

    /**
     * Propagate dirty DownPropagation and UpPropagation structures.
     */
    void propagateDirty();
private:
    /**
    * Allocate a new node structure and return a pointer
    * to it.
    * Returned pointer is not remembered in any way and
    * should be kept by the caller for later call to
    * destroyNode(node).
    * @param args Arguments passed to the constructor.
    * @throws Throws any exceptions the allocator throws
    * when allocating and constructing.
    */
    template <typename... Ts>
    pointer allocNode(Ts... args);

    /**
     * Allocate array for storing child pointers.
     * @param size Requested number of elements in the 
     * array.
     * @return Returns pointer to the child array.
     * @throws Throws any exception the allocator throws 
     * when constructing and allocating.
     */
    child_array_pointer allocChildArray(size_type size);

    /**
    * De-allocate a node, previously allocated by the
    * allocator.
    * @param node Node which should be destroyed.
    * @throws Throws any exceptions the allocator throws
    * when destructing and de-allocating.
    */
    void deallocNode(pointer node);

    /**
     * De-allocate array for storing child pointers.
     * @param arr Pointer to the child pointer array.
     * @param size Size of the array.
     * @throws Throws any exception the allocator throws 
     * when detructing and de-allocating.
     */
    void deallocChildArray(child_array_pointer arr, size_type size);

    /**
     * Copy data structure from other scene graph.
     * @param other Source of the copy operation.
     * @throws Throws any exception encountered by 
     * the allocator when allocating nodes.
     */
    void copyFrom(const NaiveSceneGraphImplementation &other);

    /**
     * Copy structure of other scene graph.
     * @param other Copy structure of this scene graph.
     */
    void performDeepCopyFrom(const NaiveSceneGraphImplementation &other);

    /**
     * Copy a single tree (or stump) starting with given node..
     * @param root Pointer to the root node in the other scene graph..
     */
    void deepCopyTree(pointer root);

    /**
     * Register a newly created node within the data 
     * structure. The new node will become a top-level 
     * node in the graph.
     * @param newNode Node being registered.
     * @param addToTopLevel Should the node be also 
     * registered as a top-level node?
     * @throws Throws any allocator exception encountered 
     * and InvalidParameter if a node with the same key 
     * already exists.
     */
    void registerNode(pointer newNode, bool addToTopLevel = true);

    /**
     * Un-register provided node from the data structure.
     * After removing the node, its neighbours in the 
     * linked list will be repaired and its children will 
     * be moved to the top-level.
     * @param node Node which should be removed from the 
     * data structure.
     * @throws Throws InvalidParameter if the provided 
     * node has not been registered before.
     */
    void unRegisterNode(pointer node);

    /**
     * Append a new child to the parents children array.
     * @param parent Parent node which should contain the 
     * child node.
     * @param child Child node being added to the parent.
     * @throws Throws any exceptions which occur when allocating 
     * space for the children array.
     * @warning Does NOT check whether the child is already 
     * a child of any other parent!
     */
    void childArrayAppend(pointer parent, pointer child);

    /**
     * Set the parent node for given child node.
     * @param parent New parent node of the child node.
     * @param child Child node which is being changed.
     */
    void setParent(pointer parent, pointer child) noexcept;

    /**
     * Remove a child from the parents children array.
     * @param parent Parent node which is being edited.
     * @param child Child node being removed from the parent.
     * @throws Throws InvalidParameter exception if the child 
     * node is not contained in the parents children array.
     */
    void childArrayRemove(pointer parent, pointer child);

    /**
     * Free the child array of given node.
     * If the child array is not allocated, this action 
     * does nothing.
     * @param node Node with child array.
     * @throws Throws any exceptions encountered when 
     * destructing and de-allocating the child array. 
     */
    void childArrayFree(pointer node);

    /**
     * Reset the parent node for given child node.
     * @param child Child node which is being changed.
     */
    void resetParent(pointer child);

    /**
     * Attempt to erase a top-level node form the 
     * top-level node array.
     * @param node Which node should be erased.
     */
    void eraseTopLevelNode(pointer node);

    /**
     * Check if nodes within provided ranges are equal.
     * @param fbegin Beginning iterator of the first range.
     * @param fend Ending iterator of the second range.
     * @param sbegin Beginning iterator of the second range.
     * @warning Both ranges must have the same size!
     */
    bool nodesEqual(const_pointer fbegin, const_pointer fend, const_pointer sbegin) const;

    /**
     * Get the right-most child for given root node.
     * @param root Root of the search.
     * @return Returns right-most child of given node 
     * or the node itself, if it has no children.
     */
    pointer rightMostChild(pointer root) const;

    /**
     * Propagate given nodes' UpPropagate structure 
     * through its parents.
     * @param node Node which should be propagated.
     */
    void upPropagate(pointer node);

    /**
     * Perform a propagation of UpPropagate from 
     * the 'from' node to the 'to' node.
     * @param from Propagate from this node.
     * @param to Propagate to this node.
     * @tparam HasUpPropagate Should the propagation 
     * be performed?
     */
    template <bool HasUpPropagate>
    typename std::enable_if<HasUpPropagate, void>::type 
    upPropagateFrom(pointer from, pointer to);
    template <bool HasUpPropagate>
    typename std::enable_if<!HasUpPropagate, void>::type 
    upPropagateFrom(pointer from, pointer to);

    /**
     * Propagate given nodes' DownPropagate structure 
     * through its children.
     * @param node Node which should be propagated.
     */
    void downPropagate(pointer node);

    /**
     * Perform a propagation of DownPropagate from 
     * the 'from' node to the 'to' node.
     * @param from Propagate from this node.
     * @param to Propagate to this node.
     * @tparam HasDownPropagate Should the propagation 
     * be performed?
     */
    template <bool HasDownPropagate>
    typename std::enable_if<HasDownPropagate, void>::type 
    downPropagateFrom(pointer from, pointer to);
    template <bool HasDownPropagate>
    typename std::enable_if<!HasDownPropagate, void>::type 
    downPropagateFrom(pointer from, pointer to);

    /**
     * Minimize the number of nodes in the downDirtyNodes 
     * list, without changing the final result of propagation.
     */
    void minimizeDownDirty();

    /**
     * Minimize the number of nodes in the upDirtyNodes 
     * list, without changing the final result of propagation.
     */
    void minimizeUpDirty();

    /// Allocator used for allocation of node data.
    node_allocator_type mAllocator;
    /// Allocator used for allocation of child arrays.
    child_allocator_type mChildAllocator;

    /// Mapping from identifier to its node.
    std::map<KeyT, pointer> mNodeMap;
    /// List of top-level nodes - i.e. nodes without parents.
    std::vector<pointer> mTopLevelNodes;

    /// List of nodes with dirty UpPropagation structure.
    std::set<pointer> mUpDirtyNodes;
    /// List of nodes with dirty DownPropagation structure.
    std::set<pointer> mDownDirtyNodes;

    /// Last node in the linked list.
    pointer mLast{ NullNodePtr };
protected:
}; // class NaiveGraphImplementation

/**
 * SFINAE-based tester, which detects whether given type 
 * has a method with given prototype: 
 * void propagateFrom(const T&)
 * @tparam T Tested type.
 */
template <typename T>
struct hasPropagateFrom
{
private:
    // R has propagateFrom(const R&)
    template <typename R> 
    static constexpr auto tester(int) ->
        typename std::enable_if<
            std::is_same_v<
                decltype(std::declval<R>().propagateFrom(std::declval<const R&>())), 
                void>, 
            std::true_type>::type;
    // R does NOT have propagateFrom(const R&)
    template <typename R> 
    static constexpr auto tester(...) -> std::false_type;
public:
    static constexpr bool value{ decltype(tester<T>(int(0)))::value };
}; // struct hasPropagateFrom

/// Does given type has propagateFrom method?
template <typename T>
static constexpr bool hasPropagateFromV{ hasPropagateFrom<T>::value };

} // namespace impl

/**
 * Universal scene graph which allows the user to: 
 *   1) Specify what data will be contained in the nodes.
 *   2) Use specified DownPropagateT type which will 
 *   propagate its changes through the graph downwards.
 *   2) Use specified UpPropagateT type which will 
 *   propagate its changes through the graph upwards.
 * Container is partially intrusive, which in case 
 * of DownPropagateT and UpPropagateT means, that 
 * the user provides their own storage.
 * Association of KeyT to the node and any internal 
 * data will still be allocated and kept as a part 
 * of the scene graph.
 * From the point of theory, the scene graph is a 
 * directed acyclic graph, more specifically a 
 * forest.
 * @tparam KeyTT Node identifier type.
 * @tparam DownPropagatePtrTT Pointer to the container 
 * which should be propagated downwards. For example 
 * this structure may contain the transform matrix 
 * which, when changed, should update matrices of 
 * child nodes. Can be set to DownPropagateBase 
 * for no down-propagation. Can be any type, which 
 * satisfies NullablePointer and de-references 
 * into non-const reference of a class which inherits 
 * from the DownPropagateBase.
 * @tparam UpPropagatePtrTT Pointer to the container 
 * which should be propagated upwards. For example this 
 * structure may contain a bounding sphere which, 
 * when changed, should propagate upwards to reflect 
 * the new size of the parent nodes. Can be set to 
 * UpPropagateBase for no up-propagation. Can be any 
 * type, which satisfies NullablePointer and de-references 
 * into non-const reference of a class which inherits 
 * from the UpPropagateBase. 
 * @tparam AllocatorTT Allocator used for allocating 
 * the book-keeping node structure. This allocator is 
 * not guaranteed to be used only for allocation of 
 * KeyTT elements!
 */
template <typename KeyTT, 
          typename DownPropagatePtrTT = DownPropagateBase::DefaultPropagateDownPtrT, 
          typename UpPropagatePtrTT = UpPropagateBase::DefaultPropagateUpPtrT, 
          typename AllocatorTT = std::allocator<KeyTT>>
class SceneGraph
{
public:
    // Assertion of correct template parameters.
    static_assert(std::is_default_constructible_v<KeyTT>, 
        "Key must default constructible!");

    static_assert(util::isDereferencableV<DownPropagatePtrTT>, 
        "DownPropagatePtrTT must be dereferencable!");
    static_assert(util::isComparableToNullptrV<DownPropagatePtrTT>, 
        "DownPropagatePtrTT must comparable to nullptr!");
    static_assert(util::isDereferencableV<UpPropagatePtrTT>, 
        "UpPropagatePtrTT must be dereferencable!");
    static_assert(util::isComparableToNullptrV<UpPropagatePtrTT>, 
        "UpPropagatePtrTT must comparable to nullptr!");

    /// Shortcut for type of this scene graph.
    using ThisSceneGraphT = SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>;
    /// Type for the identifier of a node.
    using KeyT = KeyTT;
    /// Pointer typ for the down propagate structure.
    using DownPropagatePtrT = DownPropagatePtrTT;
    /// Type for the down-propagated data.
    using DownPropagateT = typename std::pointer_traits<DownPropagatePtrT>::element_type;
    /// Pointer typ for the up propagate structure.
    using UpPropagatePtrT = UpPropagatePtrTT;
    /// Type for the up-propagated data.
    using UpPropagateT = typename std::pointer_traits<UpPropagatePtrT>::element_type;
    /// Type for the internal data for each node.
    using NodeDataT = impl::SceneGraphInternalData<KeyT, typename std::allocator_traits<AllocatorTT>::pointer, DownPropagatePtrT, UpPropagatePtrT>;
    /// Allocator type used for allocating the internal nodes.
    using AllocatorT = typename std::allocator_traits<AllocatorTT>::template rebind_alloc<NodeDataT>;
    /// Type used for passing propagation parameters.
    using DownUpPackT = typename NodeDataT::DownUpPack;

    static_assert(std::is_base_of_v<DownPropagateBase, DownPropagateT>, 
        "Type gained by de-referencing DownPropagatePtrTT must inherit from DownPropagateBase!");
    static_assert(std::is_base_of_v<UpPropagateBase, UpPropagateT>, 
        "Type gained by de-referencing UpPropagatePtrTT must inherit from UpPropagateBase!");

    static_assert(std::is_same_v<DownPropagateBase::DefaultPropagateDownPtrT, DownPropagatePtrT> ||
        impl::hasPropagateFromV<DownPropagateT>, 
        "Custom DownPropagate must have void propagateFrom(const T&) method!");
    static_assert(std::is_same_v<UpPropagateBase::DefaultPropagateUpPtrT, UpPropagatePtrT> ||
        impl::hasPropagateFromV<UpPropagateT>, 
        "Custom UpPropagate must have void propagateFrom(const T&) method!");

private:
    /// Implementation of the scene graph container.
    using ImplT = impl::NaiveSceneGraphImplementation<KeyTT, NodeDataT, AllocatorTT>;
    using ImplIteratorT = typename ImplT::iterator;
    using ImplConstIteratorT = typename ImplT::const_iterator;
    static constexpr auto ImplNullNodePtr{ ImplT::NullNodePtr };
    using ImplPointerT = typename ImplT::pointer;
    using ImplConstPointerT = typename ImplT::const_pointer;
public:
    // Forward declaration.
    template <bool ConstIter>
    class NodeIterator;

    /// Base for node array iterators.
    template <bool ConstIter = false>
    class NodeIteratorBase
    {
    public:
        /*
         * Do NOT remove, looks like a bug in the MSVC: 
         * Access to the private/protected member of nested 
         * class from the outer class should be fine, but 
         * without friend declaration the compiler refuses 
         * to compile code which accesses node_pointer type.
         */
        template <typename, typename, typename, typename>
        friend class SceneGraph;

        using value_type = const typename NodeDataT::UserData;
        using reference = value_type&;
        using const_reference = const reference;
        using pointer = value_type*;
        using const_pointer = const pointer;
        using difference_type = std::ptrdiff_t;
        using size_type = std::size_t;

        /**
         * Access the current node.
         * @warning When called on invalid iterator, access to 
         * unallocated memory may occur.
         */ 
        reference operator*();
        /**
         * Access the current node.
         * @warning When called on invalid iterator, access to 
         * unallocated memory may occur.
         */ 
        const_reference operator*() const;

        /**
         * Access the current node.
         * @warning When called on invalid iterator, access to 
         * unallocated memory may occur.
         */ 
        pointer operator->();
        /**
         * Access the current node.
         * @warning When called on invalid iterator, access to 
         * unallocated memory may occur.
         */
        const_pointer operator->() const;

        /// Compare iterators.
        bool operator==(const NodeIteratorBase &other) const noexcept;
        /// Compare iterators.
        bool operator!=(const NodeIteratorBase &other) const noexcept;

        /**
         * Get iterator to the first child in the 
         * child array. Should be paired with chldend().
         * @warning When called on invalid iterator, access to 
         * unallocated memory may occur.
         */ 
        NodeIterator<ConstIter> chldbegin() const noexcept;

        /**
         * Get iterator to the end of the child array. 
         * Should be paired with chldend().
         * @warning When called on invalid iterator, access to 
         * unallocated memory may occur.
         */ 
        NodeIterator<ConstIter> chldend() const noexcept;

        /**
         * Set this nodes DownPropagation structure as dirty.
         * @warning When called on invalid iterator, access to 
         * unallocated memory may occur.
         */
        void setDownDirty() const;

        /**
         * Set this nodes UpPropagation structure as dirty.
         * @warning When called on invalid iterator, access to 
         * unallocated memory may occur.
         */
        void setUpDirty() const;
    private:
    protected:
        /// Pointer to the owner scene graph.
        using scene_graph_pointer = typename std::conditional<ConstIter, 
            const ThisSceneGraphT*, ThisSceneGraphT*>::type;

        /// Iterator to the internal node structure used by this iterator.
        using node_iterator = typename std::conditional<ConstIter, 
            ImplConstIteratorT, ImplIteratorT>::type;

        /// Pointer to the internal node structure used by this iterator.
        using node_pointer = typename std::conditional<ConstIter, 
            ImplConstPointerT, ImplPointerT>::type;

        /// Default initialized iterator.
        NodeIteratorBase(scene_graph_pointer sg);
        /// Copy-constructor.
        template <bool FromConst>
        NodeIteratorBase(const NodeIteratorBase<FromConst> &other);
        /// Copy-assignment.
        template <bool FromConst>
        NodeIteratorBase &operator=(const NodeIteratorBase<FromConst> &other);

        /// Iterator to provided node.
        explicit NodeIteratorBase(node_iterator ptr, scene_graph_pointer sg);

        /// Get pointer to the pointed node.
        node_pointer ptr() const;

        /// Node currently pointed to by this iterator.
        node_iterator mCurrent{ };
        /// Pointer to the scene graph who owns the nodes.
        scene_graph_pointer mSG{ nullptr };
    }; // class NodeIteratorBase

    /// Iterator used for iterating through nodes in a contiguous block.
    template <bool ConstIter = false>
    class NodeIterator : public NodeIteratorBase<ConstIter>
    {
    private:
        using this_base = NodeIteratorBase<ConstIter>;
    public:
        template <typename, typename, typename, typename>
        friend class SceneGraph;

        using iterator_category = std::random_access_iterator_tag;
        using this_iterator = NodeIterator<ConstIter>;
        using this_base = NodeIteratorBase<ConstIter>;

        /// Default-initialized invalid iterator.
        NodeIterator(typename this_base::scene_graph_pointer sg = nullptr);
        /// Copy-constructor.
        template <bool FromConst>
        NodeIterator(const NodeIterator<FromConst> &other);
        /// Copy-assignment.
        template <bool FromConst>
        NodeIterator &operator=(const NodeIterator<FromConst> &other);

        /// Allow conversion from the non-const version to the const one.
        operator NodeIterator<true>() const;

        /**
         * Pre-increment, moving to the next node 
         * in the list.
         * @return Returns iterator after moving.
         */
        this_iterator &operator++();

        /**
         * Pre-decrement, moving to the previous node 
         * in the list.
         * @return Returns iterator after moving.
         */
        this_iterator &operator--();

        /**
         * Post-increment, moving to the next node 
         * in the list.
         * @return Returns iterator before moving.
         * @warning This iterator remains unchanged!
         */
        this_iterator operator++(int);

        /**
         * Post-decrement, moving to the previous node 
         * in the list.
         * @return Returns iterator before moving.
         * @warning This iterator remains unchanged!
         */
        this_iterator operator--(int);

        /**
         * Move this iterator by a given number of nodes.
         * @param n By how much should the iterator move.
         * @return Returns iterator after moving.
         * @warning This iterator remains unchanged!
         */
        this_iterator operator+(const typename this_base::difference_type &n) const;
        friend this_iterator operator+(const typename this_base::difference_type &n, const this_iterator &it)
        { return it + n; }

        /**
         * Move this iterator by a given number of nodes.
         * @param n By how much should the iterator move.
         * @return Returns iterator after moving.
         * @warning This iterator remains unchanged!
         */
        this_iterator operator-(const typename this_base::difference_type &n) const;

        /**
         * Does this iterator point to the same element 
         * as the other iterator?
         * @param other The other iterator.
         * @return Returns true if they point to the 
         * same element.
         */
        bool operator==(const this_iterator &other) const;

        /**
         * Does this iterator point to a different element 
         * than the other iterator?
         * @param other The other iterator.
         * @return Returns false if they point to the 
         * same element.
         */
        bool operator!=(const this_iterator &other) const;

        /**
         * Move this iterator by a given number of nodes.
         * @param n By how much should the iterator move.
         * @return Returns iterator after moving.
         */
        this_iterator &operator+=(const typename this_base::difference_type &n);

        /**
         * Move this iterator by a given number of nodes.
         * @param n By how much should the iterator move.
         * @return Returns iterator after moving.
         */
        this_iterator &operator-=(const typename this_base::difference_type &n);

        /**
         * Get difference between two iterators.
         * @param other The other iterator.
         * @return Returns difference in elements between 
         * the two iterators.
         */
        typename this_base::difference_type operator-(const this_iterator &other) const;

        /**
         * Access a node located at specified distance 
         * from the current element.
         * @param i Distance to access.
         * @return Returns a reference to the target node.
         */
        typename this_base::reference operator[](const typename this_base::difference_type &i) const;

        /**
         * Does this iterator precede the provided 
         * iterator?
         * @return Returns true if this iterator 
         * precedes the provided iterator.
         */
        bool operator<(const this_iterator &other) const;

        /**
         * Does this iterator precede or is the same as 
         * the provided iterator?
         * @return Returns true if this iterator 
         * precedes or is the same as the provided iterator.
         */
        bool operator<=(const this_iterator &other) const;

        /**
         * Does this iterator go after the provided 
         * iterator?
         * @return Returns true if this iterator 
         * goes after the provided iterator.
         */
        bool operator>(const this_iterator &other) const;

        /**
         * Does this iterator go after or is the same as 
         * the provided iterator?
         * @return Returns true if this iterator 
         * goes after or is the same as the provided iterator.
         */
        bool operator>=(const this_iterator &other) const;
    private:
    protected:
        /// Create iterator from a pointer to the target node.
        explicit NodeIterator(typename this_base::node_iterator ptr, typename this_base::scene_graph_pointer sg);
    }; // class NodeIterator

    /// Base for scene graph iterators.
    template <bool ConstIter = false>
    class StructureIteratorBase
    {
    public:
        /*
         * Do NOT remove, looks like a bug in the MSVC: 
         * Access to the private/protected member of nested 
         * class from the outer class should be fine, but 
         * without friend declaration the compiler refuses 
         * to compile code which accesses node_pointer type.
         */
        template <typename, typename, typename, typename>
        friend class SceneGraph;

        using value_type = const typename NodeDataT::UserData;
        using reference = value_type&;
        using const_reference = const reference;
        using pointer = value_type*;
        using const_pointer = const pointer;
        using difference_type = std::ptrdiff_t;
        using size_type = std::size_t;

        /**
         * Access the current node.
         * @warning When called on invalid iterator, access to 
         * unallocated memory may occur.
         */
        reference operator*();
        /**
         * Access the current node.
         * @warning When called on invalid iterator, access to 
         * unallocated memory may occur.
         */
        const_reference operator*() const;

        /**
         * Access the current node.
         * @warning When called on invalid iterator, access to 
         * unallocated memory may occur.
         */
        pointer operator->();
        /**
         * Access the current node.
         * @warning When called on invalid iterator, access to 
         * unallocated memory may occur.
         */
        const_pointer operator->() const;

        /// Compare iterators.
        bool operator==(const StructureIteratorBase &other) const noexcept;
        /// Compare iterators.
        bool operator!=(const StructureIteratorBase &other) const noexcept;

        /**
         * Get iterator to the first child in the 
         * child array. Should be paired with chldend().
         * @warning When called on invalid iterator, access to 
         * unallocated memory may occur.
         */ 
        NodeIterator<ConstIter> chldbegin() const noexcept;

        /**
         * Get iterator to the end of the child array. 
         * Should be paired with chldend().
         * @warning When called on invalid iterator, access to 
         * unallocated memory may occur.
         */ 
        NodeIterator<ConstIter> chldend() const noexcept;

        /**
         * Set this nodes DownPropagation structure as dirty.
         * @warning When called on invalid iterator, access to 
         * unallocated memory may occur.
         */
        void setDownDirty() const;

        /**
         * Set this nodes UpPropagation structure as dirty.
         * @warning When called on invalid iterator, access to 
         * unallocated memory may occur.
         */
        void setUpDirty() const;
    private:
    protected:
        /// Pointer to the owner scene graph.
        using scene_graph_pointer = typename std::conditional<ConstIter, 
            const ThisSceneGraphT*, ThisSceneGraphT*>::type;

        /// Pointer to the internal node structure used by this iterator.
        using node_pointer = typename std::conditional<ConstIter, 
            ImplConstPointerT, ImplPointerT>::type;

        /// Default initialized iterator.
        StructureIteratorBase(scene_graph_pointer sg);
        /// Copy-constructor.
        template <bool FromConst>
        StructureIteratorBase(const StructureIteratorBase<FromConst> &other);
        /// Copy-assignment.
        template <bool FromConst>
        StructureIteratorBase &operator=(const StructureIteratorBase<FromConst> &other);

        /// Construct from any node iterator.
        template <bool FromConst>
        StructureIteratorBase(const NodeIteratorBase<FromConst> &other);
        /// Copy from any node iterator.
        template <bool FromConst>
        StructureIteratorBase &operator=(const NodeIteratorBase<FromConst> &other);

        /// Iterator to provided node.
        explicit StructureIteratorBase(node_pointer ptr, scene_graph_pointer sg);

        /// Get pointer to the pointed node.
        node_pointer ptr() const;

        /// Node currently pointed to by this iterator.
        node_pointer mCurrent{ ImplNullNodePtr };
        /// Pointer to the scene graph who owns the nodes.
        scene_graph_pointer mSG{ nullptr };
    }; // class StructureIteratorBase

    /// Iterator used for iterating through node parents up to the top-most node.
    template <bool ConstIter = false>
    class ParentIterator : public StructureIteratorBase<ConstIter>
    {
    public:
        template <typename, typename, typename, typename>
        friend class SceneGraph;

        using iterator_category = std::forward_iterator_tag;
        using this_iterator = ParentIterator<ConstIter>;
        using this_base = NodeIteratorBase<ConstIter>;

        /// Default-initialized invalid iterator.
        ParentIterator(typename this_base::scene_graph_pointer sg = nullptr);
        /// Copy-constructor.
        template <bool FromConst>
        ParentIterator(const ParentIterator<FromConst> &other);
        /// Copy-assignment.
        template <bool FromConst>
        ParentIterator &operator=(const ParentIterator<FromConst> &other);

        /// Allow conversion from the non-const version to the const one.
        operator ParentIterator<true>() const;

        /// Construct from any node iterator.
        template <bool FromConst>
        ParentIterator(const NodeIteratorBase<FromConst> &other);
        /// Copy from any node iterator.
        template <bool FromConst>
        ParentIterator &operator=(const NodeIteratorBase<FromConst> &other);

        /// Get the end iterator for all parent iterators.
        static this_iterator end() noexcept;

        /**
         * Pre-increment, moving to the parent node.
         * @return Returns iterator after moving.
         */
        this_iterator &operator++();

        /**
         * Post-increment, moving to the parent node.
         * @return Returns iterator before moving.
         */
        this_iterator operator++(int);

        /**
         * Does this iterator point to the same element 
         * as the other iterator?
         * @param other The other iterator.
         * @return Returns true if they point to the 
         * same element.
         */
        bool operator==(const this_iterator &other) const;

        /**
         * Does this iterator point to a different element 
         * than the other iterator?
         * @param other The other iterator.
         * @return Returns false if they point to the 
         * same element.
         */
        bool operator!=(const this_iterator &other) const;
    private:
    protected:
        /// Create iterator from a pointer to the target node.
        explicit ParentIterator(typename this_base::node_pointer ptr, typename this_base::scene_graph_pointer sg);
    }; // class ParentIterator

    /**
     * Iterator used for iterating through all of the nodes in the 
     * scene graph. The order is implementation specific.
     */
    template <bool ConstIter = false>
    class LinkedIterator : public StructureIteratorBase<ConstIter>
    {
    public:
        template <typename, typename, typename, typename>
        friend class SceneGraph;

        using iterator_category = std::bidirectional_iterator_tag;
        using this_iterator = LinkedIterator<ConstIter>;
        using this_base = StructureIteratorBase<ConstIter>;

        /// Default-initialized invalid iterator.
        LinkedIterator(typename this_base::scene_graph_pointer sg = nullptr);
        /// Copy-constructor.
        template <bool FromConst>
        LinkedIterator(const LinkedIterator<FromConst> &other);
        /// Copy-assignment.
        template <bool FromConst>
        LinkedIterator &operator=(const LinkedIterator<FromConst> &other);

        /// Allow conversion from the non-const version to the const one.
        operator LinkedIterator<true>() const;

        /// Construct from any node iterator.
        template <bool FromConst>
        LinkedIterator(const NodeIteratorBase<FromConst> &other);
        /// Copy from any node iterator.
        template <bool FromConst>
        LinkedIterator &operator=(const NodeIteratorBase<FromConst> &other);

        /// Get the end iterator for all linked iterators.
        static this_iterator end() noexcept;

        /**
         * Pre-increment, moving to the parent node.
         * @return Returns iterator after moving.
         */
        this_iterator &operator++();

        /**
         * Post-increment, moving to the parent node.
         * @return Returns iterator before moving.
         */
        this_iterator operator++(int);

        /**
         * Pre-decrement, moving to the parent node.
         * @return Returns iterator after moving.
         */
        this_iterator &operator--();

        /**
         * Post-decrement, moving to the parent node.
         * @return Returns iterator before moving.
         */
        this_iterator operator--(int);

        /**
         * Does this iterator point to the same element 
         * as the other iterator?
         * @param other The other iterator.
         * @return Returns true if they point to the 
         * same element.
         */
        bool operator==(const this_iterator &other) const;

        /**
         * Does this iterator point to a different element 
         * than the other iterator?
         * @param other The other iterator.
         * @return Returns false if they point to the 
         * same element.
         */
        bool operator!=(const this_iterator &other) const;
    private:
    protected:
        /// Create iterator from a pointer to the target node.
        explicit LinkedIterator(typename this_base::node_pointer ptr, typename this_base::scene_graph_pointer sg);
    }; // class LinkedIterator

    /// Exception thrown when parameter is wrongly specified.
    using InvalidParameter = typename ImplT::InvalidParameter;

    /// Value for a node pointer which is pointing to no node.
    static constexpr auto NullNodePtr{ ImplNullNodePtr };

    // container.requirements.general Allocator-aware requirements:  
    using allocator_type = AllocatorT;
    using allocator_traits = std::allocator_traits<allocator_type>;

    // container.requirements.general Container requirements: 
    using value_type = typename NodeDataT::UserData;
    using reference = value_type&;
    using const_reference = const value_type&;
    using pointer = ImplPointerT;
    using const_pointer = ImplConstPointerT;
    using iterator = LinkedIterator<false>;
    using const_iterator = LinkedIterator<true>;
    using node_iterator = NodeIterator<false>;
    using const_node_iterator = NodeIterator<true>;
    using difference_type = std::ptrdiff_t;
    using size_type = typename ImplT::size_type;

    static_assert(std::is_same_v<typename allocator_traits::value_type, NodeDataT>,
        "Allocator must be specialized to allocate node structures!");
    /// Pointer for allocated internal node structures.
    using node_pointer = typename allocator_traits::pointer;
    /// Pointer for allocated internal node structures.
    using const_node_pointer = typename allocator_traits::const_pointer;

    static_assert(std::is_same_v<pointer, typename iterator::node_pointer>,
        "Iterator must use the same pointer as the implementation!");
    static_assert(std::is_same_v<const_pointer, typename const_iterator::node_pointer>,
        "Iterator must use the same pointer as the implementation!");

    static_assert(std::is_same_v<ImplIteratorT, typename node_iterator::node_iterator>,
        "Iterator must use the same iterator as the implementation!");
    static_assert(std::is_same_v<ImplConstIteratorT, typename const_node_iterator::node_iterator>,
        "Iterator must use the same iterator as the implementation!");

    /// Create empty scene graph.
    SceneGraph(const allocator_type &allocator = allocator_type());

    /**
     * Free any node structure, keeping the 
     * intrusive data as is.
     * It is users responsibility to free the 
     * DownPropagateT and UpPropagateT.
     */
    ~SceneGraph();

    /**
     * Create a copy of given scene graph.
     * Node structure is deep-copied, but the 
     * intrusive data is just taken over.
     * @param other What to make a copy of.
     * @throws Throws std::bad_alloc if allocation 
     * of the node structure fails.
     */
    SceneGraph(const SceneGraph &other);
    /**
     * Move data over from given scene graph.
     * Data is moved using swap.
     * @param other What to move over.
     */
    SceneGraph(SceneGraph &&other) noexcept;
    /**
     * Create a copy of given scene graph.
     * Node structure is deep-copied, but the 
     * intrusive data is just taken over.
     * @param other What to make a copy of.
     * @throws Throws std::bad_alloc if allocation 
     * of the node structure fails.
     */
    SceneGraph &operator=(const SceneGraph &other);
    /**
     * Move data over from given scene graph.
     * Data is moved using swap.
     * @param other What to move over.
     */
    SceneGraph &operator=(SceneGraph &&other) noexcept;

    /**
     * Get iterator to the first node, allowing 
     * iterator through the nodes in un-specified 
     * order.
     */
    iterator begin() noexcept;

    /**
     * Get iterator to the first node, allowing 
     * iterator through the nodes in un-specified 
     * order.
     */
    const_iterator begin() const noexcept;

    /// Iterator paired with the begin() iterator.
    iterator end() noexcept;

    /// Iterator paired with the begin() iterator.
    const_iterator end() const noexcept;

    /**
     * Get iterator to the first node, allowing 
     * iterator through the nodes in un-specified 
     * order.
     */
    const_iterator cbegin() const noexcept;

    /// Iterator paired with the cbegin() iterator.
    const_iterator cend() const noexcept;

    /**
     * Get iterator to the first top-level node, allowing 
     * iterator through the top-level nodes in un-specified 
     * order.
     */
    node_iterator tlbegin() noexcept;

    /**
     * Get iterator to the first top-level node, allowing 
     * iterator through the top-level nodes in un-specified 
     * order.
     */
    const_node_iterator tlbegin() const noexcept;

    /// Iterator paired with the tlbegin() iterator.
    node_iterator tlend() noexcept;

    /// Iterator paired with the tlbegin() iterator.
    const_node_iterator tlend() const noexcept;

    /**
     * Get iterator to the first top-level node, allowing 
     * iterator through the top-level nodes in un-specified 
     * order.
     */
    const_node_iterator ctlbegin() const noexcept;

    /// Iterator paired with the ctlbegin() iterator.
    const_node_iterator ctlend() const noexcept;

    /**
     * Compare scene graph structure.
     * @param other What to compare to.
     * @return Returns true if the two scene 
     * graphs have the same structure.
     */
    bool operator==(const SceneGraph &other) const noexcept;

    /**
     * Compare scene graph structure.
     * @param other What to compare to.
     * @return Returns false if the two scene 
     * graphs have the same structure.
     */
    bool operator!=(const SceneGraph &other) const noexcept;

    /**
     * Print out scene graph status.
     * @param out Text output stream.
     * @param sc Scene graph to print for.
     */
    template <typename KTT, typename DPPTT, typename UPPTT, typename ATT>
    friend std::ostream &operator<<(std::ostream &out, const SceneGraph<KTT, DPPTT, UPPTT, ATT> &sc);

    /**
     * Swap scene graph structures.
     * @param other What to swap this scene 
     * graph for.
     */
    void swap(SceneGraph &other) noexcept;

    /**
     * Swap scene graph structures.
     * @param first Swap this scene graph with 
     * the second one.
     * @param second Swap this scene graph with 
     * the first one.
     */
    template <typename KeyST, typename DownPropagatePtrST, 
              typename UpPropagatePtrST, typename AllocatorST>
    friend void swap(util::SceneGraph<KeyST, DownPropagatePtrST, UpPropagatePtrST, AllocatorST> &first, 
        util::SceneGraph<KeyST, DownPropagatePtrST, UpPropagatePtrST, AllocatorST> &second) noexcept;

    /**
     * Get number of nodes in the scene graph.
     * @return Returns the number of nodes in this 
     * scene graph.
     */
    size_type size() const noexcept;

    /// Maximum number of nodes in the scene graph.
    constexpr size_type max_size() const noexcept;

    /**
     * Check, whether the scene graph is empty.
     * @returns Returns true for empty scene 
     * graph - graph with no nodes.
     */
    bool empty() const noexcept;

    /**
     * Get allocator used by this instance.
     * @return Returns allocator used by this 
     * scene graph.
     */
    allocator_type get_allocator() const;

    /// Is the scene graph empty?
    explicit operator bool() const noexcept;

    /**
     * Insert a new node into the scene graph, at the 
     * top-most level - with no parent.
     * The insertion may end with the node with given 
     * key already existing, in which case the node will 
     * be left unchanged and the return value will 
     * contain false as the second element.
     * @param key Key of the inserted node.
     * @param propagation Propagation data structures which 
     * may be left unspecified, in case of scene graph 
     * without DownPropagateT and UpPropagateT specified.
     * @return Returns pair with the first element containing 
     * iterator to the inserted (existing) node and the second 
     * will be true if the node had to be created.
     * @throws Throws any exception encountered when allocating 
     * and constructing the nodes.
     */
    std::pair<iterator, bool> insert(const KeyT &key, DownUpPackT propagation = {});

    /**
     * Does the scene graph contain a node with 
     * given key identifier?
     * @param key Key to check for.
     * @return Returns true if the node 
     * exists, otherwise returns false.
     */
    bool contains(const KeyT &key) const;

    /**
     * Find node with given identifier in 
     * this scene graph and return an iterator 
     * to it.
     * @param key What key to look for.
     * @return Returns iterator the the node 
     * with given key identifier. If such node 
     * does NOT exist, returns end() instead.
     */
    iterator find(const KeyT &key);

    /**
     * Find node with given identifier in 
     * this scene graph and return an iterator 
     * to it.
     * @param key What key to look for.
     * @return Returns iterator the the node 
     * with given key identifier. If such node 
     * does NOT exist, returns cend() instead.
     */
    const_iterator find(const KeyT &key) const;

    /**
     * Erase node with given key identifier and 
     * all of its children.
     * If no such node exists, no action will 
     * be taken.
     * @param key Key to identify the target node.
     * @return Returns true if any nodes were 
     * erased, else returns false.
     * @throws Throws any exceptions encountered when 
     * de-allocating and destroying the node. 
     */
    bool erase(const KeyT &key);

    /**
     * Erase node pointed to by given iterator 
     * and any child nodes connected will be left 
     * with no parent.
     * @param pos Iterator pointing to the 
     * target node.
     * @return Returns iterator pointing to the 
     * next node in line after erasure.
     */
    iterator erase(iterator pos);

    /**
     * Clear the graph, removing all of the nodes.
     * @throws Throws any exceptions encountered when 
     * de-allocating and destroying the nodes.
     */
    void clear();

    /**
     * Make given parent node the parent of given 
     * child node.
     * Parent node can be end(), in which case the 
     * child will be a top-level node - root of a 
     * new tree in the forest.
     * @param parent Node which should become the 
     * parent of the child node.
     * @param child Node which should become the 
     * child of the provided parent node.
     * @warning The parent and child nodes must not 
     * be the same!
     * @throws Throws InvalidParameter if the parent 
     * and child nodes are the same node. Also throws 
     * InvalidParameter in case the child is already 
     * indirect parent of the target parent. Throws 
     * any exceptions encountered when allocating the 
     * child array.
     */
    void makeParentOf(iterator parent, iterator child);

    /**
     * Is given parent node the parent of given child 
     * node?
     * @param parent Is this node the parent of the 
     * child node?
     * @param child Is this node the child of the parent 
     * node?
     * @return Returns true if the parent node is the 
     * parent of the child node.
     */
    bool isParentOf(const_iterator parent, const_iterator child) const;

    /**
     * Is given parent node indirect parent of given 
     * child node? Indirect parent is the direct 
     * parent of the node, or any parent on the chain 
     * from the child to the top-most node.
     * @param parent Is this node the indirect parent 
     * of the child node?
     * @param child Is this node the indirect child of 
     * the parent node?
     * @return Returns true if the parent node is an 
     * indirect parent of the child node.
     */
    bool isIndirectParentOf(const_iterator parent, const_iterator child) const;

    /**
     * Set given node as dirty, specifying that the UpPropagate 
     * structure has been changed.
     * @param node Node to set as dirty.
     * @throws Throws InvalidParameter exception if the node does 
     * not belong into this scene graph.
     */
    void setUpDirty(iterator node);

    /**
     * Set given node as not dirty, specifying that the UpPropagate 
     * structure has not been changed.
     * @param node Node to set as not dirty.
     * @throws Throws InvalidParameter exception if the node does 
     * not belong into this scene graph.
     */
    void resetUpDirty(iterator node);

    /**
     * Set given node as dirty, specifying that the DownPropagate 
     * structure has been changed.
     * @param node Node to set as dirty.
     * @throws Throws InvalidParameter exception if the node does 
     * not belong into this scene graph.
     */
    void setDownDirty(iterator node);

    /**
     * Set given node as not dirty, specifying that the DownPropagate 
     * structure has not been changed.
     * @param node Node to set as not dirty.
     * @throws Throws InvalidParameter exception if the node does 
     * not belong into this scene graph.
     */
    void resetDownDirty(iterator node);

    /**
     * Set given node as not dirty for both down and up.
     * @param node Node to set as not dirty.
     * @throws Throws InvalidParameter exception if the node does 
     * not belong into this scene graph.
     */
    void resetDirty(iterator node);

    /**
     * Propagate dirty DownPropagation and UpPropagation structures.
     */
    void propagateDirty();
private:
    /**
     * Create a new node in the graph and place it 
     * without attaching it to any parents.
     * @param key Key of the inserted node.
     * @param propagation Propagation data structures which 
     * may be left unspecified, in case of scene graph 
     * without DownPropagateT and UpPropagateT specified.
     * @return Returns iterator to the new node.
     * @throws If a node with given key already 
     * exists, InvalidParameter exception is thrown. 
     * Throws any exceptions the allocator throws when 
     * allocating and constructing.
     */
    iterator createNode(const KeyT &key, DownUpPackT propagation);

    /**
     * Remove node from the graph, destroying it 
     * and any children attached to it will be left 
     * with no parent. 
     * @param node Which node should be removed.
     * @throws Throws any exceptions the allocator throws 
     * when allocating and constructing.
     */
    void destroyNode(iterator node);

    /**
     * Add child node to the parent node.
     * Child must not have any current parent, else 
     * an exception is thrown.
     * @param parent Make this node the parent of 
     * the child node.
     * @param child Make this node the child of 
     * the parent node.
     * @throw Throws InvalidParameter exception if 
     * the child already has a parent. Throws any 
     * exceptions encountered by when allocating 
     * child array.
     */
    void addChild(iterator parent, iterator child);

    /**
     * Remove a child node from the parent node.
     * Parent node must be the parent of the child 
     * node, else an exception is thrown.
     * @param parent Current parent of the child node.
     * @param child Child of the parent node.
     * @throws Throws InvalidParameter exception if 
     * the parent node is not the parent of the child 
     * node.
     */
    void removeChild(iterator parent, iterator child);

    /**
     * Get node attached to the provided key.
     * @param key Key to search for.
     * @return Returns iterator to the node. If 
     * no such node exists, the returned value 
     * will be equal to end();
     */
    iterator findNode(const KeyT &key);

    /**
     * Get node attached to the provided key.
     * @param key Key to search for.
     * @return Returns iterator to the node. If 
     * no such node exists, the returned value 
     * will be equal to cend();
     */
    const_iterator findNode(const KeyT &key) const;

    /// Implementation of the scene graph structure.
    ImplT mImpl;
protected:
}; // class SceneGraph

} // namespace util

// Template implementation.

namespace util
{

namespace impl
{

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::NaiveSceneGraphImplementation(
    const allocator_type &allocator) :
    mAllocator(allocator),
    mChildAllocator(allocator),
    mNodeMap(mChildAllocator),
    mTopLevelNodes(mChildAllocator),
    mUpDirtyNodes(mChildAllocator), 
    mDownDirtyNodes(mChildAllocator),
    mLast{ NullNodePtr }
{ }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::~NaiveSceneGraphImplementation()
{ clear(); }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::NaiveSceneGraphImplementation(
    const NaiveSceneGraphImplementation &other) :
    NaiveSceneGraphImplementation( 
        allocator_traits::select_on_container_copy_construction(other.get_allocator()) )
{ copyFrom(other); }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::NaiveSceneGraphImplementation(
    NaiveSceneGraphImplementation &&other) noexcept :
    NaiveSceneGraphImplementation()
{
    /*
    using std::swap;
    swap(*this, other);
    */
    *this = std::move(other);
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT> & NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT
>::operator=(const NaiveSceneGraphImplementation &other)
{
    copyFrom(other);
    return *this;
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT> & NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT
>::operator=(NaiveSceneGraphImplementation &&other) noexcept
{
    if (allocator_traits::propagate_on_container_move_assignment::value)
    { // We can move the data over.
        mAllocator = std::move(other.mAllocator);
        mChildAllocator = std::move(other.mChildAllocator);
        mNodeMap = std::move(other.mNodeMap);
        mTopLevelNodes = std::move(other.mTopLevelNodes);
        mUpDirtyNodes = std::move(other.mUpDirtyNodes);
        mDownDirtyNodes = std::move(other.mDownDirtyNodes);
        mLast = std::move(other.mLast);
    }
    else
    { // Fallback to copy.
        copyFrom(other);
    }

    return *this;
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::pointer NaiveSceneGraphImplementation<KeyT,
NodeDataT, AllocatorTT>::begin() noexcept
{ return mTopLevelNodes.empty() ? end() : *mTopLevelNodes.data(); }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::const_pointer NaiveSceneGraphImplementation<KeyT,
NodeDataT, AllocatorTT>::begin() const noexcept
{ return mTopLevelNodes.empty() ? end() : *mTopLevelNodes.data(); }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::const_pointer NaiveSceneGraphImplementation<KeyT,
NodeDataT, AllocatorTT>::cbegin() const noexcept
{ return mTopLevelNodes.empty() ? cend() : *mTopLevelNodes.data(); }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::pointer NaiveSceneGraphImplementation<KeyT,
NodeDataT, AllocatorTT>::end() noexcept
{ return NullNodePtr; }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::const_pointer NaiveSceneGraphImplementation<KeyT,
NodeDataT, AllocatorTT>::end() const noexcept
{ return NullNodePtr; }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::const_pointer NaiveSceneGraphImplementation<KeyT,
NodeDataT, AllocatorTT>::cend() const noexcept
{ return NullNodePtr; }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::iterator NaiveSceneGraphImplementation<KeyT,
NodeDataT, AllocatorTT>::tlbegin() noexcept
{ return mTopLevelNodes.data(); }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::const_iterator NaiveSceneGraphImplementation<KeyT,
NodeDataT, AllocatorTT>::tlbegin() const noexcept
{ return mTopLevelNodes.data(); }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::const_iterator NaiveSceneGraphImplementation<KeyT,
NodeDataT, AllocatorTT>::ctlbegin() const noexcept
{ return mTopLevelNodes.data(); }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::iterator NaiveSceneGraphImplementation<KeyT,
NodeDataT, AllocatorTT>::tlend() noexcept
{ return mTopLevelNodes.data() + mTopLevelNodes.size(); }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::const_iterator NaiveSceneGraphImplementation<KeyT,
NodeDataT, AllocatorTT>::tlend() const noexcept
{ return mTopLevelNodes.data() + mTopLevelNodes.size(); }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::const_iterator NaiveSceneGraphImplementation<KeyT,
NodeDataT, AllocatorTT>::ctlend() const noexcept
{ return mTopLevelNodes.data() + mTopLevelNodes.size(); }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::registerNode(pointer newNode, bool addToTopLevel)
{
    // Register the node under its ID.
    if (!mNodeMap.insert(std::make_pair(newNode->userData.id, newNode)).second)
    { // Node was not inserted -> it already existed.
        throw InvalidParameter("Unable to register new node: Node with given ID already exists!");
    }

    // Handle the linked list: 
    pointer next{ NullNodePtr };
    pointer prev{ NullNodePtr };

    if (addToTopLevel)
    {
        if (mLast != NullNodePtr)
        { // If this is not the only node -> put it in the linked list.
            prev = mLast;
            mLast->next = newNode;
        }

        mLast = newNode;
        // Add the node as a new top-level node.
        mTopLevelNodes.push_back(newNode);
    }

    newNode->next = next;
    newNode->prev = prev;
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::unRegisterNode(pointer node)
{
    // Unregister the node using its ID.
    if (mNodeMap.erase(node->userData.id) == 0u)
    { // Node has not been registered before.
        throw InvalidParameter("Unable to un-register node: Node has never been registered!");
    }

    // Remove it from the linked list.
    pointer nextNode{ node->next };
    pointer prevNode{ node->prev };

    if (nextNode != NullNodePtr)
    { // Node is not last -> repair next node.
        nextNode->prev = prevNode;
    }

    if (prevNode != NullNodePtr)
    { // Node is not first -> repair previous node.
        nextNode->next = nextNode;
    }

    if (node == mLast)
    {
        mLast = node->prev;
    }

    if (node->parent == NullNodePtr)
    { // If the removed node was top-level node, remove it.
        eraseTopLevelNode(node);
    }

    for (auto child : node->children)
    { // Move children to the top-level.
        resetParent(child);
    }
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::childArrayAppend(pointer parent, pointer child)
{
    // Keep a pointer to the original, while we make a copy.
    auto origChildArray = parent->children;
    // Make a new child array with a spot for the new child.
    auto newSize{ origChildArray.size() + 1u };
    decltype(origChildArray) newChildArray(allocChildArray(newSize), newSize);

    try
    {
        // Copy over the old data...
#ifdef _MSC_VER
        std::copy(origChildArray.cbegin(), origChildArray.cend(), 
            stdext::make_checked_array_iterator(newChildArray.begin(), newSize));
#else
        std::copy(origChildArray.cbegin(), origChildArray.cend(), 
            newChildArray.begin());
#endif
        // Append the new child.
        newChildArray[newSize - 1u] = child;

        // Use the new child array.
        parent->children = newChildArray;

        // De-allocate the old child array.
        deallocChildArray(origChildArray.begin(), origChildArray.size());
    } catch (...)
    { // If we fail in any way, we need to free the new child array.
        deallocChildArray(newChildArray.begin(), newChildArray.size());
        throw;
    }
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::setParent(pointer parent, pointer child) noexcept
{ child->parent = parent; }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::childArrayRemove(pointer parent, pointer child)
{
    // Look for the child in the parent array.
    auto findIt{ std::find(parent->children.cbegin(), parent->children.cend(), child) };
    if (findIt == parent->children.cend())
    { // Child is not present in the parents children array!
        throw InvalidParameter("Failed to remove child from the child array: Parent node isn't the parent of the provided child!");
    }

    // Keep a pointer to the original, while we make the new one.
    auto origChildArray = parent->children;
    // Make a new child array without the removed child.
    auto newSize{ origChildArray.size() - 1u };
    decltype(origChildArray) newChildArray(allocChildArray(newSize), newSize);

    try
    {
#ifdef _MSC_VER
        // Copy over only children which are not the removed child.
        std::remove_copy(origChildArray.cbegin(), origChildArray.cend(), 
            stdext::make_checked_array_iterator(newChildArray.begin(), newSize), child);
#else
        // Copy over data before the removed child.
        auto it{ std::copy(origChildArray.cbegin(), findIt,
            newChildArray.begin()) };
        // Copy over the rest of the child pointers.
        std::copy(findIt + 1u, origChildArray.cend(), it);
#endif

        // Use the new child array.
        parent->children = newChildArray;

        // De-allocate the old child array.
        deallocChildArray(origChildArray.begin(), origChildArray.size());
    } catch (...)
    { // If we fail in any way, we need to free the new child array.
        deallocChildArray(newChildArray.begin(), newChildArray.size());
        throw;
    }
}

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::childArrayFree(pointer node)
{
    if (!node->children.empty())
    { // Node has a children array, de-allocate it.
        deallocChildArray(node->children.begin(), node->children.size());
    }
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::resetParent(pointer child)
{ child->parent = NullNodePtr; }

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::eraseTopLevelNode(pointer node)
{
    auto findIt{ std::find(mTopLevelNodes.begin(), mTopLevelNodes.end(), node) };
    if (findIt != mTopLevelNodes.end())
    {
        mTopLevelNodes.erase(findIt);
    }
}

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
bool NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::nodesEqual(const_pointer fbegin, 
    const_pointer fend, const_pointer sbegin) const
{
    for (auto it1 = fbegin, it2 = sbegin; it1 != fend; it1 = it1->next, it2 = it2->next)
    {
        if (!((*it1) == (*it2)))
        { return false; }
    }
    return true;
}

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
typename NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::pointer NaiveSceneGraphImplementation<KeyTT,
NodeDataTT, AllocatorTT>::rightMostChild(pointer root) const
{
    auto currentNode{ root };

    while (!currentNode->children.empty())
    {
        // Get the right-most child.
        currentNode = currentNode->children[currentNode->children.size() - 1u];
    }

    return currentNode;
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
bool NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::operator==(
    const NaiveSceneGraphImplementation &other) const noexcept
{ return mAllocator == other.mAllocator && size() == other.size() && nodesEqual(cbegin(), cend(), other.cbegin()); }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
bool NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::operator!=(
    const NaiveSceneGraphImplementation &other) const noexcept
{ return !operator==(other); }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::swap(NaiveSceneGraphImplementation &other) noexcept
{
    using std::swap;
    swap(*this, other);
}

template <typename KeyST, typename NodeDataST, typename AllocatorST>
inline void swap(NaiveSceneGraphImplementation<KeyST, NodeDataST, AllocatorST> &first,
    NaiveSceneGraphImplementation<KeyST, NodeDataST, AllocatorST> &second) noexcept
{
    using std::swap;

    using allocator_traits = typename NaiveSceneGraphImplementation<KeyST, NodeDataST, AllocatorST>::allocator_traits;
    if (allocator_traits::propagate_on_container_swap::value)
    { // We can perform a swap.
        swap(first.mAllocator, second.mAllocator);
        swap(first.mChildAllocator, second.mChildAllocator);
        swap(first.mNodeMap, second.mNodeMap);
        swap(first.mTopLevelNodes, second.mTopLevelNodes);
        swap(first.mLast, second.mLast);
    }
    else
    { // Need to exchange through a third scene graph.
        auto copy = first;
        second = std::move(first);
        first = std::move(copy);
    }
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::size_type NaiveSceneGraphImplementation<KeyT,
NodeDataT, AllocatorTT>::size() const noexcept
{ return mNodeMap.size(); }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
constexpr typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::size_type NaiveSceneGraphImplementation<
KeyT, NodeDataT, AllocatorTT>::max_size() const noexcept
{ return mNodeMap.max_size(); }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
bool NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::empty() const noexcept
{ return mNodeMap.empty(); }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::allocator_type NaiveSceneGraphImplementation<KeyT,
NodeDataT, AllocatorTT>::get_allocator() const
{ return mAllocator; }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::operator bool() const noexcept
{ return !empty(); }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::clear() noexcept
{
    for (auto nodePair : mNodeMap)
    {
        try
        { // We need to finish clearing even when exception occurs.
            deallocNode(nodePair.second);
        } catch (...)
        { }
    }
    mTopLevelNodes.clear();
    mNodeMap.clear();
    mUpDirtyNodes.clear();
    mDownDirtyNodes.clear();
    mLast = NullNodePtr;
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
template <typename ... Ts>
std::pair<typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::pointer, bool>
NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::createNode(const KeyT &key, Ts... nodeArgs)
{
    auto resultNode{ findNode(key) };
    if (resultNode != end())
    { // Node for this key already existed.
        return { resultNode, false };
    }
    // else, we need to create a new one: 
    resultNode = allocNode(key, std::forward<Ts>(nodeArgs)...);
    // Add node to the data structure.
    registerNode(resultNode, true);

    // Return the newly created node.
    return { resultNode, true };
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::destroyNode(pointer node)
{
    // Remove the node from dirty lists.
    resetDirty(node);

    // Remove node from scene graph.
    unRegisterNode(node);

    // Destruct and free memory used by the children array.
    childArrayFree(node);

    // Destruct and free memory used by the node structure.
    deallocNode(node);
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::addChild(pointer parent, pointer child, bool topLevel)
{
    if (child->parent != NullNodePtr)
    { // Child already has a parent
        if (child->parent != parent)
        { // And it is not the one specified.
            throw InvalidParameter("Unable to add a child: child node already has a parent!");
        }
        else
        { // The parent is already correctly set.
            return;
        }
    }
    // else, we need to add the child.

    if (isIndirectParentOf(child, parent))
    { // We cannot add the child node, since a cycle would appear in the graph.
        throw InvalidParameter("Unable to add a child: child node is already the parent of the parent node!");
    }

    // Right-most childs' child.
    const auto rmCsChild{ rightMostChild(child) };

    // Remove the child from linked list.
    if (child->prev)
    { child->prev->next = rmCsChild->next; }
    if (rmCsChild->next)
    { rmCsChild->next->prev = child->prev; }

    // Right-most parents' child.
    const auto rmPsChild{ rightMostChild(parent) };

    // Move it to the linked list at the end of new parents child list.
    if (rmPsChild->next)
    { rmPsChild->next->prev = rmCsChild; }
    rmCsChild->next = rmPsChild->next;
    rmPsChild->next = child;
    child->prev = rmPsChild;

    // Replace the last linked list pointer.
    if (child->next == NullNodePtr)
    {
        mLast = child;
    }

    // Append the new child to the parents child array.
    childArrayAppend(parent, child);
    // Set the correct parent for the child.
    setParent(parent, child);

    if (topLevel)
    { // If the node may be a top-level node, try to remove it.
        eraseTopLevelNode(child);
    }
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::removeChild(pointer parent, pointer child)
{
    if (child->parent != parent)
    { // Parent node is not the parent of the child node.
        throw InvalidParameter("Unable to remove a child: specified node is not the parent of the child node!");
    }
    // else, remove the child node.

    // TODO - Fix next, prev.
    //throw std::exception("TODO - removeChild(...)");

    // Remove the child from the parents child array.
    childArrayRemove(parent, child);
    // Reset the parent pointer for the child node.
    resetParent(child);
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::pointer NaiveSceneGraphImplementation<KeyT,
NodeDataT, AllocatorTT>::findNode(const KeyT &key)
{
    auto findIt{ mNodeMap.find(key) };
    if (findIt != mNodeMap.end())
    { // Node has been found.
        return findIt->second;
    }
    else
    { // No node with given key exists.
        return end();
    }
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::const_pointer NaiveSceneGraphImplementation<KeyT,
NodeDataT, AllocatorTT>::findNode(const KeyT &key) const
{
    auto findIt{ mNodeMap.find(key) };
    if (findIt != mNodeMap.end())
    { // Node has been found.
        return findIt->second;
    }
    else
    { // No node with given key exists.
        return cend();
    }
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::makeParentOf(pointer parent, pointer child)
{
    auto isTopLevel{ child->parent == NullNodePtr };

    if (!isTopLevel && child->parent != parent)
    { // We first need to remove the old parent.
        removeChild(child->parent, child);
    }

    // Finalize the operation, adding child to the new parent.
    addChild(parent, child, isTopLevel);
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
bool NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::isParentOf(const_pointer parent, const_pointer child) const
{ return child->parent == parent; }

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
bool NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::isIndirectParentOf(const_pointer parent,
    const_pointer child) const
{
    auto it{ child };
    // Search through the parent nodes.
    while (it->parent != NullNodePtr && it->parent != parent)
    { it = it->parent; }
    // Check whether we found the specified parent.
    return it->parent == parent;
}

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::setUpDirty(pointer node)
{
    if (findNode(node->userData.id) == end())
    { throw InvalidParameter("Cannot set node UpDirty: Node does not belong to this scene graph!"); }

    mUpDirtyNodes.insert(node);
}

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::resetUpDirty(pointer node)
{ mUpDirtyNodes.erase(node); }

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::setDownDirty(pointer node)
{
    if (findNode(node->userData.id) == end())
    { throw InvalidParameter("Cannot set node DownDirty: Node does not belong to this scene graph!"); }

    mDownDirtyNodes.insert(node);
}

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::resetDownDirty(pointer node)
{ mUpDirtyNodes.erase(node); }

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::resetDirty(pointer node)
{
    resetUpDirty(node);
    resetDownDirty(node);
}

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::propagateDirty()
{
    // TODO - Optimize, don't update multiple times.
    // Minimize the number of nodes in the upDirtyNodes.
    minimizeUpDirty();
    // Perform up propagation.
    for (auto upDirtyNode : mUpDirtyNodes)
    { upPropagate(upDirtyNode); }
    // Reset up dirty nodes.
    mUpDirtyNodes.clear();

    // Minimize the number of nodes in the downDirtyNodes.
    minimizeDownDirty();
    // Perform down propagation.
    for (auto downDirtyNode : mDownDirtyNodes)
    { downPropagate(downDirtyNode); }
    // Reset down dirty nodes.
    mDownDirtyNodes.clear();
}

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::upPropagate(pointer node)
{
    // Initial nodes.
    pointer lastNode{ node };
    pointer parentNode{ lastNode->parent };

    while (parentNode != NullNodePtr) 
    {
        // Propagate!
        upPropagateFrom<NodeDataT::HasUpPropagate>(lastNode, parentNode);

        // Move one up.
        lastNode = parentNode;
        parentNode = lastNode->parent;
    }
}

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
template <bool HasUpPropagate>
typename std::enable_if<HasUpPropagate, void>::type NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::
upPropagateFrom(pointer from, pointer to)
{
    to->userData.data.upPropagate->propagateFrom(
        *from->userData.data.upPropagate);
}

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
template <bool HasUpPropagate>
typename std::enable_if<!HasUpPropagate, void>::type NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::
upPropagateFrom(pointer from, pointer to)
{ /* No propagation is performed */ }

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::downPropagate(pointer node)
{
    // Initial nodes.
    pointer parentNode{ node };
    pointer endNode{ rightMostChild(parentNode)->next };

    while (parentNode != endNode)
    {
        for (auto childNode : parentNode->children)
        { // Go through each parents' children.
            // And propagate...
            downPropagateFrom<NodeDataT::HasDownPropagate>(parentNode, childNode);
        }
        
        // Then, move to the next node.
        parentNode = parentNode->next;
    }
}

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
template <bool HasDownPropagate>
typename std::enable_if<HasDownPropagate, void>::type NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::
downPropagateFrom(pointer from, pointer to)
{
    to->userData.data.downPropagate->propagateFrom(
        *from->userData.data.downPropagate);
}

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
template <bool HasDownPropagate>
typename std::enable_if<!HasDownPropagate, void>::type NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::
downPropagateFrom(pointer from, pointer to)
{ /* No propagation is performed */ }

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::minimizeDownDirty()
{
    // TODO
}

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::minimizeUpDirty()
{
    // TODO
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
template <typename ... Ts>
typename NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::pointer NaiveSceneGraphImplementation<KeyT,
NodeDataT, AllocatorTT>::allocNode(Ts... args)
{
    auto newMem{ allocator_traits::allocate(mAllocator, 1u) };
    try
    { // Attemp to construct the node with provided arguments.
        allocator_traits::construct(mAllocator, newMem, std::forward<Ts>(args)...);
    } catch (...)
    { // Construction failed, deallocate and rethrow...
        allocator_traits::deallocate(mAllocator, newMem, 1u);
        throw;
    }

    return newMem;
}

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
typename NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::child_array_pointer NaiveSceneGraphImplementation<
KeyTT, NodeDataTT, AllocatorTT>::allocChildArray(size_type size)
{
    auto newMem{ child_allocator_traits::allocate(mChildAllocator, size) };
    try
    { // Attemp to default-construct the pointer array.
        // TODO - Is this required by the standard?
        for (size_type iii = 0; iii < size; ++iii)
        {
            child_allocator_traits::construct(mChildAllocator, newMem + iii);
        }
    } catch (...)
    { // Construction failed, deallocate and rethrow...
        child_allocator_traits::deallocate(mChildAllocator, newMem, size);
        throw;
    }

    return newMem;
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::deallocNode(pointer node)
{
    // Destruct the node...
    allocator_traits::destroy(mAllocator, node);
    // and de-allocate.
    allocator_traits::deallocate(mAllocator, node, 1u);
}

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::deallocChildArray(child_array_pointer arr, size_type size)
{
    // Destruct the child pointers...
    // TODO - Is this required by the standard?
    for (size_type iii = 0; iii < size; ++iii)
    {
        child_allocator_traits::destroy(mChildAllocator, arr + iii);
    }
    // and de-allocate.
    child_allocator_traits::deallocate(mChildAllocator, arr, size);
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::copyFrom(const NaiveSceneGraphImplementation &other)
{
    clear();
    if (allocator_traits::propagate_on_container_copy_assignment::value)
    {
        mAllocator = other.mAllocator;
        mChildAllocator = other.mChildAllocator;
    }

    performDeepCopyFrom(other);
}

template <typename KeyT, typename NodeDataT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyT, NodeDataT, AllocatorTT>::performDeepCopyFrom(
    const NaiveSceneGraphImplementation &other)
{
    // TODO - Optimization...
    // TODO - Implement...

    const auto tlBegin{ other.tlbegin() };
    const auto tlEnd{ other.tlend() };
    const auto tlIterable{ Iterable(tlBegin, tlEnd) };

    for (const auto &nodePtr : tlIterable)
    { // For each top-level node.
        deepCopyTree(nodePtr);
    }

    // Copy dirty nodes.
    for (auto upDirtyNode: other.mUpDirtyNodes)
    { setUpDirty(findNode(upDirtyNode->userData.id)); }
    for (auto downDirtyNode: other.mDownDirtyNodes)
    { setDownDirty(findNode(downDirtyNode->userData.id)); }
}

template <typename KeyTT, typename NodeDataTT, typename AllocatorTT>
void NaiveSceneGraphImplementation<KeyTT, NodeDataTT, AllocatorTT>::deepCopyTree(pointer root)
{
    // Allocate the root node: 
    const auto rootCopyCC{ createNode(root->userData.id, root->userData.data) };
    // Assert we actually created the node successfully.
    ASSERT_FAST(rootCopyCC.second);
    const auto rootCopy{ createNode(root->userData.id, root->userData.data).first };
    // TODO - Use correct allocator?
    // List of allocated nodes, which need to be processed.
    std::vector<std::pair<pointer, pointer>> toProcess; 
    toProcess.push_back(std::make_pair(rootCopy, root));

    while (!toProcess.empty())
    {
        // Get the next node to process.
        const auto curNode{ toProcess.back() };
        toProcess.pop_back();

        // Get original and allocated copy node pointers.
        const auto curNodeCopy{ curNode.first };
        const auto curNodeOrig{ curNode.second };

        // Allocate array for children in the copied node.
        const auto numChildren{ curNodeOrig->children.size() };
        //curNodeCopy->children = util::MemoryView<pointer>(allocChildArray(numChildren), numChildren);

        for (auto iii = 0u; iii < numChildren; ++iii)
        { // For each child in the original.
            // Get pointer to the original child.
            const auto childOrig{ curNodeOrig->children[iii] };
            // Copy the node.
            const auto childCopy{ allocNode(childOrig->userData.id, childOrig->userData.data) };
            // Register its ID.
            registerNode(childCopy, false);

            // Pointer setup.
            //childCopy->parent = curNodeCopy;
            //curNodeCopy->children[iii] = childCopy;
            addChild(curNodeCopy, childCopy);
            // TODO - next, prev...

            // Add the child for later processing.
            toProcess.push_back(std::make_pair(childCopy, childOrig));
        }
    }
}

}

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIteratorBase<ConstIter>::reference
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIteratorBase<ConstIter>::operator*()
{ return ptr()->userData; }

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIteratorBase<ConstIter>::const_reference
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIteratorBase<ConstIter>::operator*() const
{ return ptr()->userData; }

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIteratorBase<ConstIter>::pointer SceneGraph
<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIteratorBase<ConstIter>::operator->()
{ return &(operator*()); }

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIteratorBase<ConstIter>::const_pointer
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIteratorBase<ConstIter>::operator->() const
{ return &(operator*()); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
bool SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::NodeIteratorBase<ConstIter>::operator==(
    const NodeIteratorBase &other) const noexcept
{ return mCurrent == other.mCurrent && mSG == other.mSG; }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
bool SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::NodeIteratorBase<ConstIter>::operator!=(
    const NodeIteratorBase &other) const noexcept
{ return !operator==(other); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::NodeIterator<ConstIter> SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::NodeIteratorBase<ConstIter>::chldbegin() const noexcept
{ return NodeIterator<ConstIter>(ptr()->children.begin(), mSG); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::NodeIterator<ConstIter> SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::NodeIteratorBase<ConstIter>::chldend() const noexcept
{ return NodeIterator<ConstIter>(ptr()->children.end(), mSG); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
void SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::NodeIteratorBase<ConstIter>::
setDownDirty() const
{ mSG->setDownDirty(*this); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
void SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::NodeIteratorBase<ConstIter>::
setUpDirty() const
{ mSG->setUpDirty(*this); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::NodeIterator<ConstIter> SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::chldbegin() const noexcept
{ return NodeIterator<ConstIter>(ptr()->children.begin(), mSG); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::NodeIterator<ConstIter> SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::chldend() const noexcept
{ return NodeIterator<ConstIter>(ptr()->children.end(), mSG); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
void SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::
setDownDirty() const
{ mSG->setDownDirty(iterator(mCurrent, mSG)); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
void SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::
setUpDirty() const
{ mSG->setUpDirty(iterator(mCurrent, mSG)); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::NodeIteratorBase<ConstIter>::NodeIteratorBase(
    scene_graph_pointer sg) :
    mSG{ sg }
{ }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::NodeIterator<ConstIter>::NodeIterator(
    typename this_base::scene_graph_pointer sg) :
    this_base(sg)
{ }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::
reference SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::
operator*()
{ return ptr()->userData; }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::
const_reference SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::
operator*() const
{ return ptr()->userData; }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::pointer
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::operator->()
{ return &(ptr()->userData); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::
const_pointer SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::
operator->() const
{ return &(ptr()->userData); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
bool SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::operator==(
    const StructureIteratorBase &other) const noexcept
{ return mCurrent == other.mCurrent && mSG == other.mSG; }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
bool SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::operator!=(
    const StructureIteratorBase &other) const noexcept
{ return !operator==(other); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::
StructureIteratorBase(scene_graph_pointer sg) :
    mSG{ sg }
{ }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
template <bool FromConst>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::
StructureIteratorBase(const StructureIteratorBase<FromConst> &other) :
    mCurrent{ other.mCurrent },
    mSG{ other.mSG }
{ }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
template <bool FromConst>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter> &
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::operator=(
    const StructureIteratorBase<FromConst> &other)
{
    mCurrent = other.mCurrent; 
    mSG = other.mSG;
    return *this;
}

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
template <bool FromConst>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::
StructureIteratorBase(const NodeIteratorBase<FromConst> &other) :
    mCurrent{ other.ptr() },
    mSG{ other.mSG }
{ }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
template <bool FromConst>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter> &
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::operator=(
    const NodeIteratorBase<FromConst> &other)
{
    mCurrent = other.ptr();
    mSG = other.mSG;
    return *this;
}

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::
StructureIteratorBase(node_pointer ptr, scene_graph_pointer sg) :
    mCurrent{ ptr },
    mSG{ sg }
{ }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::
node_pointer SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::StructureIteratorBase<ConstIter>::
ptr() const
{ return mCurrent; }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::ParentIterator<ConstIter>::ParentIterator(
    typename this_base::scene_graph_pointer sg) :
    this_base(sg)
{ }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
template <bool FromConst>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::ParentIterator<ConstIter>::ParentIterator(
    const NodeIteratorBase<FromConst> &other) :
    this_base(other)
{ }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
template <bool FromConst>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::ParentIterator<ConstIter> & SceneGraph<
KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::ParentIterator<ConstIter>::operator=(
    const NodeIteratorBase<FromConst> &other)
{ return this_base::operator=(this, other); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::LinkedIterator(
    typename this_base::scene_graph_pointer sg) :
    this_base(sg)
{ }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
template <bool FromConst>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::LinkedIterator(
    const LinkedIterator<FromConst> &other) :
    this_base(other)
{ }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
template <bool FromConst>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter> & SceneGraph<
KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::operator=(
    const LinkedIterator<FromConst> &other)
{ return this_base::operator=(other); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::operator LinkedIterator
<true>() const
{ return LinkedIterator<true>(*this); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
template <bool FromConst>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::LinkedIterator(
    const NodeIteratorBase<FromConst> &other) :
    this_base(other)
{ }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
template <bool FromConst>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter> & SceneGraph<
KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::operator=(
    const NodeIteratorBase<FromConst> &other)
{ return this_base::operator=(other); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::this_iterator
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::end() noexcept
{ return NullNodePtr; }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::this_iterator
& SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::operator++()
{
    if (this_base::ptr() != NullNodePtr)
    { this_base::mCurrent = this_base::ptr()->next; }
    return *this;
}

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::this_iterator
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::operator++(int)
{
    auto temp = *this;
    ++(*this);
    return temp;
}

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::this_iterator
& SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::operator--()
{
    if (this_base::ptr() != NullNodePtr)
    { this_base::mCurrent = this_base::ptr()->prev; }
    return *this;
}

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::this_iterator
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::operator--(int)
{
    auto temp = *this;
    --(*this);
    return temp;
}

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
bool SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::operator==(
    const this_iterator &other) const
{ return this_base::operator==(other); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
bool SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::operator!=(
    const this_iterator &other) const
{ return this_base::operator!=(other); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::LinkedIterator<ConstIter>::LinkedIterator(
    typename this_base::node_pointer ptr, typename this_base::scene_graph_pointer sg) :
    this_base(ptr, sg)
{ }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::node_iterator SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::tlbegin() noexcept
{ return node_iterator(mImpl.tlbegin(), this); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::const_node_iterator SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::tlbegin() const noexcept
{ return const_node_iterator(mImpl.tlbegin(), this); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::node_iterator SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::tlend() noexcept
{ return node_iterator(mImpl.tlend(), this); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::const_node_iterator SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::tlend() const noexcept
{ return const_node_iterator(mImpl.tlend(), this); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::const_node_iterator SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::ctlbegin() const noexcept
{ return const_node_iterator(mImpl.ctlbegin(), this); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::const_node_iterator SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::ctlend() const noexcept
{ return const_node_iterator(mImpl.ctlend(), this); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
template <bool FromConst>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::NodeIteratorBase<ConstIter>::NodeIteratorBase(
    const NodeIteratorBase<FromConst> &other) :
    mCurrent{ other.mCurrent },
    mSG{ other.mSG }
{ }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
template <bool FromConst>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::NodeIteratorBase<ConstIter> & SceneGraph<
KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::NodeIteratorBase<ConstIter>::operator=(
    const NodeIteratorBase<FromConst> &other)
{
    mCurrent = other.mCurrent;
    mSG = other.mSG;
    return *this;
}

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::SceneGraph(const allocator_type &allocator) : 
    mImpl(allocator)
{ }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::~SceneGraph()
{ }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::SceneGraph(const SceneGraph &other) :
    mImpl(other.mImpl)
{ }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::SceneGraph(SceneGraph &&other) noexcept :
    mImpl(std::move(other.mImpl))
{ }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT> & SceneGraph<KeyTT, DownPropagatePtrTT,
UpPropagatePtrTT, AllocatorTT>::operator=(const SceneGraph &other)
{
    auto copy(other);
    using std::swap;
    swap(*this, copy);
    return *this;
}

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT> & SceneGraph<KeyTT, DownPropagatePtrTT,
UpPropagatePtrTT, AllocatorTT>::operator=(SceneGraph &&other) noexcept
{
    using std::swap;
    swap(*this, other);
    return *this;
}

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::iterator SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::begin() noexcept
{ return iterator(mImpl.begin(), this); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::const_iterator SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::begin() const noexcept
{ return const_iterator(mImpl.begin(), this); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::iterator SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::end() noexcept
{ return iterator(mImpl.end(), this); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::const_iterator SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::end() const noexcept
{ return const_iterator(mImpl.end(), this); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::const_iterator SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::cbegin() const noexcept
{ return const_iterator(mImpl.cbegin(), this); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::const_iterator SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::cend() const noexcept
{ return const_iterator(mImpl.cend(), this); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
bool SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::operator==(
    const SceneGraph &other) const noexcept
{ return mImpl == other.mImpl; }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
bool SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::operator!=(
    const SceneGraph &other) const noexcept
{ return !operator==(other); }

template <typename KTT, typename DPPTT, typename UPPTT, typename ATT>
inline std::ostream &operator<<(std::ostream &out, const SceneGraph<KTT, DPPTT, UPPTT, ATT> &sc)
{
    for (auto it = sc.begin(); it != sc.end(); ++it)
    {
        out << "Node: " << it->id << "; Children = [ ";
        for (auto cit = it.chldbegin(); cit != it.chldend(); ++cit)
        {
            out << cit->id << " ";
        }
        out << " ]\n";
    }

    return out;
}

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
void SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::swap(SceneGraph &other) noexcept
{
    using std::swap;
    swap(*this, other);
}

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
void swap(SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT> &first,
    SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT> &second) noexcept
{
    using std::swap;
    swap(first.mImpl, second.mImpl);
}

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::size_type SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::size() const noexcept
{ return mImpl.size(); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
constexpr typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::size_type SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::max_size() const noexcept
{ return mImpl.max_size(); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
bool SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::empty() const noexcept
{ return mImpl.empty(); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::allocator_type SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::get_allocator() const
{ return mImpl.get_allocator(); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::operator bool() const noexcept
{ return !empty(); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
std::pair<typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::iterator, bool> SceneGraph<
KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::insert(const KeyT &key, DownUpPackT propagation)
{
    auto createNode{ mImpl.createNode(key, propagation) };
    return { iterator(createNode.first, this), createNode.second };
}

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
bool SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::contains(const KeyT &key) const
{ return find(key) != end(); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::iterator SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::find(const KeyT &key)
{ return iterator(mImpl.findNode(key), this); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::const_iterator SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::find(const KeyT &key) const
{ return const_iterator(mImpl.findNode(key), this); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
bool SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::erase(const KeyT &key)
{
    auto node = find(key);
    if (node == end())
    {
        return false;
    }
    
    mImpl.destroyNode(node.ptr());
    return true;
}

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::iterator SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::erase(iterator pos)
{
    mImpl.destroyNode(pos.ptr());
    return ++pos;
}

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
void SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::clear()
{ mImpl.clear(); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
void SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::makeParentOf(iterator parent, iterator child)
{ mImpl.makeParentOf(parent.ptr(), child.ptr()); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
bool SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::isParentOf(const_iterator parent, 
    const_iterator child) const
{ return mImpl.isParentOf(parent.ptr(), child.ptr()); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
bool SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::isIndirectParentOf(const_iterator parent,
    const_iterator child) const
{ return mImpl.isIndirectParentOf(parent.ptr(), child.ptr()); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
void SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::setUpDirty(iterator node)
{ mImpl.setUpDirty(node.ptr()); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
void SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::resetUpDirty(iterator node)
{ mImpl.resetUpDirty(node.ptr()); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
void SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::setDownDirty(iterator node)
{ mImpl.setDownDirty(node.ptr()); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
void SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::resetDownDirty(iterator node)
{ mImpl.resetDownDirty(node.ptr()); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
void SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::resetDirty(iterator node)
{ mImpl.resetDirty(node.ptr()); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
void SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::propagateDirty()
{ mImpl.propagateDirty(); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::iterator SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::createNode(const KeyT &key, DownUpPackT propagation)
{ return mImpl.createNode(key, propagation); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
void SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::destroyNode(iterator node)
{ mImpl.destroyNode(node.ptr()); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
void SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::addChild(iterator parent, iterator child)
{ mImpl.addChild(parent.ptr(), child.ptr()); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
void SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::removeChild(iterator parent, iterator child)
{ mImpl.removeChild(parent.ptr(), child.ptr()); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::iterator SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::findNode(const KeyT &key)
{ return mImpl.findNode(key); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::const_iterator SceneGraph<KeyTT,
DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::findNode(const KeyT &key) const
{ return mImpl.findNode(key); }

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIteratorBase<ConstIter>::NodeIteratorBase(
    node_iterator ptr, scene_graph_pointer sg) :
    mCurrent{ ptr },
    mSG{ sg }
{ }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::NodeIteratorBase<ConstIter>::node_pointer
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::NodeIteratorBase<ConstIter>::ptr() const
{ return mCurrent ? *mCurrent : NullNodePtr; }

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
template <bool FromConst>
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::NodeIterator(
    const NodeIterator<FromConst> &other) :
    this_base(other)
{ }

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
template <bool FromConst>
typename SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter> & SceneGraph<KeyTT,
DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::operator=(const NodeIterator<FromConst> &other)
{
    this_base::mCurrent = other.mCurrent;
    return *this;
}

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::operator NodeIterator
<true>() const
{ return NodeIterator<true>(this_base::mCurrent, this_base::mSG); }

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::template NodeIterator<ConstIter>::this_iterator &
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::operator++()
{
    ++this_base::mCurrent;
    return *this;
}

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::template NodeIterator<ConstIter>::this_iterator &
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::operator--()
{
    --this_base::mCurrent;
    return *this;
}

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::template NodeIterator<ConstIter>::this_iterator
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::operator++(int)
{
    this_iterator it{ *this };
    ++(*this);
    return it;
}

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::template NodeIterator<ConstIter>::this_iterator
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::operator--(int)
{
    this_iterator it{ *this };
    --(*this);
    return it;
}

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::template NodeIterator<ConstIter>::this_iterator
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::operator+(
    const typename this_base::difference_type &n) const
{
    this_iterator it{ *this };
    return it += n;
}

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::template NodeIterator<ConstIter>::this_iterator
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::operator-(
    const typename this_base::difference_type &n) const
{
    this_iterator it{ *this };
    return it -= n;
}

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
bool SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::operator==(
    const this_iterator &other) const
{ return this_base::operator==(other); }

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
bool SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::operator!=(
    const this_iterator &other) const
{ return this_base::operator!=(other); }

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::template NodeIterator<ConstIter>::this_iterator &
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::operator+=(
    const typename this_base::difference_type &n)
{
    this_base::mCurrent += n;
    return *this;
}

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::template NodeIterator<ConstIter>::this_iterator &
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::operator-=(
    const typename this_base::difference_type &n)
{
    this_base::mCurrent -= n;
    return *this;
}

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::template NodeIteratorBase<ConstIter>::difference_type
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::operator-(
    const this_iterator &other) const
{ return this_base::mCurrent - other.mCurrent; }

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::template NodeIteratorBase<ConstIter>::reference
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::operator[](
    const typename this_base::difference_type &i) const
{ return this_base::mCurrent[i]->userData; }

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
bool SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::operator<(
    const this_iterator &other) const
{ return this_base::mCurrent < other.mCurrent; }

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
bool SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::operator<=(
    const this_iterator &other) const
{ return this_base::mCurrent <= other.mCurrent; }

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
bool SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::operator>(
    const this_iterator &other) const
{ return this_base::mCurrent > other.mCurrent; }

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
bool SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::NodeIterator<ConstIter>::operator>=(
    const this_iterator &other) const
{ return this_base::mCurrent >= other.mCurrent; }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::NodeIterator<ConstIter>::NodeIterator(
    typename this_base::node_iterator ptr, typename this_base::scene_graph_pointer sg) :
    this_base(ptr, sg)
{ }

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
template <bool FromConst>
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::ParentIterator<ConstIter>::ParentIterator(
    const ParentIterator<FromConst> &other) :
    this_base(other)
{ }

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
template <bool FromConst>
typename SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::ParentIterator<ConstIter> & SceneGraph<KeyTT,
DownPropagateTT, UpPropagateTT, AllocatorTT>::ParentIterator<ConstIter>::operator=(
    const ParentIterator<FromConst> &other)
{
    this_base::mCurrent = other.mCurrent;
    return *this;
}

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::ParentIterator<ConstIter>::operator ParentIterator
<true>() const
{ return ParentIterator<true>(this_base::mCurrent, this_base::mSG); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::ParentIterator<ConstIter>::this_iterator
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::ParentIterator<ConstIter>::end() noexcept
{ return this_iterator(NullNodePtr, this); }

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::template ParentIterator<ConstIter>::this_iterator &
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::ParentIterator<ConstIter>::operator++()
{
    if (this_base::mCurrent != NullNodePtr)
    { // If we are not already at the end.
        this_base::mCurrent = this_base::mCurrent.parent();
    }
    return *this;
}

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
typename SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::template ParentIterator<ConstIter>::this_iterator
SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::ParentIterator<ConstIter>::operator++(int)
{
    this_iterator it{ *this };
    ++(*this);
    return it;
}

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
bool SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::ParentIterator<ConstIter>::operator==(
    const this_iterator &other) const
{ return this_base::operator==(other); }

template <typename KeyTT, typename DownPropagateTT, typename UpPropagateTT, typename AllocatorTT>
template <bool ConstIter>
bool SceneGraph<KeyTT, DownPropagateTT, UpPropagateTT, AllocatorTT>::ParentIterator<ConstIter>::operator!=(
    const this_iterator &other) const
{ return this_base::operator!=(other); }

template <typename KeyTT, typename DownPropagatePtrTT, typename UpPropagatePtrTT, typename AllocatorTT>
template <bool ConstIter>
SceneGraph<KeyTT, DownPropagatePtrTT, UpPropagatePtrTT, AllocatorTT>::ParentIterator<ConstIter>::ParentIterator(
    typename this_base::node_pointer ptr, typename this_base::scene_graph_pointer sg) :
    this_base(ptr, sg)
{ }

}

// Template implementation end.
