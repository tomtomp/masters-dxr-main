/**
 * @file util/Timer.h
 * @author Tomas Polasek
 * @brief Simple timer.
 */

#pragma once

#include <chrono>

/// Utilities and helpers.
namespace util
{

/// Simple timer.
template <typename ClockT = std::chrono::high_resolution_clock>
class Timer
{
protected:
    using TimePointT = typename ClockT::time_point;
public:
    using Seconds = std::chrono::seconds;
    using Milliseconds = std::chrono::milliseconds;
    using Microseconds = std::chrono::microseconds;
    using Nanoseconds = std::chrono::nanoseconds;

    using SecondsF = std::chrono::duration<float>;
    using MillisecondsF = std::chrono::duration<float, std::milli>;
    using MicrosecondsF = std::chrono::duration<float, std::micro>;
    using NanosecondsF = std::chrono::duration<float, std::nano>;

    /// Number of milliseconds in one second.
    static constexpr float MS_IN_S{ 1000.0 };
    /// How many microseconds are in one millisecond.
    static constexpr float US_IN_MS{ 1000.0 };

    /// Initialize the timer and start it.
    Timer()
    { reset(); }

    /// Get the current time.
    TimePointT now() const
    { return ClockT::now(); }

    /// Start the timer.
    void reset()
    { resetAt(now()); }

    /**
     * Get the elapsed time and reset the timer.
     * @tparam UnitT Unit, in which the time will be returned (e.g. Timer::seconds).
     * @return Elapsed time from the last reset.
     */
    template <typename UnitT>
    typename UnitT::rep elapsedResetI()
    {
        const auto elapsedTime{ elapsed<UnitT>().count() };
        reset();
        return elapsedTime;
    }

    /**
     * Get the elapsed time and reset the timer.
     * @tparam UnitT Unit, in which the time will be returned (e.g. Timer::seconds).
     * @return Elapsed time from the last reset.
     */
    template <typename UnitT>
    UnitT elapsedReset()
    {
        const auto elapsedTime{ elapsed<UnitT>() };
        reset();
        return elapsedTime;
    }

    /**
     * Get how many time elapsed from the start time.
     * @tparam UnitT Unit, in which the time will be returned (eg. Timer::seconds).
     * @return Returns elapsed time in requested units.
     */
    template <typename UnitT>
    UnitT elapsed() const
    { return std::chrono::duration_cast<UnitT>(now() - mStart); }

    /**
     * Convert provided amount of seconds into 
     * milliseconds.
     * @param seconds Number of seconds.
     * @return Returns number of milliseconds 
     * in provided number of seconds.
     */
    static float sToMs(Seconds seconds)
    { return std::chrono::duration<float, std::milli>(seconds).count(); }

    /**
     * Convert provided amount of milliseconds into 
     * seconds.
     * @param milliseconds Number of milliseconds.
     * @return Returns number of seconds in provided 
     * number of milliseconds.
     */
    static float msToS(Milliseconds milliseconds)
    { return std::chrono::duration<float, std::kilo>(milliseconds).count(); }
private:
    /// Time, when the timer started.
    TimePointT mStart;
protected:
    /**
     * Reset the timer, starting at specified time.
     * @param time Starting time point.
     */
    void resetAt(TimePointT time)
    { mStart = time; }
}; // class Timer

/// Timer using high resolution clock.
using HrTimer = Timer<std::chrono::high_resolution_clock>;

/**
 * Base for all timers with tick() function.
 */
class BaseTickTimer
{
public:
    /// Duration type provided to the tick function.
    using TickTimeT = HrTimer::Nanoseconds;

    /// Initialize the inner high resolution timer.
    BaseTickTimer() = default;
    virtual ~BaseTickTimer() = default;

    /**
     * Tick the timer by provided time.
     * @param change Change since the last tick.
     */
    void tickBy(TickTimeT change)
    {
        mElapsed += change;
        tick(mElapsed);
    }

    /**
     * Use provided timer to tick this timer by.
     * This version also resets the provided timer.
     * @tparam TimerTimeT Time type of the timer.
     * @param timer Timer used for getting the time.
     */
    template <typename TimerTimeT>
    void tickResetUsing(Timer<TimerTimeT> &timer)
    { tickBy(timer.elapsedReset<TickTimeT>()); }

    /**
     * Use provided timer to tick this timer by.
     * @tparam TimerTimeT Time type of the timer.
     * @param timer Timer used for getting the time.
     */
    template <typename TimerTimeT>
    void tickUsing(Timer<TimerTimeT> &timer)
    { tickBy(timer.elapsed<TickTimeT>()); }

    /**
     * Reset the elapsed time.
     */
    void reset()
    { mElapsed = TickTimeT(0u); }

    /**
     * How much time has been collected.
     * @tparam UnitT what time unit should be returned.
     * @return Returns elapsed time remaining.
     */
    template <typename UnitT = HrTimer::MillisecondsF>
    UnitT remaining() const
    { return std::chrono::duration_cast<UnitT>(mElapsed); }

    /**
     * How much time has been collected.
     * @tparam UnitT what time unit should be returned.
     * @return Returns elapsed time remaining.
     */
    template <typename UnitT = HrTimer::MillisecondsF>
    typename UnitT::rep remainingI() const
    { return remaining<UnitT>().count(); }

    /**
     * Take given amount of time from the remaining time.
     * @tparam UnitT What time unit should be used.
     * @param time What time should be subtracted from 
     * elapsed time.
     * @return Returns amount of time which is left 
     * after subtraction.
     * @throws If taken time is greater than time 
     * available, std::runtime_error is thrown.
     */
    template <typename UnitT = HrTimer::MillisecondsF>
    UnitT take(UnitT time)
    {
        if (time > mElapsed)
        {
            throw std::runtime_error("Not enough time to take from elapsed time!");
        }

        mElapsed -= std::chrono::duration_cast<TickTimeT>(time);
        return remaining<UnitT>();
    }

    /**
     * Is there enough elapsed time to cover required time?
     * @param required Time duration to check.
     * @return Returns true, if there is enough elapsed time 
     * for given duration.
     */
    template <typename UnitT = HrTimer::MillisecondsF>
    bool has(UnitT required)
    { return std::chrono::duration_cast<TickTimeT>(required) <= mElapsed; }

    /**
     * Attempt to take provided time duration from the 
     * elapsed time.
     * @param time Time duration to take.
     * @return Returns true, if the action was 
     * successful, else returns false.
     */
    template <typename UnitT = HrTimer::MillisecondsF>
    bool tryTake(UnitT time)
    {
        if (has(time))
        { // Enough elapsed time to proceed.
            mElapsed -= std::chrono::duration_cast<TickTimeT>(time);
            return true;
        }

        // Else: 
        return false;
    }

    /**
     * Take given amount of time from the elapsed time.
     * @tparam UnitT What time unit should be used.
     * @param time What time should be subtracted from 
     * elapsed time.
     * @return Returns amount of time which is left 
     * after subtraction.
     * @throws If taken time is greater than time 
     * available, std::runtime_error is thrown.
     */
    template <typename UnitT = HrTimer::MillisecondsF>
    typename UnitT::rep takeI(UnitT time)
    { return take<UnitT>(time).count(); }

    /**
     * Take all remaining elapsed time and reset it to zero.
     * @tparam UnitT What time unit should be used.
     * @return Returns how much time has been taken.
     */
    template <typename UnitT = HrTimer::MillisecondsF>
    UnitT takeAll()
    {
        const auto tmp{ mElapsed };
        mElapsed = TickTimeT(0u);

        return std::chrono::duration_cast<UnitT>(tmp);
    }

    /**
     * Take all remaining elapsed time and reset it to zero.
     * @tparam UnitT What time unit should be used.
     * @return Returns how much time has been taken.
     */
    template <typename UnitT = HrTimer::MillisecondsF>
    typename UnitT::rep takeAllI()
    { return takeAll<UnitT>().count(); }
private:
    /// How much has has elapsed since reset.
    TickTimeT mElapsed{ 0u };
protected:
    /**
     * Tick the timer, performing timing functions.
     * This function should be periodically called 
     * so that the function is triggered as precisely 
     * to the target time as possible.
     * @param elapsed How much time elapsed since
     */
    virtual void tick(TickTimeT elapsed) = 0;
}; // class BaseTickTimer

/**
 * Timer executing a lambda when time runs out.
 * The function is called once, even when the time 
 * may have ran out several times. After that, the 
 * timer is reset.
 * 
 * Should be used along with TimerTicker or other 
 * timer, since tickBy function requires time specified.
 */
class TickTimer : public BaseTickTimer
{
public:
    /// Type of the function called.
    using TickFunT = std::function<void()>;

    /**
     * Initialize tick timer which will call provided 
     * function once every given duration. In order to 
     * work, the tick function must be periodically called.
     */
    template <typename UnitT>
    TickTimer(UnitT onceEvery, TickFunT fun) :
        BaseTickTimer(), mFun{ fun }, mOnceEvery{ std::chrono::duration_cast<TickTimeT>(onceEvery) }
    { }
    virtual ~TickTimer() = default;
private:
    /// Callback function.
    TickFunT mFun;
    /// Once how much time should the function be called.
    TickTimeT mOnceEvery;
protected:
    /**
     * Tick the timer. Checks the time and if the specified 
     * time has elapsed, calls the function.
     * After that, the timer is reset - if the time has 
     * elapsed multiple times, the function is still 
     * called only once!
     */
    virtual void tick(TickTimeT elapsed) override final
    {
        if (elapsed >= mOnceEvery)
        { // Enought time to run function.
            reset();
            mFun();
        }
    }
}; // class TickTimer

/**
 * Timer executing a lambda when time runs out.
 * The function is called once, even when the time 
 * may have ran out several times. The user function 
 * is provided with how much time has actually elapsed.
 * 
 * Should be used along with TimerTicker or other 
 * timer, since tickBy function requires time specified.
 * 
 * @tparam TUnitT Time unit provided to the user 
 * function.
 */
template <typename TUnitT>
class TickDeltaTimer : public BaseTickTimer
{
public:
    /// Type of the function called.
    using TickFunT = std::function<void(TUnitT elapsed)>;

    /**
     * Initialize tick timer which will call provided 
     * function once every given duration. In order to 
     * work, the tick function must be periodically called.
     */
    template <typename UnitT>
    TickDeltaTimer(UnitT onceEvery, TickFunT fun) :
        BaseTickTimer(), mFun{ fun }, mOnceEvery{ std::chrono::duration_cast<TickTimeT>(onceEvery) }
    { }
    virtual ~TickDeltaTimer() = default;
private:
    /// Callback function.
    TickFunT mFun;
    /// Once how much time should the function be called.
    TickTimeT mOnceEvery;
protected:
    /**
     * Tick the timer. Checks the time and if the specified 
     * time has elapsed, calls the function.
     * After that, the timer is reset - if the time has 
     * elapsed multiple times, the function is still 
     * called only once! The user function is provided 
     * with how much time has elapsed.
     */
    virtual void tick(TickTimeT elapsed) override final
    {
        if (elapsed >= mOnceEvery)
        { // Enough time has passed to perform the operation.
            reset();
            mFun(std::chrono::duration_cast<TUnitT>(elapsed));
        }
    }
}; // class TickDeltaTimer

/**
 * Timer used for measuring time until action 
 * should be taken.
 * Can be used by itself as it contains a timer.
 */
class DeltaTimer : public BaseTickTimer
{
public:
    /// Initialize and start the timer.
    DeltaTimer() = default;

    /**
     * Tick the timer, resetting it and remembering time change.
     * @tparam UnitT What time unit should be returned.
     * @return Returns elapsed time remaining.
     */
    template <typename UnitT = HrTimer::MillisecondsF>
    UnitT tick()
    {
        tickBy(mTimer.elapsed<TickTimeT>());
        return remaining<UnitT>();
    }

    /**
     * Tick the timer, resetting it and remembering time change.
     * @tparam UnitT What time unit should be returned.
     * @return Returns elapsed time remaining.
     */
    template <typename UnitT = HrTimer::MillisecondsF>
    typename UnitT::rep tickI()
    { return tick<UnitT>().count(); }
private:
    /// Timer used for ticking.
    HrTimer mTimer;
protected:
    /// Only need to reset the internal timer.
    virtual void tick(TickTimeT elapsed) override final
    { mTimer.reset(); }
}; // class DeltaTimer.

/**
 * Class used for ticking over many timers at the 
 * same time in sychronized manner.
 */
class TimerTicker
{
public:
    /**
     * Initialize ticker with empty timer list.
     */
    TimerTicker() = default;

    /**
     * Add timer while checking that it is not 
     * already present.
     * @param timer Timer to add.
     * @throws Throws std::runtime_error if the 
     * timer is already present in this ticker.
     */
    void addTimer(BaseTickTimer &timer)
    {
        checkNotPresent(timer);
        addTimerNoCheck(timer);
    }

    /**
     * Add timer without checking for its presence
     * @param timer Timer to add.
     */
    void addTimerNoCheck(BaseTickTimer &timer)
    { mTickTimers.emplace_back(&timer); }

    /**
     * Check for presence of provided timer.
     * @param timer Timer to check for.
     * @throw Throws std::runtime_error if the 
     * timer has been found.
     */
    void checkNotPresent(const BaseTickTimer &timer) const
    {
        if (timerPresent(timer))
        { // Found the timer.
            throw std::runtime_error("Specified timer is already present in this ticker!");
        }
    }

    /**
     * Check for presence of provided timer.
     * @param timer Timer to check for.
     * @return Returns true if the timer is already 
     * present in this ticker.
     */
    bool timerPresent(const BaseTickTimer &timer) const
    {
        const auto findIt{ std::find_if(mTickTimers.begin(), mTickTimers.end(),[&timer](auto t) { return t == &timer; }) };
        return findIt != mTickTimers.end();
    }

    /**
     * Reset the inner timer and all of the tick timers.
     * Also moves the internal timer to the current time.
     */
    void reset()
    {
        for (auto &tickTimer : mTickTimers)
        {
            tickTimer->reset();
        }
        mTimer.reset();
    }

    /**
     * Tick all of the tick timers and move the internal 
     * timer to the current time.
     */
    void tick()
    {
        const auto elapsed{ mTimer.elapsedReset<BaseTickTimer::TickTimeT>() };
        for (auto &tickTimer : mTickTimers)
        {
            tickTimer->tickBy(elapsed);
        }
    }
private:
    /// List of timers in order of ticking.
    std::vector<BaseTickTimer*> mTickTimers;
    /// Timer used for getting the time.
    HrTimer mTimer;
protected:
}; // class TimerTicker

} // namespace util
