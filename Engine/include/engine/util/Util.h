/**
* @file util/Util.h
* @author Tomas Polasek
* @brief Utilities and helper functions/classes.
*/

#pragma once

#include <string>
#include <optional>

#include "engine/util/SafeWindows.h"

/// Utilities and helpers.
namespace util
{

/**
 * Transforming printf format and parameters into std::string.
 * Inspired by https://stackoverflow.com/questions/69738/c-how-to-get-fprintf-results-as-a-stdstring-w-o-sprintf#69911
 * @param fmt Format used by printf.
 * @param ... Parameters used in the formatting string.
 */
std::string format(const char *fmt, ...);

/**
 * Transforming printf format and parameters into std::string.
 * Inspired by https://stackoverflow.com/questions/69738/c-how-to-get-fprintf-results-as-a-stdstring-w-o-sprintf#69911
 * @param fmt Format used by printf.
 * @param ap Parameters used in the formatting string.
 */
std::string vFormat(const char *fmt, va_list ap);

/// Get number of elements of given array.
template <typename T, std::size_t N>
static constexpr std::size_t count(const T (&arr)[N])
{ return N; }

/**
 * Get windows error as a string message.
 * Source: https://stackoverflow.com/a/17387176 .
 * @param error Error to transform into a string.
 * @return Returns string representation of provided error code.
 */
std::string getErrorAsString(DWORD error);

/**
 * Get windows error as a wide string message.
 * Source: https://stackoverflow.com/a/17387176 .
 * @param error Error to transform into a string.
 * @return Returns string representation of provided error code.
 */
std::wstring getErrorAsWString(DWORD error);

/**
 * Attempt to convert wide string into string.
 * @param original Original wide string.
 * @return Returns original string converted into a string.
 */
std::string toString(const std::wstring &original);

/**
 * Convert given string to wide character string..
 * @param original Original string.
 * @return Returns original string converted into a wide string.
 */
std::wstring toWString(const std::string &original);

/**
 * Get last windows error as a string message.
 * @return Returns string representation of the ::GetLastError().
 */
static std::string getLastErrorAsString()
{ return getErrorAsString(::GetLastError()); }

/**
 * Get last windows error as a wstring message.
 * @return Returns string representation of the ::GetLastError().
 */
static std::wstring getLastErrorAsWString()
{ return getErrorAsWString(::GetLastError()); }

template <typename T>
struct Point2
{
    Point2() = default;
    ~Point2() = default;

    Point2(const T &val) :
        x{ val }, y{ val }
    { }
    Point2(const T &xx, const T&yy) :
        x{ xx }, y{ yy }
    { }

    Point2(const Point2 &other) = default;
    Point2(Point2 &&other) = default;
    Point2 &operator=(const Point2 &other) = default;
    Point2 &operator=(Point2 &&other) = default;

    T x{ };
    T y{ };
}; // struct Point2

template <typename T>
std::ostream &operator<<(std::ostream &out, const Point2<T> &p)
{
    out << p.x << " " << p.y;
    return out;
}

/**
 * Color holder, where colour are stored 
 * in contiguous array, ordered RGBA.
 * @tparam FT Which type should be used 
 * for color values.
 */
template <typename FT>
struct ColorRGBA
{
    /// Number of color channels.
    static constexpr uint32_t NUM_CHANNELS{ 4u };
    /// Create black, opaque color.
    constexpr ColorRGBA() = default;

    /// Create specified color
    constexpr ColorRGBA(FT red, FT green, FT blue, FT alpha)
    {
        r = red;
        g = green;
        b = blue;
        a = alpha;
    }

    /// Create specified RGB color, which is fully opaque.
    constexpr ColorRGBA(FT red, FT green, FT blue) :
        ColorRGBA(red, green, blue, 1.0f)
    { }

    /// Contiguous array of colors.
    std::array<FT, NUM_CHANNELS> channels{ 0.0f, 0.0f, 0.0f, 1.0f };

    /// Red channel.
    FT &r{ channels[0] };
    /// Green channel.
    FT &g{ channels[1] };
    /// Blue channel.
    FT &b{ channels[2] };
    /// Alpha channel.
    FT &a{ channels[3] };
}; // struct ColorRGBA

using ColorRGBAF = ColorRGBA<float>;

/**
 * Singleton base class using CRTP.
 */
template <typename T>
class Singleton
{
public:
    Singleton(const Singleton &) = delete;
    Singleton(Singleton &&) = delete;
    Singleton &operator=(const Singleton &) = delete;
    Singleton &operator=(Singleton &&) = delete;

    static T &get()
    {
        static T instance;
        return instance;
    }
private:
protected:
    Singleton() = default;
    ~Singleton() = default;
}; // class Singleton

/**
 * Runtime chooser between two objects.
 * @tparam FirstT Type of the first object.
 * @tparam SecondT Type of the second object.
 * @tparam AsT As what type should these 
 * objects be accessed?
 */
template <typename FirstT, typename SecondT, typename AsT>
struct RuntimeChooser
{
    static_assert(std::is_base_of_v<AsT, FirstT> && std::is_base_of_v<AsT, SecondT>, 
        "Both types must have the target type as a base!");

    /**
     * Initialize an object, based on 
     * the provided flags.
     * @param initFirst When set to 
     * true, the first object will 
     * be initialized. Else the second 
     * object will be initialized.
     */
    explicit RuntimeChooser(bool initFirst)
    {
        if (initFirst)
        {
            mFirst.emplace();
        }
        else
        {
            mSecond.emplace();
        }
    }

    /// Get reference to the chosen object.
    AsT &ref()
    { return ((mFirst.has_value()) ? static_cast<AsT&>(mFirst.value()) : static_cast<AsT&>(mSecond.value())); }

    /// Get reference to the chosen object.
    const AsT &ref() const
    { return ((mFirst.has_value()) ? static_cast<const AsT&>(mFirst.value()) : static_cast<const AsT&>(mSecond.value())); }

    /// Get pointer to the chosen object.
    AsT *ptr()
    { return &ref(); }

    /// Get pointer to the chosen object.
    const AsT *ptr() const
    { return &ref(); }

    /// Access the chosen object.
    AsT &operator*()
    { return ref(); }

    /// Access the chosen object.
    const AsT &operator*() const
    { return ref(); }

    /// Access the chosen object.
    AsT *operator->() 
    { return ptr(); }

    /// Access the chosen object.
    const AsT *operator->() const
    { return ptr(); }
private:
    /// Object of the first type.
    std::optional<FirstT> mFirst;
    /// Object of the second type.
    std::optional<SecondT> mSecond;
}; // struct RuntimeChooser

/**
 * Minimal wrapper for automatic ranged loop.
 * @tparam ItT Type of the iterator.
 */
template <typename ItT>
class Iterable
{
public:
    /**
     * Iterable object over <beginIt, endIt).
     * @param beginIt Beginning iterator.
     * @param endIt Ending iterator.
     */
    Iterable(ItT beginIt, ItT endIt) :
        mBegin{ beginIt }, mEnd{ endIt }
    { }

    /// Get begin iterator.
    ItT begin()
    { return mBegin; }
    /// Get begin iterator.
    ItT begin() const
    { return mBegin; }

    /// Get end iterator.
    ItT end()
    { return mEnd; }
    /// Get end iterator.
    ItT end() const
    { return mEnd; }
private:
    /// Beginning iterator.
    ItT mBegin;
    /// Ending iterator.
    ItT mEnd;
protected:
}; // Iterable

} // namespace util
