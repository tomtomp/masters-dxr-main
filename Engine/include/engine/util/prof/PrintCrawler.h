/**
* @file util/prof/PrintCrawler.h
* @author Tomas Polasek
* @brief Printing class for the profiler.
*/

#pragma once

#include "engine/util/prof/Profiler.h"
#include "engine/util/logging/Logging.h"

/// Utilities and helpers.
namespace util
{

/// Profiler and tools used by it.
namespace prof
{

/// Crawler who prints information about the call stack.
class PrintCrawler : public CallStackCrawler
{
public:
    /**
     * Function is called by the ProfilingManager.
     * @param root The root node of the profiling manager.
     */
    virtual void crawl(prof::ThreadNode *root) override
    {
        auto &mgr = ProfilingManager::instance();

        // Print stats
        log<Prof>() << "Profiling statistics: "
            "\n\tScopes entered : " << mgr.getNumScopeEnter() <<
            "\n\tScopes exited : " << mgr.getNumScopeExit() <<
            "\n\tOverhead per scope [ticks] : " << mgr.getScopeOverhead() <<
            "\n\tThreads entered : " << mgr.getNumThreadEnter() <<
            "\n\tThreads exited : " << mgr.getNumThreadExit() <<
            "\n\t============================" <<
            "\n\t\tInclCh[Mt] Parent[%] Samples[num] AvgPS[Mt] Self[Mt]  Self[%] Name" << 
            std::endl;

        ForeachPrintThread threadPrinter;
        root->foreachnn(threadPrinter);

        lognp<Prof>() << "\n\t============================" << std::endl;
    }
private:
    class ForeachPrintThread
    {
    public:
        void operator()(prof::ThreadNode *node) 
        {
            lognp<Prof>() << "\t" << node->name() << ":" << std::endl;

            prof::ForeachPrint printer;

            {
#ifdef PROFILE_LOCK
                std::lock_guard l(node->data().getLock());
#endif
                printer(node->data().getRoot());
            }
        }
    private:
    protected:
    };
protected:
}; // class PrintCrawler

} // namespace prof

} // namespace util
