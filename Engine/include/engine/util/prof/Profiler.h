/**
 * @file util/prof/Profiler.h
 * @author Tomas Polasek
 * @brief Code profiler.
 */

#pragma once

#include "engine/util/Asserts.h"
#include "engine/util/Threading.h"
#include "engine/util/prof/HashNode.h"

#include <atomic>
#include <thread>
#include <limits>

#ifdef _WIN32
#	include <intrin.h>
#endif

// Profiling macros:
#ifndef NPROFILE
#   define PROF_CONCAT(FIRST, SECOND) \
        FIRST ## SECOND
#   define PROF_NAME(FIRST, SECOND) \
        PROF_CONCAT(FIRST, SECOND)
#   define PROF_BLOCK(NAME) \
        ::util::prof::BlockProfiler PROF_NAME(_bp, __LINE__)(NAME)
#   define PROF_BLOCK_END() \
        ::util::prof::BlockProfiler::end()
#   define PROF_SCOPE(NAME) \
        ::util::prof::ScopeProfiler PROF_NAME(_sp, __LINE__)(NAME)
#   define PROF_DUMP(CRAWLER) \
        ::util::prof::ProfilingManager::instance().useCrawler(CRAWLER)
#   define PROF_THREAD(NAME) \
        ::util::prof::ThreadProfiler PROF_NAME(_tp, __LINE__)(NAME)
#else
#   define PROF_CONCAT(F, S)
#   define PROF_NAME(F, S)
#   define PROF_BLOCK(_)
#   define PROF_BLOCK_END(_)
#   define PROF_SCOPE(_)
#   define PROF_DUMP(_)
#   define PROF_THREAD(_)
#endif

/// Utilities and helpers.
namespace util
{

/// Profiler and tools used by it.
namespace prof
{

/**
 * Timer class, using rdtsc instruction.
 */
class Timer
{
public:
    static constexpr double TICKS_TO_MEGACYCLES{ 1000000.0 };

    Timer()
    { reset(); }

    /**
     * Reset this timer.
     */
    void reset()
    {
        mNumSamples = 0;
        mRunning = false;
        mStart = 0;
        mTicks = 0;
    }

    /**
     * Get the current number of ticks taken by active timer.
     * Returns 0, if the timer is not active.
     * @return Returns the actual number of ticks from active timer.
     */
    uint64_t runningTicks() const
    {
        if (mRunning)
        {
            return getTicks() - mStart;
        }

        return 0;
    }

    /**
     * Get the current number of megacycles from running timer.
     * Returns 0.0, if the timer is not running.
     * @return Returns the number of megacycles, from the start of this timer.
     */
    double runningMegacycles() const
    { return ticksToMegacycles(runningTicks()); }

    /**
     * Convert ticks to megacycles.
     * @param ticks Number of ticks.
     * @return Number of megacycles.
     */
    static double ticksToMegacycles(uint64_t ticks)
    { return ticks / TICKS_TO_MEGACYCLES; }

    /**
     * Convert megacycles to ticks.
     * @param megacycles Amount of megacycles.
     * @return Amount of ticks.
     */
    static uint64_t megacyclesToTicks(double megacycles)
    { return static_cast<uint64_t>(megacycles * TICKS_TO_MEGACYCLES); }

    /**
     * Add given number of ticks to the internal counter.
     * @param ticks Number of ticks to add.
     */
    void addTicks(uint64_t ticks)
    { mTicks += ticks; }

    /**
     * Start the timer.
     */
    void start()
    {
        mNumSamples++;
        mRunning = true;
        mStart = getTicks();
    }

    /**
     * Stop the timer and calculate the number of ticks
     * passed between now and start.
     */
    void stop()
    {
        mTicks += (getTicks() - mStart);
        mRunning = false;
    }

    /**
     * Stop the timer, only if it is running.
     */
    void condStop()
    {
        if (mRunning)
        {
            stop();
        }
    }

    /**
     * Get the number of ticks counted by this timer.
     * @return The number of ticks.
     */
    uint64_t ticks() const
    { return mTicks; }

    /**
     * Get the actual number of ticks.
     * If the timer is currently running, count the ticks
     * from the start.
     * @return The actual number of ticks.
     */
    uint64_t actualTicks() const
    {
        if (mRunning)
        {
            return mTicks + (getTicks() - mStart);
        }
        else
            return mTicks;
    }

    /**
     * Set the number of megacycles to given number.
     * @param megacycles Number of megacycles.
     */
    void setMegacycles(double megacycles)
    { mTicks = megacyclesToTicks(megacycles); }

    /**
     * Get the number of megacycles counted by this timer.
     * @return Returns the number of ticks divided by 10^6
     */
    double megacycles() const
    { return ticksToMegacycles(mTicks); }

    /**
     * Get the actual number of megacycles counted by this timer.
     * @return Returns the actual number of ticks divided by 10^6
     */
    double actualMegacycles() const
    { return ticksToMegacycles(actualTicks()); }

    /**
     * Get the average number of megacycles.
     * @return Get the average number of megacycles per sample
     */
    double avg() const
    { return megacycles() / mNumSamples; }

    /**
     * Get the average number of megacycles.
     * @return Get the average number of megacycles per sample
     */
    double actualAvg() const
    { return actualMegacycles() / mNumSamples; }

    /**
     * Get the average number of ticks.
     * @return Get the average number of ticks per sample
     */
    double avgTicks() const
    { return static_cast<double>(mTicks) / mNumSamples; }

    /**
     * Get the number of samples taken by this timer.
     */
    uint64_t numSamples() const
    { return mNumSamples; }

    /**
     * Get the actual number of samples, including the running one.
     */
    uint64_t actualNumSamples() const
    { return mNumSamples; }

    /**
     * Get current number of ticks passed since reset.
     * Uses rdtsc instruction.
    * @return The number of ticks passed.
    */
    static uint64_t getTicks()
    {
#ifdef _WIN32
        return __rdtsc();
#else
#if 1
        u32 lo;
        u32 hi;
        __asm__ __volatile__("rdtsc" : "=a" (lo), "=d" (hi));
        return ((u64)hi << 32) | lo;
#elif 0
        timespec t;
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t);
        return static_cast<u64>(t.tv_nsec);
#else
        return
            std::chrono::duration_cast<std::chrono::nanoseconds>
            (std::chrono::high_resolution_clock::now().time_since_epoch()).count();
#endif
#endif
    }
private:
protected:
    /// Start time in ticks.
    uint64_t mStart{ 0 };
    /// Current time passed in ticks.
    uint64_t mTicks{ 0 };
    /// Number of samples taken with this timer.
    uint64_t mNumSamples{ 0 };
    /// Is the timer currently running?
    bool mRunning{ false };
}; // class Timer

/**
 * Scope timer. Uses already existing Timer to measure ticks taken
 * for a single scope.
 */
class ScopeTimer
{
public:
    explicit ScopeTimer(Timer &timer) :
        mTimer(timer)
    { mTimer.start(); }

    ScopeTimer(const ScopeTimer &other) = delete;
    ScopeTimer(ScopeTimer &&other) = delete;
    ScopeTimer &operator=(const ScopeTimer &rhs) = delete;
    ScopeTimer &operator=(ScopeTimer &&rhs) = delete;

    ~ScopeTimer()
    { mTimer.stop(); }
private:
protected:
    /// Holds reference to the used timer object.
    Timer &mTimer;
}; // class ScopeTimer

/**
 * Measurement data for each node.
 */
struct CallData
{
public:
    /// Start the timer.
    void startTimer()
    { mTimer.start(); }

    /// Stop the timer.
    void stopTimer()
    { mTimer.stop(); }

    /// Get the current megacycles from the timer.
    double getActualMegacycles() const
    { return mTimer.actualMegacycles(); }

    /// Get the current average megacycles per run.
    double getActualAvg() const
    { return mTimer.actualAvg(); }

    /// Get the number of samples taken by the timer.
    uint64_t getActualNumSamples() const
    { return mTimer.actualNumSamples(); }

    /// Add given number of ticks to the timer.
    void addTicks(uint64_t ticks)
    { mTimer.addTicks(ticks); }
private:
protected:
    /// Timer used for timing this node.
    Timer mTimer;
}; // class CallData

using CallNode = util::HashNode<CallData>;

/**
 * Data for each thread name.
 */
struct ThreadData
{
public:
    ThreadData() :
        mRoot(new CallNode("/", nullptr))
    { }

    ThreadData(const ThreadData &other) = delete;
    ThreadData(ThreadData &&other) = delete;
    ThreadData &operator=(const ThreadData &rhs) = delete;
    ThreadData &operator=(ThreadData &&rhs) = delete;

    ~ThreadData()
    { delete mRoot; }

    /**
     * Try to occupy this thread call stack.
     * @return Returns true, if the occupation succeeded, else
     *  returns false.
     */
    bool occupy()
    { return mOccupied.tryLock(); }

    /**
     * Free this call stack.
     */
    void freeOccupation()
    { mOccupied.unlock(); }

    /// Get lock for this thread.
    std::mutex &getLock()
    { return mLock; }

    /// Get the root of threads call stack.
    CallNode *getRoot()
    { return mRoot; }
private:
protected:
    /// Used for locking the call stack.
    std::mutex mLock;

    /// Root node of the call stack.
    CallNode *mRoot;

    /// Only one thread is allowed in each thread call stack.
    thread::SpinLock mOccupied;
}; // class ThreadData

using ThreadNode = util::HashNode<ThreadData>;

/**
 * Print information about each node.
 */
class ForeachPrint
{
public:
    /// Initialize printer parameters.
    ForeachPrint(uint32_t indent = 0, double parentMegacycles = 0);

    /// Run the printer.
    void operator()(CallNode *node) const;
protected:
    /// Level of indentation.
    uint32_t mIndentLevel{ 0 };
    /// Megacycles of parent node.
    double mParentMegacycles{ 0.0 };
}; // class ForeachPrint

/**
 * Sum megacycles of all children
 */
class ForeachSumMegacycles
{
public:
    /// Run the summer.
    void operator()(CallNode *node);

    /**
     * Sum getter.
     * @return Returns the sum.
     */
    double sum() const
    { return mSum; }

    /**
     * Number of samples getter.
     * @return Number of samples.
     */
    uint64_t numSamples() const
    { return mNumSamples; }
protected:
    /// Sum of megacycles.
    double mSum{ 0.0 };
    /// Number of samples taken.
    uint64_t mNumSamples{ 0 };
}; // class ForeachSumMegacycles

/**
 * Contains information about threads call stack.
 */
class ThreadStatus
{
public:
    /// Ptr to the current node in the call stack.
    //CallNode *mCurNode{nullptr};
    CallNode *mCurNode;
    /// Ptr to the root node of the call stack.
    //CallNode *mRoot{nullptr};
    CallNode *mRoot;
    /// Ptr to the current thread node.
    //ThreadNode *mThreadNode{nullptr};
    ThreadNode *mThreadNode;
private:
protected:
}; // class ThreadStatus

/**
 * Abstract call stack crawler.
 */
class CallStackCrawler
{
public:
    /**
     * Function is called by the ProfilingManager.
     * @param root The root node of the profiling manager.
     */
    virtual void crawl(ThreadNode *root) = 0;

    CallStackCrawler() = default;

    CallStackCrawler(const CallStackCrawler &other) = delete;
    CallStackCrawler(CallStackCrawler &&other) = delete;
    CallStackCrawler &operator=(const CallStackCrawler &other) = delete;
    CallStackCrawler &operator=(CallStackCrawler &&other) = delete;

    virtual ~CallStackCrawler() = default;
private:
protected:
}; // class CallStackCrawler

/**
 * Profiling manager keeps information about each thread
 * and its location in the call stack.
 * It also contains the interface to all profiling functions.
 */
class ProfilingManager
{
public:
    /// Initialize profiling structures.
    ProfilingManager();

    ProfilingManager(const ProfilingManager &other) = delete;
    ProfilingManager(ProfilingManager &&other) = delete;
    ProfilingManager &operator=(const ProfilingManager &other) = delete;
    ProfilingManager &operator=(ProfilingManager &&other) = delete;

    /// Free resources and clean up.
    ~ProfilingManager();

    /**
     * Enter a scope with given name.
     * @param name Identifier of the scope.
     * @return Returns ptr to the scope node.
     */
    CallNode *enterScope(const char* name);

    /**
     * Exit the current scope and go to the
     * parent scope.
     * If there is no parent scope, returns nullptr.
     * @return Returns ptr to the parent, or nullptr.
     */
    CallNode *exitScope();

    /**
     * Enter a thread with given name.
     * @param name Identifier of the thread.
     * @return Returns ptr to the thread node.
     */
    ThreadNode *enterThread(const char* name);

    /**
     * Exit the current thread.
     */
    void exitThread();

    /**
     * Get the current scope node.
     * @return Returns ptr to the current node,
     * should never return nullptr.
     */
    CallNode *getCurrentScope();

    /**
     * Use given crawler object to crawl through
     * the call stack.
     * @param crawler Reference to the crawler object.
     */
    void useCrawler(CallStackCrawler &crawler);

    /**
     * Get the overhead of entering and exiting scope.
     * @return The overhead.
     */
    double getScopeOverhead() const
    { return mScopeOverhead; }

    // Statistics getters
    uint64_t getNumScopeEnter() const
    { return mNumScopeEnter; }

    uint64_t getNumScopeExit() const
    { return mNumScopeExit; }

    uint64_t getNumThreadEnter() const
    { return mNumThreadEnter; }

    uint64_t getNumThreadExit() const
    { return mNumThreadExit; }

    /**
     * Get instance of the profiling manager.
     * @return Singleton instance of the profiling manager.
     */
    static ProfilingManager &instance();
private:
    void reset();
protected:
    /// Root node of the profiling tree.
    ThreadNode * mRoot{ nullptr };
    /// Global lock for adding threads to the ProfilingManager.
    thread::SpinLock mGlobalLock;
    /// Thread local storage containing call stack information.
    thread_local static ThreadStatus mThreadStatus;
    // Statistics
    uint64_t mNumScopeEnter{ 0u };
    uint64_t mNumScopeExit{ 0u };
    uint64_t mNumThreadEnter{ 0u };
    uint64_t mNumThreadExit{ 0u };
    /// Overhead per enter/exit scope.
    double mScopeOverhead{ 0.0 };

    /// Used for checking, that only one construction occurred.
    static uint64_t sConstructions;
}; // class ProfilingManager

/// Profiler for a single scope.
class ScopeProfiler
{
public:
    /**
     * Create a scope profiler, add it to the global structure
     * (if necessary) and start the timer.
     * @param desc Description for this profiler.
     */
    ScopeProfiler(const char *desc);

    ScopeProfiler(const ScopeProfiler &other) = delete;
    ScopeProfiler(ScopeProfiler &&other) = delete;
    ScopeProfiler &operator=(const ScopeProfiler &other) = delete;
    ScopeProfiler &operator=(ScopeProfiler &&other) = delete;

    /**
     * Exit the scope and stop the timer.
     */
    ~ScopeProfiler();
private:
protected:
}; // class ScopeProfiler

/// Profiler for a single code block.
class BlockProfiler
{
public:
    /**
     * Create a block profiler, add it to the global structure
     * (if necessary) and start the timer.
     * @param desc Description for this profiler.
     */
    BlockProfiler(const char *desc);

    BlockProfiler(const BlockProfiler &other) = delete;
    BlockProfiler(BlockProfiler &&other) = delete;
    BlockProfiler &operator=(const BlockProfiler &other) = delete;
    BlockProfiler &operator=(BlockProfiler &&other) = delete;

    /**
     * Does NOT complete the profiling block!
     */
    ~BlockProfiler() = default;

    /**
     * Exit the scope and stop the timer.
     */
    void end();
private:
    /// has this profiler been ended already?
    bool mEnded{ false };
protected:
}; // class BlockProfiler

/// Profiler for a single thread.
class ThreadProfiler
{
public:
    /**
     * Create a thread profiler.
     * @param desc Description for this profiler.
     */
    ThreadProfiler(const char *desc);

    ThreadProfiler(const ThreadProfiler &other) = delete;
    ThreadProfiler(ThreadProfiler &&other) = delete;
    ThreadProfiler &operator=(const ThreadProfiler &other) = delete;
    ThreadProfiler &operator=(ThreadProfiler &&other) = delete;

    /**
     * End the thread profiler.
     */
    ~ThreadProfiler();
private:
protected:
}; // class ThreadProfiler

} // namespace prof

} // namespace util
