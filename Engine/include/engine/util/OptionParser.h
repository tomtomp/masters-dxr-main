/**
* @file util/OptionParser.h
* @author Tomas Polasek
* @brief Command line options parsing.
*/

#pragma once

#include "engine/util/wexception.h"

/// Utilities and helpers.
namespace util
{

/**
 * Option parsing class. Allows registration of option names 
 * and types. This information can then be used on an argument 
 * vector provided by the OS. 
 */
class OptionParser
{
public:
    /// Exception carrying information about parsing error.
    struct ParsingException : public util::wruntime_error
    {
        ParsingException(const std::wstring &msg) :
            wruntime_error(msg)
        { }
        ParsingException(const wchar_t *msg) :
            wruntime_error(msg)
        { }
    }; // struct ParsingException

    /**
     * Callback function type.
     * @tparam T What type is extracted from the argument vector.
     */
    template <typename T>
    using CbFunT = std::function<void(const T&)>;

    /**
     * Option checker function type.
     * Returns true, if the value passed the check.
     * May throw OptionParser::ParsingException with 
     * message why the check failed.
     * @tparam T What type is being checked.
     */
    template <typename T>
    using CheckFunT = std::function<bool(const T&)>;

    /**
     * Callback function type for flag options.
     */
    using FlagCbFunT = std::function<void()>;

    /// Initialize empty option parser.
    OptionParser() = default;

    /**
     * Add new option for parsing the argument vector.
     * This version is for flag options, without 
     * any values.
     * If there already is an option with the same 
     * identifier, util::wruntime_error will be thrown.
     * @param opt String marking the start of this 
     * option, e.g. -a, --all .
     * @param cb Callback called when this option is 
     * found in the argument vector.
     * @param description Description of the option.
     * @throws Throws util::wruntime_error when option 
     * with the same identifier is already registered.
     */
    void addOption(const std::wstring &opt, FlagCbFunT cb, const std::wstring &description = L"");

    /**
     * Add new option for parsing the argument vector.
     * If there already is an option with the same 
     * identifier, util::wruntime_error will be thrown.
     * @param opt String marking the start of this 
     * option, e.g. -a, --all .
     * @param cb Callback triggered when the option is 
     * found during parsing. Parsed value will be passed 
     * back to the user through this callback.
     * @param cf Parameter value checking function. Returns 
     * true, if the value passed the check, or false if 
     * it didn't. Also can throw ParsingException with 
     * message containing the reason of failure.
     * @param description Description of the option.
     * @throws Throws util::wruntime_error when option 
     * with the same identifier is already registered.
     */
    template <typename T>
    void addOption(const std::wstring &opt, CbFunT<T> cb, const std::wstring &description = L"", 
        CheckFunT<T> cf = CheckFunT<T>());

    /**
     * Parse argument vector using configured rules.
     * @param argv Argument vector containing command line options.
     * @param argc Length of the argument vector.
     * @throws Throws ParsingException when parsing error 
     * occurs. The message can then be displayed to user.
     */
    void parse(const wchar_t * const * const argv, int argc);

    /**
     * Parse argument vector using configured rules.
     * @param args List of arguments.
     * @throws Throws ParsingException when parsing error 
     * occurs. The message can then be displayed to user.
     */
    void parse(const std::vector<std::wstring> &args);

    /**
     * Set application name. If it was not set before running 
     * parse, the first argument from the arg list will be used.
     * @param appName Name of the application used for the help 
     * message.
     */
    void setAppName(const std::wstring appName)
    { mAppName = appName; }

    /**
     * Generate help message, containing all of the configured 
     * options. If no application name has been set, default name 
     * will be used instead.
     * @return Returns string containing the help menu.
     */
    std::wstring generateHelp() const;
private:
    /// Iterator used for iterating through command line arguments.
    using ArgIter = std::vector<std::wstring>::const_iterator;

    /**
     * Base class used for parsing arguments and dispatching 
     * results back to caller.
     */
    class BaseParser
    {
    public:
        /**
         * Initialize parser with information about parsed option.
         * @param opt Option string identification, e.g. "-a" or "--all".
         * @param description Description of the option.
         */
        BaseParser(const std::wstring &opt, const std::wstring &description) :
            mOpt{ opt }, mDescription{ description }
        { }

        virtual ~BaseParser() = default;

        /**
         * Do the parsing and dispatch results back to 
         * configurator.
         * @param argIter iterator, which will be moved 
         * as far as the parser does its parsing. Starting 
         * position should be on the option which is 
         * being parsed.
         * @param end End iterator for the argument vector.
         * @throws Throws ParsingException on error. 
         */
        virtual void parse(ArgIter &argIter, const ArgIter &end) = 0;

        /// Generate help message for this option.
        std::wstring generateHelp();

        /// Get option string identification, e.g. "-a" or "--all".
        const std::wstring &option() const
        { return mOpt; }

        /// Get value description for this option, e.g. "--val NUMBER".
        const std::wstring &valueDesc() const
        { return mValueDesc; }
    private:
        /// Option string identification.
        std::wstring mOpt{};
        /// Description of the option.
        std::wstring mDescription{};
    protected:
        /// Option value description, e.g. "--val NUMBER".
        std::wstring mValueDesc{};
    }; // class BaseParser

    /// Parser for flag options.
    class FlagParser : public BaseParser
    {
    public:
        /**
         * Set the callback function for this parser.
         * @param opt Option string identification, e.g. "-a" or "--all".
         * @param description Description of the option.
         * @param cb Callback function called when 
         * option is found.
         */
        FlagParser(const std::wstring &opt, const std::wstring &description, FlagCbFunT &&cb);

        virtual ~FlagParser() = default;
        FlagParser(const FlagParser &other) = default;
        FlagParser(FlagParser &&other) = default;
        FlagParser &operator=(const FlagParser &other) = default;
        FlagParser &operator=(FlagParser &&other) = default;

        /// Simple parse function, just calls the callback and moves iterator by one.
        virtual void parse(ArgIter &argIter, const ArgIter &end) override final;
    private:
        /// Callback function.
        FlagCbFunT mCb;
    protected:
    }; // class FlagParser

    /**
     * Parser for options with value.
     * @tparam T Type of the value.
     */
    template <typename T>
    class Parser : public BaseParser
    {
    public:
        /**
         * Set the callback function for this parser.
         * @param opt Option string identification, e.g. "-a" or "--all".
         * @param description Description of the option.
         * @param cb Callback function called when 
         * @param cf Parameter checking function.
         * parsing is finished.
         */
        Parser(const std::wstring &opt, const std::wstring &description, CbFunT<T> &&cb, CheckFunT<T> &&cf) :
            BaseParser(opt, description), 
            mCb{ cb },
            mCf{ cf }
        { initValueDesc(); }

        virtual ~Parser() = default;
        Parser(const Parser &other) = default;
        Parser(Parser &&other) = default;
        Parser &operator=(const Parser &other) = default;
        Parser &operator=(Parser &&other) = default;

        /**
         * Perform type specific parsing and trigger callback.
         * @param argIter iterator, which will be moved 
         * as far as the parser does its parsing. Starting 
         * position should be on the option which is 
         * being parsed.
         * @param end End iterator for the argument vector.
         * @throws Throws ParsingException on error. 
         */
        virtual void parse(ArgIter &argIter, const ArgIter &end) override final;
    private:
        /**
         * Attempt parsing and return the value.
         * @param argIter iterator, which will be moved 
         * as far as the parser does its parsing. Starting 
         * position should be on the option which is 
         * being parsed.
         * @param end End iterator for the argument vector.
         * @throws Throws ParsingException on error. 
         */
        T parseValue(ArgIter &argIter, const ArgIter &end);

        /// Initialize the value descriptor, e.g. "--val NUMBER".
        void initValueDesc();

        /// Callback function.
        CbFunT<T> mCb;
        /// Parameter checking function.
        CheckFunT<T> mCf;
    protected:
    }; // class Parser

    /// Parsing rule added by user.
    struct ParsingRule
    {
        /// Option string, e.g. "-a" or "--all".
        std::wstring option;

        /// Parser/dispatcher for this rule.
        std::unique_ptr<BaseParser> parser;
    }; // struct ParsingRule

    /// Default application name.
    static constexpr const wchar_t *DEFAULT_APP_NAME{ L"application.exe" };

    /// Application name.
    std::wstring mAppName{ L"" };

    /**
     * Is there already rule registered for 
     * specified option identifier?
     * @param opt Option identifier.
     * @return Returns true, if option with 
     * specified identifier is already registered.
     */
    bool hasRule(const std::wstring &opt);

    /**
     * Save rule into the rule map.
     * @param opt String representing the rule.
     * @param ptr Pointer to parser for this rule.
     */
    void setRule(const std::wstring &opt, std::unique_ptr<BaseParser> &&ptr);

    /**
     * Generate message for unknown option.
     * @param opt Option which is unknown.
     * @return Returns the message including end line.
     */
    static std::wstring unknownOptionMsg(const std::wstring &opt);

    /**
     * Generate message for parsing error.
     * @param opt Option for which the error occurred.
     * @param err Error throws by the parser.
     * @return Returns the error message including end line.
     */
    static std::wstring errorParsingMsg(const std::wstring &opt, const util::wruntime_error &err);

    /// Mapping from option to parser.
    std::map<std::wstring, std::unique_ptr<BaseParser>> mRules;
protected:
}; // class OptionParser

} // namespace util

// Parsing specialization.

namespace util
{

template <>
std::wstring OptionParser::Parser<std::wstring>::parseValue(ArgIter &argIter, const ArgIter &end);
template <>
void OptionParser::Parser<std::wstring>::initValueDesc();

template <>
uint32_t OptionParser::Parser<uint32_t>::parseValue(ArgIter &argIter, const ArgIter &end);
template <>
void OptionParser::Parser<uint32_t>::initValueDesc();

template <>
bool OptionParser::Parser<bool>::parseValue(ArgIter &argIter, const ArgIter &end);
template <>
void OptionParser::Parser<bool>::initValueDesc();

}

// End of parsing specialization.

// Template implementation.

namespace util
{

template <typename T>
void OptionParser::addOption(const std::wstring& opt, CbFunT<T> cb, const std::wstring &description, 
    CheckFunT<T> cf)
{
    if (hasRule(opt))
    { // Rule already registered.
        throw util::wruntime_error(std::wstring(L"Option with identifier \"") + 
            opt + L"\" is already registered!");
    }

    setRule(opt, std::make_unique<Parser<T>>(opt, description, std::move(cb), std::move(cf)));
}

template <typename T>
void OptionParser::Parser<T>::parse(ArgIter &argIter, const ArgIter &end)
{
    // Perform value parsing.
    auto parsedValue{ parseValue(argIter, end) };

    if (mCf && !mCf(parsedValue))
    { // Parsed value has failed the user check.
        throw ParsingException(L"Failed value requirements!");
    }

    // Trigger the callback.
    mCb(parsedValue);
}

template <typename T>
T OptionParser::Parser<T>::parseValue(ArgIter &argIter, const ArgIter &end)
{ throw ParsingException(L"Unable to parse unknown type!"); }

template <typename T>
void OptionParser::Parser<T>::initValueDesc()
{ mValueDesc = L""; }

} 
// Template implementation end.
