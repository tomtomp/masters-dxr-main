/**
* @file util/Threading.h
* @author Tomas Polasek
* @brief Thread utilities.
*/

#pragma once

#include <atomic>
#include <thread>

/// Utilities and helpers.
namespace util
{

/// Threading helper function and classes.
namespace thread
{

/**
* Spin lock implementation, using std::atomic_flag.
*/
class SpinLock
{
public:
    /**
    * Lock this lock.
    * Yields this thread, if the lock is locked.
    */
    void lock()
    {
        while (mFlag.test_and_set(std::memory_order_acquire))
        {
            std::this_thread::yield();
        }
    }

    /**
     * Attempt to lock this lock and return its value.
     * Does NOT block.
     * @return Returns true if the locking has been successful.
     */
    bool tryLock()
    { return !mFlag.test_and_set(std::memory_order_acquire); }

    /**
    * Unlock this lock.
    */
    void unlock()
    { mFlag.clear(std::memory_order_release); }
private:
    std::atomic_flag mFlag = ATOMIC_FLAG_INIT;
protected:
}; //class SpinLock

/// Simple barrier implementation.
class Barrier
{
public:
    /// Create a barrier for specified number of threads.
    explicit Barrier(std::size_t count) :
        mCount{ count }
    { }

    /**
     * Wait until this function is called count times.
     */
    void wait()
    {
        std::unique_lock<std::mutex> lock{ mMutex };
        if (mCount == 0u)
        { // Check for access beyond advertised number.
            return;
        }

        if (--mCount == 0u) 
        { // The last thread has arrived.
            // Release all of them.
            mCv.notify_all();
        }
        else 
        { // Not all of the threads have arrived yet.
            // Wait for all of them to arrive.
            mCv.wait(lock, [this] { return mCount == 0; });
        }
    }
private:
    /// Mutex used for waiting with condition_variable.
    std::mutex mMutex;
    /// Waiting for all threads to arrive.
    std::condition_variable mCv;
    /// Number of threads to wait for.
    std::size_t mCount;
protected:
}; // class Barrier

} // namespace thread

} // namespace util
