/**
* @file util/Asserts.h
* @author Tomas Polasek
* @brief Assertion system.
*/

#pragma once

#include <cstdio>
#include <cstdlib>
#include <csignal>

/// Performs a single evaluation of given expression and throws std::runtime_error, if it fails.
#define THROW_IF_FAILED(HRES_EXPR) \
    do { \
        HRESULT hr{HRES_EXPR}; \
        if (FAILED(hr)) \
        { \
            throw std::runtime_error("HRESULT failed: " #HRES_EXPR); \
        } \
    } while (false)

#define UNUSED(VAR) (static_cast<void>(VAR))

#define ASSERT_FATAL(COND_EXP) \
    if ((COND_EXP)) \
    {} \
    else \
    { \
        log<Fatal>() << "Fatal error \"" #COND_EXP "\" " << \
                        __FILE__ << ":" << __LINE__ << \
                        " ( " << __func__ << " )." << \
                        std::endl; \
        std::raise(SIGABRT); \
        exit(-1); \
    }

#ifndef NDEBUG
#   define ASSERT_BASE(COND_EXP, LINE, FILE, FUNC) \
    if ((COND_EXP)) \
    {} \
    else \
    { \
        log<Error>() << "Assertion \"" #COND_EXP "\" " << \
                        (FILE) << ":" << (LINE) << \
                        " ( " << (FUNC) << " ) failed." << \
                        std::endl; \
        std::raise(SIGABRT); \
    }
#else
#   define ASSERT_BASE(COND_EXP, LINE, FILE, FUNC)
#endif

#ifndef NDEBUG
/// Choose first, if debug is on, else choose second.
#   define CHOOSE_DEBUG(first, second) first
/// If NDEBUG is not defined, the command in brackets will be present.
#   define DO_IF_DEBUG(cmd) cmd
#else
/// Choose first, if debug is on, else choose second.
#   define CHOOSE_DEBUG(first, second) second
/// If NDEBUG is not defined, the command in brackets will be present.
#   define DO_IF_DEBUG(_)
#endif

// Assert used in places, where the performance hit is not too big.
#ifndef NDEBUG_FAST
#   define ASSERT_FAST(COND_EXP) ASSERT_BASE((COND_EXP), \
                                 __LINE__, __FILE__, __func__)
#else
#   define ASSERT_FAST(_)
#endif

// Assert used in places, where the performance hit is quite notable.
#ifndef NDEBUG_SLOW
#   define ASSERT_SLOW(COND_EXP) ASSERT_BASE((COND_EXP), \
                                 __LINE__, __FILE__, __func__)
#else
#   define ASSERT_SLOW(_)
#endif

// Print warning message.
#ifndef NDEBUG
#   define WARNING(MSG) \
    do{ \
        log<Warning>() << "Warning: \"" MSG "\" " << \
                        __FILE__ << ":" << __LINE__ << \
                        " ( " << __func__ << " ).\n" << \
                        std::endl; \
    } while(false)
#else
#   define WARNING(_)
#endif
