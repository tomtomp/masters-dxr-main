/**
* @file util/BitwiseEnum.h
* @author Tomas Polasek
* @brief Provides bitwise operators for all enumeration types.
*/

#pragma once

#include <type_traits>

namespace util
{

namespace impl
{

// Source : https://stackoverflow.com/a/26936868
template <class T>
using is_scoped_enum = std::integral_constant<bool, 
    !std::is_convertible<T, 
                         typename std::underlying_type<T>::type>::value && 
    std::is_enum<T>::value>;
// Create a flag that is true when type cannot be implicitly converted into the underlying type and is an enum.

template <typename T>
using TypeIfEnumClass = typename std::enable_if<is_scoped_enum<T>::value, T>::type;

} // namespace impl

} // namespace util

template <typename T>
inline util::impl::TypeIfEnumClass<T> operator~ (T first) 
{ return static_cast<T>( ~ static_cast<std::underlying_type_t<T>>(first) ); }

template <typename T> 
inline util::impl::TypeIfEnumClass<T> operator|(T first, T second) 
{ return static_cast<T>(static_cast<std::underlying_type_t<T>>(first) | static_cast<std::underlying_type_t<T>>(second)); }

template <typename T> 
inline util::impl::TypeIfEnumClass<T> operator&(T first, T second) 
{ return static_cast<T>(static_cast<std::underlying_type_t<T>>(first) & static_cast<std::underlying_type_t<T>>(second)); }

template <typename T> 
inline util::impl::TypeIfEnumClass<T> operator^(T first, T second) 
{ return static_cast<T>(static_cast<std::underlying_type_t<T>>(first) ^ static_cast<std::underlying_type_t<T>>(second)); }

template <typename T> 
inline util::impl::TypeIfEnumClass<T> &operator|=(T& first, T second) 
{ return first = static_cast<T>(static_cast<std::underlying_type_t<T>>(first) | static_cast<std::underlying_type_t<T>>(second)); }

template <typename T> 
inline util::impl::TypeIfEnumClass<T> &operator&=(T& first, T second) 
{ return first = static_cast<T>(static_cast<std::underlying_type_t<T>>(first) & static_cast<std::underlying_type_t<T>>(second)); }

template <typename T> 
inline util::impl::TypeIfEnumClass<T> &operator^=(T& first, T second) 
{ return first = static_cast<T>(static_cast<std::underlying_type_t<T>>(first) ^ static_cast<std::underlying_type_t<T>>(second)); }
 