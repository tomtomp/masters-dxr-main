/**
* @file renderer/ClearPresentRL.h
* @author Tomas Polasek
* @brief Rendering layer allowing clearing and presenting buffers in 
* a flip-discard swap chain.
*/

#pragma once

#include "engine/renderer/RenderLayerBase.h"

#include "engine/resources/d3d12/D3D12CommandQueueMgr.h"
#include "engine/lib/dxtk.h"

/// Namespace containing the engine code.
namespace engine
{

/// Rendering classes.
namespace rndr
{

/**
 * Basic rendering layer allowing clearing and presenting buffers 
 * in a flip-discard swap chain.
 */
class ClearPresentRL : public RenderLayerBase
{
public:
    /// Create clear-present rendering layer for given renderer.
    ClearPresentRL(Renderer &renderer);

    /// No resource to free.
    ~ClearPresentRL() = default;

    /**
     * Clear the current backbuffer in the main swap chain.
     * @param clearColor Which color should be used for 
     * clear background.
     * @return Returns direct command list containing the clear 
     * command.
     * @throws Throws util::winexception on error.
     */
    res::d3d12::D3D12CommandList clear(dxtk::math::Color clearColor = { 1.0f, 0.0f, 1.0f, 1.0f });

    /**
     * Present the current backbuffer in the main swap chain.
     * @param cmdList Command list to execute before presenting.
     * @param useVSync Should vSync be used?
     * @param useTearing Should tearing be enabled? Only works 
     * when vsync is also disabled.
     * @example present(std::move(cmdList))
     * @throws Throws util::winexception on error.
     */
    void present(res::d3d12::D3D12CommandList &&cmdList, bool useVSync, bool useTearing);
private:
    /// Using this renderer.
    Renderer &mRenderer;
protected:
}; // class ClearPresentRL

} // namespace rndr

} // namespace engine
