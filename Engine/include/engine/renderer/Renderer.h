/**
* @file renderer/Renderer.h
* @author Tomas Polasek
* @brief Rendering system.
*/

#pragma once

//#include "EngineConfig.h"
#include "EngineRuntimeConfig.h"

// Windows resources: 
#include "engine/resources/win32/Window.h"

// Direct3D 12 resources: 
#include "engine/resources/d3d12/D3D12Adapter.h"
#include "engine/resources/d3d12/D3D12DXGIFactory5.h"
#include "engine/resources/d3d12/D3D12Device.h"
#include "engine/resources/d3d12/D3D12CommandQueueMgr.h"
#include "engine/resources/d3d12/D3D12SwapChain.h"

// Direct3D 12 helpers: 
#include "engine/helpers/d3d12/D3D12LiveObjectReporter.h"
#include "engine/helpers/d3d12/D3D12DebugConfigurator.h"
#include "engine/helpers/d3d12/D3D12ImGuiWrapper.h"
#include "engine/helpers/d3d12/D3D12CommittedAllocator.h"

// DirectX ToolKit for math and some helper classes.
#include "engine/lib/dxtk.h"

/// Namespace containing the engine code.
namespace engine
{

/// Rendering classes.
namespace rndr
{

/// Main interface for the rendering system.
class Renderer
{
public:
    /**
     * Initialize renderer and default rendering pipeline.
     * @param cfg Runtime configuration.
     * @param window Window which will be used as output for the renderer.
     */
    Renderer(const EngineRuntimeConfig &cfg, const res::win::Window &window);

    /// Flush all command queues, free resources and quit.
    ~Renderer();

    /**
     * Resize the rendering swap chain to given size.
     * @param width New width in pixels.
     * @param height New height in pixels.
     * @warning The values will be clamped to 1, on 
     * the lower end.
     */
    void resize(uint32_t width, uint32_t height);

    /**
     * Flush all of the command queues.
     */
    void flush();

    /// Access the D3D12 device.
    res::d3d12::D3D12Device &device()
    { return mDevice; }

    /// Access the main D3D12 swap chain.
    res::d3d12::D3D12SwapChain &swapChain()
    { return mSwapChain; }

    /// Access the D3D12 command queue which allows direct commands.
    res::d3d12::D3D12CommandQueueMgr &directCmdQueue()
    { return mDirectCommandQueue; }

    /// Access the D3D12 command queue which allows compute commands.
    res::d3d12::D3D12CommandQueueMgr &computeCmdQueue()
    { return mComputeCommandQueue; }

    /// Access the D3D12 command queue which allows copy commands.
    res::d3d12::D3D12CommandQueueMgr &copyCmdQueue()
    { return mCopyCommandQueue; }
private:
    /// Compile-time configuration.
    using Config = EngineConfig;

    /// Automatic live object reporting on destruction.
    helpers::d3d12::D3D12LiveObjectReporter mLiveObjectReporter;
    /// Automatic configuration of debug mode.
    helpers::d3d12::D3D12DebugConfigurator mDebugConfigurator;
    /// Factory used for Direct3D 12 object construction.
    res::d3d12::D3D12DXGIFactory5 mFactory5;
    /// Main rendering adapter.
    res::d3d12::D3D12Adapter mAdapter;
    /// Main rendering device.
    res::d3d12::D3D12Device mDevice;
    /// Command queue used for direct commands.
    res::d3d12::D3D12CommandQueueMgr mDirectCommandQueue;
    /// Command queue used for compute commands.
    res::d3d12::D3D12CommandQueueMgr mComputeCommandQueue;
    /// Command queue used for copy commands.
    res::d3d12::D3D12CommandQueueMgr mCopyCommandQueue;
    /// Main rendering swap chain.
    res::d3d12::D3D12SwapChain mSwapChain;
protected:
}; // class Renderer

} // namespace rndr

} // namespace engine
