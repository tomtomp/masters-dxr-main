/**
* @file renderer/ImGuiRL.h
* @author Tomas Polasek
* @brief Rendering layer for drawing GUI using ImGUI.
*/

#pragma once

#include "engine/renderer/RenderLayerBase.h"

#include "engine/helpers/d3d12/D3D12ImGuiWrapper.h"

/// Namespace containing the engine code.
namespace engine
{

/// Rendering classes.
namespace rndr
{

/**
 * Rendering layer for drawing GUI using ImGUI.
 */
class ImGuiRL : public RenderLayerBase
{
public:
    /// Create ImGUI rendering layer for given renderer.
    ImGuiRL(Renderer &renderer);

    /// Free ImGUI resources.
    ~ImGuiRL();

    /**
     * Start ImGui frame.
     */
    void imGuiBeginFrame();

    /**
     * End ImGui frame and store draw commands on 
     * provided command list.
     * @param cmdList Command list to draw with.
     */
    void imGuiEndFrame(res::d3d12::D3D12CommandList &cmdList);

    /**
     * Resize the rendering buffers used by ImGUI.
     * Should be called before resizing of the swap chain!
     * @param width New width in pixels.
     * @param height New height in pixels.
     */
    void resizeBeforeSwapChain(uint32_t width, uint32_t height);

    /**
     * Finalize resizing of the ImGUI buffers.
     * Should be called after resizing of the swap chain!
     */
    void resizeAfterSwapChain();

    /// Display ImGUI demo window, called after beginFrame.
    void showDemoWindow(bool &doOpen);
private:
    /// Using this renderer.
    Renderer &mRenderer;
    /// ImGui helper object, used for drawing GUI.
    helpers::d3d12::D3D12ImGuiWrapper mImGui;
protected:
}; // class ImGuiRL

} // namespace rndr

} // namespace engine
