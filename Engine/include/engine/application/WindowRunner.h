/**
* @file application/WindowRunner.h
* @author Tomas Polasek
* @brief Simple class used for instantiating a window and running its 
* message pump on the same thread.
*/

#pragma once 

#include "engine/resources/ThreadPool.h"
#include "engine/resources/win32/Window.h"

#include "engine/application/MessageHandler.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing application interface.
namespace app
{

/// Simple class used for executing window creation and message pump.
class WindowRunner
{
public:
    /**
     * Set all required parts of the window runner 
     * and instantiate the window.
     * @param workerPool Where the runner thread 
     * should be created.
     * @param window Window bound to this runner.
     * @param threadName Name of the thread used for 
     * profiling.
     * @param msgHandler Handler which will be used 
     * for this window.
     * @warning Thread pool and Window MUST be alive for 
     * the whole lifetime of the WindowRunner!
     */
    WindowRunner(res::thread::ThreadPool &workerPool, res::win::Window &window, 
        app::MessageHandler &msgHandler, const char *threadName = "MessageHandler");

    // Copy and move not required.
    WindowRunner(const WindowRunner &other) = delete;
    WindowRunner(WindowRunner &&other) = delete;
    WindowRunner &operator=(const WindowRunner &other) = delete;
    WindowRunner &operator=(WindowRunner &&other) = delete;

    /**
     * Signalize end to the runner thread and wait 
     * for it to finish.
     */
    ~WindowRunner();

    /**
     * Run message pump on the thread dedicated to 
     * window bound to this runner.
     * @param handler Handler to run.
     * @throws Throws std::runtime_error if this function 
     * is called multiple times.
     */
    void runMessagePump(MessageHandler &handler);
private:
    /// Main function of the window runner thread.
    static void runnerMain(std::future<MessageHandler&> &handlerProvider);

    /// Future used for waiting for the runner to finish.
    std::future<void> mRunnerFinished;
    /// Used for providing runner with handler to run.
    std::promise<MessageHandler&> mHandlerProvider;
    /// Is the message loop running?
    bool mRunning{ false };
protected:
}; // class WindowRunner

} // namespace app

} // namespace engine
