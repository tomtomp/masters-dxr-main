/**
* @file application/MessageHandler.h
* @author Tomas Polasek
* @brief Message pump and message handling class.
*/

#pragma once

#include "engine/input/Keyboard.h"
#include "engine/input/Mouse.h"

#include "engine/application/Events.h"

#include "engine/resources/win32/Window.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing application interface.
namespace app
{

/// Window message handling class.
class MessageHandler
{
public:
    /// Initialize message handling.
    MessageHandler();

    ~MessageHandler() = default;

    // No copying.
    MessageHandler(const MessageHandler &other) = delete;
    MessageHandler &operator=(const MessageHandler &other) = delete;

    // No moving.
    MessageHandler(MessageHandler &&other) = delete;
    MessageHandler &operator=(MessageHandler &&other) = delete;

    /**
     * Run message pump in continuous cycle, until WM_QUIT message 
     * is received. Uses blocking ::GetMessage!
     * @warning Message pump MUST be ran on the same thread which 
     * created the target window!
     */
    void runBlockingMsgPump();

    /**
     * Run a single iteration of non-blocking message pump, if 
     * WM_QUIT is received, the function returns false.
     * If no messages are available, the function returns 
     * immediately with return value true.
     * @return Returns false if WM_QUIT message has been 
     * received, else returns true.
     * @warning Message pump MUST be ran on the same thread which 
     * created the target window!
     */
    bool processMessage();

    /**
     * Get window callback procedure, it will expect 
     * optional window initialization data is set to 
     * pointer to instance of this class.
     * @return Returns window procedure usable in 
     * registering window class.
     */
    static WNDPROC windowProcCallback()
    { return &handlerConnector; }

    /// Get reader for the keyboard event bus.
    auto keyboardEventReader() const
    { return mKeyboardEventBus->reader(); }

    /// Get reader for the mouse button event bus.
    auto mouseButtonEventReader() const
    { return mMouseButtonEventBus->reader(); }

    /// Get reader for the mouse move event bus.
    auto mouseMoveEventReader() const
    { return mMouseMoveEventBus->reader(); }

    /// Get reader for the mouse scroll event bus.
    auto mouseScrollEventReader() const
    { return mMouseScrollEventBus->reader(); }

    /// Get reader for the resize event bus.
    auto resizeEventReader() const
    { return mResizeEventBus->reader(); }

    /// Get reader for the exit event bus.
    auto exitEventReader() const
    { return mExitEventBus->reader(); }

    /**
     * Get reader for specified event type.
     * @tparam EventT Type of event, must be 
     * used by this MessageHandler.
     * @return Returns new reader for specified 
     * events.
     */
    template <typename EventT>
    typename MessageBus<EventT>::ReaderT eventReader() const;

    /**
     * Register provided window with this message handler.
     * @param window Window to register.
     */
    void registerWindow(res::win::Window &window);
private:
    /// Message handler called by windows.
    static LRESULT CALLBACK handlerConnector(
        _In_ HWND   hwnd,
        _In_ UINT   uMsg,
        _In_ WPARAM wParam,
        _In_ LPARAM lParam);

    /// Message handler.
    LRESULT wndProc(
        res::win::Window &window,
        UINT uMsg,
        WPARAM wParam,
        LPARAM lParam);

    /**
     * Find window by hwnd.
     * @param hwnd Handle of the searched window.
     * @return Returns pointer to the window or 
     * nullptr if the window has not been found.
     */
    res::win::Window *findWindow(HWND hwnd);

    /// Event bus for sending keyboard events.
    const std::shared_ptr<KeyboardEventBus> mKeyboardEventBus;
    /// Writer for the keyboard event bus.
    KeyboardEventBus::WriterT mKeyboardEventWriter;

    /// Event bus for sending mouse button events.
    const std::shared_ptr<MouseButtonEventBus> mMouseButtonEventBus;
    /// Writer for the mouse button event bus.
    MouseButtonEventBus::WriterT mMouseButtonEventWriter;

    /// Event bus for sending mouse move events.
    const std::shared_ptr<MouseMoveEventBus> mMouseMoveEventBus;
    /// Writer for the mouse move event bus.
    MouseMoveEventBus::WriterT mMouseMoveEventWriter;

    /// Event bus for sending mouse scroll events.
    const std::shared_ptr<MouseScrollEventBus> mMouseScrollEventBus;
    /// Writer for the mouse scroll event bus.
    MouseScrollEventBus::WriterT mMouseScrollEventWriter;

    /// Event bus for sending resize events.
    const std::shared_ptr<ResizeEventBus> mResizeEventBus;
    /// Writer for the resize event bus.
    ResizeEventBus::WriterT mResizeEventWriter;

    /// Event bus for sending exit events.
    const std::shared_ptr<ExitEventBus> mExitEventBus;
    /// Writer for the resize event bus.
    ExitEventBus::WriterT mExitEventWriter;

    /// Keyboard message handler.
    input::Keyboard mKeyboard;
    /// Mouse message handler.
    input::Mouse mMouse;

    /// Registered windows mapped from their handles.
    std::map<HWND, res::win::Window&> mRegisteredWindows;
protected:
}; // class MessagePump
} // namespace app

} // namespace engine

// Template implementation

namespace engine
{

namespace app
{

template <>
inline MessageBus<events::KeyboardEvent>::ReaderT MessageHandler::eventReader<events::KeyboardEvent>() const
{ return mKeyboardEventBus->reader(); }

template <>
inline MessageBus<events::MouseButtonEvent>::ReaderT MessageHandler::eventReader<events::MouseButtonEvent>() const
{ return mMouseButtonEventBus->reader(); }

template <>
inline MessageBus<events::MouseMoveEvent>::ReaderT MessageHandler::eventReader<events::MouseMoveEvent>() const
{ return mMouseMoveEventBus->reader(); }

template <>
inline MessageBus<events::MouseScrollEvent>::ReaderT MessageHandler::eventReader<events::MouseScrollEvent>() const
{ return mMouseScrollEventBus->reader(); }

template <>
inline MessageBus<events::ResizeEvent>::ReaderT MessageHandler::eventReader<events::ResizeEvent>() const
{ return mResizeEventBus->reader(); }

template <>
inline MessageBus<events::ExitEvent>::ReaderT MessageHandler::eventReader<events::ExitEvent>() const
{ return mExitEventBus->reader(); }

}

}

// Template implementation end
