/**
* @file application/MessageBus.h
* @author Tomas Polasek
* @brief Message bus allowing concurrent processing from multiple threads.
*/

#pragma once

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing application interface.
namespace app
{

// Writer forward declaration.
template <typename MsgT>
class MessageBusWriter;

// Reader forward declaration.
template <typename MsgT>
class MessageBusReader;

/**
 * Bus abstraction used for storing information about 
 * registered readers and writers.
 *
 * Whenever a writer wants to write anything onto 
 * the bus, he has the disadvantage of having 
 * to put the message into as many mailboxes as 
 * there are readers.
 *
 * When reader decides to read messages, the 
 * mailbox for that reader is locked and swapped 
 * with his private mailbox. After that the 
 * public mailbox is immediately released again.
 * 
 * After creation, the lifetime of the object 
 * is until all of the readers/writers and 
 * other references to this bus are destroyed!
 * 
 * @tparam MsgT Each message has its own message bus 
 * and this type specifies what kind of message 
 * is this bus used for.
 */
template <typename MsgT>
class MessageBus : public std::enable_shared_from_this<MessageBus<MsgT>>
{
private:
    /// Helper struct for getting message type name.
    template <typename T>
    struct NameExtractor
    {
        template <typename R>
        static constexpr decltype(R::Name) extract(int)
        { return R::Name; }

        template <typename R>
        static constexpr auto extract(float)
        { return "Unknown"; }

        static constexpr const char *Name{ extract<T>(0) };
    }; // NameExtractor
public:
    /// Writer type for this message bus.
    using WriterT = MessageBusWriter<MsgT>;
    /// Reader type for this message bus.
    using ReaderT = MessageBusReader<MsgT>;

    /// Name of the message.
    static constexpr const char *BUS_NAME{ NameExtractor<MsgT>::Name };

    /// Create instance of this message bus.
    static std::shared_ptr<MessageBus<MsgT>> create()
    {
        /*
         * Since only constructor is private and make_shared 
         * does not have access to it, we create a proxy.
         * Based on: https://stackoverflow.com/a/25069711
         */

        struct SharedEnabler : public MessageBus<MsgT> { };
        return std::make_shared<SharedEnabler>();
    }

    /**
     * Create new writer, register it and return 
     * the final object.
     */
    MessageBusWriter<MsgT> writer();

    /**
     * Create new reader, register it and return 
     * the final object.
     */
    MessageBusReader<MsgT> reader();

    /**
     * Post a new message onto this message bus.
     * @tparam Ts Types of arguments passed to the 
     * message constructor.
     * @param args Arguments passed to the message
     * constructor.
     * @warning Operation may block, until all 
     * mailboxes receive the message.
     */
    template <typename... Ts>
    void postMessage(Ts... args);

    /// Get number of registered mailboxes.
    std::size_t numMailboxes();
private:
    friend class MessageBusWriter<MsgT>;
    friend class MessageBusReader<MsgT>;

    /**
     * Initialize message bus with no readers or 
     * writers. Instance must be created using the 
     * create static function!
     */
    MessageBus() = default;

    /// Mailbox abstraction.
    struct Mailbox
    {
    public:
        Mailbox() = default;
        ~Mailbox() = default;

        /**
         * Get a copy of messages in this mailbox 
         * and empty it.
         * @return Returns a copy of messages in 
         * this mailbox.
         * @warning Locks the mailbox.
         */
        std::vector<MsgT> fetchMessages();

        /**
         * Get a copy of messages in this mailbox 
         * without emptying it.
         * @return Returns a copy of messages in 
         * this mailbox.
         * @warning Locks the mailbox.
         */
        std::vector<MsgT> getMessages();

        /**
         * Emplace a new message to the end of 
         * the current mailbox.
         * @tparam Ts Types of arguments passed to 
         * the constructor.
         * @param args Arguments passed to the 
         * constructor.
         */
        template <typename... Ts>
        void postMessage(Ts... args);
    private:
        /// List of messages in the mailbox.
        std::vector<MsgT> mMessages;
        /// Mutex used when accessing messages.
        std::mutex mMutex;
    protected:
    }; // struct Mailbox

    /**
     * Create and register a new mailbox 
     * and return a pointer to it.
     * @return Returns a pointer to the new mailbox.
     */
    Mailbox *createMailbox();

    /**
     * De-register and destroy provided mailbox.
     * @param mailbox Mailbox to destroy. if this 
     * mailbox does not exist on this bus, nothing 
     * happens.
     */
    void destroyMailbox(Mailbox *mailbox);

    /// One mailbox for each reader.
    std::vector<std::unique_ptr<Mailbox>> mMailboxes;
    /**
     * Mutex for mailboxes - multiple writers can work, 
     * but adding/removing mailboxes requires complete lock.
     */
    std::shared_mutex mMailboxesMutex;
protected:
}; // class MessageBus

/**
 * Bus writer, whose main goal is to deliver 
 * messages to all registered readers.
 * @tparam MsgT Type of messages this writer 
 * works with.
 */
template <typename MsgT>
class MessageBusWriter
{
public:
    /// What type of message does this writer use.
    using MessageT = MsgT;

    /// Unbound writer, methods called on this object do nothing.
    MessageBusWriter() = default;

    /// Automatic destruction using shared_ptr.
    ~MessageBusWriter() = default;

    // Copying is allowed, since shared_ptr is used.
    MessageBusWriter(const MessageBusWriter &other) = default;
    MessageBusWriter &operator=(const MessageBusWriter &other) = default;

    // Moving is allowed, since shared_ptr is used.
    MessageBusWriter(MessageBusWriter &&other) = default;
    MessageBusWriter &operator=(MessageBusWriter &&other) = default;

    /**
     * Post a new message using this message writer.
     * @tparam Ts Types of arguments passed to the 
     * message constructor.
     * @param args Arguments passed to the message
     * constructor.
     * @warning Operation may block, until all 
     * mailboxes receive the message.
     */
    template <typename... Ts>
    void postMessage(Ts... args);

    /// Is this writer usable?
    explicit operator bool() const
    { return mBus.get(); }
private:
    friend class MessageBus<MsgT>;

    /**
     * Create writer for given message bus.
     * @param bus Bus to write to.
     */
    MessageBusWriter(const std::shared_ptr<MessageBus<MsgT>> &bus);

    /// Message bus this writer is connected to.
    std::shared_ptr<MessageBus<MsgT>> mBus;
protected:
}; // class MessageBusWriter

/**
 * Bus reader allows users to read messages 
 * poster onto the message bus.
 * @tparam MsgT Type of messages this reader 
 * works with.
 */
template <typename MsgT>
class MessageBusReader
{
public:
    /// What type of message does this reader use.
    using MessageT = MsgT;

    /// Unbound reader, methods called on this object do nothing.
    MessageBusReader() = default;

    ~MessageBusReader();

    // Copying if forbidden, since there is only one mailbox.
    MessageBusReader(const MessageBusReader &other) = delete;
    MessageBusReader &operator=(const MessageBusReader &other) = delete;

    // Moving is allowed, mailbox changes ownership.
    MessageBusReader(MessageBusReader &&other) noexcept;
    MessageBusReader &operator=(MessageBusReader &&other) noexcept;

    /**
     * Get a copy of messages in the mailbox 
     * and empty it.
     * @return Returns a copy of messages in 
     * this mailbox. When called on an invalid 
     * reader, returns empty vector.
     * @warning Locks the mailbox.
     */
    std::vector<MsgT> fetchMessages();

    /**
     * Get a copy of messages in the mailbox 
     * without emptying it.
     * @return Returns a copy of messages in 
     * this mailbox. When called on an invalid 
     * reader, returns empty vector.
     * @warning Locks the mailbox.
     */
    std::vector<MsgT> getMessages();

    /// Is this reader usable?
    explicit operator bool() const
    { return mBus && mMailbox; }
private:
    friend class MessageBus<MsgT>;

    /// Mailbox type name shortcut.
    using Mailbox = typename MessageBus<MsgT>::Mailbox;

    /**
     * Create reader for given message bus.
     * @param bus Bus to read from.
     * @param mailbox Mailbox bound to this reader.
     */
    MessageBusReader(const std::shared_ptr<MessageBus<MsgT>> &bus, Mailbox *mailbox);

    /**
     * Swap content with other reader object.
     * @param other Which object to swap with.
     */
    void swap(MessageBusReader &other) noexcept;

    /// Message bus this writer is connected to.
    std::shared_ptr<MessageBus<MsgT>> mBus{ nullptr };
    /// Mailbox bound to this reader.
    Mailbox *mMailbox{ nullptr };
protected:
}; // class MessageBusReader

/// Generate declaration for message bus of given type.
#define DECLARE_EXTERN_MESSAGE_BUS(MSG_TYPE)            \
    extern template class MessageBus<MSG_TYPE>;         \
    extern template class MessageBusWriter<MSG_TYPE>;   \
    extern template class MessageBusReader<MSG_TYPE>;   \

/// Generate definition for message bus of given type.
#define DEFINE_EXTERN_MESSAGE_BUS(MSG_TYPE)      \
    template class MessageBus<MSG_TYPE>;         \
    template class MessageBusWriter<MSG_TYPE>;   \
    template class MessageBusReader<MSG_TYPE>;   \

} // namespace app

} // namespace engine

// Template implementation

namespace engine
{

namespace app
{

template <typename MsgT>
MessageBusWriter<MsgT> MessageBus<MsgT>::writer()
{
    log<Debug>() << "Creating writer for MessageBus<" << BUS_NAME << ">" << std::endl;
    return { this->shared_from_this() };
}

template <typename MsgT>
MessageBusReader<MsgT> MessageBus<MsgT>::reader()
{
    log<Debug>() << "Creating reader for MessageBus<" << BUS_NAME << ">" << std::endl;
    return { this->shared_from_this(), createMailbox() };
}

template <typename MsgT>
template <typename... Ts>
void MessageBus<MsgT>::postMessage(Ts... args)
{
    // Shared mutex, since multiple writers can access mailboxes vector.
    std::shared_lock<std::shared_mutex> l(mMailboxesMutex);

    for (auto &mailbox : mMailboxes)
    {
        mailbox->postMessage(std::forward<Ts>(args)...);
    }
}

template <typename MsgT>
std::size_t MessageBus<MsgT>::numMailboxes()
{
    // Shared mutex, since multiple writers can access mailboxes vector.
    std::shared_lock<std::shared_mutex> l(mMailboxesMutex);

    return mMailboxes.size();
}

template <typename MsgT>
std::vector<MsgT> MessageBus<MsgT>::Mailbox::fetchMessages()
{
    std::unique_lock<std::mutex> l(mMutex);

    return { std::move(mMessages) };
}

template <typename MsgT>
std::vector<MsgT> MessageBus<MsgT>::Mailbox::getMessages()
{
    std::unique_lock<std::mutex> l(mMutex);

    return mMessages;
}

template <typename MsgT>
template <typename... Ts>
void MessageBus<MsgT>::Mailbox::postMessage(Ts... args)
{
    std::unique_lock<std::mutex> l(mMutex);

    mMessages.emplace_back(std::forward<Ts>(args)...);
}

template <typename MsgT>
typename MessageBus<MsgT>::Mailbox *MessageBus<MsgT>::createMailbox()
{
    // Unique lock, since we are changing vector of mailboxes.
    std::unique_lock<std::shared_mutex> l(mMailboxesMutex);

    mMailboxes.emplace_back(std::make_unique<Mailbox>());
    return mMailboxes.back().get();
}

template <typename MsgT>
void MessageBus<MsgT>::destroyMailbox(Mailbox *mailbox)
{
    // Unique lock, since we are changing vector of mailboxes.
    std::unique_lock<std::shared_mutex> l(mMailboxesMutex);

    const auto findIt{ std::find_if(mMailboxes.begin(), mMailboxes.end(), [&mailbox] (const auto &ptr)
    {
        return ptr.get() == mailbox;
    }) };
    if (findIt != mMailboxes.end())
    {
        mMailboxes.erase(findIt);
    }
}

template <typename MsgT>
template <typename... Ts>
void MessageBusWriter<MsgT>::postMessage(Ts... args)
{
    if (mBus)
    {
        mBus->postMessage(std::forward<Ts>(args)...);
    }
}

template <typename MsgT>
MessageBusWriter<MsgT>::MessageBusWriter(const std::shared_ptr<MessageBus<MsgT>> &bus) :
    mBus{ bus }
{
    if (mBus.get() == nullptr)
    {
        throw std::runtime_error("Message bus writer received a null pointer!");
    }
}

template <typename MsgT>
MessageBusReader<MsgT>::~MessageBusReader()
{
    if (mBus && mMailbox)
    {
        mBus->destroyMailbox(mMailbox);
    }

    mMailbox = nullptr;
}

template <typename MsgT>
MessageBusReader<MsgT>::MessageBusReader(MessageBusReader &&other) noexcept :
    MessageBusReader()
{ swap(other); }

template <typename MsgT>
MessageBusReader<MsgT> &MessageBusReader<MsgT>::operator=(MessageBusReader &&other) noexcept
{
    swap(other);
    return *this;
}

template <typename MsgT>
std::vector<MsgT> MessageBusReader<MsgT>::fetchMessages()
{
    if (mMailbox)
    {
        return mMailbox->fetchMessages();
    }

    return { };
}

template <typename MsgT>
std::vector<MsgT> MessageBusReader<MsgT>::getMessages()
{
    if (mMailbox)
    {
        return mMailbox->getMessages();
    }

    return { };
}

template <typename MsgT>
MessageBusReader<MsgT>::MessageBusReader(const std::shared_ptr<MessageBus<MsgT>> &bus,
    typename MessageBus<MsgT>::Mailbox *mailbox) :
    mBus{ bus }, mMailbox{ mailbox }
{
    if (mBus.get() == nullptr || mMailbox == nullptr)
    {
        throw std::runtime_error("Message bus reader received a null pointer!");
    }
}

template <typename MsgT>
void MessageBusReader<MsgT>::swap(MessageBusReader &other) noexcept
{
    std::swap(mBus, other.mBus);
    std::swap(mMailbox, other.mMailbox);
}

}

}

// Template implementation end
