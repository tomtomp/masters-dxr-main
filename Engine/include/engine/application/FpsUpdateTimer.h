/**
* @file application/FpsUpdateTimer.h
* @author Tomas Polasek
* @brief Simple class used for counting number of 
* frames/updates per second.
*/

#pragma once 

#include "engine/util/Timer.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing application interface.
namespace app
{

/// Simple class used for counting number of frames/updates per second.
class FpsUpdateTimer : public util::TickDeltaTimer<util::HrTimer::MicrosecondsF>
{
private:
    /// Type of milliseconds used.
    using Milliseconds = util::HrTimer::MillisecondsF;
    /// Type of seconds used.
    using Seconds = util::HrTimer::Seconds;
public:
    /**
     * Initialize the counter, specify whether to 
     * print gathered information periodically.
     * @param printInfo Should the information be 
     * printed periodically into the <Info> log?
     */
    FpsUpdateTimer(bool printInfo) :
        TickDeltaTimer(Seconds(1u), [this](auto ms) { this->timerTicked(ms); }), 
        mDoPrint(printInfo)
    { }

    /// Call this function when update occurs.
    void update()
    { mUpdates++; }

    /// Call this function when frame is rendered.
    void frame()
    { mFrames++; }

    /// Get updates per second for the last interval.
    uint32_t ups() const
    { return mUps; }

    /// Get frames per second for the last interval.
    uint32_t fps() const
    { return mFps; }

    /// Get frame time in milliseconds for the last interval.
    float frameTime() const
    { return mFrameTime; }

    /// Print current information.
    void printInfo() const
    {
        log<Info>() << "FPS: " << fps() << " (FrameTime: " << frameTime() << 
            " ms) Ups: " << ups() << std::endl;
    }
private:
    /// Inner function called when the timer ticks over.
    void timerTicked(Milliseconds ms)
    {
        // Update the values.
        mUps = mUpdates;
        mUpdates = 0u;
        mFps = mFrames;
        mFrames = 0u;
        mFrameTime = ms.count() / mFps;

        if (mDoPrint)
        {
            printInfo();
        }
    }

    /// Should the information be periodically printed?
    bool mDoPrint{ false };
    /// Number of updates since last tick.
    uint32_t mUpdates{ 0u };
    /// Current updates per second.
    uint32_t mUps{ 0u };
    /// Number of frames since last tick.
    uint32_t mFrames{ 0u };
    /// Current frames per second.
    uint32_t mFps{ 0u };
    /// How long does a single frame take.
    float mFrameTime{ 0.0f };
protected:
}; // class FpsUpdateTimer

} // namespace app

} // namespace engine
