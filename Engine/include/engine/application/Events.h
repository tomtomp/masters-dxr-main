/**
* @file application/Events.h
* @author Tomas Polasek
* @brief Event structures.
*/

#pragma once

#include "engine/application/MessageBus.h"

#include "engine/input/KeyboardEvent.h"
#include "engine/input/MouseEvent.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing application interface.
namespace app
{

/// Namespace containing application events.
namespace events
{

// Input event shortcuts.

/// Event triggered when keyboard changes state.
using KeyboardEvent = ::engine::input::KeyboardEvent;
/// Event triggered when mouse button changes state.
using MouseButtonEvent = ::engine::input::MouseButtonEvent;
/// Event triggered when mouse moves.
using MouseMoveEvent = ::engine::input::MouseMoveEvent;
/// Event triggered when mouse scroll wheel is used.
using MouseScrollEvent = ::engine::input::MouseScrollEvent;

/// Event triggered when window changes size.
struct ResizeEvent
{
    /// Name of this event.
    static constexpr const char *Name{ "ResizeEvent" };

    ResizeEvent() = default;

    ResizeEvent(uint32_t width, uint32_t height) :
        newWidth{ width }, newHeight{ height }
    { }

    uint32_t newWidth{ 0u };
    uint32_t newHeight{ 0u };
}; // struct ResizeEvent

/// Event triggered when the main window is closed.
struct ExitEvent
{
    /// Name of this event.
    static constexpr const char *Name{ "ExitEvent" };
}; // struct ExitEvent

} // namespace events

// Extern template declarations.
DECLARE_EXTERN_MESSAGE_BUS(::engine::app::events::KeyboardEvent);
DECLARE_EXTERN_MESSAGE_BUS(::engine::app::events::MouseButtonEvent);
DECLARE_EXTERN_MESSAGE_BUS(::engine::app::events::MouseMoveEvent);
DECLARE_EXTERN_MESSAGE_BUS(::engine::app::events::MouseScrollEvent);
DECLARE_EXTERN_MESSAGE_BUS(::engine::app::events::ResizeEvent);
DECLARE_EXTERN_MESSAGE_BUS(::engine::app::events::ExitEvent);

using KeyboardEventBus = ::engine::app::MessageBus<::engine::app::events::KeyboardEvent>;
using MouseButtonEventBus = ::engine::app::MessageBus<::engine::app::events::MouseButtonEvent>;
using MouseMoveEventBus = ::engine::app::MessageBus<::engine::app::events::MouseMoveEvent>;
using MouseScrollEventBus = ::engine::app::MessageBus<::engine::app::events::MouseScrollEvent>;
using ResizeEventBus = ::engine::app::MessageBus<::engine::app::events::ResizeEvent>;
using ExitEventBus = ::engine::app::MessageBus<::engine::app::events::ExitEvent>;

} // namespace app

} // namespace engine
