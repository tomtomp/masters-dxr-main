/**
* @file resources/BaseResourceMgr.h
* @author Tomas Polasek
* @brief Base class for all resource managers.
*/

#pragma once

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing resource management helpers.
namespace res
{

/// Base class for resource managers.
class BaseResourceMgr
{
public:
    BaseResourceMgr() = default;
    virtual ~BaseResourceMgr() = default;

    // Managers never allow copying/moving.
    BaseResourceMgr(const BaseResourceMgr &other) = delete;
    BaseResourceMgr(BaseResourceMgr &&other) = delete;
    BaseResourceMgr &operator=(const BaseResourceMgr &rhs) = delete;
    BaseResourceMgr &operator=(BaseResourceMgr &&rhs) = delete;
private:
protected:
}; // class BaseResourceMgr

} // namespace res

} // namespace engine
