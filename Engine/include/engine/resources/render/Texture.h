/**
* @file resources/render/Texture.h
* @author Tomas Polasek
* @brief Wrapper around an image, usable in rendering.
*/

#pragma once

#include "engine/resources/BaseResource.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing resource management helpers.
namespace res
{

/// Rendering resources.
namespace rndr
{

/// Texture usable in rendering.
class Texture : BaseResource
{
public:
private:
protected:
}; // class Texture

} // namespace rndr

} // namespace res

} // namespace engine
