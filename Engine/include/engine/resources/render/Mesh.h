/**
* @file resources/render/Mesh.h
* @author Tomas Polasek
* @brief Wrapper around a vertex and index buffer, usable in rendering.
*/

#pragma once

#include "engine/resources/BaseResource.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing resource management helpers.
namespace res
{

/// Rendering resources.
namespace rndr
{

/// Mesh usable in rendering.
class Mesh : BaseResource
{
public:
private:
protected:
}; // class Mesh

} // namespace rndr

} // namespace res

} // namespace engine
