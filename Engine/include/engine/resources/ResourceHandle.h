/**
* @file resources/ResourceHandle.h
* @author Tomas Polasek
* @brief Wrapper around unique identifier and manager used to get a resource.
*/

#pragma once

#include <cstdint>

#include "engine/resources/BaseResourceMgr.h"
#include "engine/resources/BaseResource.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing resource management helpers.
namespace res
{

/**
 * Indirection for accessing resources. Contains a pointer to manager 
 * containing the target resource and unique index which specifies which 
 * object is being indexed.
 * @tparam MgrT Type of the manager.
 * @tparam ResT Type of the resource.
 * @tparam IdxT Type for the unique index specifying target resource.
 */
template <typename MgrT, typename ResT, typename IdxT = uint16_t>
class ResourceHandle
{
public:
    static_assert(std::is_base_of_v<BaseResourceMgr, MgrT>, 
        "Manager type used with a handle must inherit from BaseResourceMgr!");
    /// Type of the manager who holds target resources.
    using ManagerT = MgrT;

    static_assert(std::is_base_of_v<BaseResource, ResT>, 
        "Resource type used with a handle must inherit from BaseResource!");
    /// Type of target resource.
    using ResourceT = ResT;

    /// Type of indexing parameter.
    using IndexT = IdxT;

    /**
     * Get unique resource index.
     */
    const IndexT &extract() const
    { return mIndex; }

    /**
     * Assign given index to this handle.
     * @param idx Index valid withing the manager.
     * @warning The state of this handle after assigning 
     * a new index may be invalid, checking with valid() 
     * function is highly recommended!
     */
    void assign(const IndexT &idx)
    { mIndex = idx; }

    /**
     * Test handle for validity.
     * @returns Returns true if this handle is valid.
     * @warning Does NOT check if the index is valid 
     * within the manager!
     */
    explicit operator bool() const
    { return mMgr; }

    /**
     * Test handle for validity and existence of target 
     * resource.
     * @returns Returns true if this handle is valid and 
     * index points to a valid resource.
     * @warning Also checks validity of unique index pointing 
     * to the target resource. This may result in locking 
     * the resource manager or, depending on manger, throwing 
     * exceptions.
     */
    bool valid() const
    { return (operator bool()) && mMgr->handleValid(mIndex); }

    /**
     * Get the target resource.
     * @throws Throws std::runtime_error if the resource could 
     * not be found.
     */
    const ResourceT &get() const
    { throw std::runtime_error("TODO"); }

    /**
     * Get the target resource. This version always gets a resource 
     * which can be used. When a target resource is not found a 
     * dummy resource is returned in its stead.
     */
    const ResourceT &getOrDefault() const
    { throw std::runtime_error("TODO"); }
private:
    // Creation of new handles should be in hands of the manager.
    friend ManagerT;

    /// Create invalid handle.
    ResourceHandle() = default;

    /**
     * Create new handle with pointing to specified resource.
     * @param mgr Pointer to the manager holding the resource.
     * @param idx Index valid within the provided manager.
     */
    ResourceHandle(ManagerT *mgr, IndexT idx) :
        mMgr{ mgr }, mIndex{ idx }
    { }

    /// Manager containing the target object.
    ManagerT *mMgr{ nullptr };

    /// Unique index specifying the target object.
    IndexT mIndex{ 0u };
protected:
}; // class ResourceHandle

} // namespace res

} // namespace engine
