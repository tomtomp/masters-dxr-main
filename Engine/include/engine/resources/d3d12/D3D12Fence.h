/**
* @file resources/d3d12/D3D12Fence.h
* @author Tomas Polasek
* @brief Wrapper around Direct3D 12 fence.
*/

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/d3d12/D3D12Device.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Wrapper around a Direct3D 12 fence. 
 */
class D3D12Fence : BaseResource
{
public:
    // Empty wrapper.
    D3D12Fence() = default;

    /**
     * Create a new Direct3D 12 fence on specified device.
     * @param device Device to create the fence on.
     * @param initialValue Initial value of the fence.
     * @param dbgName Debug name for this fence.
     * @throws Throws util::winexception containing message about what 
     * went wrong.
     */
    D3D12Fence(D3D12Device &device, uint64_t initialValue = 0u, const wchar_t *dbgName = nullptr);

    /// Free any resources used by this object.
    virtual ~D3D12Fence();

    // No copying.
    D3D12Fence(const D3D12Fence &other) = delete;
    D3D12Fence &operator=(const D3D12Fence &rhs) = delete;

    // Allow moving.
    D3D12Fence(D3D12Fence &&other) = default;
    D3D12Fence &operator=(D3D12Fence &&rhs) = default;

    /**
     * Wait for this fence to reach, or surpass, specified value.
     * @param value Fence value to reach or surpass.
     * @param duration After how many milliseconds should the wait 
     * timeout.
     * @throws Throws util::winexception if setting the wait event 
     * fails.
     */
    void waitForValue(uint64_t value, 
        std::chrono::milliseconds duration = std::chrono::milliseconds::max());

    /**
     * Has the fence reached, or surpassed, specified value?
     * @param value Value to check.
     * @return Returns true, if the fence has already reached 
     * given value.
     */
    bool valueReached(uint64_t value) const
    { return mFence->GetCompletedValue() >= value; }

    /// Get the inner pointer.
    const ComPtr<::ID3D12Fence> &getPtr() const
    { return mFence; }

    /// Get the inner fence.
    ::ID3D12Fence *get() const
    { return mFence.Get(); }

    /// Access the fence.
    const ComPtr<::ID3D12Fence> &operator->() const
    { return mFence; }
private:
    /// Inner command fence.
    ComPtr<::ID3D12Fence> mFence{ nullptr };
    /// Handle for the waiting event.
    HANDLE mEvent{ nullptr };
protected:
}; // class D3D12Fence

} // namespace d3d12

} // namespace res

} // namespace engine
