/**
* @file resources/d3d12/D3D12SwapChain.h
* @author Tomas Polasek
* @brief Wrapper around a Direct3D 12 swap chain.
*/

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/d3d12/D3D12DXGIFactory5.h"
#include "engine/resources/d3d12/D3D12CommandQueueMgr.h"
#include "engine/resources/d3d12/D3D12Resource.h"
#include "engine/resources/d3d12/D3D12DescHeap.h"
#include "engine/resources/win32/Window.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Wrapper around a Direct3D 12 swap chain.
 */
class D3D12SwapChain : BaseResource
{
public:
    /**
     * Create a new flip-discard swap chain for given window. Uses specified 
     * command queue and automatically detects tearing support, which will be 
     * enabled if supported.
     * @param window Window to create the swap chain for, uses clientWidth 
     * and clientHeight of the window to set buffer resolution.
     * @param directQueue Command queue used with this swap chain. Must be of 
     * type D3D12_COMMAND_LIST_TYPE_DIRECT!
     * @param factory Factory used for creating the required objects.
     * @param numBuffers Number of buffers in this swap chain.
     * @param usage Usage of the buffers.
     * @param format Format of the buffers.
     * @param scaling Scaling when the buffer size does not correspond 
     * to the drawing area.
     * @param alphaMode Alpha blending mode.
     * @throw Throws util::winexception if any error occurs. The contained 
     * message details what happened.
     */
    D3D12SwapChain(const win::Window &window, const D3D12CommandQueueMgr &directQueue, 
        const D3D12DXGIFactory5 &factory, uint32_t numBuffers, 
        ::DXGI_USAGE usage = DXGI_USAGE_RENDER_TARGET_OUTPUT, 
        ::DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM, 
        ::DXGI_SCALING scaling = DXGI_SCALING_STRETCH, 
        ::DXGI_ALPHA_MODE alphaMode = DXGI_ALPHA_MODE_UNSPECIFIED);

    /// Clean up and free any resources allocated.
    virtual ~D3D12SwapChain();

    // No copying.
    D3D12SwapChain(const D3D12SwapChain &other) = delete;
    D3D12SwapChain &operator=(const D3D12SwapChain &rhs) = delete;

    // Allow moving.
    D3D12SwapChain(D3D12SwapChain &&other) = default;
    D3D12SwapChain &operator=(D3D12SwapChain &&rhs) = default;

    /**
     * Get index of swap chain's current back buffer.
     * @return Returns index of the current back buffer.
     */
    uint32_t currentBackbufferIndex() const
    { return mSwapChain->GetCurrentBackBufferIndex(); }

    /**
     * Get pointer to the current back buffer.
     * @return Returns pointer to the current backbuffer 
     * in use by this swap chain.
     */
    D3D12Resource &currentBackbuffer();

    /**
     * Get handle to the render target view of 
     * current backbuffer.
     * @return Returns handle to the render target 
     * view of current backbuffer.
     */
    CD3DX12_CPU_DESCRIPTOR_HANDLE currentBackbufferRtv() const;

    /**
     * Present rendered image to the screen.
     * @param syncInterval Specification of how to 
     * synchronize the presentation with vertical 
     * refresh rate of the monitor.
     * @param flags Combination of DXGI_PRESENT_... 
     * flags.
     * @throw Throws util::winexception if any error occurs. The contained 
     * message details what happened.
     */
    void present(uint32_t syncInterval, uint32_t flags);

    /**
     * Resize swap chain buffers to provided size.
     * @param width New width in pixels.
     * @param height New height in pixels.
     * @warning All commands concerning the backbuffers within 
     * this swap chain should be executed before resizing.
     * @throw Throws util::winexception if any error occurs. The contained 
     * message details what happened.
     */
    void resize(uint32_t width, uint32_t height);

    /// Get number of back buffers.
    int numBuffers() const
    { return mSwapChainDesc.BufferCount; }

    /// Get format of the back buffers.
    ::DXGI_FORMAT bufferFormat() const
    { return mSwapChainDesc.Format; }

    /// Get CPU start pointer of RTV descriptor heap.
    ::D3D12_CPU_DESCRIPTOR_HANDLE rtvHeapCpuStart() 
    { return mDescHeap->GetCPUDescriptorHandleForHeapStart(); }

    /// Get GPU start pointer of RTV descriptor heap.
    ::D3D12_GPU_DESCRIPTOR_HANDLE rtvHeapGpuStart() 
    { return mDescHeap->GetGPUDescriptorHandleForHeapStart(); }

    /// Is tearing feature supported?
    bool tearingSupported() const
    { return mTearingSupported; }

    /// Get the inner pointer.
    auto &getPtr() const
    { return mSwapChain; }

    /// Get the inner swap chain.
    auto get() const
    { return mSwapChain.Get(); }

    /// Access the swap chain.
    auto operator->() const
    { return mSwapChain; }
private:
    /**
     * Create a new flip-discard swap chain for given window. 
     * Saves swap chain configuration into mSwapChainDesc member.
     * @param hwnd Window handle this swap chain is created for.
     * @param width Width of the buffers.
     * @param height Height of the buffers.
     * @param queue Command queue used with this swap chain. Must be of 
     * type D3D12_COMMAND_LIST_TYPE_DIRECT!
     * @param factory Factory used for creating the required objects.
     * @param numBuffers Number of buffers in this swap chain.
     * @param usage Usage of the buffers.
     * @param format Format of the buffers.
     * @param scaling Scaling when the buffer size does not correspond 
     * to the drawing area.
     * @param alphaMode Alpha blending mode.
     * @param flags Additional flags.
     * @throw Throws util::winexception if any error occurs. The contained 
     * message details what happened.
     */
    ComPtr<::IDXGISwapChain4> createFlipDiscard(HWND hwnd, uint32_t width, 
        uint32_t height, ::ID3D12CommandQueue *queue, ::IDXGIFactory5 *factory, 
        uint32_t numBuffers, ::DXGI_USAGE usage, ::DXGI_FORMAT format, 
        ::DXGI_SCALING scaling, ::DXGI_ALPHA_MODE alphaMode, UINT flags);

    /**
     * Create Render Target Views for all buffers and compile them 
     * into an array.
     * @param device Device used in creation of this swap chain.
     * @param descHeap Descriptor heap used for storing the 
     * RTV descriptors.
     * @param descHandleSize Size of a single descriptor handle.
     * @param numBuffers Number of buffers in this swap chain.
     * @throw Throws util::winexception if any error occurs. The contained 
     * message details what happened.
     */
    std::vector<D3D12Resource> createBufferRtv(D3D12Device &device, 
        D3D12DescHeap &descHeap, uint32_t descHandleSize, 
        uint32_t numBuffers);

    /// Device used to create this swap chain.
    D3D12Device &mDevice;
    /// Description of contained swap chain.
    ::DXGI_SWAP_CHAIN_DESC1 mSwapChainDesc{ };
    /// Number of buffers in this swap chain.
    const uint32_t mNumBuffers{ 0u };
    /// Size of a single descriptor handle
    const uint32_t mDescHandleSize{ 0u };
    /// Inner swap chain pointer.
    ComPtr<::IDXGISwapChain4> mSwapChain{ nullptr };
    /// Descriptor heap used for storing RTV descriptors.
    D3D12DescHeap mDescHeap;
    /// Array holding pointers to the back buffers.
    std::vector<D3D12Resource> mBackBuffers{ };
    /// Is unlimited frame-rate supported?
    bool mTearingSupported{ false };
protected:
}; // class D3D12SwapChain

} // namespace d3d12

} // namespace res

} // namespace engine
