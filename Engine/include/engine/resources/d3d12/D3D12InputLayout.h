/**
* @file resources/d3d12/D3D12InputLayout.h
* @author Tomas Polasek
* @brief Wrapper around a Direct3D 12 input layout.
*/

#pragma once

#include <vector>

#include "engine/lib/d3dx12.h"
#include "engine/resources/BaseResource.h"

#include "engine/helpers/d3d12/D3D12PipelineStateSOBuilder.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/// Wrapper around Direct3D 12 input layout.
class D3D12InputLayout : BaseResource
{
public:
    /// Uninitialized input layout.
    D3D12InputLayout()
    { /* Automatic */ }

    /// Free resources.
    virtual ~D3D12InputLayout()
    { /* Automatic */ }

    // Allow copying.
    D3D12InputLayout(const D3D12InputLayout &other) = default;
    D3D12InputLayout &operator=(const D3D12InputLayout &rhs) = default;

    // Allow moving.
    D3D12InputLayout(D3D12InputLayout &&other) = default;
    D3D12InputLayout &operator=(D3D12InputLayout &&rhs) = default;

    /**
     * Add input element description to this layout.
     * @param semanticName HLSL semantic name.
     * @param semanticIndex Differentiation between 
     * multiple elements with the same semantic name.
     * @param format Format of the element data.
     * @param inputSlot Index of the input buffer.
     * @param alignedByteOffset Offset in bytes, between 
     * each element.
     * @param inputSlotClass Defines whether the elements 
     * are per-vertex or per-instance.
     * @param instanceDataStepRate Number of instances to 
     * draw with the same per-instance elements.
     */
    void addElement(const char *semanticName, ::UINT semanticIndex, 
        ::DXGI_FORMAT format, ::UINT inputSlot = 0u, 
        ::UINT alignedByteOffset = D3D12_APPEND_ALIGNED_ELEMENT, 
        ::D3D12_INPUT_CLASSIFICATION inputSlotClass = ::D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 
        ::UINT instanceDataStepRate = 0u);

    /// Get pipeline state sub-object for input layout.
    helpers::d3d12::psot::InputLayout ilState() const
    { return ::D3D12_INPUT_LAYOUT_DESC{ layout(), static_cast<::UINT>(size()) }; }

    /// Get pointer to the input layout.
    const ::D3D12_INPUT_ELEMENT_DESC *layout() const noexcept
    { return mLayout.data(); }

    /// Get number of elements in the layout.
    std::size_t size() const noexcept
    { return mLayout.size(); }
private:
    /// Current layout.
    std::vector<::D3D12_INPUT_ELEMENT_DESC> mLayout;
protected:
}; // class D3D12InputLayout

} // namespace d3d12

} // namespace res

} // namespace engine

// Template implementation

namespace engine
{

namespace res
{

namespace d3d12
{

inline void D3D12InputLayout::addElement(const char *semanticName, ::UINT semanticIndex, ::DXGI_FORMAT format,
    ::UINT inputSlot, ::UINT alignedByteOffset, ::D3D12_INPUT_CLASSIFICATION inputSlotClass,
    ::UINT instanceDataStepRate)
{
    mLayout.emplace_back(
        ::D3D12_INPUT_ELEMENT_DESC{ 
            semanticName, semanticIndex, format, 
            inputSlot, alignedByteOffset, 
            inputSlotClass, instanceDataStepRate });
}

}

}

}

// Template implementation end
