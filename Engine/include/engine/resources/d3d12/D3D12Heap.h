/**
* @file resources/d3d12/D3D12Heap.h
* @author Tomas Polasek
* @brief Wrapper around Direct3D 12 heap.
*/

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/d3d12/D3D12Device.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Wrapper around a Direct3D 12 heap. 
 */
class D3D12Heap : BaseResource
{
public:
    /**
     * Create heap according to the provided 
     * description structure.
     * @param device Device to create the heap on.
     * @param desc Description of the requested 
     * heap.
     */
    D3D12Heap(D3D12Device &device, const ::CD3DX12_HEAP_DESC &desc);

    /**
     * Create heap according to the provided 
     * description structure.
     * @param device Device to create the heap on.
     * @param desc Description of the requested 
     * heap.
     */
    D3D12Heap(D3D12Device &device, const ::D3D12_HEAP_DESC &desc);

    /// Heap is automatically released.
    virtual ~D3D12Heap() = default;

    // No copying.
    D3D12Heap(const D3D12Heap &other) = delete;
    D3D12Heap &operator=(const D3D12Heap &other) = delete;

    // Allow moving.
    D3D12Heap(D3D12Heap &&other) = default;
    D3D12Heap &operator=(D3D12Heap &&other) = default;

    /// Get the inner pointer.
    const ComPtr<::ID3D12Heap> &getPtr() const
    { return mHeap; }

    /// Get the inner device.
    ::ID3D12Heap *get() const
    { return mHeap.Get(); }

    /// Access the device.
    const ComPtr<::ID3D12Heap> &operator->() const
    { return mHeap; }
private:
    /**
     * Create heap on given device.
     * @param device Device to create the heap on.
     * @param desc Requested heap specification.
     * @throws Throws util::winexception on error.
     */
    ComPtr<::ID3D12Heap> createHeap(D3D12Device &device, const ::CD3DX12_HEAP_DESC &desc);

    /// The inner heap pointer.
    ComPtr<::ID3D12Heap> mHeap;
    /// Description of the contained heap.
    ::CD3DX12_HEAP_DESC mDesc;
protected:
}; // class D3D12Heap

} // namespace d3d12

} // namespace res

} // namespace engine
