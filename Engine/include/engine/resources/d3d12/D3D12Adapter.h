/**
* @file resources/d3d12/D3D12Adapter.h
* @author Tomas Polasek
* @brief Direct3D 12 adapter wrapper.
*/

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/d3d12/D3D12DXGIFactory5.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/// Resource wrapper for Direct3D 12 adapter.
class D3D12Adapter : BaseResource
{
public:
    /// Empty wrapper.
    D3D12Adapter() = default;

    /**
     * Choose a suitable Direct3D 12 adapter. The main parameter for 
     * choosing the adapter is presence of requested features. Second 
     * parameter used for choosing the adapter is available video memory, 
     * where the more memory adapter has the more powerful it probably is. 
     * If useWarp is set to true, then instead of choosing physical 
     * adapter a virtual Windows Advanced Rasterization Platform adapter 
     * is chosen.
     * @param factory Factory used for the creation of objects.
     * @param useWarp Whether Windows Advanced Rasterization Platform 
     * should be used.
     * @param features Required features to be supported by the chosen adapter.
     * @throws Throws util::winexception containing message about what 
     * went wrong.
     */
    D3D12Adapter(D3D12DXGIFactory5 &factory, bool useWarp, ::D3D_FEATURE_LEVEL features);

    /// Free resources used by this object.
    virtual ~D3D12Adapter();

    // No copying.
    D3D12Adapter(const D3D12Adapter &other) = delete;
    D3D12Adapter &operator=(const D3D12Adapter &rhs) = delete;

    // Allow moving.
    D3D12Adapter(D3D12Adapter &&other) = default;
    D3D12Adapter &operator=(D3D12Adapter &&rhs) = default;

    /// Get the inner pointer.
    const ComPtr<::IDXGIAdapter3> &getPtr() const
    { return mAdapter; }

    /// Get the inner adapter.
    ::IDXGIAdapter3 *get() const
    { return mAdapter.Get(); }

    /// Access the adapter.
    const ComPtr<::IDXGIAdapter3> &operator->() const
    { return mAdapter; }

    /// Print information about specified adapter.
    static void printAdapterInfo(const ::DXGI_ADAPTER_DESC1 &adapterDesc);
private:
    /// Inner adapter pointer.
    ComPtr<::IDXGIAdapter3> mAdapter{ nullptr };
protected:
}; // class D3D12Adapter

} // namespace d3d12

} // namespace res

} // namespace engine
