/**
* @file resources/d3d12/D3D12Shader.h
* @author Tomas Polasek
* @brief Wrapper around a Direct3D 12 shader.
*/

#pragma once

#include <string>
#include <vector>

#include <d3d12shader.h>
#include <d3dcommon.h>

#include "engine/resources/BaseResource.h"

#include "engine/helpers/d3d12/D3D12PipelineStateSOBuilder.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/// Include handler wrapper for Direct3D 12 shader compilation.
class D3D12ShaderInclude
{
public:
    /// Use default include handler.
    D3D12ShaderInclude();
    /// Use provided include handler.
    explicit D3D12ShaderInclude(::ID3DInclude *handler) noexcept;

    /// Get pointer to the handler instance.
    ::ID3DInclude *handler() const noexcept
    { return mIncludePtr; }
private:
    /// Pointer to the handler.
    ::ID3DInclude *mIncludePtr;
protected:
}; // class D3D12ShaderInclude

/// Macro definitions for Direct3D 12 shader compilation.
class D3D12ShaderDefines
{
public:
    /// Empty defines list.
    D3D12ShaderDefines() = default;

    /**
     * Add a new define to the list of defines.
     * @param define Define specification.
     */
    void addDefine(const ::D3D_SHADER_MACRO &define);

    /**
     * Get pointer to NULL terminated list of 
     * defines.
     * @return Returns pointer to the first element 
     * in NULL terminated list. If the list is 
     * empty, nullptr is returned instead.
     */
    const ::D3D_SHADER_MACRO *defines() const noexcept
    { return mDefines.empty() ? nullptr : mDefines.data(); }
private:
    /// List of defines.
    std::vector<::D3D_SHADER_MACRO> mDefines;
protected:
}; // class D3D12ShaderDefines

/// Wrapper around Direct3D 12 shader.
class D3D12Shader : BaseResource
{
public:
    /// Uninitialized shader.
    D3D12Shader();

    /// Free resources.
    virtual ~D3D12Shader();

    // No copying.
    D3D12Shader(const D3D12Shader &other) = delete;
    D3D12Shader &operator=(const D3D12Shader &rhs) = delete;

    // Allow moving.
    D3D12Shader(D3D12Shader &&other) = default;
    D3D12Shader &operator=(D3D12Shader &&rhs) = default;

    /**
     * Load a pre-compiled shader from given file.
     * @param filename Name of the file which contains compiled 
     * shader code. Commonly used extension is ".cso"
     * @throws Throws util::winexception on error.
     */
    explicit D3D12Shader(const std::wstring &filename);

    /**
     * Load source code from file and compile it into a 
     * shader.
     * @param filename Name of the file to load.
     * @param entryPoint String representing the name of the 
     * main function of the shader.
     * @param target Target specification for the shader, for 
     * example "ps_5_0" is pixel shader, with shader model 5.
     * @param compilerFlags Flags passed to the compiler.
     * @param defines Defines used in compilation of this 
     * shader.
     * @param include Include handler, default value allows 
     * including of files from the current directory.
     * @throws Throws util::winexception on error.
     */
    D3D12Shader(const std::wstring &filename, 
        const std::string &entryPoint, const std::string &target, 
        ::UINT compilerFlags = 0u, 
        const D3D12ShaderDefines &defines = { }, 
        const D3D12ShaderInclude &include = { });

    /**
     * Load a pre-compiled shader from given file.
     * @param filename Name of the file which contains compiled 
     * shader code. Commonly used extension is ".cso"
     * @throws Throws util::winexception on error.
     */
    void precompiledFromFile(const std::wstring &filename);

    /**
     * Load source code from file and compile it into a 
     * shader.
     * @param filename Name of the file to load.
     * @param entryPoint String representing the name of the 
     * main function of the shader.
     * @param target Target specification for the shader, for 
     * example "ps_5_0" is pixel shader, with shader model 5.
     * @param compilerFlags Flags passed to the compiler.
     * @param defines Defines used in compilation of this 
     * shader.
     * @param include Include handler, default value allows 
     * including of files from the current directory.
     * @throws Throws util::winexception on error.
     */
    void compileFromFile(const std::wstring &filename, 
        const std::string &entryPoint, const std::string &target, 
        ::UINT compilerFlags = 0u, 
        const D3D12ShaderDefines &defines = { }, 
        const D3D12ShaderInclude &include = { });

    /**
     * Compile source code into a shader.
     * @param source Source code of the shader.
     * @param entryPoint String representing the name of the 
     * main function of the shader.
     * @param target Target specification for the shader, for 
     * example "ps_5_0" is pixel shader, with shader model 5.
     * @param compilerFlags Flags passed to the compiler.
     * @param defines Defines used in compilation of this 
     * shader.
     * @param include Include handler, default value allows 
     * including of files from the current directory.
     * @throws Throws util::winexception on error.
     */
    void compileFromSource(const std::wstring &source, 
        const std::string &entryPoint, const std::string &target, 
        ::UINT compilerFlags = 0u, 
        const D3D12ShaderDefines &defines = { }, 
        const D3D12ShaderInclude &include = { });

    /// Get pipeline state sub-object for vertex shader.
    helpers::d3d12::psot::VertexShader vsState() const
    { ASSERT_FAST(mType == ::D3D12_SHVER_VERTEX_SHADER); return getBytecode(); }
    /// Get pipeline state sub-object for hull shader.
    helpers::d3d12::psot::HullShader hsState() const
    { ASSERT_FAST(mType == ::D3D12_SHVER_HULL_SHADER); return getBytecode(); }
    /// Get pipeline state sub-object for domain shader.
    helpers::d3d12::psot::DomainShader dsState() const
    { ASSERT_FAST(mType == ::D3D12_SHVER_DOMAIN_SHADER); return getBytecode(); }
    /// Get pipeline state sub-object for geometry shader.
    helpers::d3d12::psot::GeometryShader gsState() const
    { ASSERT_FAST(mType == ::D3D12_SHVER_GEOMETRY_SHADER); return getBytecode(); }
    /// Get pipeline state sub-object for pixel shader.
    helpers::d3d12::psot::PixelShader psState() const
    { ASSERT_FAST(mType == ::D3D12_SHVER_PIXEL_SHADER); return getBytecode(); }
    /// Get pipeline state sub-object for compute shader.
    helpers::d3d12::psot::ComputeShader csState() const
    { ASSERT_FAST(mType == ::D3D12_SHVER_COMPUTE_SHADER); return getBytecode(); }

    /// Check validity of contained resource.
    bool valid() const
    { return operator bool(); }

    /// Check validity of contained resource.
    explicit operator bool() const
    { return mShader; }
private:
    /// Get byte code of the current shader.
    ::CD3DX12_SHADER_BYTECODE getBytecode() const
    { return CD3DX12_SHADER_BYTECODE(mShader.Get()); }

    /**
     * Process result of shader compilation.
     * @param result Result code returned by the compilation 
     * function.
     * @param messages Buffer which would contain error 
     * messages.
     * @throws Throws util::winexception if FAILED(result) == true.
     */
    void processCompilationResult(HRESULT result, ID3DBlob *messages);

    /**
     * Extract information from the current shader 
     * blob, saved in the mShader member.
     * Uses D3D shader reflection.
     * @throws Throws util::winexception on error.
     */
    void extractInformation();

    /// Compiled shader blob.
    ComPtr<::ID3DBlob> mShader;
    /// Type of the shader, e.g. pixel shader.
    ::D3D12_SHADER_VERSION_TYPE mType{ };
protected:
}; // class D3D12Shader

} // namespace d3d12

} // namespace res

} // namespace engine

// Template implementation

namespace engine
{

namespace res
{

namespace d3d12
{

}

}

}

// Template implementation end
