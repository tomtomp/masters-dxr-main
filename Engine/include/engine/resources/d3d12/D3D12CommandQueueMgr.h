/**
* @file resources/d3d12/D3D12CommandQueueMgr.h
* @author Tomas Polasek
* @brief Dispenser of command lists, handles command queue and allocator creation.
*/

#pragma once

#include "engine/resources/BaseResource.h"
#include "engine/resources/BaseResourceMgr.h"

#include "engine/resources/d3d12/D3D12Device.h"
#include "engine/resources/d3d12/D3D12Fence.h"

#include "engine/helpers/d3d12/D3D12Helpers.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

class D3D12CommandList;

/**
 * Class allowing the creation of multiple command lists. 
 * Contains automatic management of a single command queue 
 * and multiple command allocators.
 */
class D3D12CommandQueueMgr : BaseResourceMgr
{
public:
    /**
     * Initialize command queue manager of specified type. All resources 
     * will be created on provided device, which has to exist for the 
     * lifetime of this manager.
     * @param device Device used in creation of all sub-objects.
     * @param type Type of the command queue, specifying what type 
     * of commands can be used.
     * @param dbgName Debug name used for the command queue, list and 
     * allocators.
     * @throws Throws util::winexception on error, which 
     * contains error message specifying what happened.
     */
    D3D12CommandQueueMgr(D3D12Device &device, D3D12_COMMAND_LIST_TYPE type, 
        const wchar_t *dbgName = L"CommandQueue");

    /// Free resources used by the manager.
    virtual ~D3D12CommandQueueMgr() = default;

    // No copying.
    D3D12CommandQueueMgr(const D3D12CommandQueueMgr &other) = delete;
    D3D12CommandQueueMgr &operator=(const D3D12CommandQueueMgr &rhs) = delete;

    // No moving.
    D3D12CommandQueueMgr(D3D12CommandQueueMgr &&other) = delete;
    D3D12CommandQueueMgr &operator=(D3D12CommandQueueMgr &&rhs) = delete;

    /**
     * Execute given command list and return without waiting for 
     * execution to finish.
     * @param list List to execute.
     * @return Returns a value which can be used with fenceWaitFor method.
     */
    uint64_t executeNoWait(D3D12CommandList *list);

    /**
     * Execute given command list and wait for it to finish executing.
     * @param list List to execute.
     */
    void executeAndWait(D3D12CommandList *list);

    /**
     * Add given command list for later execution.
     * @param list List to add for later execution.
     */
    void addForBatchExecution(D3D12CommandList *list);

    /**
     * Reset given command list, allowing it to be reused 
     * without executing recorded commands.
     * @param list List to reset.
     */
    void reset(D3D12CommandList *list);

    /**
     * Execute all prepared batch jobs in this command queue 
     * and return without waiting.
     * @return Returns a value which can be used with 
     * fenceWaitFor method. If no batch jobs are present, the 
     * returned value is 0.
     */
    uint64_t batchExecuteNoWait();

    /**
     * Execute all prepared batch jobs in this command queue 
     * and wait for them to finish.
     */
    void batchExecuteAndWait();

    /**
     * Wait for the queue fence to reach, or surpass, given value.
     * @param value Value to wait for.
     */
    void fenceWaitFor(uint64_t value);

    /**
     * Test whether the command queue fence reached, or 
     * surpassed, specified value.
     * @param value Value to test for.
     * @return Returns true if the value has been 
     * reached or surpassed.
     */
    bool fenceValueReached(uint64_t value) const;

    /**
     * Add signal command to the queue and return a value to wait 
     * for.
     * @return Returns a value which can be used with fenceWaitFor method.
     */
    uint64_t fenceSignal();

    /**
     * Wait for all commands currently in the queue to finish.
     */
    void flush();

    /**
     * Get a command list which can be used to record commands.
     * @param state Initial pipeline state, nullptr for 
     * dummy state.
     * @return Returns command list.
     * @throws Throws util::winexception which contains message 
     * with further details about the error.
     */
    D3D12CommandList getCommandList(ID3D12PipelineState *state = nullptr);

    /// Get the inner pointer.
    const ComPtr<::ID3D12CommandQueue> &getPtr() const
    { return mCommandQueue; }

    /// Get the inner command queue.
    ::ID3D12CommandQueue *get() const
    { return mCommandQueue.Get(); }

    /// Access the command queue.
    const ComPtr<::ID3D12CommandQueue> &operator->() const
    { return mCommandQueue; }

    /// Access device used in creation of this command queue.
    D3D12Device &device()
    { return mDevice; }

    /// Access device used in creation of this command queue.
    D3D12Device &device() const
    { return mDevice; }
private:
    /**
     * Setup debug name for the queue, list and allocator.
     * @param dbgName Debug name used for the command queue, list and 
     * allocators.
     * @throws Throws util::winexception on error, which 
     * contains error message specifying what happened.
     */
    void setDebugName(const wchar_t *dbgName);

    /**
     * Closes command list, executes it and returns without 
     * waiting for it to finish.
     * @param list Command list to execute.
     * @param allocator Allocator used in the command list.
     * @return Returns a value which can be used with fenceWaitFor method.
     * @throws Throws util::winexception on error, detailing what 
     * error occurred.
     */
    uint64_t executeNoWait(ComPtr<::ID3D12GraphicsCommandList2> &list, 
        ComPtr<::ID3D12CommandAllocator> &allocator);

    /**
     * Closes command list, executes it and waits for it to finish.
     * @param list Command list to execute.
     * @param allocator Allocator used in the command list.
     * @throws Throws util::winexception on error, detailing what 
     * error occurred.
     */
    void executeAndWait(ComPtr<::ID3D12GraphicsCommandList2> &list, 
        ComPtr<::ID3D12CommandAllocator> &allocator);

    /**
     * Closes command list and adds it for later execution.
     * @param list Command list to add for later execution.
     * @param allocator Allocator used in the command list.
     * @throws Throws util::winexception on error, detailing what 
     * error occurred.
     */
    void addForBatchExecution(ComPtr<::ID3D12GraphicsCommandList2> &list, 
        ComPtr<::ID3D12CommandAllocator> &allocator);

    /**
     * Reset given command list, allowing it to be reused 
     * without executing recorded commands.
     * @param list Command list to reset.
     * @param allocator Allocator used in the command list.
     */
    void reset(ComPtr<::ID3D12GraphicsCommandList2> &list, 
        ComPtr<::ID3D12CommandAllocator> &allocator);

    /**
     * Add specified command list to the reuse storage.
     * @param list List to return to the storage.
     */
    void recoverCommandList(ComPtr<::ID3D12GraphicsCommandList2> &list);

    /**
     * Add specified command allocator to the reuse storage.
     * @param allocator Allocator to return to the storage.
     * @param fenceValue Fence value which signalizes the allocator can 
     * be reused again. Each value has to be at most equal to the current 
     * back of the queue.
     */
    void recoverCommandAllocator(ComPtr<::ID3D12CommandAllocator> &allocator, uint64_t fenceValue);

    /**
     * Add specified command allocator to the reuse storage. This 
     * version uses fence value of 0, which allows immediate reuse.
     * @param allocator Allocator to return to the storage.
     */
    void recoverCommandAllocator(ComPtr<::ID3D12CommandAllocator> &allocator);

    /**
     * Get used or create new command allocator.
     * @return Returns command allocator pointer.
     * @throws Throws util::winexception which contains message 
     * with further details about the error.
     */
    ComPtr<::ID3D12CommandAllocator> getCreateAllocator();

    /**
     * Get used or create new command list.
     * @param allocator Allocator to use with this list.
     * @param state Initial pipeline state, nullptr 
     * for dummy state.
     * @return Returns command list pointer.
     * @throws Throws util::winexception which contains message 
     * with further details about the error.
     */
    ComPtr<::ID3D12GraphicsCommandList2> getCreateList(ComPtr<::ID3D12CommandAllocator> &allocator, 
        ID3D12PipelineState *state = nullptr);

    /// Device where this command queue lives on.
    D3D12Device &mDevice;
    /// Current fence value.
    uint64_t mFenceValue;
    /// Fence used for synchronization of this queue.
    D3D12Fence mFence;
    /// Type of the command queue.
    D3D12_COMMAND_LIST_TYPE mType;
    /// Inner command queue pointer.
    ComPtr<::ID3D12CommandQueue> mCommandQueue;

    /// Helper structure for keeping information about when allocator can be re-used.
    struct AllocatorFence
    {
        // No copying.
        AllocatorFence(const AllocatorFence &other) = delete;
        AllocatorFence &operator=(const AllocatorFence &other) = delete;

        // Only moving.
        AllocatorFence(AllocatorFence &&other) = default;
        AllocatorFence &operator=(AllocatorFence &&other) = default;

        /// Pointer to the allocator in question.
        ComPtr<::ID3D12CommandAllocator> allocator{ nullptr };
        /// Fence value when the allocator can be reused.
        uint64_t fenceValue{ 0u };
    }; // struct AllocatorFence
    /// Storage for in-use allocators.
    std::deque<AllocatorFence> mAllocatorStorage;

    /// Storage for reusable command lists.
    std::queue<ComPtr<::ID3D12GraphicsCommandList2>> mListStorage;

    /// Helper structure for batch jobs.
    struct BatchJob
    {
        // No copying.
        BatchJob(const BatchJob &other) = delete;
        BatchJob &operator=(const BatchJob &other) = delete;

        // Only moving.
        BatchJob(BatchJob &&other) = default;
        BatchJob &operator=(BatchJob &&other) = default;

        /// Pointer to list containing the commands.
        ComPtr<::ID3D12GraphicsCommandList2> list{ nullptr };
        /// Pointer to allocator used.
        ComPtr<::ID3D12CommandAllocator> allocator{ nullptr };
    }; // struct BatchJob
    /// Queue containing batch jobs stored for later execution.
    std::vector<BatchJob> mBatchJobs;

#ifdef _DEBUG
    /// Debug name of the command queue.
    std::wstring mDbgName;
    /// Debug name of the command allocator.
    std::wstring mDbgAllocName;
    /// Debug name of the command list.
    std::wstring mDbgListName;
#endif
protected:
}; // class D3D12CommandQueueMgr

/// Helper object used for passing command list around.
class D3D12CommandList : BaseResource
{
    friend class D3D12CommandQueueMgr;
public:
    /// Return resources back to command list queue WITHOUT execution.
    virtual ~D3D12CommandList()
    {
        if (mMgr)
        {
            reset();
        }
    }

    /// Creates unbound command list, unusable!
    D3D12CommandList() = default;

    // No copying
    D3D12CommandList(const D3D12CommandList &other) = delete;
    D3D12CommandList &operator=(const D3D12CommandList &other) = delete;

    // Allow moving.
    D3D12CommandList(D3D12CommandList &&other) :
        D3D12CommandList()
    {
        std::swap(mMgr, other.mMgr);
        mList = std::move(other.mList);
        mAllocator = std::move(other.mAllocator);
    }

    D3D12CommandList &operator=(D3D12CommandList &&other)
    {
        D3D12CommandList moveCopy(std::forward<D3D12CommandList>(other));

        std::swap(mMgr, moveCopy.mMgr);
        mList = std::move(moveCopy.mList);
        mAllocator = std::move(moveCopy.mAllocator);

        return *this;
    }

    /**
     * Execute this command list and return without waiting for 
     * execution to finish.
     * @return Returns value which can be used with the command queue 
     * manager fence to wait for this command list to finish executing.
     * @warning Can be performed only once, after which this object 
     * self-destructs.
     */
    uint64_t executeNoWait()
    {
        guard();
        const auto fenceValue{ mMgr->executeNoWait(this) };
        selfDestruct();
        return fenceValue;
    }

    /**
     * Execute this command list and wait for it to finish.
     * @warning Can be performed only once, after which this object 
     * self-destructs.
     */
    void executeAndWait()
    {
        guard();
        //mMgr->executeNoWait(this);
        mMgr->executeAndWait(this);
        selfDestruct();
    }

    /**
     * Add this command list for later execution in the 
     * command queue manager.
     * @warning Can be performed only once, after which this object 
     * self-destructs.
     */
    void addForBatchExecution()
    {
        guard();
        mMgr->addForBatchExecution(this);
        selfDestruct();
    }

    /**
     * Reset this command list and return it to the command 
     * queue manager.
     * @warning Can be performed only once, after which this object 
     * self-destructs.
     */
    void reset()
    {
        guard();
        mMgr->reset(this);
        selfDestruct();
    }

    /// Get the inner command list pointer.
    const ComPtr<::ID3D12GraphicsCommandList2> &getPtr() const
    { return mList; }

    /// Get the inner command list.
    ::ID3D12GraphicsCommandList2 *get() const
    { return mList.Get(); }

    /// Access the command list.
    const ComPtr<::ID3D12GraphicsCommandList2> &operator->() const
    { return mList; }
private:
    /**
     * Create a wrapper around a given command list and its allocator.
     * @param queueMgr Command queue manager, which created this command list.
     * @param cmdList Command list pointer.
     * @param cmdAllocator Command allocator pointer.
     */
    D3D12CommandList(D3D12CommandQueueMgr *queueMgr, 
        ComPtr<::ID3D12GraphicsCommandList2> cmdList, ComPtr<::ID3D12CommandAllocator> cmdAllocator) : 
        mMgr{ queueMgr }, mList{ cmdList }, mAllocator{ cmdAllocator }
    { ASSERT_FAST(queueMgr && cmdList && cmdAllocator); }

    /**
     * Check if this command list has already been used, if it was 
     * throw a runtime_error.
     * @throws Throws std::runtime_error if the object has been destructed.
     */
    void guard() const
    {
        if (!mMgr)
        {
            throw std::runtime_error("Unable to work with destructed D3D12CommandList!");
        }
    }

    /**
     * Set all values to default, making this object unusable.
     */
    void selfDestruct()
    {
        // Reset the pointers.
        mAllocator.Reset();
        mList.Reset();
        mMgr = nullptr;
    }

    /// Manager which created this command list.
    D3D12CommandQueueMgr *mMgr{ nullptr };
    /// Command list itself.
    ComPtr<::ID3D12GraphicsCommandList2> mList{ nullptr };
    /// Command allocator used in the command list.
    ComPtr<::ID3D12CommandAllocator> mAllocator{ nullptr };
protected:
}; // class D3D12CommandList

} // namespace d3d12

} // namespace res

} // namespace engine
