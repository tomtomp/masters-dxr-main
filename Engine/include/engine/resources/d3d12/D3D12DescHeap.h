/**
* @file resources/d3d12/D3D12DescHeap.h
* @author Tomas Polasek
* @brief Wrapper around Direct3D 12 descriptor heap.
*/

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/d3d12/D3D12Device.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 helpers.
namespace d3d12
{

// Forward declaration.
class D3D12Resource;

/// Wrapper around Direct3D 12 descriptor heap.
class D3D12DescHeap : BaseResource
{
public:
    /// Exception thrown when the descriptor heap cannot contain any more handles.
    struct DescHeapFull : public std::exception
    {
        DescHeapFull(const char *msg) :
            std::exception(msg)
        { }
    }; // struct DescHeapFull

    /// Un-initialized descriptor heap.
    D3D12DescHeap();

    /**
     * Create a descriptor heap with provided parameters.
     * @param device Where should the descriptor heap be created.
     * @param type Type of the descriptor heap.
     * @param numDescriptors Maximal number of descriptors creatable 
     * on the heap.
     * @param flags Flags for the descriptor heap.
     * @throws Throws util::winexception when error occurs.
     */
    D3D12DescHeap(D3D12Device &device, ::D3D12_DESCRIPTOR_HEAP_TYPE type, 
        uint32_t numDescriptors, D3D12_DESCRIPTOR_HEAP_FLAGS flags);

    /**
     * Create a descriptor heap with provided parameters.
     * @param device Where should the descriptor heap be created.
     * @param desc Description of requested descriptor heap.
     * @throws Throws util::winexception when error occurs.
     */
    D3D12DescHeap(D3D12Device &device, const ::D3D12_DESCRIPTOR_HEAP_DESC &desc);

    /**
     * Create a descriptor heap with provided parameters.
     * @param device Where should the descriptor heap be created.
     * @param desc Description of requested descriptor heap.
     * @throws Throws util::winexception when error occurs.
     */
    void initializeHeap(D3D12Device &device, const ::D3D12_DESCRIPTOR_HEAP_DESC &desc);

    /// Descriptor heap is automatically released.
    virtual ~D3D12DescHeap() = default;

    // No copying.
    D3D12DescHeap(const D3D12DescHeap &other) = delete;
    D3D12DescHeap &operator=(const D3D12DescHeap &other) = delete;

    // Allow moving.
    D3D12DescHeap(D3D12DescHeap &&other) = default;
    D3D12DescHeap &operator=(D3D12DescHeap &&other) = default;

    /// Get the inner pointer.
    const ComPtr<::ID3D12DescriptorHeap> &getPtr() const
    { return mDescHeap; }

    /// Get the inner descriptor heap.
    ::ID3D12DescriptorHeap *get() const
    { return mDescHeap.Get(); }

    /// Access the descriptor heap.
    const ComPtr<::ID3D12DescriptorHeap> &operator->() const
    { return mDescHeap; }

    /// Get size of a single descriptor handle.
    uint32_t descHandleSize() const
    { return mDescHandleSize; }

    /// Is this descriptor heap initialized?
    bool initialized() const
    { return mDescHeap; }

    /// Is this descriptor heap initialized?
    explicit operator bool() const
    { return initialized(); }

    /**
     * Get CPU pointer to the next unallocated descriptor 
     * handle.
     * @return Returns CPU pointer to the next descriptor 
     * handle.
     * @throws Throws DescHeapFull if the descriptor heap 
     * is already full.
     */
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE nextHandle();

    /**
     * Reset the current handle to the beginning of the 
     * descriptor heap. After calling this method, the 
     * descriptor handles previously created by it should 
     * not be used any more.
     */
    void resetHandle();

    /**
     * Create depth-stencil view handler on this descriptor heap.
     * @param device Target device, should be the same as the one 
     * the descriptor heap was created with.
     * @param resource Create depth-stencil view for this resource.
     * @param desc Description of the depth stencil view.
     * @throws Throws DescHeapFull if the descriptor heap 
     * is already full.
     */
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE createDSV(D3D12Device &device, D3D12Resource &resource, const D3D12_DEPTH_STENCIL_VIEW_DESC &desc);
private:
    /**
     * Create a descriptor heap with provided parameters.
     * @param device Where should the descriptor heap be created.
     * @param desc Description of requested descriptor heap.
     * @return Returns the created descriptor heap.
     * @throws Throws util::winexception when error occurs.
     */
    ComPtr<::ID3D12DescriptorHeap> createDescHeap(D3D12Device &device, const ::D3D12_DESCRIPTOR_HEAP_DESC &desc);

    /// Size of a single descriptor handle.
    uint32_t mDescHandleSize{ 0u };
    /// Inner pointer to the descriptor heap.
    ComPtr<::ID3D12DescriptorHeap> mDescHeap{ nullptr };
    /// Current position of the descriptor iterator.
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE mCurrentIt{ };
    /// End iterator for the descriptor handles.
    ::CD3DX12_CPU_DESCRIPTOR_HANDLE mEnd{ };
protected:
}; // class D3D12DescHeap

} // namespace d3d12

} // namespace res

} // namespace engine
