/**
* @file resources/d3d12/D3D12DXGIFactory5.h
* @author Tomas Polasek
* @brief Wrapper around DXGI factory.
*/

#pragma once

#include "engine/resources/BaseResource.h"

// DXGI 1.5 is required for CheckFeatureSupport.
#include <dxgi1_5.h>

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/// Resource wrapper for DXGI factory.
class D3D12DXGIFactory5 : BaseResource
{
public:
    /**
     * Create IDXGIFactory5, includes debug flags, if _DEBUG macro is set.
     * If _DEBUG is set, this constructor also 
     * @throw Throws util::winexception if any error occurs. The contained 
     * message details what happened.
     */
    D3D12DXGIFactory5();

    /// Free resources used by this object.
    virtual ~D3D12DXGIFactory5() = default;

    // No copying.
    D3D12DXGIFactory5(const D3D12DXGIFactory5 &other) = delete;
    D3D12DXGIFactory5 &operator=(const D3D12DXGIFactory5 &rhs) = delete;

    // No moving.
    D3D12DXGIFactory5(D3D12DXGIFactory5 &&other) = default;
    D3D12DXGIFactory5 &operator=(D3D12DXGIFactory5 &&rhs) = default;

    /**
     * Check driver for a specified feature. 
     * @param feature Which feature should be checked.
     * @return Returns whether the driver supports given feature.
     */
    bool isFeatureSupported(DXGI_FEATURE feature) const;

    /**
     * Check driver for tearing support. Tearing is required for variable 
     * refresh rate displays - FreeSync and G-Sync.
     * @return Returns whether the driver supports tearing.
     */
    bool isTearingSupported() const
    { return isFeatureSupported(DXGI_FEATURE_PRESENT_ALLOW_TEARING); }

    /**
     * Disable some of message handling done by DXGI.
     * @param hwnd Window which will have handling disabled.
     * @param flags Which messages will not be handled by 
     * DXGI - e.g. DXGI_MWA_NO_ALT_ENTER.
     * @throw Throws util::winexception if any error occurs. The contained 
     * message details what happened.
     */
    void setMessageHandling(HWND hwnd, UINT flags);

    /// Get the inner pointer.
    const ComPtr<::IDXGIFactory5> &getPtr() const
    { return mFactory; }

    /// Get the inner factory.
    ::IDXGIFactory5 *get() const
    { return mFactory.Get(); }

    /// Access the factory.
    const ComPtr<::IDXGIFactory5> &operator->() const
    { return mFactory; }
private:
    /// Inner pointer.
    ComPtr<::IDXGIFactory5> mFactory{ nullptr };
protected:
}; // class D3D12DXGIFactory5

} // namespace d3d12

} // namespace res

} // namespace engine
