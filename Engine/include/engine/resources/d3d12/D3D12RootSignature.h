/**
* @file resources/d3d12/D3D12RootSignature.h
* @author Tomas Polasek
* @brief Wrapper around a Direct3D 12 root signature.
*/

#pragma once

#include <vector>

#include "engine/resources/BaseResource.h"
#include "engine/resources/d3d12/D3D12Device.h"

#include "engine/helpers/d3d12/D3D12PipelineStateSOBuilder.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

// Forward declaration.
class D3D12RootSignatureBuilder;
    
} // namespace d3d12

} // namespace helpers

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/// Wrapper around Direct3D 12 root signature.
class D3D12RootSignature : BaseResource
{
public:
    /// Uninitialized root signature.
    D3D12RootSignature();

    /// Free resources.
    virtual ~D3D12RootSignature();

    // No copying.
    D3D12RootSignature(const D3D12RootSignature &other) = delete;
    D3D12RootSignature &operator=(const D3D12RootSignature &rhs) = delete;

    // Allow moving.
    D3D12RootSignature(D3D12RootSignature &&other) = default;
    D3D12RootSignature &operator=(D3D12RootSignature &&rhs) = default;

    /**
     * Create root signature using specified data.
     * @param rootParameters List of root parameters.
     * @param staticSamplers List of static samplers.
     * @param flags Flags passed to the root signature.
     * @param device Target device.
     * @throws Throws util::winexception on error.
     */
    D3D12RootSignature(const std::vector<::CD3DX12_ROOT_PARAMETER1> &rootParameters, 
        const std::vector<::CD3DX12_STATIC_SAMPLER_DESC> &staticSamplers,
        ::D3D12_ROOT_SIGNATURE_FLAGS flags, D3D12Device &device);

    /**
     * Create root signature using specified description.
     * @param desc Description of the root signature.
     * @param device Target device.
     * @throws Throws util::winexception on error.
     */
    D3D12RootSignature(const ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC &desc, 
        D3D12Device &device);

    /// Get pipeline state sub-object for root signature.
    helpers::d3d12::psot::RootSignature rsState() const
    { ASSERT_FAST(created()); return mRootSignature.Get(); }

    /// Get the inner pointer.
    const ComPtr<::ID3D12RootSignature> &getPtr() const
    { return mRootSignature; }

    /// Get the inner root signature.
    ::ID3D12RootSignature *get() const
    { return mRootSignature.Get(); }

    /// Access the root signature.
    const ComPtr<::ID3D12RootSignature> &operator->() const
    { return mRootSignature; }

    /// Is this root signature created?
    bool created() const noexcept
    { return mRootSignature; }

    /// Is this root signature created?
    explicit operator bool() const noexcept
    { return created(); }
private:
    // Allow builder access to the special constructor.
    friend class helpers::d3d12::D3D12RootSignatureBuilder;

    /**
     * Create root signature using specified data.
     * @param rootParameters List of root parameters.
     * @param numRootParameters Number of root parameters 
     * in the list.
     * @param staticSamplers List of static samplers.
     * @param numStaticSamplers Number of static samplers 
     * in the list.
     * @param flags Flags passed to the root signature.
     * @param supportsRS11 Does the device support 
     * root signatures with version 1.1?
     * @param device Target device.
     * @throws Throws util::winexception on error.
     */
    D3D12RootSignature(const ::CD3DX12_ROOT_PARAMETER1 *rootParameters, std::size_t numRootParameters, 
        const ::CD3DX12_STATIC_SAMPLER_DESC *staticSamplers, std::size_t numStaticSamplers,
        ::D3D12_ROOT_SIGNATURE_FLAGS flags, bool supportsRS11, D3D12Device &device);

    /**
     * Create root signature using specified description.
     * @param desc Description of the root signature.
     * @param device Target device.
     * @throws Throws util::winexception on error.
     */
    void createRootSignature(const ::CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC &desc, 
        D3D12Device &device);

    /// Inner root signature.
    ComPtr<::ID3D12RootSignature> mRootSignature;
protected:
}; // class D3D12RootSignature

} // namespace d3d12

} // namespace res

} // namespace engine

// Template implementation

namespace engine
{

namespace res
{

namespace d3d12
{

}

}

}

// Template implementation end
