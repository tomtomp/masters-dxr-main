/**
* @file resources/d3d12/D3D12Resource.h
* @author Tomas Polasek
* @brief Wrapper around a Direct3D 12 resource.
*/

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/d3d12/D3D12CommandQueueMgr.h"

#include "engine/helpers/d3d12/D3D12BaseGpuAllocator.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

/// Wrapper around Direct3D 12 resource.
class D3D12Resource : BaseResource
{
public:
    /// Initialize empty resource.
    D3D12Resource() = default;

    /**
     * Wrap around already existing Direct3D 12 
     * resource.
     * The original owner of the resource must NOT 
     * change the state of the resource, unless using 
     * this object.
     * @param res Resource being wrapped.
     * @param currentState Current state of the 
     * resource, at the time of wrapping.
     */
    D3D12Resource(const ComPtr<::ID3D12Resource> &res, ::D3D12_RESOURCE_STATES currentState);

    /**
     * Wrap around already existing Direct3D 12 
     * resource.
     * The original owner of the resource must NOT 
     * change the state of the resource, unless using 
     * this object.
     * @param res Resource being wrapped.
     * @param currentState Current state of the 
     * resource, at the time of wrapping.
     * @param desc Description of the resource.
     */
    D3D12Resource(const ComPtr<::ID3D12Resource> &res, ::D3D12_RESOURCE_STATES currentState, 
        const ::CD3DX12_RESOURCE_DESC &desc);

    /**
     * Create a new resource as specified in the description 
     * and allocate it using provided allocator.
     * @param desc Description of the resource.
     * @param initState Initial state of the resource.
     * @param optimizedClearValue Optimized clearing value 
     * for this resource. Can be nullptr.
     * @param allocator Allocator used in allocation of 
     * this resource.
     * @throws Throws util::winexception if any error 
     * occurs.
     */
    D3D12Resource(const ::CD3DX12_RESOURCE_DESC &desc, ::D3D12_RESOURCE_STATES initState, 
        const ::D3D12_CLEAR_VALUE *optimizedClearValue, helpers::d3d12::D3D12BaseGpuAllocator &allocator);

    /// Free resources.
    virtual ~D3D12Resource();

    // No copying.
    D3D12Resource(const D3D12Resource &other) = delete;
    D3D12Resource &operator=(const D3D12Resource &rhs) = delete;

    // Allow moving.
    D3D12Resource(D3D12Resource &&other) = default;
    D3D12Resource &operator=(D3D12Resource &&rhs) = default;

    /**
     * Transition resource to given state.
     * Transition command will be recorded 
     * onto the specified command list.
     * @param newState Target state to transition to.
     * @param cmdList List to record the transition 
     * command on.
     * @param subResource Sub-resource selection.
     * @param flags Flags for the transition barrier.
     * @throws Throws util::winexception if any error 
     * occurs. The contained message details what 
     * happened.
     */
    void transitionTo(::D3D12_RESOURCE_STATES newState, D3D12CommandList &cmdList, 
        UINT subResource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES, 
        ::D3D12_RESOURCE_BARRIER_FLAGS flags = ::D3D12_RESOURCE_BARRIER_FLAG_NONE);

    /**
     * Get inner pointer without checking whether 
     * the resource is initialized.
     * @return Returns pointer to the resource.
     * @warning The returned pointer may be 
     * nullptr!
     */
    const ComPtr<::ID3D12Resource> &getPtr() const
    {
        ASSERT_FAST(mResource);
        return mResource;
    }

    /**
     * Get inner pointer without checking whether 
     * the resource is initialized.
     * @return Returns pointer to the resource.
     * @warning The returned pointer may be 
     * nullptr!
     */
    ::ID3D12Resource *get() const
    {
        ASSERT_FAST(mResource);
        return mResource.Get();
    }

    /**
     * Access the resource.
     * Performs checking, whether the resource 
     * has been initialized.
     * @throws std::runtime_error if the resource 
     * has not been initialized.
     */
    const ComPtr<::ID3D12Resource> &operator->() const
    {
        if (!valid())
        {
            throw std::runtime_error("Accessing uninitialized resource!");
        }
        return mResource;
    }

    /// Description of this resource.
    const ::CD3DX12_RESOURCE_DESC &desc() const
    { return mDesc; }

    /**
     * Current state of this resource, after 
     * executing all transition barriers.
     */
    ::D3D12_RESOURCE_STATES state() const
    { return mState; }

    /// Check validity of contained resource.
    bool valid() const
    { return operator bool(); }

    /// Check validity of contained resource.
    explicit operator bool() const
    { return mResource; }

    /// Get size of the memory allocated on the GPU, in bytes.
    std::size_t allocatedSize() const;
private:
    // Allow access to current state and resource.
    friend class D3D12Device;

    /**
     * Allocate resource described by provided structure.
     * @param desc Description of the resource.
     * @param initState Initial state of the resource.
     * @param optimizedClearValue Optimized clearing value 
     * for this resource. Can be nullptr.
     * @param allocator Allocator to allocate with.
     * @return Returns the allocated resource.
     * @throws Throws util::winexception on error.
     */
    ComPtr<::ID3D12Resource> allocateResource(const ::CD3DX12_RESOURCE_DESC &desc, ::D3D12_RESOURCE_STATES initState, 
        const ::D3D12_CLEAR_VALUE *optimizedClearValue, helpers::d3d12::D3D12BaseGpuAllocator &allocator);

    /**
     * Destroy this resource, de-allocating it if 
     * allocator was used.
     */
    void destroy();

    /// Inner pointer.
    ComPtr<::ID3D12Resource> mResource{ nullptr };
    /// Allocator used to allocate this resource.
    helpers::d3d12::D3D12BaseGpuAllocator *mAllocator{ nullptr };
    /**
     * Current state of the resource, after 
     * executing all transition barriers.
     */
    ::D3D12_RESOURCE_STATES mState{ D3D12_RESOURCE_STATE_COMMON };
    /// Description of the resource.
    ::CD3DX12_RESOURCE_DESC mDesc;
protected:
}; // class D3D12Resource
} // namespace d3d12

} // namespace res

} // namespace engine

// Template implementation

namespace engine
{

namespace res
{

namespace d3d12
{

}

}

}

// Template implementation end
