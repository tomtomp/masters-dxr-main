/**
* @file resources/d3d12/D3D12Device.h
* @author Tomas Polasek
* @brief Direct3D 12 device wrapper.
*/

#pragma once

#include "engine/resources/BaseResource.h"

#include "engine/resources/d3d12/D3D12Adapter.h"

// D3D12 extension library (Different from <d3d12.h>!!): 
#include "engine/lib/d3dx12.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing resource management helpers.
namespace res
{

/// Direct3D 12 resource management.
namespace d3d12
{

// Resource forward declaration.
class D3D12Resource;

/// Resource wrapper for Direct3D 12 device.
class D3D12Device : BaseResource
{
public:
    // Empty wrapper.
    D3D12Device() = default;

    /**
     * Create Direct3D 12 device from given adapter. If _DEBUG macro is 
     * defined, also configures the debugging layer for this device using 
     * provided disable lists.
     * @param adapter Adapter to create the device from.
     * @param features Minimal feature level required from the device.
     * @param disableCategories Which message categories should be disabled.
     * @param disableSeverities Which message severities should be disabled.
     * @param disableIds Which message IDs should be disabled.
     * @param dbgName Debug name used for the device.
     * @throw Throws util::winexception if any error occurs. The contained 
     * message details what happened.
     */
    D3D12Device(D3D12Adapter &adapter, 
        ::D3D_FEATURE_LEVEL features, 
        const std::set<::D3D12_MESSAGE_CATEGORY> &disableCategories = { },
        const std::set<::D3D12_MESSAGE_SEVERITY> &disableSeverities = { },
        const std::set<::D3D12_MESSAGE_ID> &disableIds = { }, 
        const wchar_t *dbgName = L"Device");

    /// Free resources used by this object.
    virtual ~D3D12Device();

    // No copying.
    D3D12Device(const D3D12Device &other) = delete;
    D3D12Device &operator=(const D3D12Device &rhs) = delete;

    // Allow moving.
    D3D12Device(D3D12Device &&other) = default;
    D3D12Device &operator=(D3D12Device &&rhs) = default;

    /**
     * Create a new fence on this device.
     * @param initialValue Initial value of the fence.
     * @return Returns pointer to the newly created fence.
     * @throws Throws util::winexception on error, which 
     * contains error message specifying what happened.
     */
    ComPtr<::ID3D12Fence> createFence(uint64_t initialValue);

    /**
     * Create a command allocator on this device. 
     * @param type Type of commands which can be allocated 
     * using the allocator.
     * @return Returns pointer to the created allocator.
     * @throws Throws util::winexception on error, which 
     * contains error message specifying what happened.
     */
    ComPtr<::ID3D12CommandAllocator> createCommandAllocator(::D3D12_COMMAND_LIST_TYPE type);

    /**
     * Create a command list on this device. 
     * @param allocator Command allocator of the same 
     * type. Used in allocation of commands for this list.
     * @param type Type of commands which can be listed.
     * @param state Initial pipeline state, nullptr 
     * for dummy state.
     * @return Returns pointer to the created command list.
     * @throws Throws util::winexception on error, which 
     * contains error message specifying what happened.
     */
    ComPtr<::ID3D12GraphicsCommandList2> createGraphicsCommandList2(::ID3D12CommandAllocator* allocator, 
        ::D3D12_COMMAND_LIST_TYPE type, ::ID3D12PipelineState *state = nullptr);

    /**
     * Create a command queue on this device. 
     * @param type Type of commands which can be queued.
     * @param priority Priority of the command queue.
     * @return Returns pointer to the created command queue.
     * @throws Throws util::winexception on error, which 
     * contains error message specifying what happened.
     */
    ComPtr<::ID3D12CommandQueue> createCommandQueue(::D3D12_COMMAND_LIST_TYPE type, 
        ::D3D12_COMMAND_QUEUE_PRIORITY priority = ::D3D12_COMMAND_QUEUE_PRIORITY_NORMAL);

    /**
     * Create a descriptor heap on this device. 
     * @param type Type of descriptor which can be created 
     * on this heap.
     * @param numDescriptors Capacity of the heap.
     * @param flags Heap flags, which can be used to allow 
     * shaders access.
     * @return Returns pointer to the created descriptor heap.
     * @throws Throws util::winexception on error, which 
     * contains error message specifying what happened.
     */
    ComPtr<::ID3D12DescriptorHeap> createDescHeap(::D3D12_DESCRIPTOR_HEAP_TYPE type,
        uint32_t numDescriptors,
        ::D3D12_DESCRIPTOR_HEAP_FLAGS flags = ::D3D12_DESCRIPTOR_HEAP_FLAG_NONE);

    /**
     * Get size of a single descriptor handle of given type.
     * @param type Type of the descriptor handle.
     * @return Returns size of a single descriptor handle of 
     * specified type.
     */
    uint32_t getDescriptorHandleSize(::D3D12_DESCRIPTOR_HEAP_TYPE type) const
    { return mDevice->GetDescriptorHandleIncrementSize(type); }

    /**
     * Create render target view for specified resource, using 
     * default parameters. Resulting descriptor is created in 
     * place of specified descriptor handle.
     * @param res Resource to create RTV for.
     * @param desc Destination for the created RTV.
     */
    void createRenderTargetView(::ID3D12Resource *res, ::D3D12_CPU_DESCRIPTOR_HANDLE desc)
    { mDevice->CreateRenderTargetView(res, nullptr, desc); }

    /**
     * Get allocation information for specified resource.
     * @param desc Description of the resource.
     * @return Returns the allocation info structure.
     * @warning Should be used only while using a single 
     * adapter.
     */
    ::D3D12_RESOURCE_ALLOCATION_INFO getAllocationInfo(const ::CD3DX12_RESOURCE_DESC &desc) const
    { return mDevice->GetResourceAllocationInfo(0u, 1u, &desc); }

    /**
     * Get allocation information for specified resource.
     * @param desc Description of the resource.
     * @return Returns the allocation info structure.
     * @warning Should be used only while using a single 
     * adapter.
     */
    ::D3D12_RESOURCE_ALLOCATION_INFO getAllocationInfo(const ::D3D12_RESOURCE_DESC &desc) const
    { return mDevice->GetResourceAllocationInfo(0u, 1u, &desc); }

    /**
     * Check for feature support on this device.
     * @tparam FeatT Type-specific feature structure 
     * type, e.g. ::D3D12_FEATURE_DATA_ROOT_SIGNATURE.
     * @param features Tested features.
     * @param featureType Type of the tested 
     * features, e.g. ::D3D12_FEATURE_ROOT_SIGNATURE.
     * @return Returns true, if the features are supported.
     */
    template <typename FeatT>
    bool supportsFeatures(FeatT features, ::D3D12_FEATURE featureType)
    { return !FAILED(mDevice->CheckFeatureSupport(featureType, &features, sizeof(FeatT))); }

    /// Get the inner pointer.
    const ComPtr<::ID3D12Device2> &getPtr() const
    { return mDevice; }

    /// Get the inner device.
    ::ID3D12Device2 *get() const
    { return mDevice.Get(); }

    /// Access the device.
    const ComPtr<::ID3D12Device2> &operator->() const
    { return mDevice; }
private:
    /**
     * Create Direct3D 12 device from given adapter. If _DEBUG macro is 
     * defined, also configures the debugging layer for this device using 
     * provided disable lists.
     * @param adapter Adapter to create the device from.
     * @param features Minimal feature level required from the device.
     * @param disableCategories Which message categories should be disabled.
     * @param disableSeverities Which message severities should be disabled.
     * @param disableIds Which message IDs should be disabled.
     * @param dbgName Debug name used for the device.
     * @throw Throws util::winexception if any error occurs. The contained 
     * message details what happened.
     */
    ComPtr<::ID3D12Device2> createDevice(D3D12Adapter &adapter, 
        ::D3D_FEATURE_LEVEL features,
        const std::set<::D3D12_MESSAGE_CATEGORY> &disableCategories,
        const std::set<::D3D12_MESSAGE_SEVERITY> &disableSeverities,
        const std::set<::D3D12_MESSAGE_ID> &disableIds, 
        const wchar_t *dbgName);

    /// Inner device pointer.
    ComPtr<::ID3D12Device2> mDevice{ nullptr };
protected:
}; // class D3D12Device

} // namespace d3d12

} // namespace res

} // namespace engine
