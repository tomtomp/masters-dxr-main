/**
* @file resources/ThreadPool.h
* @author Tomas Polasek
* @brief Thread pool resource.
*/

#pragma once

#include <cstdint>
#include <thread>
#include <mutex>

#include "engine/resources/BaseResourceMgr.h"

#include "engine/util/prof/Profiler.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing resource management helpers.
namespace res
{

/// Threading, tasks and other computation resources.
namespace thread
{

// Forward declaration.
class Worker;

/**
 * Simple thread pool implementation with task support.
 * Workers can be added and idle workers can be removed.
 * Once added, the worker thread wait for work, meaning 
 * it is initialized only once.
 * Uses std::packaged_task as internal task representation.
 */
class ThreadPool : public BaseResourceMgr
{
public:
    /// Type for counting worker threads.
    using WorkerNum = uint32_t;

    /// All tasks are void returning functions.
    using TaskT = std::function<void()>;

    /// Ways to quit the threads.
    enum class QuitStrategy
    {
        /// Wait for threads to finish all work.
        Graceful,
        /// Quit threads after finishing the current task.
        Force
    }; // enum class QuitStrategy

    /// Record about a single exception thrown in a thread task.
    struct ExceptionRecord
    {
        ExceptionRecord() = default;
        ExceptionRecord(const util::wexception &exception, const char *name) :
            err{ exception }, threadName{ name } 
        { }
        ExceptionRecord(util::wexception &&exception, const char *name) :
            err{ exception }, threadName{ name } 
        { }
        ExceptionRecord(const std::exception &exception, const char *name) :
            err{ exception }, threadName{ name } 
        { }

        /// Exception thrown.
        util::wexception err;
        /// Name of the thread where it occurred.
        const char *threadName;
    }; // struct ExceptionRecord

    /**
     * Get optimal number of threads, while keeping the 
     * value above, or equal to the minimum.
     * @param min Minimal number of threads, if the 
     * calculation yields a lower number than the one 
     * specified, the function throws an error.
     * @return Returns the optimal number of threads.
     * @throw Throws std::runtime_error if minimal 
     * number of threads could not be confirmed.
     */
    static WorkerNum numOptimalThreads(uint32_t min = 1u);

    /**
     * Frees any resources and destroys the thread pool.
     * If there are any threads left running, when the 
     * thread pool destructs, std::terminate() will be 
     * called.
     * If autoJoin has been set to true, then joinAll() 
     * will be called.
     * @warning Before destruction, all tasks should be 
     * terminated and, if autoJoin is false, joinAll() 
     * called on the thread pool.
     */
    ~ThreadPool();

    /**
     * Create an empty thread pool.
     * @param autoJoin When set to true, the destructor of 
     * thread pool automatically calls joinAll().
     * @throws Throws std::bad_alloc when allocation fails.
     */
    explicit ThreadPool(bool autoJoin = true);

    /**
     * Create thread pool with specified number of 
     * worker threads.
     * @tparam NumWorkers Number of worker threads.
     * @param names List of names assigned to the threads.
     * @throws Throws std::bad_alloc when allocation fails.
     */
    template <WorkerNum NumWorkers>
    ThreadPool(const std::array<const char*, NumWorkers> &names);

    /**
     * Create thread pool with specified number of 
     * worker threads.
     * @param numWorkers Number of worker threads.
     * @param names List of names assigned to the threads.
     * @throws Throws std::runtime_error if there are not 
     * enough names to cover all requested workers.
     */
    ThreadPool(WorkerNum numWorkers, const std::vector<const char*> &names);

    // No copying.
    ThreadPool(const ThreadPool &other) = delete;
    ThreadPool &operator=(const ThreadPool &other) = delete;

    // TODO - Default move should suffice?
    // Allow moving.
    ThreadPool(ThreadPool &&other) = default;
    ThreadPool &operator=(ThreadPool &&other) = default;

    /**
     * Get total number of worker threads in this thread pool.
     */
    WorkerNum numWorkers() const;

    /**
     * Are there any worker threads in this thread pool?
     */
    bool hasWorkers() const
    { return numWorkers() != 0u; }

    /**
     * Get number of idle worker threads in this thread pool.
     */
    WorkerNum numIdleWorkers() const;

    /**
     * Are there any idle worker threads in this thread pool?
     */
    bool hasIdleWorkers() const
    { return numIdleWorkers() != 0u; }

    /**
     * Are all of the specified tasks completed?
     */
    bool allDone() const;

    /**
     * Add worker thread with given name.
     * @param name Name of the thread, it should 
     * have unique pointer between all active 
     * worker threads. It is used for setting 
     * up profiling structures for this thread.
     */
    void addWorker(const char *name);

    /**
     * Create specialized worker, with no task 
     * assignment. The provided main function is 
     * wrapped only in thin outer function which 
     * enables threaded profiling.
     * @param name Name of the thread, it should 
     * have unique pointer between all active 
     * worker threads. It is used for setting 
     * up profiling structures for this thread.
     * @param mainFun Main function of the thread. 
     * On returning from this function the thread 
     * which was created by this function will be 
     * joined.
     * @return Returns future, which is triggered 
     * when the main function returns.
     * @warning Specialized workers have the same 
     * requirements, concerning destruction of the 
     * thread pool. When calling joinAll(false), 
     * these specialized workers must already be 
     * returned from main. Other option is to 
     * signalize the specialized worker to return 
     * and call joinAll(true).
     */
    std::future<void> addSpecializedWorker(const char *name, TaskT mainFun);

    /**
     * Wait for all the worker threads to finish processing 
     * all of the specified tasks. Does NOT wait for 
     * specialized workers to finish.
     * @warning Blocks execution in the current thread 
     * until all of the tasks have been finished.
     */
    void waitAll();

    /**
     * Signalizes to all threads that they should end, and 
     * then waits for them to finish, using join() on each 
     * of them. 
     * Specialized workers are not checked, but they are 
     * still joined.
     * @param wait If set to true, then the thread pool 
     * will wait on non-idle threads, after setting the flag 
     * for quitting. When set to false, the thread pool 
     * checks the number of non-idle worker threads and 
     * throws if there are any. Specialized workers are 
     * not checked.
     * @param quitStrategy Specifies how to signalize the 
     * threads to quit.
     * @throws Throws std::runtime_error when wait is set 
     * to false and there are non-idle threads, or still 
     * running specialized workers.
     * @warning Blocks execution in the current thread 
     * until all of the tasks have been finished.
     */
    void joinAll(bool wait = true, QuitStrategy quitStrategy = QuitStrategy::Graceful);

    /**
     * Add specified task, in the form of a callable 
     * function with provided parameters. 
     * The task is added onto the work queue and then 
     * waits for free worker thread to execute it.
     * @tparam FunT Type of the callable object.
     * @tparam ArgTs Types of the arguments provided 
     * to the callable object.
     * @param f Function representing the task.
     * @param args Arguments passed to the function.
     * @return Returns future, which can be used to 
     * get result of the task. Void future can still 
     * be used to detect when the task is finished.
     */
    template <typename FunT, typename... ArgTs>
    auto addTask(FunT &&f, ArgTs&&... args) -> std::future<decltype(f(args...))>;

    /// Are there any exceptions to read?
    bool hasExceptions() const;

    /**
     * Retrieve all exceptions and remove them from 
     * the list. 
     * @return Returns list of exceptions.
     * @warning Exceptions escaping the standard 
     * tasks are not returned from here, but from 
     * the future, returned by addTask function!
     */
    std::vector<ExceptionRecord> fetchExceptions();
private:

    /// Quit signal types.
    enum class QuitType : uint8_t
    {
        /// Thread is not requested to exit.
        None = 0, 
        /// Threads should first finish all work.
        Graceful,
        /// Threads should quit immediately after finishing work.
        Force
    }; // enum class QuitType

    /// Resource shared between worker threads and the thread pool.
    struct WorkerShared
    {
        /// Mutex for locking members of WorkerShared.
        mutable std::mutex mtx;

        /// Number of idle workers.
        WorkerNum idleWorkers{ 0u };
        /// Number of workers.
        WorkerNum totalWorkers{ 0u };
        /// Should the worker threads quit?
        QuitType quitSignal{ QuitType::None };
        /// Condition variable triggered when new task is added.
        std::condition_variable taskCv;
        /// Condition variable triggered when task is finished.
        std::condition_variable finishedCv;

        /// Contains tasks waiting to be executed.
        std::queue<TaskT> tasks;

        /// Exceptions which escaped the specialized tasks.
        std::vector<ExceptionRecord> exceptions;
    }; // struct WorkerShared

    /// Type for worker main functions.
    using WorkerMainT = std::function<void(const char *, WorkerShared*)>;

    /**
     * Transform provided function and parameters into 
     * and packaged_task.
     * @tparam FunT Type of the callable object.
     * @tparam ArgTs Types of the arguments provided 
     * to the callable object.
     * @param f Function representing the task.
     * @param args Arguments passed to the function.
     * @return Returns the packaged_task created from 
     * provided function and arguments.
     */
    template <typename FunT, typename... ArgTs>
    auto createTask(FunT &&f, ArgTs&&... args) -> std::shared_ptr<std::packaged_task<decltype(f(args...))()>>;
    // TODO - Find a better way than shared_ptr...

    /**
     * Add given task to the task queue.
     * Mutex is locked, before accessing the tasks 
     * queue.
     * @param task Task being added.
     * @param mtx Mutex used for locking the queue.
     * @param tasks Queue of tasks, where the new 
     * task should be appended.
     * @return Returns future for retrieving the 
     * task result.
     */
    template <typename RetT>
    std::future<RetT> appendTask(std::shared_ptr<std::packaged_task<RetT()>> &&task, std::mutex &mtx, std::queue<TaskT> &tasks);
    // TODO - Find a better way than shared_ptr...

    /**
     * Get current number of workers in this thread 
     * pool.
     * @return Returns the current number of workers.
     * @warning This may only be used when the 
     * worker shared mutex is already locked!
     */
    WorkerNum numWorkersSafe() const
    { return mWorkerShared->totalWorkers; }

    /**
     * Get number of idle worker threads in this thread pool.
     * @return Returns the current number of idle workers.
     * @warning This may only be used when the 
     * worker shared mutex is already locked!
     */
    WorkerNum numIdleWorkersSafe() const
    { return mWorkerShared->idleWorkers; }

    /**
     * Add worker thread with given name.
     * @param name Name of the thread, it should 
     * have unique pointer between all created 
     * worker threads, even within other thread 
     * pools.
     * @param mainFun Main function ran by the 
     * new worker thread.
     * @warning This may only be used when the 
     * main mutex is already locked!
     */
    void addWorkerSafe(const char *name, WorkerMainT mainFun);

    /**
     * Are all of the specified tasks completed?
     * @warning This may only be used when the 
     * worker shared mutex and the queue mutex is already 
     * locked!
     */
    bool allDoneSafe() const;

    /**
     * Main loop for worker threads.
     * @param name Name of the worker thread.
     * @param sharedPtr Pointer to the shared data structure.
     */
    static void workerMain(const char *name, WorkerShared *sharedPtr);

    /**
     * Main loop for specialized worker threads.
     * @param name Name of the worker thread.
     * @param sharedPtr Pointer to the shared data structure.
     * @param task Task to execute.
     */
    static void specializedWorkerMain(const char *name, WorkerShared *sharedPtr, TaskT task);

    /// Shared resources.
    std::unique_ptr<WorkerShared> mWorkerShared;

    /// Mutex for the list of workers.
    std::mutex mWorkersMtx;
    /// List of active workers.
    std::vector<std::thread> mWorkers;

    /// Automatically join in destructor.
    bool mAutoJoin{ false };
protected:
}; // class ThreadPool

} // namespace thread

} // namespace res

} // namespace engine

// Template implementation

namespace engine
{

namespace res
{

namespace thread
{

template <ThreadPool::WorkerNum NumWorkers>
ThreadPool::ThreadPool(const std::array<const char *, NumWorkers> &names) : 
    ThreadPool(NumWorkers, { names.begin(), names.end() })
{ }

template <typename FunT, typename ... ArgTs>
auto ThreadPool::addTask(FunT &&f, ArgTs&&... args) -> std::future<decltype(f(args...))>
{
    return appendTask(createTask(std::forward<FunT>(f), std::forward<ArgTs>(args)...), 
        mWorkerShared->mtx, mWorkerShared->tasks);
}

template <typename FunT, typename ... ArgTs>
auto ThreadPool::createTask(FunT &&f, ArgTs &&... args) -> std::shared_ptr<std::packaged_task<decltype(f(args...))()>>
{
    // Type of the return value from provided task.
    using ReturnT = decltype(f(args...));
    // Wrap function and parameters using std::bind.
    std::function<ReturnT()> wrappedTask{ std::bind(std::forward<FunT>(f), 
                                          std::forward<ArgTs>(args)...) };
    // Package the task for result retrieval.
    return std::make_unique<std::packaged_task<ReturnT()>>(std::move(wrappedTask));
}

template <typename RetT>
std::future<RetT> ThreadPool::appendTask(std::shared_ptr<std::packaged_task<RetT()>> &&packagedTask, 
    std::mutex &mtx, std::queue<TaskT> &tasks)
{
    // Store future for the task.
    auto future{ packagedTask->get_future() };

    /*
     * The packaged_task is passed into a lambda, which 
     * can be called without parameters and returns no value.
     */
    /*
     * TODO - Microsoft standard library bug? https://stackoverflow.com/questions/14744588/why-is-stdpackaged-taskvoid-not-valid
     * Unable to move without triggering copy...
     */
    std::function<void()> task = [packagedTask = std::move(packagedTask)] () mutable
    { // When called: 
        // Execute the packaged_task...
        (*packagedTask)();
    };

    { // Locked queue
        std::lock_guard l(mtx);
        tasks.emplace(std::move(task));
    } // Locked queue

    // Notify potential idle workers.
    mWorkerShared->taskCv.notify_all();

    // Return the future for retrieving task result.
    return future;
}

}

}

}

// Template implementation end
