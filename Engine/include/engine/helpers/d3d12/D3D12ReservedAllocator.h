/**
* @file helpers/d3d12/D3D12ReservedAllocator.h
* @author Tomas Polasek
* @brief Allocator for Direct3D 12 reserved resources.
*/

#pragma once

#include "engine/helpers/d3d12/D3D12BaseGpuAllocator.h"

#include "engine/resources/d3d12/D3D12Device.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Simple Direct3D resource allocator, which allows 
 * the allocation of reserved resources. 
 */
class D3D12ReservedAllocator : public D3D12BaseGpuAllocator
{
public:
    /**
     * Create reserved allocator for given device.
     * @param device Device to allocate on.
     */
    D3D12ReservedAllocator(res::d3d12::D3D12Device &device);

    /// No resources need to be freed.
    virtual ~D3D12ReservedAllocator() = default;

    // Allow copying. 
    D3D12ReservedAllocator(const D3D12ReservedAllocator &other) = default;
    D3D12ReservedAllocator &operator=(const D3D12ReservedAllocator &other) = default;

    // Allow moving. 
    D3D12ReservedAllocator(D3D12ReservedAllocator &&other) = default;
    D3D12ReservedAllocator &operator=(D3D12ReservedAllocator &&other) = default;

    /**
     * Allocate resource described in the provided 
     * description structure. After creation, the 
     * resource will have requested initial state and 
     * use optimized clear value.
     * @param desc Description of the resource.
     * @param initState Initial state of the resource 
     * after allocation.
     * @param optimizedClearValue Clear value used by 
     * the resource. Can be nullptr.
     * @param riid Identifier of the resource interface. 
     * Can be gained by using IID_PPV_ARGS macro.
     * @param ptr Pointer to the resource being allocated. 
     * Can be gained by using IID_PPV_ARGS macro.
     * @throws On allocation error throws util::winexception 
     * which contains error description.
     */
    virtual void allocate(const D3D12_RESOURCE_DESC &desc,
        D3D12_RESOURCE_STATES initState,
        const D3D12_CLEAR_VALUE *optimizedClearValue,
        REFIID riid, void **ptr) override final;

    /**
     * Deallocate given resource.
     * @tparam ResT Type of the target resource.
     * @param riid Identifier of the resource interface. 
     * Can be gained by using IID_PPV_ARGS macro.
     * @param ptr Pointer to the resource being allocated. 
     * Can be gained by using IID_PPV_ARGS macro.
     * @throws On deallocation error throws util::winexception 
     * which contains error description.
     * @warning Only resource allocated by this allocator 
     * should be passed to the deallocate method!
     */
    virtual void deallocate(REFIID riid, void **ptr) override final;

    /// Is this allocator valid?
    virtual explicit operator bool() const override
    { return mDevice; }
private:
    /// Device used for allocation.
    res::d3d12::D3D12Device *mDevice{ nullptr };
protected:
}; // class D3D12ReservedAllocator

} // namespace d3d12

} // namespace helpers

} // namespace engine
