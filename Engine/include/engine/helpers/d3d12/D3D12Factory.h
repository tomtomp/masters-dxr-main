/**
* @file helpers/d3d12/D3D12Factory.h
* @author Tomas Polasek
* @brief Helper class for creation of Direct3D 12 objects.
*/

#pragma once

#include <d3d12.h>

#include "engine/helpers/d3d12/D3D12CommittedAllocator.h"
#include "engine/helpers/d3d12/D3D12ResourceUpdater.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 helpers.
namespace d3d12
{

/// Helper class for creation of Direct3D 12 objects.
class D3D12Factory
{
public:
    // Only static functions should be here.
    D3D12Factory() = delete;

    /**
     * Create a simple committed allocator which can be used 
     * to allocate resources.
     * @param device Target device.
     * @param heapProps Properties of the implicit heap.
     * @param heapFlags Flags for the implicit heap.
     */
    static helpers::d3d12::D3D12CommittedAllocator committedAllocator(
        res::d3d12::D3D12Device &device, 
        ::CD3DX12_HEAP_PROPERTIES heapProps = ::CD3DX12_HEAP_PROPERTIES(::D3D12_HEAP_TYPE_DEFAULT), 
        ::D3D12_HEAP_FLAGS heapFlags = ::D3D12_HEAP_FLAG_NONE);

    /**
     * Create an allocator compatible with the ResourceUpdater.
     * @param device Target device.
     */
    static helpers::d3d12::D3D12CommittedAllocator uploadAllocator(res::d3d12::D3D12Device &device);
private:
protected:
}; // class D3D12Factory

} // namespace d3d12

} // namespace helpers

} // namespace engine

// template implementation

namespace engine
{
namespace helpers
{
namespace d3d12
{

}
}
}

// template implementation end
