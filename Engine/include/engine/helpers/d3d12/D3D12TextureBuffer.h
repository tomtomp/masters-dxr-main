/**
* @file helpers/d3d12/D3D12TextureBuffer.h
* @author Tomas Polasek
* @brief Wrapper around D3D12Resource, providing a texture interface.
*/

#pragma once

#include <cstddef>
#include <iterator>

#include "engine/helpers/d3d12/D3D12BaseGpuAllocator.h"
#include "engine/helpers/d3d12/D3D12ResourceUpdater.h"
#include "engine/resources/d3d12/D3D12Resource.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Wrapper around D3D12Resource, providing a texture interface.
 */
class D3D12TextureBuffer
{
public:
    /**
     * Exception thrown when texture buffer is too 
     * small to contain provided data.
     */
    struct TextureTooSmallException : public std::exception
    {
        TextureTooSmallException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct TextureTooSmallException

    /**
     * Initialize empty texture buffer.
     */
    D3D12TextureBuffer();

    /// Free any allocated buffers.
    ~D3D12TextureBuffer();

    // No copying.
    D3D12TextureBuffer(const D3D12TextureBuffer &other) = delete;
    D3D12TextureBuffer &operator=(const D3D12TextureBuffer &other) = delete;
    // Moving is allowed.
    D3D12TextureBuffer(D3D12TextureBuffer &&other) = default;
    D3D12TextureBuffer &operator=(D3D12TextureBuffer &&other) = default;

    /**
     * Create a texture buffer of given size.
     * @param desc Description of the texture buffer.
     * @param allocator This allocator will be used to 
     * allocate the texture buffer.
     * @param initState Initial state of the buffer.
     * @param clearValue Optimized clear value for this texture.
     * @throws Throws util::winexception on error.
     */
    D3D12TextureBuffer(const ::CD3DX12_RESOURCE_DESC &desc, D3D12BaseGpuAllocator &allocator, 
        ::D3D12_RESOURCE_STATES initState = ::D3D12_RESOURCE_STATE_COPY_DEST, 
        const ::D3D12_CLEAR_VALUE *clearValue = nullptr);

    /**
     * Initialize the texture buffer with provided texture data.
     * Vertex buffer will be just large enough to contain provided 
     * data.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the texture data.
     * @param end End iterator for the texture data.
     * @param desc Description of the texture buffer.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param allocator Allocator used to allocate the texture 
     * buffer.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @param clearValue Optimized clear value for this texture.
     * @throws Throws util::winexception on error.
     */
    template <typename ItT>
    D3D12TextureBuffer(const ItT &begin, const ItT &end,
        const ::CD3DX12_RESOURCE_DESC &desc, res::d3d12::D3D12CommandList &cmdList,
        D3D12BaseGpuAllocator &allocator, D3D12ResourceUpdater &updater, 
        const ::D3D12_CLEAR_VALUE *clearValue = nullptr);

    /**
     * Allocate texture buffer on the GPU.
     * @param desc Description of the texture buffer.
     * @param allocator Allocator which should be used to 
     * allocate the vertex buffer.
     * @param initState Initial state of the buffer.
     * @param clearValue Optimized clear value for this texture.
     */
    void allocate(D3D12BaseGpuAllocator &allocator, const ::CD3DX12_RESOURCE_DESC &desc, 
        ::D3D12_RESOURCE_STATES initState = ::D3D12_RESOURCE_STATE_COPY_DEST, 
        const ::D3D12_CLEAR_VALUE *clearValue = nullptr);

    /**
     * Allocate the texture buffer large enough to hold 
     * provided texture data and upload them to the GPU.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the texture data.
     * @param end End iterator for the texture data.
     * @param desc Description of the texture buffer.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param allocator Allocator used to allocate the texture 
     * buffer.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @param clearValue Optimized clear value for this texture.
     * @throws Throws util::winexception on error.
     */
    template <typename ItT>
    void allocateUpload(const ItT &begin, const ItT &end,
        const ::CD3DX12_RESOURCE_DESC &desc, res::d3d12::D3D12CommandList &cmdList,
        D3D12BaseGpuAllocator &allocator, D3D12ResourceUpdater &updater, 
        const ::D3D12_CLEAR_VALUE *clearValue = nullptr);

    /**
     * Upload texture data to the GPU.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the texture data.
     * @param end End iterator for the texture data.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @throws Throws util::winexception on error. Throws 
     * TextureTooSmallException if the allocated texture buffer 
     * if too small to contain provided data.
     */
    template <typename ItT>
    typename std::enable_if<
        std::is_same_v<
            typename std::iterator_traits<ItT>::iterator_category,
            std::random_access_iterator_tag>,
        void>::type
    uploadData(const ItT &begin, const ItT &end, res::d3d12::D3D12CommandList &cmdList, 
        D3D12ResourceUpdater &updater);

    /**
     * Upload texture data to the GPU.
     * @param data Data to copy to the texture buffer.
     * @param sizeInBytes Size of the copied data. This value must 
     * be provided in number of bytes!
     * @param sizeOfVertex Size of a single texture in bytes.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @throws Throws util::winexception on error. Throws 
     * TextureTooSmallException if the allocated texture buffer 
     * if too small to contain provided data.
     */
    void uploadData(const void *data, std::size_t sizeInBytes, std::size_t sizeOfVertex, 
        res::d3d12::D3D12CommandList &cmdList, D3D12ResourceUpdater &updater);

    /**
     * Set debug name to the current buffer.
     * @param dbgName Readable debug name string.
     */
    void setDebugName(const wchar_t *dbgName);

    /// Is this texture buffer allocated?
    bool allocated() const
    { return mTextureBuffer.operator bool(); }

    /// Is this texture buffer allocated?
    explicit operator bool() const
    { return allocated(); }

    /// Access the inner resource.
    res::d3d12::D3D12Resource &resource()
    { return mTextureBuffer; }
private:
    /// Texture buffer itself.
    res::d3d12::D3D12Resource mTextureBuffer;
protected:
}; // class D3D12TextureBuffer

} // namespace d3d12

} // namespace helpers

} // namespace engine

// Template implementation

namespace engine
{
namespace helpers
{
namespace d3d12
{

template <typename ItT>
D3D12TextureBuffer::D3D12TextureBuffer(const ItT &begin, const ItT &end, const ::CD3DX12_RESOURCE_DESC &desc,
    res::d3d12::D3D12CommandList &cmdList, D3D12BaseGpuAllocator &allocator, D3D12ResourceUpdater &updater,
    const ::D3D12_CLEAR_VALUE *clearValue)
{ allocateUpload(begin, end, desc, cmdList, allocator, updater, clearValue); }

template <typename ItT>
void D3D12TextureBuffer::allocateUpload(const ItT &begin, const ItT &end, const ::CD3DX12_RESOURCE_DESC &desc,
    res::d3d12::D3D12CommandList &cmdList, D3D12BaseGpuAllocator &allocator, D3D12ResourceUpdater &updater,
    const ::D3D12_CLEAR_VALUE *clearValue)
{
    using TraitsT = std::iterator_traits<ItT>;

    using ElementT = typename TraitsT::value_type;
    typename TraitsT::pointer data{ &(*begin) };
    const std::size_t sizeOfVertex{ sizeof(ElementT) };
    const std::size_t sizeInBytes{ (end - begin) * sizeof(ElementT) };

    allocate(sizeInBytes, allocator, desc, ::D3D12_RESOURCE_STATE_COPY_DEST, clearValue);
    uploadData(data, sizeInBytes, sizeOfVertex, cmdList, updater);
}

template <typename ItT>
typename std::enable_if<std::is_same_v<typename std::iterator_traits<ItT>::iterator_category, std::random_access_iterator_tag>,
void>::type D3D12TextureBuffer::uploadData(const ItT &begin, const ItT &end, res::d3d12::D3D12CommandList &cmdList,
    D3D12ResourceUpdater &updater)
{
    using TraitsT = std::iterator_traits<ItT>;

    using ElementT = typename TraitsT::value_type;
    typename TraitsT::pointer data{ &(*begin) };
    const std::size_t sizeOfVertex{ sizeof(ElementT) };
    const std::size_t sizeInBytes{ (end - begin) * sizeof(ElementT) };

    uploadData(data, sizeInBytes, sizeOfVertex, cmdList, updater);
}

}
}
}

// Template implementation end
