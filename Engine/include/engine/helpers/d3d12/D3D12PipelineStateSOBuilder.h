/**
* @file helpers/d3d12/D3D12PipelineStateSOBuilder.h
* @author Tomas Polasek
* @brief Generator of pipeline state sub-objects used for pipeline configuration.
*/

#pragma once

#include "engine/lib/d3dx12.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

using D3D12PipelineStateSubObjectAlignment = void*;

/**
 * Helper structure for wrapping pipeline state 
 * sub-objects.
 * Inspired by CD3DX12_PIPELINE_STATE_STREAM_VS.
 * @tparam InnerTT Type of the inner configuration structure.
 * @tparam SubObjIdT Identifier of the sub-object.
 * @tparam DefaultTT Type of the default value for the InnerT.
 */
template <typename InnerTT, ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE SubObjIdT, typename DefaultTT = InnerTT>
class alignas(D3D12PipelineStateSubObjectAlignment) D3D12PipelineStateSubObject
{
public:
    /// Type of the sub-object.
    using InnerT = InnerTT;
    /// Type of the sub-object default value.
    using DefaultT = DefaultTT;
    /// Identifier of the sub-object.
    static constexpr const auto SubObjId{ SubObjIdT };

    /// Initialize the sub-object to default state.
    D3D12PipelineStateSubObject() noexcept
    { }
    /// Initialize the sub-object to provided state.
    D3D12PipelineStateSubObject(const InnerT &inner) :
        mInner{ inner }
    { }

    /// Set the sub-object to provided state.
    D3D12PipelineStateSubObject &operator=(const InnerT &inner)
    { mInner = inner; return *this; }

    /// Automatic conversion to the sub-object.
    operator InnerT() const 
    { return mInner; }
    /// Automatic conversion to the sub-object reference.
    operator InnerT&() 
    { return mInner; }
private:
    // Required order is: ID, configuration structure: 
    /// Identifier of the sub-object.
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE mSubObjId{ SubObjId };
    /// Sub-object itself.
    InnerT mInner{ InnerT() };
protected:
}; // class D3D12PipelineStateSubObject

/// Pipeline sub-object state types.
namespace psot
{

// Partialy copied from d3dx12.h : 
using Flags = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::D3D12_PIPELINE_STATE_FLAGS, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_FLAGS>;
using NodeMask = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::UINT, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_NODE_MASK>;
using RootSignature = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::ID3D12RootSignature*, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_ROOT_SIGNATURE>;
using InputLayout = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::D3D12_INPUT_LAYOUT_DESC, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_INPUT_LAYOUT>;
using IBStripCutValue = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::D3D12_INDEX_BUFFER_STRIP_CUT_VALUE, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_IB_STRIP_CUT_VALUE>;
using PrimitiveTopology = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::D3D12_PRIMITIVE_TOPOLOGY_TYPE, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_PRIMITIVE_TOPOLOGY>;
using VertexShader = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::D3D12_SHADER_BYTECODE, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_VS>;
using GeometryShader = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::D3D12_SHADER_BYTECODE, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_GS>;
using StreamOutput = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::D3D12_STREAM_OUTPUT_DESC, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_STREAM_OUTPUT>;
using HullShader = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::D3D12_SHADER_BYTECODE, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_HS>;
using DomainShader = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::D3D12_SHADER_BYTECODE,
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_DS>;
using PixelShader = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::D3D12_SHADER_BYTECODE, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_PS>;
using ComputeShader = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::D3D12_SHADER_BYTECODE, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_CS>;
using Blend = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::CD3DX12_BLEND_DESC, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_BLEND, 
    ::CD3DX12_DEFAULT>;
using DepthStencil = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::CD3DX12_DEPTH_STENCIL_DESC, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_DEPTH_STENCIL, 
    ::CD3DX12_DEFAULT>;
using DepthStencil1 = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::CD3DX12_DEPTH_STENCIL_DESC1, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_DEPTH_STENCIL1, 
    ::CD3DX12_DEFAULT>;
using DepthStencilFormat = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::DXGI_FORMAT, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_DEPTH_STENCIL_FORMAT>;
using Rasterizer = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::CD3DX12_RASTERIZER_DESC,
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_RASTERIZER, 
    ::CD3DX12_DEFAULT>;
using RenderTargetFormats = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::D3D12_RT_FORMAT_ARRAY, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_RENDER_TARGET_FORMATS>;
using Sample = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::DXGI_SAMPLE_DESC, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_SAMPLE_DESC, 
    ::DefaultSampleDesc>;
using SampleMask = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::UINT, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_SAMPLE_MASK, 
    ::DefaultSampleMask>;
using CachedPipelineState = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::D3D12_CACHED_PIPELINE_STATE, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_CACHED_PSO>;
using ViewInstancing = ::engine::helpers::d3d12::D3D12PipelineStateSubObject<
    ::CD3DX12_VIEW_INSTANCING_DESC, 
    ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_VIEW_INSTANCING, 
    ::CD3DX12_DEFAULT>;

} // namespace psot

/**
 * Generator of pipeline state sub-objects used for pipeline configuration.
 */
class D3D12PipelineStateSOBuilder
{
public:
    /// Total number of sub-object types.
    static constexpr std::size_t NUM_SUBOBJECT_TYPES{ ::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE_MAX_VALID };

    /**
     * Get pipeline state sub-object for primitive topology.
     * @param type Type of the primitive used.
     */
    static psot::PrimitiveTopology primitiveTopology(::D3D12_PRIMITIVE_TOPOLOGY_TYPE type = ::D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE);

    /**
     * Get pipeline state sub-object for depth-stencil format.
     * @param format Format used for the dept-stencil buffer.
     */
    static psot::DepthStencilFormat depthStencilFormat(::DXGI_FORMAT format = ::DXGI_FORMAT_D32_FLOAT);

    /**
     * Get pipeline state sub-object for render-target formats.
     * @param rtFormats List of render-target formats. List must 
     * be at most 8 elements long!
     * @throws Throws util::winexception when the size of the 
     * rtFormats list is larger than 8.
     */
    static psot::RenderTargetFormats renderTargetFormats(std::initializer_list<::DXGI_FORMAT> rtFormats);
private:
protected:
}; // class D3D12PipelineStateSOBuilder

} // namespace d3d12

} // namespace helpers

} // namespace engine

// Template implementation

namespace engine
{
namespace helpers
{
namespace d3d12
{


}
}
}

// Template implementation end
