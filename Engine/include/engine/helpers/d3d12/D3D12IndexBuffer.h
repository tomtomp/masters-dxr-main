/**
* @file helpers/d3d12/D3D12IndexBuffer.h
* @author Tomas Polasek
* @brief Wrapper around D3D12Resource allowing storage of indices.
*/

#pragma once

#include <cstddef>
#include <iterator>

#include "engine/helpers/d3d12/D3D12BaseGpuAllocator.h"
#include "engine/helpers/d3d12/D3D12ResourceUpdater.h"
#include "engine/helpers/d3d12/D3D12Helpers.h"
#include "engine/resources/d3d12/D3D12Resource.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Wrapper around D3D12Resource allowing storage of indices.
 */
class D3D12IndexBuffer
{
public:
    /**
     * Exception thrown when index buffer is too 
     * small to contain provided data.
     */
    struct IBTooSmallException : public std::exception
    {
        IBTooSmallException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct IBTooSmallException

    /**
     * Exception thrown when given size of data 
     * in bytes is not multiple of specified element 
     * size.
     */
    struct IBWrongFormat : public std::exception
    {
        IBWrongFormat(const char *msg) :
            std::exception(msg)
        { }
    }; // struct IBWrongFormat

    /**
     * Initialize empty index buffer.
     */
    D3D12IndexBuffer();

    /// Free any allocated buffers.
    ~D3D12IndexBuffer();

    // No copying.
    D3D12IndexBuffer(const D3D12IndexBuffer &other) = delete;
    D3D12IndexBuffer &operator=(const D3D12IndexBuffer &other) = delete;
    // Moving is allowed.
    D3D12IndexBuffer(D3D12IndexBuffer &&other) = default;
    D3D12IndexBuffer &operator=(D3D12IndexBuffer &&other) = default;

    /**
     * Create a Index buffer of given size.
     * @param sizeInBytes Requested size of the Index buffer in 
     * bytes.
     * @param allocator This allocator will be used to 
     * allocate the Index buffer.
     */
    D3D12IndexBuffer(std::size_t sizeInBytes, D3D12BaseGpuAllocator &allocator);

    /**
     * Initialize the index buffer with provided index data.
     * Vertex buffer will be just large enough to contain provided 
     * data.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the index data.
     * @param end End iterator for the index data.
     * @param indexFormat Format of the index data.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param allocator Allocator used to allocate the index 
     * buffer.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @throws Throws util::winexception on error. Throws 
     * IBWrongFormat if given data size is not multiple of 
     * size of single element specified by given format.
     */
    template <typename ItT>
    D3D12IndexBuffer(const ItT &begin, const ItT &end, ::DXGI_FORMAT indexFormat, 
        res::d3d12::D3D12CommandList &cmdList, D3D12BaseGpuAllocator &allocator, 
        D3D12ResourceUpdater &updater);

    /**
     * Allocate index buffer on the GPU.
     * Allocated buffer will have default resource description 
     * created by ::CD3DX12_RESOURCE_DESC::Buffer and its starting 
     * state will be ::D3D12_RESOURCE_STATE_COPY_DEST.
     * @param sizeInBytes Requested size of the index buffer 
     * in bytes.
     * @param flags Flags passed to the buffer allocator.
     * @param initState Initial state of the buffer.
     * @param allocator Allocator which should be used to 
     * allocate the index buffer.
     */
    void allocate(std::size_t sizeInBytes, D3D12BaseGpuAllocator &allocator, 
        ::D3D12_RESOURCE_FLAGS flags = ::D3D12_RESOURCE_FLAG_NONE, 
        ::D3D12_RESOURCE_STATES initState = ::D3D12_RESOURCE_STATE_COPY_DEST);

    /**
     * Allocate the index buffer large enough to hold 
     * provided index data and upload them to the GPU.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the index data.
     * @param end End iterator for the index data.
     * @param indexFormat Format of the index data.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param allocator Allocator used to allocate the index 
     * buffer.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @throws Throws util::winexception on error. Throws 
     * IBWrongFormat if given data size is not multiple of 
     * size of single element specified by given format.
     */
    template <typename ItT>
    void allocateUpload(const ItT &begin, const ItT &end, ::DXGI_FORMAT indexFormat, 
        res::d3d12::D3D12CommandList &cmdList, D3D12BaseGpuAllocator &allocator, 
        D3D12ResourceUpdater &updater);

    /**
     * Upload index data to the GPU.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the index data.
     * @param end End iterator for the index data.
     * @param indexFormat Format of the index data.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @throws Throws util::winexception on error. Throws 
     * IBTooSmallException if the allocated index buffer 
     * if too small to contain provided data. Throws 
     * IBWrongFormat if given data size is not multiple of 
     * size of single element specified by given format.
     */
    template <typename ItT>
    typename std::enable_if<
        std::is_same_v<
            typename std::iterator_traits<ItT>::iterator_category,
            std::random_access_iterator_tag>,
        void>::type
    uploadData(const ItT &begin, const ItT &end, ::DXGI_FORMAT indexFormat, 
        res::d3d12::D3D12CommandList &cmdList, D3D12ResourceUpdater &updater);

    /**
     * Upload index data to the GPU.
     * @param data Data to copy to the index buffer.
     * @param sizeInBytes Size of the copied data. This value must 
     * be provided in number of bytes!
     * @param indexFormat Format of the index data.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @throws Throws util::winexception on error. Throws 
     * IBTooSmallException if the allocated index buffer 
     * if too small to contain provided data. Throws 
     * IBWrongFormat if given data size is not multiple of 
     * size of single element specified by given format.
     */
    void uploadData(const void *data, std::size_t sizeInBytes, ::DXGI_FORMAT indexFormat, 
        res::d3d12::D3D12CommandList &cmdList, D3D12ResourceUpdater &updater);

    /**
     * Set debug name to the current buffer.
     * @param dbgName Readable debug name string.
     */
    void setDebugName(const wchar_t *dbgName);

    /// Get pointer to the index buffer view for this index buffer.
    const ::D3D12_INDEX_BUFFER_VIEW *ibw() const noexcept
    { return &mIndexBufferView; }

    /// Is this index buffer allocated?
    bool allocated() const
    { return mIndexBuffer.operator bool(); }

    /// Is this index buffer filled with index data?
    bool prepared() const
    { return mIndexBufferView.BufferLocation; }

    /// Is this index buffer allocated and filled?
    explicit operator bool() const
    { return allocated() && prepared(); }
private:
    /**
     * Create index buffer view for given index buffer.
     * @param indexBuffer Target index buffer.
     * @param sizeInBytes Size of the index buffer in bytes.
     * @param indexFormat Format of the index data.
     * @return Returns filled index buffer view structure.
     */
    static ::D3D12_INDEX_BUFFER_VIEW createIndexBufferView(const res::d3d12::D3D12Resource &indexBuffer, 
        std::size_t sizeInBytes, ::DXGI_FORMAT indexFormat);

    /// Information about the vertex buffer.
    ::D3D12_INDEX_BUFFER_VIEW mIndexBufferView{ 0u, 0u, ::DXGI_FORMAT_UNKNOWN };
    /// Vertex buffer itself.
    res::d3d12::D3D12Resource mIndexBuffer;
protected:
}; // class D3D12VertexBuffer
} // namespace d3d12

} // namespace helpers

} // namespace engine

// Template implementation

namespace engine
{
namespace helpers
{
namespace d3d12
{

template <typename ItT>
D3D12IndexBuffer::D3D12IndexBuffer(const ItT &begin, const ItT &end, ::DXGI_FORMAT indexFormat, 
    res::d3d12::D3D12CommandList &cmdList, D3D12BaseGpuAllocator &allocator, D3D12ResourceUpdater &updater)
{ allocateUpload(begin, end, indexFormat, cmdList, allocator, updater); }

template <typename ItT>
void D3D12IndexBuffer::allocateUpload(const ItT &begin, const ItT &end, ::DXGI_FORMAT indexFormat, 
    res::d3d12::D3D12CommandList &cmdList, D3D12BaseGpuAllocator &allocator, D3D12ResourceUpdater &updater)
{
    using TraitsT = std::iterator_traits<ItT>;

    using ElementT = typename TraitsT::value_type;
    typename TraitsT::pointer data{ &(*begin) };
    const std::size_t sizeInBytes{ (end - begin) * sizeof(ElementT) };

    allocate(sizeInBytes, allocator);
    uploadData(data, sizeInBytes, indexFormat, cmdList, updater);
}

template <typename ItT>
typename std::enable_if<std::is_same_v<typename std::iterator_traits<ItT>::iterator_category, std::random_access_iterator_tag>,
void>::type D3D12IndexBuffer::uploadData(const ItT &begin, const ItT &end, ::DXGI_FORMAT indexFormat, 
    res::d3d12::D3D12CommandList &cmdList, D3D12ResourceUpdater &updater)
{
    using TraitsT = std::iterator_traits<ItT>;

    using ElementT = typename TraitsT::value_type;
    typename TraitsT::pointer data{ &(*begin) };
    const std::size_t sizeInBytes{ (end - begin) * sizeof(ElementT) };

    uploadData(data, sizeInBytes, indexFormat, cmdList, updater);
}

}
}
}

// Template implementation end
