/**
* @file helpers/d3d12/D3D12RootSignatureBuilder.h
* @author Tomas Polasek
* @brief D3D12 root signature configurator and builder.
*/

#pragma once

#include "engine/resources/d3d12/D3D12RootSignature.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * D3D12 root signature configurator and builder.
 */
class D3D12RootSignatureBuilder
{
public:
    /**
     * Initialize empty builder.
     */
    D3D12RootSignatureBuilder();

    /// Free any allocated buffers.
    ~D3D12RootSignatureBuilder();

    // Allow copying.
    D3D12RootSignatureBuilder(const D3D12RootSignatureBuilder &other) = default;
    D3D12RootSignatureBuilder &operator=(const D3D12RootSignatureBuilder &other) = default;
    // Moving is allowed.
    D3D12RootSignatureBuilder(D3D12RootSignatureBuilder &&other) = default;
    D3D12RootSignatureBuilder &operator=(D3D12RootSignatureBuilder &&other) = default;

    /**
     * Add given root parameter to the signature.
     * @param parameter New root parameter.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &addRootParameter(const ::CD3DX12_ROOT_PARAMETER1 &parameter);
    
    /**
     * Add a root parameter to the signature.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &addDescriptorTableParameter(
        ::UINT numDescriptorRanges,
        const ::D3D12_DESCRIPTOR_RANGE1 *descriptorRanges,
        ::D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);
    
    /**
     * Add a root parameter to the signature.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &addConstantParameter(
        ::UINT num32BitValues,
        ::UINT shaderRegister,
        ::UINT registerSpace = 0u,
        ::D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);

    /**
     * Add a root parameter to the signature.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &addConstantBufferViewParameter(
        ::UINT shaderRegister,
        ::UINT registerSpace = 0u,
        ::D3D12_ROOT_DESCRIPTOR_FLAGS flags = D3D12_ROOT_DESCRIPTOR_FLAG_NONE,
        ::D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);

    /**
     * Add a root parameter to the signature.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &addShaderResourceViewParameter(
        ::UINT shaderRegister,
        ::UINT registerSpace = 0u,
        ::D3D12_ROOT_DESCRIPTOR_FLAGS flags = D3D12_ROOT_DESCRIPTOR_FLAG_NONE,
        ::D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);

    /**
     * Add a root parameter to the signature.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &addUnorderedAccessViewParameter(
        ::UINT shaderRegister,
        ::UINT registerSpace = 0u,
        ::D3D12_ROOT_DESCRIPTOR_FLAGS flags = D3D12_ROOT_DESCRIPTOR_FLAG_NONE,
        ::D3D12_SHADER_VISIBILITY visibility = D3D12_SHADER_VISIBILITY_ALL);

    /**
     * Add given root paramter to the signature.
     * @param parameter New root parameter.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &operator<<(const ::CD3DX12_ROOT_PARAMETER1 &parameter);
    
    /**
     * Clear the list of root parameters.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &clearRootParameters();

    /**
     * Add given static sampler description to the signature.
     * @param sampler New static sampler.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &addStaticSampler(const ::CD3DX12_STATIC_SAMPLER_DESC &sampler);

    /**
     * Add a static sampler to the signature.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &addStaticSampler(
        UINT shaderRegister,
        D3D12_FILTER filter = D3D12_FILTER_ANISOTROPIC,
        D3D12_TEXTURE_ADDRESS_MODE addressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP,
        D3D12_TEXTURE_ADDRESS_MODE addressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP,
        D3D12_TEXTURE_ADDRESS_MODE addressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP,
        FLOAT mipLODBias = 0,
        UINT maxAnisotropy = 16,
        D3D12_COMPARISON_FUNC comparisonFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL,
        D3D12_STATIC_BORDER_COLOR borderColor = D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE,
        FLOAT minLOD = 0.f,
        FLOAT maxLOD = D3D12_FLOAT32_MAX,
        D3D12_SHADER_VISIBILITY shaderVisibility = D3D12_SHADER_VISIBILITY_ALL,
        UINT registerSpace = 0);

    /**
     * Add given static sampler description to the signature.
     * @param sampler New static sampler.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &operator<<(const ::CD3DX12_STATIC_SAMPLER_DESC &sampler);
    
    /**
     * Clear the list of static sampler descriptions.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &clearStaticSamplers();

    /**
     * Set root signature flags.
     * @param flags Flags of the root signature.
     * @return Returns reference to this object.
     */
    D3D12RootSignatureBuilder &setFlags(::D3D12_ROOT_SIGNATURE_FLAGS flags);

    /**
     * Build the root signature with configuration within this 
     * builder.
     * @param device Device to create the root signature on.
     * @return Returns the root signature object.
     * @throws Throws util::winexception on error.
     */
    res::d3d12::D3D12RootSignature build(res::d3d12::D3D12Device &device) const;
private:
    /// List of root parameters.
    std::vector<::CD3DX12_ROOT_PARAMETER1> mRootParamters;
    /// List of static samplers.
    std::vector<::CD3DX12_STATIC_SAMPLER_DESC> mStaticSamplers;

    /// Flags passed to the root signature.
    ::D3D12_ROOT_SIGNATURE_FLAGS mFlags{ };
protected:
}; // class D3D12RootSignatureBuilder

} // namespace d3d12

} // namespace helpers

} // namespace engine

// Template implementation

namespace engine
{
namespace helpers
{
namespace d3d12
{

}
}
}

// Template implementation end
