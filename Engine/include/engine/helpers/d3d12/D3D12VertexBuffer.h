/**
* @file helpers/d3d12/D3D12VertexBuffer.h
* @author Tomas Polasek
* @brief Wrapper around D3D12Resource allowing storage of vertices.
*/

#pragma once

#include <cstddef>
#include <iterator>

#include "engine/helpers/d3d12/D3D12BaseGpuAllocator.h"
#include "engine/helpers/d3d12/D3D12ResourceUpdater.h"
#include "engine/resources/d3d12/D3D12Resource.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * Wrapper around D3D12Resource allowing storage of vertices.
 */
class D3D12VertexBuffer
{
public:
    /**
     * Exception thrown when vertex buffer is too 
     * small to contain provided data.
     */
    struct VBTooSmallException : public std::exception
    {
        VBTooSmallException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct VBTooSmallException

    /**
     * Initialize empty vertex buffer.
     */
    D3D12VertexBuffer();

    /// Free any allocated buffers.
    ~D3D12VertexBuffer();

    // No copying.
    D3D12VertexBuffer(const D3D12VertexBuffer &other) = delete;
    D3D12VertexBuffer &operator=(const D3D12VertexBuffer &other) = delete;
    // Moving is allowed.
    D3D12VertexBuffer(D3D12VertexBuffer &&other) = default;
    D3D12VertexBuffer &operator=(D3D12VertexBuffer &&other) = default;

    /**
     * Create a vertex buffer of given size.
     * @param sizeInBytes Requested size of the vertex buffer in 
     * bytes.
     * @param allocator This allocator will be used to 
     * allocate the vertex buffer.
     */
    D3D12VertexBuffer(std::size_t sizeInBytes, D3D12BaseGpuAllocator &allocator);

    /**
     * Initialize the vertex buffer with provided vertex data.
     * Vertex buffer will be just large enough to contain provided 
     * data.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the vertex data.
     * @param end End iterator for the vertex data.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param allocator Allocator used to allocate the vertex 
     * buffer.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @throws Throws util::winexception on error.
     */
    template <typename ItT>
    D3D12VertexBuffer(const ItT &begin, const ItT &end, res::d3d12::D3D12CommandList &cmdList, 
        D3D12BaseGpuAllocator &allocator, D3D12ResourceUpdater &updater);

    /**
     * Allocate vertex buffer on the GPU.
     * Allocated buffer will have default resource description 
     * created by ::CD3DX12_RESOURCE_DESC::Buffer and its starting 
     * state will be ::D3D12_RESOURCE_STATE_COPY_DEST.
     * @param sizeInBytes Requested size of the vertex buffer 
     * in bytes.
     * @param flags Flags passed to the buffer allocator.
     * @param initState Initial state of the buffer.
     * @param allocator Allocator which should be used to 
     * allocate the vertex buffer.
     */
    void allocate(std::size_t sizeInBytes, D3D12BaseGpuAllocator &allocator, 
        ::D3D12_RESOURCE_FLAGS flags = ::D3D12_RESOURCE_FLAG_NONE, 
        ::D3D12_RESOURCE_STATES initState = ::D3D12_RESOURCE_STATE_COPY_DEST);

    /**
     * Allocate the vertex buffer large enough to hold 
     * provided vertex data and upload them to the GPU.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the vertex data.
     * @param end End iterator for the vertex data.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param allocator Allocator used to allocate the vertex 
     * buffer.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @throws Throws util::winexception on error.
     */
    template <typename ItT>
    void allocateUpload(const ItT &begin, const ItT &end, res::d3d12::D3D12CommandList &cmdList, 
        D3D12BaseGpuAllocator &allocator, D3D12ResourceUpdater &updater);

    /**
     * Upload vertex data to the GPU.
     * @tparam ItT Type of the iterator, which must be of 
     * random-access iterator type.
     * @param begin Begin iterator for the vertex data.
     * @param end End iterator for the vertex data.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @throws Throws util::winexception on error. Throws 
     * VBTooSmallException if the allocated vertex buffer 
     * if too small to contain provided data.
     */
    template <typename ItT>
    typename std::enable_if<
        std::is_same_v<
            typename std::iterator_traits<ItT>::iterator_category,
            std::random_access_iterator_tag>,
        void>::type
    uploadData(const ItT &begin, const ItT &end, res::d3d12::D3D12CommandList &cmdList, 
        D3D12ResourceUpdater &updater);

    /**
     * Upload vertex data to the GPU.
     * @param data Data to copy to the vertex buffer.
     * @param sizeInBytes Size of the copied data. This value must 
     * be provided in number of bytes!
     * @param sizeOfVertex Size of a single vertex in bytes.
     * @param cmdList Command list used to record the copy 
     * command.
     * @param updater This resource updater will be used to 
     * copy data over to the GPU side.
     * @throws Throws util::winexception on error. Throws 
     * VBTooSmallException if the allocated vertex buffer 
     * if too small to contain provided data.
     */
    void uploadData(const void *data, std::size_t sizeInBytes, std::size_t sizeOfVertex, 
        res::d3d12::D3D12CommandList &cmdList, D3D12ResourceUpdater &updater);

    /**
     * Set debug name to the current buffer.
     * @param dbgName Readable debug name string.
     */
    void setDebugName(const wchar_t *dbgName);

    /// Get pointer to the vertex buffer view for this vertex buffer.
    const ::D3D12_VERTEX_BUFFER_VIEW *vbw() const noexcept
    { return &mVertexBufferView; }

    /// Is this vertex buffer allocated?
    bool allocated() const
    { return mVertexBuffer.operator bool(); }

    /// Is this vertex buffer filled with vertex data?
    bool prepared() const
    { return mVertexBufferView.BufferLocation; }

    /// Is this vertex buffer allocated and filled?
    explicit operator bool() const
    { return allocated() && prepared(); }
private:
    /**
     * Create vertex buffer view for given vertex buffer.
     * @param vertexBuffer Target vertex buffer.
     * @param sizeInBytes Size of the vertex buffer in bytes.
     * @param sizeOfVertex Size of a single vertex in bytes.
     * @return Returns filled vertex buffer view structure.
     */
    static ::D3D12_VERTEX_BUFFER_VIEW createVertexBufferView(const res::d3d12::D3D12Resource &vertexBuffer, 
        std::size_t sizeInBytes, std::size_t sizeOfVertex);

    /// Information about the vertex buffer.
    ::D3D12_VERTEX_BUFFER_VIEW mVertexBufferView{ 0u, 0u, 0u };
    /// Vertex buffer itself.
    res::d3d12::D3D12Resource mVertexBuffer;
protected:
}; // class D3D12VertexBuffer
} // namespace d3d12

} // namespace helpers

} // namespace engine

// Template implementation

namespace engine
{
namespace helpers
{
namespace d3d12
{

template <typename ItT>
D3D12VertexBuffer::D3D12VertexBuffer(const ItT &begin, const ItT &end, res::d3d12::D3D12CommandList &cmdList,
    D3D12BaseGpuAllocator &allocator, D3D12ResourceUpdater &updater)
{ allocateUpload(begin, end, cmdList, allocator, updater); }

template <typename ItT>
void D3D12VertexBuffer::allocateUpload(const ItT &begin, const ItT &end, res::d3d12::D3D12CommandList &cmdList,
    D3D12BaseGpuAllocator &allocator, D3D12ResourceUpdater &updater)
{
    using TraitsT = std::iterator_traits<ItT>;

    using ElementT = typename TraitsT::value_type;
    typename TraitsT::pointer data{ &(*begin) };
    const std::size_t sizeOfVertex{ sizeof(ElementT) };
    const std::size_t sizeInBytes{ (end - begin) * sizeof(ElementT) };

    allocate(sizeInBytes, allocator);
    uploadData(data, sizeInBytes, sizeOfVertex, cmdList, updater);
}

template <typename ItT>
typename std::enable_if<std::is_same_v<typename std::iterator_traits<ItT>::iterator_category, std::random_access_iterator_tag>,
void>::type D3D12VertexBuffer::uploadData(const ItT &begin, const ItT &end, res::d3d12::D3D12CommandList &cmdList,
    D3D12ResourceUpdater &updater)
{
    using TraitsT = std::iterator_traits<ItT>;

    using ElementT = typename TraitsT::value_type;
    typename TraitsT::pointer data{ &(*begin) };
    const std::size_t sizeOfVertex{ sizeof(ElementT) };
    const std::size_t sizeInBytes{ (end - begin) * sizeof(ElementT) };

    uploadData(data, sizeInBytes, sizeOfVertex, cmdList, updater);
}

}
}
}

// Template implementation end
