/**
* @file helpers/d3d12/D3D12PipelineStateBuilder.h
* @author Tomas Polasek
* @brief D3D12 pipeline state configurator and builder.
*/

#pragma once

#include "engine/resources/d3d12/D3D12PipelineState.h"

#include "engine/helpers/d3d12/D3D12PipelineStateSOBuilder.h"

/// Namespace containing the engine code.
namespace engine
{

/// Namespace containing helper classes and methods.
namespace helpers
{

/// Direct3D 12 resource management.
namespace d3d12
{

/**
 * D3D12 pipeline state configurator and builder.
 */
class D3D12PipelineStateBuilder
{
public:
    /// Exception thrown when adding already existing sub-object.
    struct SubObjectAlreadyAddedException : std::exception
    {
        SubObjectAlreadyAddedException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct SubObjectAlreadyAddedException

    /// Exception thrown when getting a sub-object which was not previously set.
    struct SubObjectNotFoundException : std::exception
    {
        SubObjectNotFoundException(const char *msg) :
            std::exception(msg)
        { }
    }; // struct SubObjectNotFoundException

    /**
     * Initialize empty builder.
     */
    D3D12PipelineStateBuilder();

    /// Free any allocated buffers.
    ~D3D12PipelineStateBuilder();

    // Allow copying.
    D3D12PipelineStateBuilder(const D3D12PipelineStateBuilder &other) = default;
    D3D12PipelineStateBuilder &operator=(const D3D12PipelineStateBuilder &other) = default;
    // Moving is allowed.
    D3D12PipelineStateBuilder(D3D12PipelineStateBuilder &&other) = default;
    D3D12PipelineStateBuilder &operator=(D3D12PipelineStateBuilder &&other) = default;

    /**
     * Add provided pipeline stream sub-object to the 
     * configuration of the current pipeline state.
     * If there already was a sub-object with the same ID 
     * specified, then the new value replaces the old one.
     * @tparam SubT Sub-object type, must be instance of 
     * the template D3D12PipelineStateSubObject.
     * @param subObject Value of the sub-object.
     */
    template <typename SubT>
    D3D12PipelineStateBuilder &set(const SubT &subObject);

    /**
     * Add provided pipeline stream sub-object to the 
     * configuration of the current pipeline state.
     * @tparam SubT Sub-object type, must be instance of 
     * the template D3D12PipelineStateSubObject.
     * @param subObject Value of the sub-object.
     * @throws Throws SubObjectAlreadyAddedException if 
     * the sub-object type is already present.
     */
    template <typename SubT>
    D3D12PipelineStateBuilder &add(const SubT &subObject);

    /**
     * Get value of previously added sub-object.
     * @tparam SubT Sub-object type, must be instance of 
     * the template D3D12PipelineStateSubObject.
     * @throws Throws SubObjectNotFoundException if the 
     * sub-object type has not been previously added.
     */
    template <typename SubT>
    D3D12PipelineStateBuilder &get() const;

    /**
     * Remove previously added sub-object.
     * @tparam SubT Sub-object type, must be instance of 
     * the template D3D12PipelineStateSubObject.
     * @throws Throws SubObjectNotFoundException if the 
     * sub-object type has not been previously added.
     */
    template <typename SubT>
    void remove();

    /**
     * Add provided pipeline stream sub-object to the 
     * configuration of the current pipeline state.
     * If there already was a sub-object with the same ID 
     * specified, then the new value replaces the old one.
     * @tparam SubT Sub-object type, must be instance of 
     * the template D3D12PipelineStateSubObject.
     * @param subObject Value of the sub-object.
     */
    template <typename SubT>
    D3D12PipelineStateBuilder &operator<<(const SubT &subObject);

    /**
     * Build current pipeline state configuration into a 
     * pipeline state object and return it.
     * @param device Device to build the pipeline state on.
     * @return Returns pipeline state object created from 
     * the current configuration of this builder.
     * @throws Throws util::winexception on error.
     */
    res::d3d12::D3D12PipelineState build(res::d3d12::D3D12Device &device);
private:
    /**
     * Get pointer to the beginning of given sub-object type.
     * If given sub-object type is not added yet, return nullptr.
     * @param type Type of the sub-object.
     * @return Returns pointer to the sub-object or nullptr if it 
     * is not added yet.
     */
    uint8_t *find(::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE type);

    /**
     * Set value of a sub-object on given poiner.
     * @tparam SubT Sub-object type, must be instance of 
     * @param ptr Pointer to the sub-object memory.
     * @param subObject Value of the sub-object.
     */
    template <typename SubT>
    void setValue(uint8_t *ptr, const SubT &subObject);

    /**
     * Get value of a sub-object on given poiner.
     * @tparam SubT Sub-object type, must be instance of 
     * @param ptr Pointer to the sub-object memory.
     * @return Returns reference to the sub-object.
     */
    template <typename SubT>
    SubT &getValue(uint8_t *ptr);

    /**
     * Allocate memory for given sub-object type, set its mapping 
     * and return a pointer to the memory.
     * @tparam SubT Sub-object type, must be instance of 
     * @return Returns pointer to the sub-object.
     * @throws Throws SubObjectAlreadyAdded if the sub-object 
     * has been allocated previously.
     */
    template <typename SubT>
    uint8_t *allocate();

    /**
     * Allocate memory for given sub-object type, set its mapping 
     * and return a pointer to the memory.
     * @param type Type of the sub-object.
     * @param size Size of the sub-object in bytes.
     * @return Returns pointer to the sub-object.
     * @throws Throws SubObjectAlreadyAdded if the sub-object 
     * has been allocated previously.
     */
    uint8_t *allocateInner(::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE type, std::size_t size);

    /**
     * De-allocate sub-object of given type.
     * @tparam SubT Sub-object type, must be instance of 
     * @throws Throws SubObjectNotFoundException if the 
     * sub-object has not been allocated before.
     */
    template <typename SubT>
    void deallocate();

    /**
     * De-allocate sub-object of given type and size.
     * @param type Type of the sub-object.
     * @param size Size of the sub-object in bytes.
     * @throws Throws SubObjectNotFoundException if the 
     * sub-object has not been allocated before.
     */
    void deallocateInner(::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE type, std::size_t size);

    /**
     * Helper for getting sub-object ID of given type.
     * @tparam SubT Sub-object type, must be instance of 
     * the template D3D12PipelineStateSubObject.
     */
    template <typename SubT>
    static constexpr auto SubObjId{ SubT::SubObjId };

    /// Number of sub-object types.
    static constexpr auto NUM_SUB_OBJ_TYPES{ D3D12PipelineStateSOBuilder::NUM_SUBOBJECT_TYPES };
    /// Alignment of sub-object in bytes, presuming all of the sub-object have the same alignment.
    static constexpr auto SUB_OBJ_ALIGNMENT{ alignof(psot::Flags) };

    /// Buffer containing all of the currently set sub-objects.
    std::vector<uint8_t> mCurrentState{ };

    /// Mapping from sub-object ID to its offset within the current state buffer.
    std::size_t mSubObjectOffsets[NUM_SUB_OBJ_TYPES]{ };
protected:
}; // class D3D12PipelineStateBuilder
} // namespace d3d12

} // namespace helpers

} // namespace engine

// Template implementation

namespace engine
{
namespace helpers
{
namespace d3d12
{

template <typename SubT>
D3D12PipelineStateBuilder &D3D12PipelineStateBuilder::set(const SubT &subObject)
{
    const auto ptr{ find(SubObjId<SubT>) };
    if (ptr == nullptr)
    { // Not yet created: 
        return add(subObject);
    }
    // Else, just set the new value.
    setValue(ptr, subObject);

    return *this;
}

template <typename SubT>
D3D12PipelineStateBuilder & D3D12PipelineStateBuilder::add(const SubT &subObject)
{
    const auto ptr{ allocate<SubT>() };
    setValue(ptr, subObject);

    return *this;
}

template <typename SubT>
D3D12PipelineStateBuilder & D3D12PipelineStateBuilder::get() const
{
    const auto ptr{ find(SubObjId<SubT>) };
    if (ptr == nullptr)
    { throw SubObjectNotFoundException("Failed to get pipeline state sub-object - Sub-object not previously added!"); }

    return getValue<SubT>(ptr);
}

template <typename SubT>
void D3D12PipelineStateBuilder::remove()
{ deallocate<SubT>(); }

template <typename SubT>
D3D12PipelineStateBuilder & D3D12PipelineStateBuilder::operator<<(const SubT &subObject)
{ return set(subObject); }

template <typename SubT>
void D3D12PipelineStateBuilder::setValue(uint8_t *ptr, const SubT &subObject)
{
    const auto soPtr{ reinterpret_cast<SubT*>(ptr) };
    (*soPtr) = subObject;
}

template <typename SubT>
SubT &D3D12PipelineStateBuilder::getValue(uint8_t *ptr)
{ return reinterpret_cast<SubT*>(ptr); }

template <typename SubT>
uint8_t *D3D12PipelineStateBuilder::allocate()
{ return allocateInner(SubObjId<SubT>, sizeof(SubT)); }

template <typename SubT>
void D3D12PipelineStateBuilder::deallocate()
{ deallocateInner(SubObjId<SubT>, sizeof(SubT)); }

}
}
}

// Template implementation end
