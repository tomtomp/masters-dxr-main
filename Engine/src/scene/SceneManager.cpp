/**
 * @file scene/SceneManager.h
 * @author Tomas Polasek
 * @brief Helper class for managing scenes - loading, unloading, activating, etc.
 */

#include "stdafx.h"

#include "engine/scene/SceneManager.h"

namespace engine
{

namespace scene
{

SceneManager::SceneManager() :
    mUniverse{ SceneUniverse::instance() }
{ }

}

}
