/**
 * @file scene/ECS.cpp
 * @author Tomas Polasek
 * @brief Definition of Entity-Component-System universe.
 */

#include "stdafx.h"

#include "engine/scene/ECS.h"

namespace engine
{

namespace scene
{

template ent::Universe<SceneUniverse>;

SceneUniverse &SceneUniverse::instance()
{
    static SceneUniverse u;
    return u;
}

}

}
