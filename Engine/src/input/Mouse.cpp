/**
 * @file input/Mouse.cpp
 * @author Tomas Polasek
 * @brief Mouse handling class.
 */

#include "stdafx.h"

#include "engine/input/Mouse.h"

namespace engine
{

namespace input
{

Mouse *Mouse::sHookMouse{ nullptr };

Mouse::~Mouse()
{
    if (this == sHookMouse)
    {
        resetCallback();
    }
}

void Mouse::setCallbacks(HINSTANCE hInstance, AppButtonCallbackT btnCallback, 
    AppMoveCallbackT mvCallback, AppScrollCallbackT scCallback)
{
    resetCallback();

    mCallbackHook = SetWindowsHookEx(WH_MOUSE, callback(), hInstance, 0u);
    if (!mCallbackHook)
    {
        throw util::winexception("Unable to set callback hook!");
    }

    sHookMouse = this;
    mBtnCallback = btnCallback;
    mMoveCallback = mvCallback;
    mScrollCallback = scCallback;
}

void Mouse::resetCallback()
{
    if (sHookMouse)
    {
        sHookMouse->resetHook();
        sHookMouse = nullptr;
    }
}

LRESULT Mouse::mouseCallbackDispatch(int code, WPARAM wParam, LPARAM lParam)
{
    if (code < 0 || code == HC_NOREMOVE)
    { // Pass through.
        return CallNextHookEx(nullptr, code, wParam, lParam);
    }

    switch (code)
    { // Different processing, based on message type.
    case WM_MOUSEMOVE:
        { // Mouse movement.
            ASSERT_FAST(sHookMouse->mMoveCallback);
            sHookMouse->mMoveCallback(processMoveInner(code, wParam, lParam));
            break;
        }
    case WM_LBUTTONDOWN:
    case WM_LBUTTONUP: 
    case WM_LBUTTONDBLCLK: 
    case WM_RBUTTONDOWN: 
    case WM_RBUTTONUP: 
    case WM_RBUTTONDBLCLK: 
    case WM_MBUTTONDOWN: 
    case WM_MBUTTONUP: 
    case WM_MBUTTONDBLCLK: 
    case WM_XBUTTONDOWN: 
    case WM_XBUTTONUP: 
    case WM_XBUTTONDBLCLK:
        { // Mouse button changes.
            ASSERT_FAST(sHookMouse->mBtnCallback);
            sHookMouse->mBtnCallback(processBtnInner(code, wParam, lParam));
            break;
        }
    case WM_MOUSEWHEEL:
        { // Mouse wheel change.
            ASSERT_FAST(sHookMouse->mScrollCallback);
            sHookMouse->mScrollCallback(processScrollInner(code, wParam, lParam));
            break;
        }
    default:
        { // Not interested.
            break;
        }
    }

    // Continue the hook chain.
    return CallNextHookEx(nullptr, code, wParam, lParam);
}

void Mouse::resetHook()
{
    if (mCallbackHook)
    {
        UnhookWindowsHookEx(mCallbackHook);
        mCallbackHook = nullptr;
        mBtnCallback = nullptr;
        mMoveCallback = nullptr;
        mScrollCallback = nullptr;
    }
}

MouseButton Mouse::btnFromMessage(int messageCode, WPARAM wParam)
{
    switch (messageCode)
    {
    case WM_LBUTTONDOWN:
    case WM_LBUTTONUP: 
    case WM_LBUTTONDBLCLK:
        { // Left mouse button.
            return MouseButton::LButton;
        }
    case WM_RBUTTONDOWN: 
    case WM_RBUTTONUP: 
    case WM_RBUTTONDBLCLK: 
        { // Right mouse button.
            return MouseButton::RButton;
        }
    case WM_MBUTTONDOWN: 
    case WM_MBUTTONUP: 
    case WM_MBUTTONDBLCLK: 
        { // Middle mouse button.
            return MouseButton::MButton;
        }
    case WM_XBUTTONDOWN: 
    case WM_XBUTTONUP: 
    case WM_XBUTTONDBLCLK:
        { // Mouse buttons X1 or X2.
            return (wParam & XBUTTON1) ? MouseButton::X1Button : MouseButton::X2Button;
        }
    default:
        { // Not interested.
            return MouseButton::None;
        }
    }
}

MouseMods Mouse::modsFromWParam(WPARAM wParam)
{ return static_cast<MouseMods>(LOWORD(wParam) & (MK_CONTROL | MK_SHIFT)); }

MouseAction Mouse::actionFromMessage(int messageCode)
{
    switch (messageCode)
    {
    case WM_LBUTTONDOWN:
    case WM_RBUTTONDOWN: 
    case WM_MBUTTONDOWN: 
    case WM_XBUTTONDOWN: 
        { // Press.
            return MouseAction::Press;
        }
    case WM_LBUTTONUP: 
    case WM_RBUTTONUP: 
    case WM_MBUTTONUP: 
    case WM_XBUTTONUP: 
        { // Release.
            return MouseAction::Release;
        }
    case WM_LBUTTONDBLCLK:
    case WM_RBUTTONDBLCLK: 
    case WM_MBUTTONDBLCLK: 
    case WM_XBUTTONDBLCLK:
        { // Double click.
            return MouseAction::DoubleClick;
        }
    default:
        { // Not interested.
            return MouseAction::None;
        }
    }
}

MouseButtons Mouse::buttonsFromWParam(WPARAM wParam)
{ return static_cast<MouseButtons>(LOWORD(wParam) & (MK_LBUTTON | MK_RBUTTON | MK_MBUTTON | MK_XBUTTON1 | MK_XBUTTON2)); }

util::Point2<int16_t> Mouse::posFromLParam(LPARAM lParam)
{
    const auto &p{ MAKEPOINTS(lParam) };
    return { p.x, p.y };
}

int16_t Mouse::deltaFromWParam(WPARAM wParam)
{ return HIWORD(wParam); }

MouseButtonEvent Mouse::processBtnInner(int messageCode, WPARAM wParam, LPARAM lParam)
{
    ASSERT_FAST(messageCode >= WM_LBUTTONDOWN && messageCode <= WM_XBUTTONDBLCLK);

    const auto btn{ btnFromMessage(messageCode, wParam) };
    const auto mods{ modsFromWParam(wParam) };
    const auto action{ actionFromMessage(messageCode) };
    const auto buttons{ buttonsFromWParam(wParam) };
    const auto pos{ posFromLParam(lParam) };

    return { btn, mods, action, buttons, pos };
}

MouseScrollEvent Mouse::processScrollInner(int messageCode, WPARAM wParam, LPARAM lParam)
{
    ASSERT_FAST(messageCode == WM_MOUSEWHEEL);

    const auto delta{ deltaFromWParam(wParam) };
    const auto mods{ modsFromWParam(wParam) };
    const auto buttons{ buttonsFromWParam(wParam) };
    const auto pos{ posFromLParam(lParam) };

    return { delta, mods, buttons, pos };
}

MouseMoveEvent Mouse::processMoveInner(int messageCode, WPARAM wParam, LPARAM lParam)
{
    ASSERT_FAST(messageCode == WM_MOUSEMOVE);

    const auto mods{ modsFromWParam(wParam) };
    const auto buttons{ buttonsFromWParam(wParam) };
    const auto pos{ posFromLParam(lParam) };

    return { mods, buttons, pos };
}

}

}
