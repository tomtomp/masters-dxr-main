/**
* @file util/prof/Profiler.cpp
* @author Tomas Polasek
* @brief Contains profiling code.
*/

#include "stdafx.h"

#include "engine/util/prof/Profiler.h"

namespace util
{

namespace prof
{
uint64_t ProfilingManager::sConstructions{ 0 };
thread_local ThreadStatus ProfilingManager::mThreadStatus;
ProfilingManager &mgr{ ProfilingManager::instance() };

ProfilingManager &ProfilingManager::instance()
{
    static ProfilingManager mgr;
    return mgr;
}

ForeachPrint::ForeachPrint(uint32_t indent, double parentMegacycles) :
    mIndentLevel(indent),
    mParentMegacycles(parentMegacycles)
{
}

void ForeachPrint::operator()(CallNode *node) const
{
    const auto &data = node->data();
    const auto including = data.getActualMegacycles();

    ForeachSumMegacycles adder;
    node->foreachnn(adder);
    const auto inner = adder.sum();

    const auto self = including - inner;
    const auto selfPercentage = including ?
        (self / including) * 100.0 :
        (100.0);

    const auto avg = data.getActualAvg();
    const auto parentPercentage = mParentMegacycles ?
        (including / mParentMegacycles) * 100.0 :
        (100.0);

    const auto strNodeData = ::util::format("%10.3f %9.3f %12.0llu %9.3f %8.3f %7.3f%% %*s%s\n",
        including, parentPercentage, data.getActualNumSamples(), avg, self,
        selfPercentage, mIndentLevel, "", node->name());
    lognp<Prof>() << "\t\t" << strNodeData << std::endl;

    ForeachPrint printer(mIndentLevel + 1, including);
    node->foreachnn(printer);
}

void ForeachSumMegacycles::operator()(CallNode *node)
{
    mSum += node->data().getActualMegacycles();
    mNumSamples += node->data().getActualNumSamples();
}

void ProfilingManager::reset()
{
    delete mRoot;
    mRoot = new ThreadNode("/", nullptr);

    mNumScopeEnter = 0;
    mNumScopeExit = 0;
    mNumThreadEnter = 0;
    mNumThreadExit = 0;

    mThreadStatus.mCurNode = nullptr;
    mThreadStatus.mRoot = nullptr;
    mThreadStatus.mThreadNode = nullptr;
}

ProfilingManager::ProfilingManager() :
    mRoot(new ThreadNode("/", nullptr))
{
    // Only one construction should happen.
    ASSERT_FAST(sConstructions == 0);
    sConstructions++;

    //mRoot->data().timer().start();

    // Measure amount of time per enterScope->exitScope.
    enterThread("main");

    double minOverhead{ std::numeric_limits<float>::max() };

    for (uint64_t jjj = 0; jjj < 100; ++jjj)
    {
        prof::Timer timer;

        for (uint64_t iii = 0; iii < 1000; ++iii)
        {
            timer.start();

            enterScope("Testing");
            exitScope();

            timer.stop();
        }

        const auto newAvg{ timer.avgTicks() };

        if (newAvg < minOverhead)
            minOverhead = newAvg;
    }

    mScopeOverhead = minOverhead;

    exitThread();

    reset();

    // Create the main thread node.
    enterThread("main");
}

ProfilingManager::~ProfilingManager()
{ delete mRoot; }

inline CallNode *ProfilingManager::enterScope(const char *name)
{
    CallNode *retValue{ nullptr };

    {
#ifdef PROF_LOCK_SCOPE
        std::lock_guard l(mThreadStatus.mThreadNode->data().getLock());
#endif

        ASSERT_SLOW(mThreadStatus.mCurNode);
        mThreadStatus.mCurNode = mThreadStatus.mCurNode->getCreateChild(name);
        ASSERT_SLOW(mThreadStatus.mCurNode);
        retValue = mThreadStatus.mCurNode;

        mNumScopeEnter++;

        mThreadStatus.mCurNode->data().startTimer();
    }

    return retValue;
}

inline CallNode *ProfilingManager::exitScope()
{
    CallNode *retValue{ nullptr };

    {
#ifdef PROF_LOCK_SCOPE
        std::lock_guard l(mThreadStatus.mThreadNode->data().getLock());
#endif

        ASSERT_SLOW(mThreadStatus.mCurNode);
        mThreadStatus.mCurNode->data().stopTimer();
        mThreadStatus.mCurNode = mThreadStatus.mCurNode->parent();
        ASSERT_SLOW(mThreadStatus.mCurNode);
        retValue = mThreadStatus.mCurNode;

        mNumScopeExit++;
    }

    return retValue;
}

inline ThreadNode *ProfilingManager::enterThread(const char* name)
{
    ThreadNode *threadNode{ nullptr };

    {
        std::lock_guard l(mGlobalLock);
        threadNode = mRoot->getCreateChild(name);

        std::lock_guard lD(threadNode->data().getLock());

        const auto tryOccupy{ threadNode->data().occupy() };
        // Test for attempting to enter the same thread at different places.
        ASSERT_FATAL(tryOccupy);

        mThreadStatus.mThreadNode = threadNode;
        mThreadStatus.mCurNode = threadNode->data().getRoot();
        mThreadStatus.mRoot = mThreadStatus.mCurNode;

        mThreadStatus.mRoot->data().startTimer();

        mNumThreadEnter++;
    }

    return threadNode;
}

void ProfilingManager::exitThread()
{
    ThreadData &thData = mThreadStatus.mThreadNode->data();

    {
        std::lock_guard l(mGlobalLock);
        std::lock_guard lD(thData.getLock());

        mThreadStatus.mRoot->data().stopTimer();

        mNumThreadExit++;

        thData.freeOccupation();

        mThreadStatus.mThreadNode = nullptr;
        mThreadStatus.mCurNode = nullptr;
        mThreadStatus.mRoot = nullptr;
    }
}

inline CallNode *ProfilingManager::getCurrentScope()
{
    ASSERT_SLOW(mThreadStatus.mCurNode);
    return mThreadStatus.mCurNode;
}

void ProfilingManager::useCrawler(CallStackCrawler &crawler)
{
    std::lock_guard l(mGlobalLock);
    crawler.crawl(mRoot);
}

ScopeProfiler::ScopeProfiler(const char *desc)
{ mgr.enterScope(desc); }

ScopeProfiler::~ScopeProfiler()
{ mgr.exitScope(); }

BlockProfiler::BlockProfiler(const char *desc)
{ mgr.enterScope(desc); }

void BlockProfiler::end()
{
    if (!mEnded)
    {
        mgr.exitScope();
        mEnded = true;
    }
}

ThreadProfiler::ThreadProfiler(const char *desc)
{ mgr.enterThread(desc); }

ThreadProfiler::~ThreadProfiler()
{ mgr.exitThread(); }

}

}
