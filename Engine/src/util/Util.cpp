/**
* @file util/Util.cpp
* @author Tomas Polasek
* @brief Utilities and helper functions/classes.
*/

#include "stdafx.h"

#include "engine/util/Util.h"

namespace util
{

std::string format(const char* fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);

    auto buf = vFormat(fmt, ap);

    va_end(ap);

    return buf;
}

std::string vFormat(const char* fmt, va_list ap)
{
    // Calculate size of the resulting string.
    const auto resultSize{ ::vsnprintf(nullptr, 0u, fmt, ap) };
    // Allocate space for the string.
    std::vector<char> result(resultSize);
    // Print formatted string into the prepared result string.
    const auto written{ ::vsnprintf(result.data(), resultSize, fmt, ap) };

    ASSERT_FAST(written == resultSize);

    return { result.data() };
}

std::string getErrorAsString(DWORD error)
{
    if (error == 0)
    { // No error -> empty message
        return { };
    }

    // Create the message.
    LPSTR msgBuffer{ nullptr };
    const auto msgSize{ FormatMessageA(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        nullptr, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), reinterpret_cast<LPSTR>(&msgBuffer), 0, nullptr) };

    std::string message(msgBuffer, msgSize);

    // Free the buffer.
    LocalFree(msgBuffer);

    return message;
}

std::wstring getErrorAsWString(DWORD error)
{
    if (error == 0)
    { // No error -> empty message
        return { };
    }

    // Create the message.
    LPWSTR msgBuffer{ nullptr };
    const auto msgSize{ FormatMessageW(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        nullptr, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), reinterpret_cast<LPWSTR>(&msgBuffer), 0, nullptr) };

    std::wstring message(msgBuffer, msgSize);

    // Free the buffer.
    LocalFree(msgBuffer);

    return message;
}

std::string toString(const std::wstring& original)
{
    // Found on https://stackoverflow.com/a/18374698 .
    using convert_type = std::codecvt_utf8<wchar_t>;
    std::wstring_convert<convert_type, wchar_t> converter;
    return converter.to_bytes(original);
}

std::wstring toWString(const std::string& original)
{
    // Found on https://stackoverflow.com/a/18374698 .
    using convert_type = std::codecvt_utf8<wchar_t>;
    std::wstring_convert<convert_type, wchar_t> converter;
    return converter.from_bytes(original);
}

}
