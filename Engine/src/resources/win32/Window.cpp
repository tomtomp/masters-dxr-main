/**
* @file resources/win32/Window.cpp
* @author Tomas Polasek
* @brief Windows window handling class.
*/

#include "stdafx.h"

#include "engine/resources/win32/Window.h"

namespace engine
{

namespace res
{

namespace win
{

Window::Window(HINSTANCE hInstance, uint32_t width, uint32_t height, 
    const std::wstring& name, const WindowClass &windowClass, LPVOID ptr) :
    mHInstance{ hInstance }, 
    mClientWidth{ width }, mClientHeight{ height }, 
    mName{ name }, 
    mClassName{ windowClass.className() }, 
    mDataPtr{ ptr }
{ }

void Window::sendExitMessage()
{ ::SendMessage(mHandle, WM_CLOSE, 0, 0); }

void Window::initialize()
{
    // Create the window.
    mHandle = createWindow(mHInstance, mClientWidth, mClientHeight, mName, mClassName, mDataPtr);
    // By default the window will be in windowed mode and invisible.
    mVisible = false;
    mFullscreen = false;

    log<WInfo>() << "Created a window named: " << mName << std::endl;
}

void Window::sendFullscreenMessage(bool value)
{ ::SendMessage(mHandle, windowMsgToId(value ? WindowMessage::Maximize : WindowMessage::Restore), 0, 0); }

void Window::sendVisibleMessage(bool value)
{ ::SendMessage(mHandle, windowMsgToId(value ? WindowMessage::Show : WindowMessage::Hide), 0, 0); }

void Window::setFullscreen(bool value)
{
    // TODO - Does it work even when (value && mFullscreen) ?
    if (value)
    { // Fullscreen mode.
        makeFullscreen(mWindowedRect);
        mFullscreen = true;
    }
    else
    { // Windowed mode.
        makeWindowed(mWindowedRect);
        mFullscreen = false;
    }
}

void Window::setVisible(bool value)
{
    // TODO - Does it work even when (value && mVisible) ?
    if (value)
    { // Showing the window.
        showWindow();
        mVisible = true;
    }
    else
    { // Hiding the window.
        hideWindow();
        mVisible = false;
    }
}

void Window::destroy()
{
    if (mHandle)
    {
        ::DestroyWindow(mHandle);
        mHandle = nullptr;
    }
}

RECT Window::getMonitorSize(HWND hwnd, DWORD searchFlag)
{
    // Get the monitor where this window is being displayed.
    const auto monitor = ::MonitorFromWindow(hwnd, searchFlag);
    if (!monitor)
    { // Monitor not found -> return 0 size.
        return { };
    }

    // Get monitor information, including its size.
    MONITORINFOEX monitorInfo = {};
    monitorInfo.cbSize = sizeof(MONITORINFOEX);
    ::GetMonitorInfo(monitor, &monitorInfo);

    return monitorInfo.rcMonitor;
}

HWND Window::createWindow(HINSTANCE hInstance, uint32_t width, uint32_t height, const std::wstring& name,
    const std::wstring& className, LPVOID ptr)
{
    // Get display dimensions.
    const auto displayWidth = ::GetSystemMetrics(SM_CXSCREEN);
    const auto displayHeight = ::GetSystemMetrics(SM_CYSCREEN);

    // Fill in requested size of the window.
    RECT displayRect = { 0, 0, static_cast<LONG>(width), static_cast<LONG>(height) };
    // Window has normal frame.
    ::AdjustWindowRect(&displayRect, WS_OVERLAPPEDWINDOW, FALSE);

    // Calculate real dimensions of the window.
    const auto windowWidth = displayRect.right - displayRect.left;
    const auto windowHeight = displayRect.bottom - displayRect.top;

    // Calculate upper left corner of the window, so that the window is centered. 
    // Clamp to left-top corner.
    const auto windowPosX = std::max<int>(0, (displayWidth - windowWidth) / 2);
    const auto windowPosY = std::max<int>(0, (displayHeight - windowHeight) / 2);

    const auto hwnd = ::CreateWindowExW(
        0u,                     // Extended window style
        className.c_str(),      // Window class identifier
        name.c_str(),           // Window title
        WINDOWED_WINDOW_STYLE,  // Window style
        windowPosX,             // Window position from the left
        windowPosY,             // Window position from the top
        windowWidth,            // Width of the window
        windowHeight,           // Height of the window
        nullptr,                // Pointer to parent window
        nullptr,                // Pointer to application menu
        hInstance,              // Module instance containing the window message handler.
        ptr                     // Pointer passed to the window on creation.
    );

    if (!hwnd)
    {
        throw util::winexception("Unable to create window!");
    }

    return hwnd;
}

void Window::getWindowRect(LPRECT rect) const
{ ::GetWindowRect(mHandle, rect); }

void Window::showWindow() const
{
    ::ShowWindow(mHandle, SW_SHOW);
}

void Window::hideWindow() const
{
    ::ShowWindow(mHandle, SW_HIDE);
}

void Window::makeWindowed(const RECT& dimensions) const
{
    // Restore window style to windowed mode.
    ::SetWindowLong(mHandle, GWL_STYLE, WINDOWED_WINDOW_STYLE);

    // Reset the window position and size.
    ::SetWindowPos(
        // Put window on top, but on on always-on-top windows.
        mHandle, HWND_NOTOPMOST,
        // Position - restore from storage.
        dimensions.left,
        dimensions.top,
        // Size - restore from storage.
        dimensions.right - dimensions.left,
        dimensions.bottom - dimensions.top,
        // Apply the window style change.
        SWP_FRAMECHANGED | 
        // No need to activate the window.
        SWP_NOACTIVATE);

    // Restore normal window function.
    ::ShowWindow(mHandle, SW_NORMAL);
}

void Window::makeFullscreen(RECT& dimensions) const
{
    // Save the current window dimensions to provided storage.
    getWindowRect(&dimensions);

    // Set window style to fullscreen.
    ::SetWindowLongW(mHandle, GWL_STYLE, FULLSCREEN_WINDOW_STYLE);

    // Get the size of a monitor where the window is being displayed.
    const auto monitorSize = getMonitorSize(mHandle, MONITOR_DEFAULTTONEAREST);

    // Make window cover the whole screen and be on top.
    ::SetWindowPos(
        // Put this window on top.
        mHandle, HWND_TOP,
        // Position - top left corner.
        monitorSize.left,
        monitorSize.top,
        // Size - cover the whole screen.
        monitorSize.right - monitorSize.left,
        monitorSize.bottom - monitorSize.top,
        // Apply the window style change.
        SWP_FRAMECHANGED | 
        // No need to activate the window.
        SWP_NOACTIVATE);

    // Maximize the window.
    ::ShowWindow(mHandle, SW_MAXIMIZE);
}

}

}

}