/**
* @file resources/win32/WindowClass.cpp
* @author Tomas Polasek
* @brief Windows window handling class.
*/

#include "stdafx.h"

#include "engine/resources/win32/WindowClass.h"

namespace engine
{

namespace res
{

namespace win
{

WindowClass::WindowClass(HINSTANCE hInstance, const std::wstring &className, WNDPROC wndProc) :
    WindowClass(hInstance, className, wndProc, defaultIcon(hInstance), 
        defaultCursor(hInstance), colorToBrush(), defaultIcon(hInstance))
{
    log<Info>() << "Registered window class." << std::endl;
}

WindowClass::WindowClass(HINSTANCE hInstance, const std::wstring &className, 
    WNDPROC wndProc, HICON appIcon, HCURSOR cursor, HBRUSH background, 
    HICON classIcon) :
    mClassName{ className }
{
    WNDCLASSEXW windowClass = {};

    // Size of structure.
    windowClass.cbSize = sizeof(WNDCLASSEX);
    // Features of this window - redraw on horizontal or vertical change.
    windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
    // Function used for handling messages passed to the window.
    windowClass.lpfnWndProc = wndProc;
    // Extra bytes allocated after window class - UNUSED.
    windowClass.cbClsExtra = 0u;
    // Extra bytes allocated after window class - UNUSED.
    windowClass.cbWndExtra = 0u;
    // Handle to the module instance which contains the window message handler.
    windowClass.hInstance = hInstance;
    // Use the provided application icon.
    windowClass.hIcon = appIcon;
    // Use the provided application cursor.
    windowClass.hCursor = cursor;
    // Background brush, use the provided window color.
    windowClass.hbrBackground = background;
    // No application menu.
    windowClass.lpszMenuName = nullptr;
    // Name of the window class.
    windowClass.lpszClassName = className.c_str();
    // Use default window class icon.
    windowClass.hIconSm = classIcon;

    // Register the class.
    static auto atom{ ::RegisterClassExW(&windowClass) };
    if (atom <= 0)
    {
        throw util::winexception("Unable to register window class!");
    }
}

} 

} 

} 
