/**
* @file resources/ThreadPool.cpp
* @author Tomas Polasek
* @brief Thread pool resource.
*/

#include "stdafx.h"

#include "engine/resources/ThreadPool.h"

namespace engine
{

namespace res
{

namespace thread
{

uint32_t ThreadPool::numOptimalThreads(uint32_t min)
{
    const auto result{ std::thread::hardware_concurrency() };
    if (result < min)
    {
        throw std::runtime_error("Unable to satisfy minimal required number of threads!");
    }
    return result;
}

ThreadPool::~ThreadPool()
{
    if (mAutoJoin)
    {
        joinAll();
    }
}

ThreadPool::ThreadPool(bool autoJoin):
    mWorkerShared(std::make_unique<WorkerShared>()),
    mAutoJoin{ autoJoin }
{ /* Empty thread pool */ }

ThreadPool::ThreadPool(WorkerNum numWorkers, const std::vector<const char *> &names) : 
    ThreadPool()
{
    if (numWorkers > names.size())
    {
        throw std::runtime_error("Now enough names for specified number of workers!");
    }

    // Lock, so the starting threads don't interfere.
    std::lock_guard l(mWorkersMtx);

    for (auto name : names)
    { // Create specified number of threads.
        addWorkerSafe(name, &ThreadPool::workerMain);
    }

    mWorkerShared->totalWorkers = static_cast<WorkerNum>(names.size());

    waitAll();
}

ThreadPool::WorkerNum ThreadPool::numWorkers() const
{
    std::lock_guard l(mWorkerShared->mtx);
    return numWorkersSafe();
}

ThreadPool::WorkerNum ThreadPool::numIdleWorkers() const
{
    std::lock_guard l(mWorkerShared->mtx);
    return numIdleWorkersSafe();
}

bool ThreadPool::allDone() const
{
    std::lock_guard l(mWorkerShared->mtx);
    return allDoneSafe();
}

void ThreadPool::addWorker(const char *name)
{
    std::lock_guard l(mWorkersMtx);
    addWorkerSafe(name, &ThreadPool::workerMain);
    mWorkerShared->totalWorkers++;
}

std::future<void> ThreadPool::addSpecializedWorker(const char *name, TaskT mainFun)
{
    std::lock_guard l(mWorkersMtx);

    // TODO - Again, microsoft standard library, find a better way.
    // Create the promise-future pair for signalizing the main function finish.
    std::shared_ptr<std::promise<void>> promise{ std::make_shared<std::promise<void>>() };
    auto future{ promise->get_future() };

    addWorkerSafe(name, [task = mainFun, promise = std::move(promise)] (const char *name, WorkerShared *sharedPtr) mutable
    {
        promise->set_value_at_thread_exit();
        ThreadPool::specializedWorkerMain(name, sharedPtr, task);
    });

    return future;
}

void ThreadPool::waitAll()
{
    if (!hasWorkers())
    { // No workers, no worry.
        return;
    }

    std::unique_lock l(mWorkerShared->mtx);
    mWorkerShared->finishedCv.wait(l, [&] ()
    {
        return allDoneSafe();
    });
}

void ThreadPool::joinAll(bool wait, QuitStrategy quitStrategy)
{
    if (wait)
    { // Waiting for tasks to be finished.
        waitAll();
    }
    else if (!allDone())
    { // No waiting specified, but not all threads finished.
        throw std::runtime_error("Unable to join non-idle threads with wait set to false!");
    }

    {
        std::lock_guard l(mWorkerShared->mtx);

        switch (quitStrategy)
        {
            case QuitStrategy::Force: 
                { // Signalize workers, they should quit as soon as possible.
                    mWorkerShared->quitSignal = QuitType::Force;
                    break;
                }
            case QuitStrategy::Graceful:
            default:
                { // Signalize workers to quit after finishing the work.
                    mWorkerShared->quitSignal = QuitType::Graceful;
                    break;
                }
        }

        // Notify workers that changed has occurred.
        mWorkerShared->taskCv.notify_all();
    }

    {
        std::lock_guard l(mWorkersMtx);

        for (auto &worker : mWorkers)
        { // Join all worker threads.
            if (worker.joinable())
            {
                worker.join();
            }
        }

        // Remove the joined threads.
        mWorkers.clear();
    }

    {
        std::lock_guard l(mWorkerShared->mtx);
        ASSERT_FAST(mWorkerShared->idleWorkers == 0u);
        mWorkerShared->totalWorkers = 0u;
    }
}

bool ThreadPool::hasExceptions() const
{
    std::lock_guard l(mWorkerShared->mtx);
    return !mWorkerShared->exceptions.empty();
}

std::vector<ThreadPool::ExceptionRecord> ThreadPool::fetchExceptions()
{
    std::lock_guard l(mWorkerShared->mtx);

    const auto result{ std::move(mWorkerShared->exceptions) };
    // Just to be sure, empty it.
    mWorkerShared->exceptions.clear();

    return result;
}

void ThreadPool::addWorkerSafe(const char *name, WorkerMainT mainFun)
{
    // Make sure we have the shared memory ready.
    ASSERT_FAST(mWorkerShared.get());

    // Create the worker.
    mWorkers.emplace_back([mainFun = std::bind(mainFun, name, mWorkerShared.get())] ()
    {
        mainFun();
    });
}

bool ThreadPool::allDoneSafe() const
{ return mWorkerShared->tasks.empty() && numIdleWorkersSafe() == numWorkersSafe(); }

void ThreadPool::workerMain(const char *name, WorkerShared *sharedPtr)
{
    // Enable profiling in the worker thread.
    PROF_THREAD(name);

    for (;;)
    { // Until the thread pool signalizes quit.
        std::unique_lock lock(sharedPtr->mtx);

        // Going to wait for task, idle for now.
        sharedPtr->idleWorkers++;
        { // Thread is idle.
            // Notify thread pool the current task is finished.
            sharedPtr->finishedCv.notify_all();

            // Wait until there is something to do...
            sharedPtr->taskCv.wait(lock, [&] ()
            {
                return !sharedPtr->tasks.empty() || 
                    sharedPtr->quitSignal != QuitType::None;
            });
            // Lock is already gained.
        } // Thread is waking up.
        sharedPtr->idleWorkers--;

        if (sharedPtr->quitSignal == QuitType::Force)
        { // Immediate exit.
            // Lock is unlocked when it goes out of scope.
            break;
        }
        else if (!sharedPtr->tasks.empty())
        { // We have work to do...
            // Get task to execute and remove it from queue.
            auto task{ std::move(sharedPtr->tasks.front()) };
            sharedPtr->tasks.pop();

            // Shared structure is no longer needed.
            lock.unlock();

            try
            { // Guard for the potential exceptions.
                // Execute the task.
                task();
            } catch (util::wexception &err)
            {
                std::lock_guard<std::mutex> l(sharedPtr->mtx);
                sharedPtr->exceptions.emplace_back(std::move(err), name);
            } catch (std::exception &err)
            {
                std::lock_guard<std::mutex> l(sharedPtr->mtx);
                sharedPtr->exceptions.emplace_back(err, name);
            } catch (...)
            {
                std::lock_guard<std::mutex> l(sharedPtr->mtx);
                sharedPtr->exceptions.emplace_back(std::exception("Unknown exception"), name);
            }
        }
        else if (sharedPtr->quitSignal == QuitType::Graceful)
        { // Gracefully quitting, since no more work is available.
            // Lock is unlocked when it goes out of scope.
            break;
        }
    }

    // Final notification that the work has been finished.
    sharedPtr->finishedCv.notify_all();
}

void ThreadPool::specializedWorkerMain(const char *name, WorkerShared *sharedPtr, std::function<void()> task)
{
    // Enable profiling in the worker thread.
    PROF_THREAD(name);

    try
    { // Execute the task.
        task();
    } catch (util::wexception &err)
    { 
        std::lock_guard<std::mutex> l(sharedPtr->mtx);
        sharedPtr->exceptions.emplace_back(std::move(err), name);
    } catch (std::exception &err)
    {
        std::lock_guard<std::mutex> l(sharedPtr->mtx);
        sharedPtr->exceptions.emplace_back(err, name);
    } catch (...)
    {
        std::lock_guard<std::mutex> l(sharedPtr->mtx);
        sharedPtr->exceptions.emplace_back(std::exception("Unknown exception"), name);
    }
}

} 

} 

} 
