/**
* @file resources/d3d12/D3D12Heap.cpp
* @author Tomas Polasek
* @brief Wrapper around Direct3D 12 heap.
*/

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12Heap.h"

namespace engine
{

namespace res
{

namespace d3d12
{

D3D12Heap::D3D12Heap(D3D12Device &device, const ::CD3DX12_HEAP_DESC &desc) : 
    mDesc{ desc }
{ mHeap = createHeap(device, mDesc); }

D3D12Heap::D3D12Heap(D3D12Device &device, const ::D3D12_HEAP_DESC &desc) :
    mDesc{ desc }
{ mHeap = createHeap(device, mDesc); }

ComPtr<::ID3D12Heap> D3D12Heap::createHeap(D3D12Device &device, const ::CD3DX12_HEAP_DESC &desc)
{
    ComPtr<::ID3D12Heap> result;

    util::throwIfFailed(device->CreateHeap(&desc, IID_PPV_ARGS(&result)),
        "Failed to create heap!");

    return result;
}

}

} 

} 
