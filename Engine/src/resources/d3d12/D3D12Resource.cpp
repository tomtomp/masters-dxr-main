/**
* @file resources/d3d12/D3D12Resource.cpp
* @author Tomas Polasek
* @brief Wrapper around a Direct3D 12 resource.
*/

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12Resource.h"

#include "engine/helpers/d3d12/D3D12Helpers.h"

namespace engine
{

namespace res
{

namespace d3d12
{

D3D12Resource::D3D12Resource(const ComPtr<::ID3D12Resource> &res, ::D3D12_RESOURCE_STATES currentState) :
    mResource{ res }, mState{ currentState }, mDesc{ mResource->GetDesc() }
{ }

// TODO - Add MipLevels, height, array, etc...
std::size_t D3D12Resource::allocatedSize() const
{
    auto elementSize{ helpers::d3d12::D3D12Helpers::sizeOfFormatElement(mDesc.Format) };
    return mDesc.Width * mDesc.Height * (elementSize ? elementSize : 1u);
}

D3D12Resource::D3D12Resource(const ComPtr<::ID3D12Resource> &res, ::D3D12_RESOURCE_STATES currentState,
    const ::CD3DX12_RESOURCE_DESC &desc) :
    mResource{ res }, mState{ currentState }, mDesc{ desc }
{ }

D3D12Resource::D3D12Resource(const ::CD3DX12_RESOURCE_DESC &desc, ::D3D12_RESOURCE_STATES initState,
    const ::D3D12_CLEAR_VALUE *optimizedClearValue, helpers::d3d12::D3D12BaseGpuAllocator &allocator) : 
    mDesc{ desc }, mAllocator{ &allocator }, mState{ initState }
{ mResource = allocateResource(mDesc, initState, optimizedClearValue, *mAllocator); }

D3D12Resource::~D3D12Resource()
{ destroy(); }

void D3D12Resource::transitionTo(::D3D12_RESOURCE_STATES newState, D3D12CommandList &cmdList, 
    UINT subResource, ::D3D12_RESOURCE_BARRIER_FLAGS flags)
{
    ASSERT_FAST(valid());

    // Specify translation barrier.
    const auto barrier{CD3DX12_RESOURCE_BARRIER::Transition(mResource.Get(), mState, newState, subResource, flags)};
    // Record barrier command.
    cmdList->ResourceBarrier(1u, &barrier);

    // Notify resource its state has been changed.
    mState = newState;
}

ComPtr<::ID3D12Resource> D3D12Resource::allocateResource(const ::CD3DX12_RESOURCE_DESC &desc,
    ::D3D12_RESOURCE_STATES initState, const ::D3D12_CLEAR_VALUE *optimizedClearValue,
    helpers::d3d12::D3D12BaseGpuAllocator &allocator)
{
    ComPtr<::ID3D12Resource> result;

    allocator.allocate(desc, initState, optimizedClearValue, IID_PPV_ARGS(&result));

    return result;
}

void D3D12Resource::destroy()
{
    if (mAllocator && mResource)
    {
        mAllocator->deallocate(IID_PPV_ARGS(&mResource));
    }

    mAllocator = nullptr;
    mResource = nullptr;
}

} 

} 

} 
