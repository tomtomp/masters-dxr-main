/**
* @file resources/d3d12/D3D12PipelineState.cpp
* @author Tomas Polasek
* @brief Wrapper around a Direct3D 12 pipeline state.
*/

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12PipelineState.h"

namespace engine
{

namespace res
{

namespace d3d12
{

D3D12PipelineState::D3D12PipelineState()
{ /* Automatic */ }

D3D12PipelineState::~D3D12PipelineState()
{ /* Automatic */ }

D3D12PipelineState::D3D12PipelineState(const ::D3D12_GRAPHICS_PIPELINE_STATE_DESC &desc, D3D12Device &device)
{
    util::throwIfFailed(
        device->CreateGraphicsPipelineState(&desc, IID_PPV_ARGS(&mPipelineState)), 
        "Failed to create graphical pipeline state!");
}

D3D12PipelineState::D3D12PipelineState(const ::D3D12_COMPUTE_PIPELINE_STATE_DESC &desc, D3D12Device &device)
{
    util::throwIfFailed(
        device->CreateComputePipelineState(&desc, IID_PPV_ARGS(&mPipelineState)), 
        "Failed to create compute pipeline state!");
}

D3D12PipelineState::D3D12PipelineState(const ::D3D12_PIPELINE_STATE_STREAM_DESC &desc, D3D12Device &device)
{
    util::throwIfFailed(
        device->CreatePipelineState(&desc, IID_PPV_ARGS(&mPipelineState)), 
        "Failed to create stream-specified pipeline state!");
}

}

}

}
