/**
* @file resource/d3d12/D3D12Adapter.cpp
* @author Tomas Polasek
* @brief
*/

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12Adapter.h"

namespace engine
{

namespace res
{

namespace d3d12
{

D3D12Adapter::D3D12Adapter(D3D12DXGIFactory5 &factory, bool useWarp, ::D3D_FEATURE_LEVEL features)
{
    // DirectX Graphics Infrastructure adapters.
    // DXGI 1.0 adapter.
    ComPtr<::IDXGIAdapter1> dxgiAdapter1;
    // DXGI 1.6 adapter.
    ComPtr<::IDXGIAdapter3> dxgiAdapter3;

    log<WInfo>() << "Selecting D3D12 adapter..." << std::endl;

    if (useWarp)
    { // Using Windows Advanced Rasterization Platform.
        // Get WARP adapter.
        util::throwIfFailed(factory->EnumWarpAdapter(IID_PPV_ARGS(&dxgiAdapter1)), 
            "Failed to enumerate WARP adapter!");
        // Cast DXGI 1.0 adapter to DXGI 1.6 adapter.
        util::throwIfFailed(dxgiAdapter1.As(&dxgiAdapter3), 
            "Failed to cast WARP DXGI adapter 1 to DXGI adapter 3!");

        log<WInfo>() << "Chosen Windows Advanced Rasterization Platform adapter!" << std::endl;
    }
    else
    { // Use physical graphical adapter.
        std::size_t maxVideoMemory{ 0 };

        log<WInfo>() << "Available adapters: " << std::endl;

        for (uint32_t iii = 0; 
            factory->EnumAdapters1(iii, &dxgiAdapter1) != DXGI_ERROR_NOT_FOUND; 
            ++iii)
        { // For each graphical adapter.

            // Get description of current adapter.
            ::DXGI_ADAPTER_DESC1 dxgiAdapterDesc1;
            dxgiAdapter1->GetDesc1(&dxgiAdapterDesc1);

            log<WInfo>() << "Adapter: \n";
            printAdapterInfo(dxgiAdapterDesc1);

            // Is current adapter a software one?
            const auto isSoftwareAdapter{ (dxgiAdapterDesc1.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) != 0u };
            // Check if current adapter has requested features. Dummy device creation.
            const auto hasFeatures{ SUCCEEDED(::D3D12CreateDevice(dxgiAdapter1.Get(), 
                features, _uuidof(ID3D12Device), nullptr)) };
            // How much VRAM does this adapter have?
            const auto videoMemory{ dxgiAdapterDesc1.DedicatedVideoMemory };

            // Choose adapter with highest video memory.
            if (!isSoftwareAdapter && hasFeatures && videoMemory > maxVideoMemory)
            { // Found a better candidate.
                log<WInfo>() << "Adapter satisfies all requirements!\n";
                maxVideoMemory = videoMemory;
                util::throwIfFailed(dxgiAdapter1.As(&dxgiAdapter3), 
                    "Failed to cast DXGI adapter 1 to DXGI adapter 3!");
            }
        }
    }

    // Print information about chosen adapter.
    log<WInfo>() << "Chosen adapter: \n";
    ::DXGI_ADAPTER_DESC1 dxgiAdapterDesc1;
    dxgiAdapter3->GetDesc1(&dxgiAdapterDesc1);
    printAdapterInfo(dxgiAdapterDesc1);

    mAdapter = dxgiAdapter3;
}

D3D12Adapter::~D3D12Adapter()
{
    log<Info>() << "Destroyed adapter" << std::endl;
}

void D3D12Adapter::printAdapterInfo(const ::DXGI_ADAPTER_DESC1 &adapterDesc)
{
    lognp<WInfo>() << "\t" << adapterDesc.Description << 
        "\n\tMemory: " << adapterDesc.DedicatedVideoMemory / (1024.0 * 1024.0) << " MB" << 
        std::endl;
}

}

}

}
