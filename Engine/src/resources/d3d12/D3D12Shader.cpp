/**
* @file resources/d3d12/D3D12Shader.cpp
* @author Tomas Polasek
* @brief Wrapper around a Direct3D 12 shader.
*/

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12Shader.h"

// TODO - Add ifdef, checking whether we are doing a debug build.
#include <d3dcompiler.h>

namespace engine
{

namespace res
{

namespace d3d12
{
D3D12ShaderInclude::D3D12ShaderInclude() :
    mIncludePtr{ D3D_COMPILE_STANDARD_FILE_INCLUDE }
{ }

D3D12ShaderInclude::D3D12ShaderInclude(::ID3DInclude *handler) noexcept:
    mIncludePtr{handler}
{ }

void D3D12ShaderDefines::addDefine(const ::D3D_SHADER_MACRO &define)
{
    if (mDefines.empty())
    { // Create the first one.
        mDefines.push_back({});
    }

    mDefines.back() = define;
    mDefines.emplace_back(::D3D_SHADER_MACRO{ nullptr, nullptr });
}

D3D12Shader::D3D12Shader()
{ /* Automatic */ }

D3D12Shader::~D3D12Shader()
{ /* Automatic */ }

D3D12Shader::D3D12Shader(const std::wstring &filename)
{ precompiledFromFile(filename); }

D3D12Shader::D3D12Shader(const std::wstring &filename, const std::string &entryPoint, const std::string &target,
    ::UINT compilerFlags, const D3D12ShaderDefines &defines, const D3D12ShaderInclude &include)
{ compileFromFile(filename, entryPoint, target, compilerFlags, defines, include); }

void D3D12Shader::precompiledFromFile(const std::wstring &filename)
{
    util::throwIfFailed(::D3DReadFileToBlob(filename.c_str(), &mShader), 
        "Failed to load precompiled shader from file!");
    extractInformation();
}

void D3D12Shader::compileFromFile(const std::wstring &filename, const std::string &entryPoint,
    const std::string &target, ::UINT compilerFlags, const D3D12ShaderDefines &defines, 
    const D3D12ShaderInclude &include)
{
    ::ID3DBlob *messages{ nullptr };

    const auto result{ ::D3DCompileFromFile(
        filename.c_str(),
        defines.defines(),
        include.handler(),
        entryPoint.c_str(),
        target.c_str(),
        compilerFlags, 0u,
        &mShader, &messages)};

    processCompilationResult(result, messages);
    extractInformation();
}


void D3D12Shader::compileFromSource(const std::wstring &source, const std::string &entryPoint,
    const std::string &target, ::UINT compilerFlags, const D3D12ShaderDefines &defines,
    const D3D12ShaderInclude &include)
{
    ::ID3DBlob *messages{ nullptr };

    const auto result{ ::D3DCompileFromFile(
        source.c_str(),
        defines.defines(),
        include.handler(),
        entryPoint.c_str(),
        target.c_str(),
        compilerFlags, 0u,
        &mShader, &messages)};

    processCompilationResult(result, messages);
    extractInformation();
}

void D3D12Shader::processCompilationResult(HRESULT result, ID3DBlob *messages)
{
    if (FAILED(result))
    {
        std::stringstream ss;
        ss << "Failed to compile shader!";
        if (messages)
        {
            ss << ": \n" << reinterpret_cast<char*>(messages->GetBufferPointer());
            messages->Release();
        }
        throw util::winexception(ss.str());
    }
}

void D3D12Shader::extractInformation()
{
    ::ID3D12ShaderReflection *reflection{ nullptr };
    util::throwIfFailed(D3DReflect(
        mShader->GetBufferPointer(), 
        mShader->GetBufferSize(), 
        IID_PPV_ARGS(&reflection)), 
        "Failed to use shader reflection to get the shader description!");

    ASSERT_FAST(reflection != nullptr);

    ::D3D12_SHADER_DESC desc;
    util::throwIfFailed(reflection->GetDesc(&desc), 
        "Failed to get shader description from the reflection!");

    /*
     * Extract shader type from the version: 
     * Program type = (Version & 0xFFFF0000) >> 16
     * Major version = (Version & 0x000000F0) >> 4
     * Minor version = (Version & 0x0000000F)
     */
    mType = static_cast<::D3D12_SHADER_VERSION_TYPE>((desc.Version & 0xFFFF0000) >> 16u);
}

}

}

}
