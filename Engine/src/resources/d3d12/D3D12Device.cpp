/**
* @file resources/d3d12/D3D12Device.cpp
* @author Tomas Polasek
* @brief Direct3D 12 device wrapper.
*/

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12Device.h"

// Included in source file, since resource itself needs device
#include "engine/resources/d3d12/D3D12Resource.h"

namespace engine
{

namespace res
{

namespace d3d12
{

D3D12Device::D3D12Device(D3D12Adapter &adapter,
    ::D3D_FEATURE_LEVEL features, 
    const std::set<::D3D12_MESSAGE_CATEGORY> &disableCategories,
    const std::set<::D3D12_MESSAGE_SEVERITY> &disableSeverities,
    const std::set<::D3D12_MESSAGE_ID> &disableIds, 
    const wchar_t *dbgName)
{
    mDevice = createDevice(adapter, features, disableCategories, disableSeverities, disableIds, dbgName);
    log<Info>() << "Created device" << std::endl;
}

D3D12Device::~D3D12Device()
{
    log<Info>() << "Destroyed device" << std::endl;
}

ComPtr<::ID3D12Fence> D3D12Device::createFence(uint64_t initialValue)
{
    ComPtr<::ID3D12Fence> fence;

    util::throwIfFailed(mDevice->CreateFence(
        // Initial value.
        initialValue,
        // No additional flags.
        D3D12_FENCE_FLAG_NONE,
        // Destination.
        IID_PPV_ARGS(&fence)),
        "Failed to create fence!");

    return fence;
}

ComPtr<::ID3D12CommandAllocator> D3D12Device::createCommandAllocator(::D3D12_COMMAND_LIST_TYPE type)
{
    ComPtr<::ID3D12CommandAllocator> cmdAlloc;
    util::throwIfFailed(mDevice->CreateCommandAllocator(type, IID_PPV_ARGS(&cmdAlloc)),
                        "Failed to create command allocator!");

    return cmdAlloc;
}

ComPtr<::ID3D12GraphicsCommandList2> D3D12Device::createGraphicsCommandList2(::ID3D12CommandAllocator* allocator, 
    ::D3D12_COMMAND_LIST_TYPE type, ::ID3D12PipelineState *state)
{
    ComPtr<::ID3D12GraphicsCommandList2> cmdList;
    util::throwIfFailed(mDevice->CreateCommandList(
        // Single adapter -> setting to zero.
        0u,
        // Type of the command list.
        type,
        // Allocator used in creating the command list.
        allocator,
        // Initial pipeline state, nullptr for dummy state.
        state,
        // Destination of created command list.
        IID_PPV_ARGS(&cmdList)), 
        "Failed to create command list!");

    // TODO - Should this be here?
    // Set the command list to common state.
    //util::throwIfFailed(cmdList->Close(), "Failed to close graphics command list!");

    return cmdList;
}

ComPtr<::ID3D12CommandQueue> D3D12Device::createCommandQueue(::D3D12_COMMAND_LIST_TYPE type, 
    ::D3D12_COMMAND_QUEUE_PRIORITY priority)
{
    ComPtr<::ID3D12CommandQueue> commandQueue;

    ::D3D12_COMMAND_QUEUE_DESC desc = {};
    desc.Type = type;
    desc.Priority = priority;
    desc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
    // Single adapter application, mask is unnecessary
    desc.NodeMask = 0;

    // Attempt to create the command queue.
    util::throwIfFailed(mDevice->CreateCommandQueue(&desc, IID_PPV_ARGS(&commandQueue)),
                        "Failed to create command queue!");

    return commandQueue;
}

ComPtr<::ID3D12DescriptorHeap> D3D12Device::createDescHeap(::D3D12_DESCRIPTOR_HEAP_TYPE type, 
    uint32_t numDescriptors, ::D3D12_DESCRIPTOR_HEAP_FLAGS flags)
{
    ComPtr<::ID3D12DescriptorHeap> descHeap;

    ::D3D12_DESCRIPTOR_HEAP_DESC descHeapDesc = { };
    // Type of the heap, specifying types of descriptor creatable on it.
    descHeapDesc.Type = type;
    // Number of descriptors on this heap.
    descHeapDesc.NumDescriptors = numDescriptors;
    // No flags.
    descHeapDesc.Flags = flags;
    // Single adapter -> setting node mask to zero.
    descHeapDesc.NodeMask = 0u;

    util::throwIfFailed(mDevice->CreateDescriptorHeap(&descHeapDesc, IID_PPV_ARGS(&descHeap)), 
        "Failed to create descriptor heap!");

    return descHeap;
}

ComPtr<::ID3D12Device2> D3D12Device::createDevice(D3D12Adapter &adapter, 
    ::D3D_FEATURE_LEVEL features, 
    const std::set<::D3D12_MESSAGE_CATEGORY> &disableCategories, 
    const std::set<::D3D12_MESSAGE_SEVERITY> &disableSeverities, 
    const std::set<::D3D12_MESSAGE_ID> &disableIds, 
    const wchar_t *dbgName)
{
    // Create the D3D12 device using chosen adapter.
    ComPtr<::ID3D12Device2> device;

    util::throwIfFailed(D3D12CreateDevice(adapter.get(), 
                                          features, IID_PPV_ARGS(&device)), 
                        "Failed to create D3D12 device!");

    // Configure DX12 debugging layer.
#ifdef _DEBUG
    ComPtr<::ID3D12InfoQueue> infoQueue;
    if (SUCCEEDED(device.As(&infoQueue)))
    { // Apply settings for created device.
        log<Info>() << "Setting up device debugging capabilities." << std::endl;
        // Enable debugging brake on chosen message types.
        infoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_CORRUPTION, true);
        infoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_ERROR, true);
        infoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_WARNING, true);

        // Make non-const copies, since message filter requires pointer to non-const array...
        std::vector dc(disableCategories.begin(), disableCategories.end());
        std::vector ds(disableSeverities.begin(), disableSeverities.end());
        std::vector di(disableIds.begin(), disableIds.end());

        // Build the filter.
        ::D3D12_INFO_QUEUE_FILTER msgFilter = { };
        // Disabled categories
        msgFilter.DenyList.NumCategories = static_cast<UINT>(dc.size());
        msgFilter.DenyList.pCategoryList = dc.data();
        // Disabled severities
        msgFilter.DenyList.NumSeverities = static_cast<UINT>(ds.size());
        msgFilter.DenyList.pSeverityList = ds.data();
        // Disabled IDs
        msgFilter.DenyList.NumIDs= static_cast<UINT>(di.size());
        msgFilter.DenyList.pIDList = di.data();

        // Apply chosen settings.
        util::throwIfFailed(infoQueue->PushStorageFilter(&msgFilter), 
                            "Failed to apply message filter!");
    }
    else
    {
        log<Warning>() << "Failed to setup device debugging capabilities!" << std::endl;
    }

    if (dbgName)
    {
        device->SetName(dbgName);
    }
#endif // _DEBUG

    return device;
}

} 

} 

} 
