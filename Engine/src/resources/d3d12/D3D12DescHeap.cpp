/**
* @file resources/d3d12/D3D12DescHeap.cpp
* @author Tomas Polasek
* @brief Wrapper around Direct3D 12 descriptor heap.
*/

#include "stdafx.h"
#include "engine/resources/d3d12/D3D12DescHeap.h"

#include "engine/resources/d3d12/D3D12Resource.h"

namespace engine
{

namespace res
{

namespace d3d12
{

D3D12DescHeap::D3D12DescHeap()
{ /* Automatic */ }

D3D12DescHeap::D3D12DescHeap(D3D12Device &device, ::D3D12_DESCRIPTOR_HEAP_TYPE type, 
    uint32_t numDescriptors, D3D12_DESCRIPTOR_HEAP_FLAGS flags) :
    D3D12DescHeap(device, { type, numDescriptors, flags, 0u })
{ }

D3D12DescHeap::D3D12DescHeap(D3D12Device &device, const ::D3D12_DESCRIPTOR_HEAP_DESC &desc) :
    mDescHandleSize{ device.getDescriptorHandleSize(desc.Type) }
{ initializeHeap(device, desc); }

void D3D12DescHeap::initializeHeap(D3D12Device &device, const ::D3D12_DESCRIPTOR_HEAP_DESC &desc)
{
    mDescHeap = createDescHeap(device, desc);

    mCurrentIt = mDescHeap->GetCPUDescriptorHandleForHeapStart();
    mEnd.InitOffsetted(mCurrentIt, desc.NumDescriptors * mDescHandleSize);
}

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::nextHandle()
{
    if (mCurrentIt == mEnd)
    {
        throw DescHeapFull("Descriptor heap is already full!");
    }

    auto result{ mCurrentIt };
    mCurrentIt.Offset(mDescHandleSize);

    return result;
}

void D3D12DescHeap::resetHandle()
{ mCurrentIt = mDescHeap->GetCPUDescriptorHandleForHeapStart(); }

::CD3DX12_CPU_DESCRIPTOR_HANDLE D3D12DescHeap::createDSV(D3D12Device &device, D3D12Resource &resource, const D3D12_DEPTH_STENCIL_VIEW_DESC &desc)
{
    auto handle{ nextHandle() };
    device->CreateDepthStencilView(resource.get(), &desc, handle);
    return handle;
}

ComPtr<::ID3D12DescriptorHeap> D3D12DescHeap::createDescHeap(D3D12Device &device, 
    const ::D3D12_DESCRIPTOR_HEAP_DESC &desc)
{
    ComPtr<::ID3D12DescriptorHeap> result;

    util::throwIfFailed(device->CreateDescriptorHeap(&desc, IID_PPV_ARGS(&result)), 
        "Failed to create the descriptor heap!");

    return result;
}

}

}

}
