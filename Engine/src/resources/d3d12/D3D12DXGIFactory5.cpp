/**
* @file resources/d3d12/D3D12DXGIFactory5.cpp
* @author Tomas Polasek
* @brief Wrapper around DXGI factory.
*/

#include "stdafx.h"

#include "engine/resources/d3d12/D3D12DXGIFactory5.h"

namespace engine
{

namespace res
{

namespace d3d12
{

D3D12DXGIFactory5::D3D12DXGIFactory5()
{
    // DirectX Graphics Infrastructure factory.
    ComPtr<::IDXGIFactory5> dxgiFactory;
    // No flags.
    uint32_t createFactoryFlags{0};

#ifdef _DEBUG
    createFactoryFlags = DXGI_CREATE_FACTORY_DEBUG;
#endif

    // Create the factory.
    util::throwIfFailed(::CreateDXGIFactory2(createFactoryFlags, IID_PPV_ARGS(&dxgiFactory)),
                        "Failed to create DXGI factory 5!");

    mFactory = dxgiFactory;
}

bool D3D12DXGIFactory5::isFeatureSupported(DXGI_FEATURE feature) const
{
    auto featureSupported{ false };

    BOOL checkFlag{ FALSE };

    if (SUCCEEDED(mFactory->CheckFeatureSupport(
        feature, &checkFlag, sizeof(checkFlag))))
    { // Check correctly executed.
        // Conversion from BOOL into bool.
        featureSupported = checkFlag == TRUE;
    }
    else
    { // Unable to check -> default to false.
        featureSupported = false;
    }

    return featureSupported;
}

void D3D12DXGIFactory5::setMessageHandling(HWND hwnd, UINT flags)
{
    util::throwIfFailed(mFactory->MakeWindowAssociation(hwnd, flags), 
        "Failed to disable DXGI message handling!");
}

} 

} 

} 
