/**
* @file helpers/d3d12/D3D12ResourceUpdater.cpp
* @author Tomas Polasek
* @brief Helper class for updating D3D12 resources.
*/

#include "stdafx.h"
#include "engine/helpers/d3d12/D3D12ResourceUpdater.h"

namespace engine
{

namespace helpers
{

namespace d3d12
{

D3D12ResourceUpdater::D3D12ResourceUpdater(res::d3d12::D3D12Device &device) :
    mAllocator(device, 
        requiredHeapProps(), 
        requiredHeapFlags())
{ }

D3D12ResourceUpdater::D3D12ResourceUpdater(D3D12CommittedAllocator allocator) :
    mAllocator(allocator)
{
    if (!allocatorMeetsRequirements(allocator))
    {
        throw IncompatibleAllocatorException("Allocator does not meet ResourceUpdater requirements!");
    }
}

D3D12ResourceUpdater::~D3D12ResourceUpdater()
{
    /* Automatic */

    // The buffers should be explicitly released by calling commandListExecuted().
    if (mStashedBuffers.empty())
    {
        log<Debug>() << "Destructing ResourceUpdater which still contains some stashed buffers!" << std::endl;
    }
}

void D3D12ResourceUpdater::copyToResource(const void *data, std::size_t sizeInBytes, 
    res::d3d12::D3D12Resource &dest, res::d3d12::D3D12CommandList &cmdList)
{
    if (data == nullptr || sizeInBytes == 0u)
    { return; }

    // Create the intermediate resource used for data upload.
    res::d3d12::D3D12Resource intermediateResource(::CD3DX12_RESOURCE_DESC::Buffer(sizeInBytes), 
        ::D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, mAllocator);

#ifdef _DEBUG
    if (mDbgName)
    {
        util::throwIfFailed(intermediateResource->SetName(mDbgName),
                            "Failed to set intermediate resource name!");
    }
#endif

    // Specify the source data.
    ::D3D12_SUBRESOURCE_DATA subresourceData{ };
    subresourceData.pData = data;
    subresourceData.RowPitch = sizeInBytes;
    subresourceData.SlicePitch = sizeInBytes;

    // TODO - Implement copy, including checking and winexception throwing.
    // Record the copy command.
    const auto result{ UpdateSubresources(cmdList.get(), dest.get(), intermediateResource.get(),
        0u, 0u, 1u, &subresourceData) };

    if (result < sizeInBytes)
    { // Copying failed.
        throw util::winexception("CopyToResource failed, number of bytes copied is smaller than the source buffer size!");
    }

    // TODO - Change dest resource state?

    // Keep the intermediate buffer alive.
    stashBuffer(intermediateResource);
}

void D3D12ResourceUpdater::setDebugName(const wchar_t *dbgName)
{
#ifdef _DEBUG
    mDbgName = dbgName;
#endif
}

bool D3D12ResourceUpdater::allocatorMeetsRequirements(const D3D12CommittedAllocator &allocator)
{ return allocator.heapProperties() == requiredHeapProps() && allocator.heapFlags() == requiredHeapFlags(); }

void D3D12ResourceUpdater::commandListExecuted()
{ mStashedBuffers.clear(); }

void D3D12ResourceUpdater::stashBuffer(res::d3d12::D3D12Resource &buffer)
{ mStashedBuffers.push_back(buffer.getPtr()); }

}

}

}
