/**
* @file helpers/d3d12/D3D12PipelineStateBuilder.cpp
* @author Tomas Polasek
* @brief Wrapper around pipeline state configuration.
*/

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12PipelineStateBuilder.h"
#include "engine/util/Math.h"

namespace engine
{

namespace helpers
{

namespace d3d12
{
D3D12PipelineStateBuilder::D3D12PipelineStateBuilder()
{ /* Automatic */ }

D3D12PipelineStateBuilder::~D3D12PipelineStateBuilder()
{ /* Automatic */ }

res::d3d12::D3D12PipelineState D3D12PipelineStateBuilder::build(res::d3d12::D3D12Device &device) 
{
    ::D3D12_PIPELINE_STATE_STREAM_DESC desc;
    desc.pPipelineStateSubobjectStream = mCurrentState.data();
    desc.SizeInBytes = mCurrentState.size();

    return res::d3d12::D3D12PipelineState(desc, device);
}

uint8_t *D3D12PipelineStateBuilder::find(::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE type)
{
    ASSERT_FAST(type < NUM_SUB_OBJ_TYPES);
    const auto offset{ mSubObjectOffsets[type] };
    ASSERT_FAST(offset == 0u || mCurrentState.size() < (offset - 1u));
    // Minus one, so we can use 0u as special value for "not set".
    return offset == 0u ? nullptr : mCurrentState.data() + (offset - 1u);
}

uint8_t *D3D12PipelineStateBuilder::allocateInner(::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE type, std::size_t size)
{
    if (find(type) != nullptr)
    { // Already allocated!
        throw SubObjectAlreadyAddedException("Unable to allocate pipeline state sub-object - Sub-object has already been added!");
    }

    const auto alignedStart{ util::math::alignTo(mCurrentState.size(), SUB_OBJ_ALIGNMENT) };
    const auto requiredSize{ alignedStart + size };

    mCurrentState.resize(requiredSize);
    mSubObjectOffsets[type] = alignedStart;

    return mCurrentState.data() + alignedStart;
}

void D3D12PipelineStateBuilder::deallocateInner(::D3D12_PIPELINE_STATE_SUBOBJECT_TYPE type, std::size_t size)
{
    const auto ptr{ find(type) };
    if (ptr == nullptr)
    { // Already allocated!
        throw SubObjectNotFoundException("Unable to de-allocate pipeline state sub-object - Sub-object has not been added!");
    }

    const auto subObjBegin{ mCurrentState.begin() + mSubObjectOffsets[type] };
    const auto subObjEnd{ subObjBegin + size };

    ASSERT_FAST(subObjEnd <= mCurrentState.end());
    mCurrentState.erase(subObjBegin, subObjEnd);

    mSubObjectOffsets[type] = 0u;
}

}

}

}
