/**
* @file helpers/d3d12/D3D12DebugConfigurator.cpp
* @author Tomas Polasek
* @brief Direct3D 12 debugging configurator.
*/

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12DebugConfigurator.h"

namespace engine
{

namespace helpers
{

namespace d3d12
{

D3D12DebugConfigurator::D3D12DebugConfigurator(bool debugLayer, bool gpuValidation)
{
    if (debugLayer)
    {
        enableDebugLayer();
    }

    if (gpuValidation)
    {
        enableGpuValidation();
    }
}

void D3D12DebugConfigurator::enableDebugLayer()
{
    ComPtr<ID3D12Debug> debugInterface;
    util::throwIfFailed(D3D12GetDebugInterface(IID_PPV_ARGS(&debugInterface)), 
        "Failed to get the D3D12 debug interface!");
    debugInterface->EnableDebugLayer();
}

void D3D12DebugConfigurator::enableGpuValidation()
{
    ComPtr<ID3D12Debug> debugInterface;
    ComPtr<ID3D12Debug1> debugInterface1;
    util::throwIfFailed(D3D12GetDebugInterface(IID_PPV_ARGS(&debugInterface)), 
        "Failed to get the D3D12 debug interface!");
    util::throwIfFailed(debugInterface->QueryInterface(IID_PPV_ARGS(&debugInterface1)), 
        "Failed to get the D3D12 debug interface1");
    debugInterface1->SetEnableGPUBasedValidation(true);
}

} 

} 

} 
