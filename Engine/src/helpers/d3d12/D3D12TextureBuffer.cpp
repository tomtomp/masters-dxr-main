/**
* @file helpers/d3d12/D3D12TextureBuffer.cpp
* @author Tomas Polasek
* @brief Wrapper around D3D12Resource, providing a texture interface.
*/

#include "stdafx.h"
#include "engine/helpers/d3d12/D3D12TextureBuffer.h"

namespace engine
{

namespace helpers
{

namespace d3d12
{

D3D12TextureBuffer::D3D12TextureBuffer()
{ /* Automatic */ }

D3D12TextureBuffer::~D3D12TextureBuffer()
{ /* Automatic */ }

D3D12TextureBuffer::D3D12TextureBuffer(const ::CD3DX12_RESOURCE_DESC &desc, D3D12BaseGpuAllocator &allocator,
    ::D3D12_RESOURCE_STATES initState, const ::D3D12_CLEAR_VALUE *clearValue)
{ allocate(allocator, desc, initState, clearValue); }

void D3D12TextureBuffer::allocate(D3D12BaseGpuAllocator &allocator, const ::CD3DX12_RESOURCE_DESC &desc, 
    ::D3D12_RESOURCE_STATES initState, const ::D3D12_CLEAR_VALUE *clearValue)
{ mTextureBuffer = res::d3d12::D3D12Resource(desc, initState, clearValue, allocator); }

void D3D12TextureBuffer::uploadData(const void *data, std::size_t sizeInBytes, std::size_t sizeOfVertex, 
    res::d3d12::D3D12CommandList &cmdList, D3D12ResourceUpdater &updater)
{
    if (!mTextureBuffer || sizeInBytes > mTextureBuffer.allocatedSize())
    {
        throw TextureTooSmallException("Texture buffer is not allocated or it is too small to contain given data!");
    }

    updater.copyToResource(data, sizeInBytes, mTextureBuffer, cmdList);
}

void D3D12TextureBuffer::setDebugName(const wchar_t *dbgName)
{
#ifdef _DEBUG
    if (dbgName && mTextureBuffer)
    {
        util::throwIfFailed(mTextureBuffer->SetName(dbgName), 
                            "Failed to set vertex buffer name.");
    }
#endif
}

}

}

}
