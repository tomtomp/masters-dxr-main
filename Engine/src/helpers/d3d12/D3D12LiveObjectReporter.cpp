/**
* @file helpers/d3d12/D3D12LiveObjectReporter.h
* @author Tomas Polasek
* @brief Direct3D 12 helper which reports live 
* objects on destruction.
*/

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12LiveObjectReporter.h"

namespace engine
{

namespace helpers
{

namespace d3d12
{

D3D12LiveObjectReporter::D3D12LiveObjectReporter(bool doReport, 
    DXGI_DEBUG_ID debugId, DXGI_DEBUG_RLO_FLAGS flags) :
    mDoReport{ doReport }, mDebugId { debugId }, mFlags{ flags }
{ }

D3D12LiveObjectReporter::~D3D12LiveObjectReporter()
{
    if (mDoReport)
    {
        ComPtr<::IDXGIDebug1> dxgiDebug;
        util::throwIfFailed(DXGIGetDebugInterface1(0, IID_PPV_ARGS(&dxgiDebug)), 
            "Failed to get the debug interface!");

        log<Info>() << "Performing live objects check: " << std::endl;
        dxgiDebug->ReportLiveObjects(mDebugId, mFlags);
        log<Info>() << "End of live objects check." << std::endl;
    }
}

} 

} 

} 
