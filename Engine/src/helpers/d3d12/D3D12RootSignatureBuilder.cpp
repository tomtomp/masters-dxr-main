/**
* @file helpers/d3d12/D3D12RootSignatureBuilder.cpp
* @author Tomas Polasek
* @brief D3D12 root signature configurator and builder.
*/

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12RootSignatureBuilder.h"

namespace engine
{

namespace helpers
{

namespace d3d12
{

D3D12RootSignatureBuilder::D3D12RootSignatureBuilder()
{ /* Automatic */ }

D3D12RootSignatureBuilder::~D3D12RootSignatureBuilder()
{ /* Automatic */ }

D3D12RootSignatureBuilder &D3D12RootSignatureBuilder::addRootParameter(const ::CD3DX12_ROOT_PARAMETER1 &parameter)
{
    mRootParamters.emplace_back(parameter);
    return *this;
}

D3D12RootSignatureBuilder & D3D12RootSignatureBuilder::addDescriptorTableParameter(::UINT numDescriptorRanges,
    const ::D3D12_DESCRIPTOR_RANGE1 *descriptorRanges, ::D3D12_SHADER_VISIBILITY visibility)
{
    ::CD3DX12_ROOT_PARAMETER1 p;
    p.InitAsDescriptorTable(numDescriptorRanges, descriptorRanges, visibility);

    return addRootParameter(p);
}

D3D12RootSignatureBuilder & D3D12RootSignatureBuilder::addConstantParameter(::UINT num32BitValues,
    ::UINT shaderRegister, ::UINT registerSpace, ::D3D12_SHADER_VISIBILITY visibility)
{
    ::CD3DX12_ROOT_PARAMETER1 p;
    p.InitAsConstants(num32BitValues, shaderRegister, registerSpace, visibility);

    return addRootParameter(p);
}

D3D12RootSignatureBuilder & D3D12RootSignatureBuilder::addConstantBufferViewParameter(::UINT shaderRegister,
    ::UINT registerSpace, ::D3D12_ROOT_DESCRIPTOR_FLAGS flags, ::D3D12_SHADER_VISIBILITY visibility)
{
    ::CD3DX12_ROOT_PARAMETER1 p;
    p.InitAsConstantBufferView(shaderRegister, registerSpace, flags, visibility);

    return addRootParameter(p);
}

D3D12RootSignatureBuilder & D3D12RootSignatureBuilder::addShaderResourceViewParameter(::UINT shaderRegister,
    ::UINT registerSpace, ::D3D12_ROOT_DESCRIPTOR_FLAGS flags, ::D3D12_SHADER_VISIBILITY visibility)
{
    ::CD3DX12_ROOT_PARAMETER1 p;
    p.InitAsShaderResourceView(shaderRegister, registerSpace, flags, visibility);

    return addRootParameter(p);
}

D3D12RootSignatureBuilder & D3D12RootSignatureBuilder::addUnorderedAccessViewParameter(::UINT shaderRegister,
    ::UINT registerSpace, ::D3D12_ROOT_DESCRIPTOR_FLAGS flags, ::D3D12_SHADER_VISIBILITY visibility)
{
    ::CD3DX12_ROOT_PARAMETER1 p;
    p.InitAsUnorderedAccessView(shaderRegister, registerSpace, flags, visibility);

    return addRootParameter(p);
}

D3D12RootSignatureBuilder & D3D12RootSignatureBuilder::addStaticSampler(UINT shaderRegister, D3D12_FILTER filter,
    D3D12_TEXTURE_ADDRESS_MODE addressU, D3D12_TEXTURE_ADDRESS_MODE addressV, D3D12_TEXTURE_ADDRESS_MODE addressW,
    FLOAT mipLODBias, UINT maxAnisotropy, D3D12_COMPARISON_FUNC comparisonFunc, D3D12_STATIC_BORDER_COLOR borderColor,
    FLOAT minLOD, FLOAT maxLOD, D3D12_SHADER_VISIBILITY shaderVisibility, UINT registerSpace)
{
    const ::CD3DX12_STATIC_SAMPLER_DESC s(
        shaderRegister, filter, 
        addressU, addressV, addressW, 
        mipLODBias, maxAnisotropy, 
        comparisonFunc, borderColor, 
        minLOD, maxLOD, 
        shaderVisibility, registerSpace);

    return addStaticSampler(s);
}

D3D12RootSignatureBuilder &D3D12RootSignatureBuilder::operator<<(const ::CD3DX12_ROOT_PARAMETER1 &parameter)
{ return addRootParameter(parameter); }

D3D12RootSignatureBuilder &D3D12RootSignatureBuilder::clearRootParameters()
{
    mRootParamters.clear();
    return *this;
}

D3D12RootSignatureBuilder &D3D12RootSignatureBuilder::addStaticSampler(const ::CD3DX12_STATIC_SAMPLER_DESC &sampler)
{
    mStaticSamplers.emplace_back(sampler);
    return *this;
}

D3D12RootSignatureBuilder &D3D12RootSignatureBuilder::operator<<(const ::CD3DX12_STATIC_SAMPLER_DESC &sampler)
{ return addStaticSampler(sampler); }

D3D12RootSignatureBuilder &D3D12RootSignatureBuilder::clearStaticSamplers()
{
    mStaticSamplers.clear();
    return *this;
}

D3D12RootSignatureBuilder &D3D12RootSignatureBuilder::setFlags(::D3D12_ROOT_SIGNATURE_FLAGS flags)
{
    mFlags = flags;
    return *this;
}

res::d3d12::D3D12RootSignature D3D12RootSignatureBuilder::build(res::d3d12::D3D12Device &device) const
{
    // Check for support of root signature version 1.1 .
    const auto supportsRS11{ device.supportsFeatures(
        ::D3D12_FEATURE_DATA_ROOT_SIGNATURE{ ::D3D_ROOT_SIGNATURE_VERSION_1_1 }, 
        ::D3D12_FEATURE_ROOT_SIGNATURE) };

    return res::d3d12::D3D12RootSignature(
        mRootParamters.data(), mRootParamters.size(), 
        mStaticSamplers.data(), mStaticSamplers.size(), 
        mFlags, supportsRS11, device);
}

}

}

}
