/**
* @file helpers/d3d12/D3D12ReservedAllocator.cpp
* @author Tomas Polasek
* @brief Allocator for Direct3D 12 reserved resources.
*/

#include "stdafx.h"

#include "engine/helpers/d3d12/D3D12ReservedAllocator.h"

namespace engine
{

namespace helpers
{

namespace d3d12
{

D3D12ReservedAllocator::D3D12ReservedAllocator(res::d3d12::D3D12Device &device) :
    mDevice{ &device }
{
}

void D3D12ReservedAllocator::allocate(const D3D12_RESOURCE_DESC &desc, D3D12_RESOURCE_STATES initState,
    const D3D12_CLEAR_VALUE *optimizedClearValue, const IID &riid, void **ptr)
{
    util::throwIfFailed((*mDevice)->CreateReservedResource(&desc, initState, 
        optimizedClearValue, riid, ptr), 
        "Failed to allocate reserved resource!");
}

void D3D12ReservedAllocator::deallocate(const IID &riid, void **ptr)
{
    UNUSED(riid);
    UNUSED(ptr);
    // No actions are required.
}

}

}

}
