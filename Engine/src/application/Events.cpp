/**
* @file application/Events.cpp
* @author Tomas Polasek
* @brief Event structures.
*/

#include "stdafx.h"

#include "engine/application/Events.h"

namespace engine
{

namespace app
{

// Extern template definitions.
DEFINE_EXTERN_MESSAGE_BUS(::engine::app::events::KeyboardEvent);
DEFINE_EXTERN_MESSAGE_BUS(::engine::app::events::MouseButtonEvent);
DEFINE_EXTERN_MESSAGE_BUS(::engine::app::events::MouseMoveEvent);
DEFINE_EXTERN_MESSAGE_BUS(::engine::app::events::MouseScrollEvent);
DEFINE_EXTERN_MESSAGE_BUS(::engine::app::events::ResizeEvent);
DEFINE_EXTERN_MESSAGE_BUS(::engine::app::events::ExitEvent);

} 

} 
