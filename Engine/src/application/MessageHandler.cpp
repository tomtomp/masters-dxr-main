/**
* @file application/MessageHandler.cpp
* @author Tomas Polasek
* @brief Message pump and message handling class.
*/

#include "stdafx.h"

#include "engine/application/MessageHandler.h"

namespace engine
{

namespace app
{

MessageHandler::MessageHandler() :
    mKeyboardEventBus{ KeyboardEventBus::create() }, 
    mKeyboardEventWriter{ mKeyboardEventBus->writer() }, 
    mMouseButtonEventBus{ MouseButtonEventBus::create() }, 
    mMouseButtonEventWriter{ mMouseButtonEventBus->writer() }, 
    mMouseMoveEventBus{ MouseMoveEventBus::create() }, 
    mMouseMoveEventWriter{ mMouseMoveEventBus->writer() }, 
    mMouseScrollEventBus{ MouseScrollEventBus::create() }, 
    mMouseScrollEventWriter{ mMouseScrollEventBus->writer() }, 
    mResizeEventBus{ ResizeEventBus::create() }, 
    mResizeEventWriter{ mResizeEventBus->writer() }, 
    mExitEventBus{ ExitEventBus::create() }, 
    mExitEventWriter{ mExitEventBus->writer() }
{ log<Info>() << "Initialized message handling sub-system" << std::endl; }

void MessageHandler::runBlockingMsgPump()
{
    MSG msg{ };

    // Process messages.
    while (msg.message != WM_QUIT && ::GetMessage(&msg, nullptr, 0, 0))
    { // Until the window is closed.
        ::TranslateMessage(&msg);
        ::DispatchMessage(&msg);
    }
}

bool MessageHandler::processMessage()
{
    MSG msg{ };
    bool result{ true };

    if (::PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE | PM_NOYIELD))
    {
        ::TranslateMessage(&msg);
        ::DispatchMessage(&msg);

        result = msg.message != WM_QUIT;
    }

    return result;
}

void MessageHandler::registerWindow(res::win::Window &window)
{ mRegisteredWindows.emplace(window.hwnd(), window); }

LRESULT MessageHandler::handlerConnector(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    // Only one instance of App exists.
    static MessageHandler *thisPtr{ nullptr };

    if (uMsg == WM_NCCREATE)
    { // Initialization message.
        // Get this pointer passed by the CreateWindow function.
        thisPtr = static_cast<MessageHandler*>(reinterpret_cast<CREATESTRUCT*>(lParam)->lpCreateParams);
    }

    if (thisPtr)
    { // Handler pointer has been set correctly.
        // Attempt to find the target window.
        res::win::Window *targetWindow{ thisPtr->findWindow(hwnd) };

        if (targetWindow)
        { // Window has been found, we can use our message handler.
            try
            {
                return thisPtr->wndProc(*targetWindow, uMsg, wParam, lParam);
            } catch (...)
            { }
        }
        // Else use the default processing...
    }

    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

LRESULT MessageHandler::wndProc(res::win::Window &window, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    // Shortcut used for user message types.
    using ::engine::res::win::Window;

    switch (uMsg)
    {
        case WM_DISPLAYCHANGE:
        case WM_PAINT:
            { // Cancel the invalidation, we present when we want to.
                ValidateRect(window.hwnd(), nullptr);
                break;
            }
        case WM_KEYDOWN:
        case WM_KEYUP:
        case WM_SYSKEYDOWN:
        case WM_SYSKEYUP:
            { // Keyboard event messages.
                const auto event{ mKeyboard.processEvent(uMsg, wParam, lParam) };

                mKeyboardEventWriter.postMessage(event);

                break;
            }
        case WM_MOUSEMOVE:
            { // Mouse move event message.
                const auto event{ mMouse.processMoveEvent(uMsg, wParam, lParam) };

                mMouseMoveEventWriter.postMessage(event);

                break;
            }
        case WM_LBUTTONDOWN:
        case WM_LBUTTONUP:
        case WM_LBUTTONDBLCLK:
        case WM_RBUTTONDOWN:
        case WM_RBUTTONUP:
        case WM_RBUTTONDBLCLK:
        case WM_MBUTTONDOWN:
        case WM_MBUTTONUP:
        case WM_MBUTTONDBLCLK:
        case WM_XBUTTONDOWN:
        case WM_XBUTTONUP:
        case WM_XBUTTONDBLCLK:
            { // Mouse button event messages.
                const auto event{ mMouse.processBtnEvent(uMsg, wParam, lParam)};

                mMouseButtonEventWriter.postMessage(event);

                break;
            }
        case WM_MOUSEWHEEL:
            { // Mouse scroll wheel event message.
                const auto event{ mMouse.processScrollEvent(uMsg, wParam, lParam)};

                mMouseScrollEventWriter.postMessage(event);

                break;
            }
        case WM_SIZE:
            { // Window changed size.
                const auto width{ LOWORD(lParam) };
                const auto height{ HIWORD(lParam) };
                const events::ResizeEvent event(width, height);

                mResizeEventWriter.postMessage(event);

                break;
            }
        case WM_DESTROY:
            { // Window is being closed.
                // Post "Quit application" message.
                ::PostQuitMessage(0);

                mExitEventWriter.postMessage();

                break;
            }
        case Window::windowMsgToId(Window::WindowMessage::Show):
            { // User requesting the window to be visible.
                window.setVisible(true);
                break;
            }
        case Window::windowMsgToId(Window::WindowMessage::Hide):
            { // User requesting the window to be invisible.
                window.setVisible(false);
                break;
            }
        case Window::windowMsgToId(Window::WindowMessage::Maximize):
            { // User requests maximization of the window.
                window.setFullscreen(true);
                break;
            }
        case Window::windowMsgToId(Window::WindowMessage::Restore):
            { // User requests restoration of the window from maximized state.
                window.setFullscreen(false);
                break;
            }
        default:
            { // Messages we don't process should be left to the default handler.
                return ::DefWindowProcW(window.hwnd(), uMsg, wParam, lParam);
            }
    }

    return 0;
}

res::win::Window *MessageHandler::findWindow(HWND hwnd)
{
    const auto findIt{ mRegisteredWindows.find(hwnd) };
    if (findIt != mRegisteredWindows.end())
    { // Found the window.
        return &(findIt->second);
    }
    else
    { // Unable to find the window.
        return nullptr;
    }
}
} 

} 
