/**
* @file lib/dxtk.cpp
* @author Tomas Polasek
* @brief Helper source for the DirectX ToolKit library.
*/

#include "../lib/dxtk/SimpleMath.cpp"
