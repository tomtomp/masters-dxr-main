/**
* @file lib/tinygltf.cpp
* @author Tomas Polasek
* @brief Sources of the tinygltf library.
*/

#ifdef _MSC_BUILD
#define _CRT_SECURE_NO_WARNINGS
#define _SILENCE_ALL_CXX17_DEPRECATION_WARNINGS
#endif

// Create implementation in this object file.
#define TINYGLTF_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION

#include "../lib/tinygltf/tiny_gltf.h"
