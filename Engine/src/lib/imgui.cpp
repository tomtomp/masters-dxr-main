/**
* @file lib/imgui.cpp
* @author Tomas Polasek
* @brief Sources of the imgui library.
*/

// Main library: 
#include "../lib/imgui/imgui.cpp"
#include "../lib/imgui/imgui_draw.cpp"
#include "../lib/imgui/imgui_widgets.cpp"
// Demo code not required.
#include "../lib/imgui/imgui_demo.cpp"
