/**
* @file EngineConfig.cpp
* @author Tomas Polasek
* @brief Engine configuration static settings can be changed 
* in this file. The runtime config is provided by the user when 
* initializing the engine.
*/

#include "stdafx.h"
#include "EngineConfig.h"

namespace engine
{

const std::initializer_list<::D3D12_MESSAGE_CATEGORY>
EngineConfig::RUNTIME_DEFAULT_DEVICE_DISABLE_MSG_CATEGORIES = { 
};

const std::initializer_list<::D3D12_MESSAGE_SEVERITY> 
EngineConfig::RUNTIME_DEFAULT_DEVICE_DISABLE_MSG_SEVERITIES = { 
    // Disable information messages, such as creation/destruction of objects.
    D3D12_MESSAGE_SEVERITY_INFO
};

const std::initializer_list<::D3D12_MESSAGE_ID>
EngineConfig::RUNTIME_DEFAULT_DEVICE_DISABLE_MSG_IDS = { 
    // Error displayed when capturing a frame in the graphics debugger.
    D3D12_MESSAGE_ID_MAP_INVALID_NULLRANGE,
    // Error displayed when capturing a frame in the graphics debugger.
    D3D12_MESSAGE_ID_UNMAP_INVALID_NULLRANGE,
};

} // namespace engine
