/**
* @file Engine.cpp
* @author Tomas Polasek
* @brief Main engine class, connects resource manager, renderer and application.
*/

#include "stdafx.h"

#include "Engine.h"

namespace engine
{

Engine::Engine(const EngineRuntimeConfig &cfg, HINSTANCE hInstance) :
    mHInstance{hInstance},
    mThreadPool(), 
    mMessageHandler(), 
    mWindowClass(mHInstance, Config::WINDOW_CLASS_MAIN_NAME, app::MessageHandler::windowProcCallback()), 
    mWindow(mHInstance, cfg.windowWidth, cfg.windowHeight, cfg.windowTitle, mWindowClass, &mMessageHandler),
    mWindowRunner(mThreadPool, mWindow, mMessageHandler, "WindowThread"), 
    mRenderer(cfg, mWindow)
{ }

Engine::~Engine()
{
    mThreadPool.joinAll();
}

} 
