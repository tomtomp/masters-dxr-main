/**
* @file renderer/ClearPresentRL.cpp
* @author Tomas Polasek
* @brief Rendering layer allowing clearing and presenting buffers in 
* a flip-discard swap chain.
*/

#include "stdafx.h"
#include "engine/renderer/ClearPresentRL/ClearPresentRL.h"

#include "engine/renderer/Renderer.h"

namespace engine
{

namespace rndr
{

ClearPresentRL::ClearPresentRL(Renderer &renderer) :
    mRenderer{ renderer }
{ }

res::d3d12::D3D12CommandList ClearPresentRL::clear(dxtk::math::Color clearColor)
{
    auto cmdList{ mRenderer.directCmdQueue().getCommandList() };
    { // Command list.
        // Transition current backbuffer, so we can use it for rendering.
        mRenderer.swapChain().currentBackbuffer().transitionTo(::D3D12_RESOURCE_STATE_RENDER_TARGET, cmdList);
        // Clear the backbuffer with provided color.
        cmdList->ClearRenderTargetView(mRenderer.swapChain().currentBackbufferRtv(), clearColor, 0u, nullptr);
    }

    return cmdList;
}

void ClearPresentRL::present(res::d3d12::D3D12CommandList &&cmdList, bool useVSync, bool useTearing)
{
    // Transition current backbuffer, so we can present it.
    mRenderer.swapChain().currentBackbuffer().transitionTo(::D3D12_RESOURCE_STATE_PRESENT, cmdList);

    /* 
     * Choose how to synchronize presentation - 0 for immediate, 
     * 1-4 for after n-th vertical blank
     */
    const uint32_t syncInterval{ useVSync ? 1u : 0u };
    /// Enable tearing, if requested, supported and vSync is disabled.
    const uint32_t presentFlags{ useTearing && mRenderer.swapChain().tearingSupported() && !useVSync ? 
        DXGI_PRESENT_ALLOW_TEARING : 0u };

    const auto fenceValue{ cmdList.executeNoWait() };
    
    mRenderer.swapChain().present(syncInterval, presentFlags);

    mRenderer.directCmdQueue().fenceWaitFor(fenceValue);
    mRenderer.directCmdQueue().flush();
}

}

}
