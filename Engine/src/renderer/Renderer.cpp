/**
* @file renderer/Renderer.h
* @author Tomas Polasek
* @brief Rendering system.
*/

#include "stdafx.h"

#include "engine/renderer/Renderer.h"

namespace engine
{

namespace rndr
{

Renderer::Renderer(const EngineRuntimeConfig &cfg, const res::win::Window &window) :
    mLiveObjectReporter(cfg.debugReportLiveObjects, 
        Config::LIVE_REPORTER_DEBUG_ID, 
        Config::LIVE_REPORTER_FLAGS), 
    mDebugConfigurator(cfg.debugEnableLayer, cfg.debugGpuValidation), 
    mFactory5(), 
    mAdapter(mFactory5, cfg.adapterUseWarp, Config::ADAPTER_FEATURE_LEVEL),
    mDevice(mAdapter, Config::ADAPTER_FEATURE_LEVEL, 
        cfg.deviceDisableMsgCategories, 
        cfg.deviceDisableMsgSeverities, 
        cfg.deviceDisableMsgIds, 
        Config::DEVICE_DEBUG_NAME), 
    mDirectCommandQueue(mDevice, 
        D3D12_COMMAND_LIST_TYPE_DIRECT, 
        Config::DIRECT_QUEUE_DEBUG_NAME), 
    mComputeCommandQueue(mDevice, 
        D3D12_COMMAND_LIST_TYPE_COMPUTE, 
        Config::COMPUTE_QUEUE_DEBUG_NAME), 
    mCopyCommandQueue(mDevice, 
        D3D12_COMMAND_LIST_TYPE_COPY, 
        Config::COPY_QUEUE_DEBUG_NAME), 
    mSwapChain(window, mDirectCommandQueue, mFactory5, cfg.swapChainNumBackBuffers, Config::SWAP_CHAIN_USAGE, 
        Config::SWAP_CHAIN_FORMAT, Config::SWAP_CHAIN_SCALING, Config::SWAP_CHAIN_ALPHA_MODE)
{
    log<Info>() << "Initialized rendering sub-system." << std::endl;
}

Renderer::~Renderer()
{ flush(); }

void Renderer::resize(uint32_t width, uint32_t height)
{
    flush();

    mSwapChain.resize(width, height);
}

void Renderer::flush()
{
    mDirectCommandQueue.flush();
    mComputeCommandQueue.flush();
    mCopyCommandQueue.flush();
}

}

}
