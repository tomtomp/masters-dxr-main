/**
* @file renderer/ImGuiRL.cpp
* @author Tomas Polasek
* @brief Rendering layer for drawing GUI using ImGUI.
*/

#include "stdafx.h"
#include "engine/renderer/imGuiRL/ImGuiRL.h"

#include "engine/renderer/Renderer.h"

#include "engine/lib/imgui.h"

/// Namespace containing the engine code.
namespace engine
{

/// Rendering classes.
namespace rndr
{

ImGuiRL::ImGuiRL(Renderer &renderer) :
    mRenderer{ renderer },
    mImGui(mRenderer.device(), mRenderer.swapChain())
{ }

ImGuiRL::~ImGuiRL()
{ /* Automatic */ }

void ImGuiRL::imGuiBeginFrame()
{ mImGui.newFrame(); }

void ImGuiRL::imGuiEndFrame(res::d3d12::D3D12CommandList &cmdList)
{ mImGui.render(cmdList); }

void ImGuiRL::resizeBeforeSwapChain(uint32_t width, uint32_t height)
{
    mImGui.resize(width, height);
    mImGui.freeD3D12Objects();
}

void ImGuiRL::resizeAfterSwapChain()
{ mImGui.createD3D12Objects(); }

void ImGuiRL::showDemoWindow(bool &doOpen)
{ ImGui::ShowDemoWindow(&doOpen); }

} // namespace rndr

} // namespace engine
