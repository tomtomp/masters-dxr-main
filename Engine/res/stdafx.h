/**
* @file stdafx.h
* @author Tomas Polasek
* @brief Pre-compiled header containing common standard 
* headers and some engine headers.
*/

#pragma once

#include "targetver.h"

// Windows Header Files:
#include "engine/util/SafeWindows.h"

// DirectX 12 specific headers: 
#include <d3d12.h>
#include <dxgi1_5.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>

// D3D12 extension library (Different from <d3d12.h>!!): 
#include "engine/lib/d3dx12.h"

// App specific headers: 
#include "engine/util/Types.h"
