# What?

This is the main repository for my Master's thesis created while on the Brno University of Technology, Faculty of Information Technology. The main goal is to answer the question "Is it worth it implementing the DXR API for new games?"

# How to use

To clone this repository, use following command: 

```
git clone --recurse-submodules https://gitlab.com/tomtomp/Masters-DXR-Main
```