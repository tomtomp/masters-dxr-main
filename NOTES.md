# Meetings
## First

Zadání:
  1. Seznamte se s vykreslovací technikou ray-tracing a grafickým API DirectX12.
  2. Otestujte a prozkoumejte možnosti real-time ray-tracing s pomocí DirectX12 a DirectX Ray Tracing (DXR).
  3. Navrhněte renderer využívající DXR, který umožňuje interaktivní ray-tracing scény. Navrhněte množinu testovaných efektů.
  4. Renderer naimplementujte.
  5. Diskutujte dosažené výsledky a vhodnost implementace DXR do aktuálních herních enginů. Navrhněte které efekty by bylo výhodné takto implementovat.
Část požadovaná pro obhajobu SP:
  1. Body 1 až 3
Kategorie:
  Počítačová grafika
Implementační jazyk:
  C++, HLSL
Volně šířený software:
  DirectX (?), CMake 
  
Results: 
* GPU not required
* Specification is good

## Second

Notes: 
* Cannot use CMake - Visual Studio support is incomplete.

