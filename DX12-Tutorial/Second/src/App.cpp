/**
* @file App.cpp
* @author Tomas Polasek
* @brief Main application header.
*/

#include "stdafx.h"

#include "App.h"

namespace dxr
{

AppSpecificConfig::AppSpecificConfig()
{
    mParser.addOption<bool>(L"--prof", [&] (const bool &val)
    {
        specificPrintProfiler = val;
    }, L"Enable/disable periodic profiler information printing.");
}

App::App(ConfigT &cfg, engine::Engine &engine) :
    Application(cfg, engine),
    mImGuiRl(engine.renderer())
{ }

void App::initialize()
{
    mBinding.kb().setAction(
        Key::Escape,
        KeyMods::None,
        KeyAction::Press,
        [&]()
    {
        exit();
    });
}

int32_t App::run()
{
    PROF_SCOPE("MainLoop");

    try
    {
        mScissorRect = ::CD3DX12_RECT(0, 0, LONG_MAX, LONG_MAX);

        mVShader.precompiledFromFile(L"VertexShader.cso");
        mPShader.precompiledFromFile(L"PixelShader.cso");

        engine::helpers::d3d12::D3D12RootSignatureBuilder rsBuilder;
        mRootSignature =  
            rsBuilder
            .addConstantParameter(sizeof(DirectX::XMMATRIX) / 4, 0u, 0u, ::D3D12_SHADER_VISIBILITY_VERTEX)
            .setFlags(
                D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
                D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
                D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
                D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
                D3D12_ROOT_SIGNATURE_FLAG_DENY_PIXEL_SHADER_ROOT_ACCESS)
            .build(mEngine.renderer().device());

        mInputLayout.addElement("POSITION", 0u, DXGI_FORMAT_R32G32B32_FLOAT, 0u, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0u);
        mInputLayout.addElement("COLOR", 0u, DXGI_FORMAT_R32G32B32_FLOAT, 0u, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0u);

        engine::helpers::d3d12::D3D12PipelineStateSOBuilder pssob;
        engine::helpers::d3d12::D3D12PipelineStateBuilder psb;

        psb << 
            mRootSignature.rsState() <<
            mInputLayout.ilState() <<
            pssob.primitiveTopology(::D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE) << 
            mVShader.vsState() << 
            mPShader.psState() << 
            pssob.depthStencilFormat(::DXGI_FORMAT_D32_FLOAT) << 
            pssob.renderTargetFormats({ ::DXGI_FORMAT_R8G8B8A8_UNORM });

        mPipelineState = psb.build(mEngine.renderer().device());

        struct VertexPosColor
        {
            DirectX::XMFLOAT3 position;
            DirectX::XMFLOAT3 color;
        };

        std::array<VertexPosColor, 8u> vertexData{ {
            { DirectX::XMFLOAT3(-1.0f, -1.0f, -1.0f), DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f) },
            { DirectX::XMFLOAT3(-1.0f,  1.0f, -1.0f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f) },
            { DirectX::XMFLOAT3(1.0f,  1.0f, -1.0f), DirectX::XMFLOAT3(1.0f, 1.0f, 0.0f) },
            { DirectX::XMFLOAT3(1.0f, -1.0f, -1.0f), DirectX::XMFLOAT3(1.0f, 0.0f, 0.0f) },
            { DirectX::XMFLOAT3(-1.0f, -1.0f,  1.0f), DirectX::XMFLOAT3(0.0f, 0.0f, 1.0f) },
            { DirectX::XMFLOAT3(-1.0f,  1.0f,  1.0f), DirectX::XMFLOAT3(0.0f, 1.0f, 1.0f) },
            { DirectX::XMFLOAT3(1.0f,  1.0f,  1.0f), DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f) },
            { DirectX::XMFLOAT3(1.0f, -1.0f,  1.0f), DirectX::XMFLOAT3(1.0f, 0.0f, 1.0f) }
        } };

        std::array<uint16_t, 36u> indexData{ {
            0, 1, 2, 0, 2, 3,
            4, 6, 5, 4, 7, 6,
            4, 5, 1, 4, 1, 0,
            3, 2, 6, 3, 6, 7,
            1, 5, 6, 1, 6, 2,
            4, 0, 3, 4, 3, 7
        } };

        engine::helpers::d3d12::D3D12CommittedAllocator allocator(mEngine.renderer().device(), 
            ::CD3DX12_HEAP_PROPERTIES(::D3D12_HEAP_TYPE_DEFAULT), ::D3D12_HEAP_FLAG_NONE);
        engine::helpers::d3d12::D3D12ResourceUpdater updater(mEngine.renderer().device());
        auto uploadCmdList{ mEngine.renderer().copyCmdQueue().getCommandList() };
        mVBuffer.allocateUpload(vertexData.begin(), vertexData.end(), uploadCmdList, allocator, updater);
        mIBuffer.allocateUpload(indexData.begin(), indexData.end(), ::DXGI_FORMAT_R16_UINT, uploadCmdList, allocator, updater);
        uploadCmdList.executeAndWait();
    } catch (const util::winexception &e)
    {
        log<Debug>() << e.what() << std::endl;
    }

    {
        mModel = DirectX::XMMatrixIdentity();
        mView = DirectX::XMMatrixLookAtLH(
            // Position of the camera.
            { 0.0f, 0.0f, -10.0f, 1.0f }, 
            // Looking at origin.
            { 0.0f, 0.0f, 0.0f, 1.0f }, 
            // Up is on the positive y-axis.
            { 0.0f, 1.0f, 0.0f, 0.0f });
        mProjection = DirectX::XMMatrixIdentity();

        mMvp = mProjection * mView * mModel;
    }

    static constexpr float MOUSE_ROTATION_DIVIDER{ 16.0f };
    bool rotatingWithMouse{ false };
    util::Point2<int16_t> rotatingWithMouseStartPos{ };
    auto rotatingWithMouseStartMatrix{ mModel };

    mBinding.mouse().setAction(
        MouseButton::LButton, 
        MouseMods::None, 
        MouseAction::Press, 
        [&] (const MouseButtonEvent &event)
    {
        rotatingWithMouse = true;
        rotatingWithMouseStartPos = util::Point2<int16_t>{ event.pos.x, event.pos.y };
        rotatingWithMouseStartMatrix = mModel;
    });

    mBinding.mouse().setAction(
        MouseButton::LButton, 
        MouseMods::None, 
        MouseAction::Release, 
        [&] (const MouseButtonEvent &event)
    {
        rotatingWithMouse = false;
    });

    mBinding.mouse().setMoveAction(
        [&] (const MouseMoveEvent &event)
    {
        if (rotatingWithMouse)
        {
            const util::Point2<int16_t> relativeToStart{ 
                event.pos.x - rotatingWithMouseStartPos.x, 
                event.pos.y - rotatingWithMouseStartPos.y };
            mModel = DirectX::XMMatrixMultiply(rotatingWithMouseStartMatrix, 
                DirectX::XMMatrixRotationRollPitchYaw(
                    -relativeToStart.y / MOUSE_ROTATION_DIVIDER, 
                    -relativeToStart.x / MOUSE_ROTATION_DIVIDER, 
                    0.0f)
                );
            mMvp = mModel * mView * mProjection;
        }
    });

    // Profiling printer.
    util::prof::PrintCrawler pc;

    // Target number of milliseconds between updates.
    const auto updateDeltaMs{ updateDelta() };
    util::DeltaTimer updateTimer;

    // Target number of milliseconds between renders.
    const auto renderDeltaMs{ renderDelta() };
    util::DeltaTimer renderTimer;

    /// Time for counting fps/ups.
    engine::app::FpsUpdateTimer fpsTimer(false);

    util::TickTimer debugPrintTimer(Seconds(1u), [&] ()
    {
        if (mCfg.specificPrintProfiler)
        {
            PROF_DUMP(pc);
        }
        fpsTimer.printInfo();
    });

    util::TimerTicker ticker;
    ticker.addTimer(updateTimer);
    ticker.addTimer(renderTimer);
    ticker.addTimer(fpsTimer);
    ticker.addTimer(debugPrintTimer);

    // Start the main loop.
    mRunning = true;
    while (mRunning)
    { // Main application loop
        ticker.tick();

        while (updateTimer.tryTake(updateDeltaMs))
        { // While there is updating to do, 
            PROF_SCOPE("Update");
            update(updateDeltaMs);
            fpsTimer.update();
        }

        if (renderTimer.tryTake(renderDeltaMs))
        { // Target time between renders exceeded.
            PROF_SCOPE("Render");
            render();
            fpsTimer.frame();
            renderTimer.takeAll();
        }
    }

    return EXIT_SUCCESS;
}

void App::update(Milliseconds delta)
{
    // Process any events gathered on the message bus.
    processEvents();
}

void App::render()
{
    auto cmdList{ clear() };

    mDepthStencilBuffer.clear(cmdList);

    cmdList->SetPipelineState(mPipelineState.get());
    cmdList->SetGraphicsRootSignature(mRootSignature.get());

    cmdList->IASetPrimitiveTopology(::D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
    cmdList->IASetVertexBuffers(0u, 1u, mVBuffer.vbw());
    cmdList->IASetIndexBuffer(mIBuffer.ibw());

    cmdList->RSSetViewports(1u, &mViewport);
    cmdList->RSSetScissorRects(1u, &mScissorRect);

    const auto rtv{ mEngine.renderer().swapChain().currentBackbufferRtv() };
    const auto dsv{ mDepthStencilBuffer.dsv() };
    cmdList->OMSetRenderTargets(1u, &rtv, false, &dsv);

    cmdList->SetGraphicsRoot32BitConstants(0u, sizeof(DirectX::XMMATRIX) / 4, &mMvp, 0u);

    cmdList->DrawIndexedInstanced(36u, 1u, 0u, 0u, 0u);

    /*
    mImGuiRl.imGuiBeginFrame();
    {
        mImGuiRl.showDemoWindow(mShowDemoWindow);
    } 
    mImGuiRl.imGuiEndFrame(cmdList);
    */

    present(std::move(cmdList));
}

void App::onEvent(const KeyboardEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const MouseButtonEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const MouseMoveEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const MouseScrollEvent &event) const
{ mBinding.processEvent(event); }

void App::onEvent(const ResizeEvent &event)
{
    log<Info>() << "Resizing to: " << event.newWidth << "x" << event.newHeight << std::endl;

    if (event.newWidth == 0u || event.newHeight == 0u)
    {
        log<Info>() << "Skipping resize, since resolution is 0!" << std::endl;
        return;
    }

    mImGuiRl.resizeBeforeSwapChain(event.newWidth, event.newHeight);
    mEngine.renderer().resize(event.newWidth, event.newHeight);
    mDepthStencilBuffer.create(event.newWidth, event.newHeight, mEngine.renderer().device());
    mImGuiRl.resizeAfterSwapChain();

    mViewport = ::CD3DX12_VIEWPORT(
        0.0f, 0.0f, 
        static_cast<float>(event.newWidth), static_cast<float>(event.newHeight));

    mProjection = DirectX::XMMatrixPerspectiveFovLH(
        // Field-of-view.
        DirectX::XMConvertToRadians(80.0f), 
        // Aspect ratio.
        static_cast<float>(event.newWidth) / event.newHeight, 
        // Near and far.
        0.1f, 100.0f);

    mMvp = DirectX::XMMatrixMultiply(DirectX::XMMatrixMultiply(mModel, mView), mProjection);
}

void App::onEvent(const engine::app::events::ExitEvent &event)
{ mRunning = false; }

}
