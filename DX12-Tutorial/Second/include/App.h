/**
* @file App.h
* @author Tomas Polasek
* @brief Main application header.
*/

#pragma once

#include "Engine.h"
#include "engine/application/Application.h"

#include "engine/renderer/imGuiRL/ImGuiRL.h"

#include "engine/helpers/d3d12/D3D12PipelineStateBuilder.h"
#include "engine/helpers/d3d12/D3D12RootSignatureBuilder.h"

#include "engine/helpers/d3d12/D3D12VertexBuffer.h"
#include "engine/helpers/d3d12/D3D12IndexBuffer.h"

#include "engine/resources/d3d12/D3D12InputLayout.h"
#include "engine/resources/d3d12/D3D12Shader.h"

#include "engine/input/KeyBinding.h"
#include "resources/render/DepthStencilBuffer.h"

/// Namespace containing the project.
namespace dxr
{

/// Application specific options should be added here.
class AppSpecificConfig : public engine::EngineRuntimeConfig
{
public:
    AppSpecificConfig();

    // Start of options section

    /// Periodically print ou profiler information.
    bool specificPrintProfiler{ false };

    // End of options section
}; // class AppSpecificRuntimeConfig

/// Main application class.
class App : public engine::app::Application<App, AppSpecificConfig>
{
public:
    /// Constructor called by the engine.
    App(ConfigT &cfg, engine::Engine &engine);

    virtual ~App() = default;
    virtual void initialize() override final;
    //virtual void destroy() override final;
    virtual int32_t run() override final;
    virtual void update(Milliseconds delta) override final;
    virtual void render() override final;
    //virtual void processEvents() override final;

    void onEvent(const KeyboardEvent &event) const;
    void onEvent(const MouseButtonEvent &event) const;
    void onEvent(const MouseMoveEvent &event) const;
    void onEvent(const MouseScrollEvent &event) const;
    void onEvent(const ResizeEvent &event);
    void onEvent(const ExitEvent &event);
private:
    /// Main binding scheme.
    engine::input::KeyBinding mBinding;

    /// ImGUI testing window.
    bool mShowDemoWindow{ true };

    /// ImGUI rendering layer.
    engine::rndr::ImGuiRL mImGuiRl;

    ::CD3DX12_VIEWPORT mViewport;
    ::CD3DX12_RECT mScissorRect;
    engine::res::rndr::DepthStencilBuffer mDepthStencilBuffer;
    DirectX::XMMATRIX mModel;
    DirectX::XMMATRIX mView;
    DirectX::XMMATRIX mProjection;
    DirectX::XMMATRIX mMvp;
    engine::res::d3d12::D3D12Shader mVShader;
    engine::res::d3d12::D3D12Shader mPShader;
    engine::res::d3d12::D3D12RootSignature mRootSignature;
    engine::res::d3d12::D3D12InputLayout mInputLayout;
    engine::res::d3d12::D3D12PipelineState mPipelineState;
    engine::helpers::d3d12::D3D12VertexBuffer mVBuffer;
    engine::helpers::d3d12::D3D12IndexBuffer mIBuffer;
protected:
}; // class App
    
} // namespace App
