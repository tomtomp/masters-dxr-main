/**
* @file stdafx.h
* @author Tomas Polasek
* @brief Pre-compiled header containing common standard 
* headers and some engine headers.
*/

#pragma once

#include "targetver.h"

// Engine types: 
#include "engine/util/Types.h"
